#include "exec_task_3.h"

void exec_task_3()
{
	std::cout << "---------TASK 3---------\n";
	std::vector< int > int_vector;
	int input_number;
	std::cout << "Input number. Input 0 to end: \n";
	do
	{
		std::cin >> input_number;
		while(std::cin.fail()) {
			std::cout << "Error" << std::endl;
			std::cin.clear();
			std::cin.ignore(256,'\n');
			std::cin >> input_number;
		}
		if (input_number != 0)
		{
			int_vector.push_back(input_number);
		}
	} while (input_number != 0);

	std::cout << "Print Vector: ";
	print_container(int_vector.begin(), int_vector.end());
	
	if (int_vector.empty()) //can't process empty vector
	{
		return;
	}
	if (int_vector.back() == 1)
	{
		std::vector< int >::iterator iter = int_vector.begin();
		while (iter < int_vector.end() - 1) //exclude last element
		{
			if ((*iter % 2) == 0)
			{
				iter = int_vector.erase(iter);
			} else
			{
				++iter;
			}
		}
	} else
	if (int_vector.back() == 2)
	{
		std::vector< int >::iterator iter = int_vector.begin();
		while (iter < int_vector.end() - 1)
		{
			if ((*iter % 3) == 0)
			{
				for (int i = 0; i < 3; i++)
				{
					iter = int_vector.insert(iter + 1, 1);
				}
				//int_vector.insert(iter--, 3, 1); return iterator only in C++11
			} else
			{
				++iter;
			}
		}
	}

	std::cout << "Print Vecor after processing: ";
	print_container(int_vector.begin(), int_vector.end());
}