#ifndef EXEC_TASK_1_H
#define EXEC_TASK_1_H

#include "slist.h"
#include "print_container_functions.h"

#include <vector>
#include <iostream>

template < typename T >
struct AccessVectorAt
{
	typedef std::vector< T > this_t;
	typedef size_t iterator;

	static iterator begin(this_t & vector)
	{
		return 0;
	}

	static iterator end(this_t & vector)
	{
		return vector.size();
	}

	static T & getElement(this_t & vector, iterator index)
	{
		return vector.at(index);
	}
};

template < typename T >
struct AccessVectorBrackets
{
	typedef std::vector< T > this_t;
	typedef size_t iterator;

	static iterator begin(this_t & vector)
	{
		return 0;
	}

	static iterator end(this_t & vector)
	{
		return vector.size();
	}

	static T & getElement(this_t & vector, iterator index)
	{
		return vector[index];
	}
};

template < typename T >
struct AccessSlist
{
	typedef Slist< int > this_t;
	typedef Slist< int >::iterator iterator;

	static iterator begin(this_t & slist)
	{
		return slist.begin();
	}

	static iterator end(this_t & slist)
	{
		return slist.end();
	}

	static T & getElement(this_t & list, iterator index)
	{
		return *index;
	}
};

template < typename CollectionType, typename Policy >
inline void sort(CollectionType & collection)
{
	if (collection.empty())
	{
		return;
	}
	for (Policy::iterator i = Policy::begin(collection); i != Policy::end(collection); ++i)
	{
		for (Policy::iterator j = i; j != Policy::end(collection); ++j)
		{
			if (Policy::getElement(collection, i) > Policy::getElement(collection, j))
			{
				std::swap(Policy::getElement(collection, i), Policy::getElement(collection, j));
			}
		}
	}
}

void exec_task_1();

#endif
