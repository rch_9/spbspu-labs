#include <time.h>

#include "print_container_functions.h"
#include "exec_task_1.h"
#include "exec_task_2.h"
#include "exec_task_3.h"
#include "exec_task_4.h"

int main()
{
	srand(static_cast< size_t >(time(0)));
	exec_task_4();
	exec_task_1();
	exec_task_2("Data");
	exec_task_3();
	return 0;
}