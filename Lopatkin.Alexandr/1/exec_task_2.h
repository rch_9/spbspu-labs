#ifndef EXEC_TASK_2_H
#define EXEC_TASK_2_H

#include "print_container_functions.h"

#include <iostream>
#include <vector>
#include <fstream>

void exec_task_2(const char * name);

#endif