#ifndef EXEC_TASK_4_H
#define EXEC_TASK_4_H

#include <iostream>

#include "print_container_functions.h"
#include "exec_task_1.h"

void fillRandom(double * arr, int size);

void exec_task_4();

#endif