#include "exec_task_4.h"

void fillRandom(double * arr, int size)
{
	if (!arr)
	{
		std::cerr << "Array pointer = NULL"; 
		return;
	}
	for (int i = 0; i < size; i++)
	{
		arr[i] = ((double)rand() / RAND_MAX) * 2 - 1;
	}
}

void exec_task_4()
{
	std::cout << "---------TASK 4---------\n";

	std::cout << "5" << std::endl;
	std::vector< double > vector(5);
	fillRandom(&vector[0], vector.size());
	sort<std::vector< double >,  AccessVectorBrackets< double > >(vector);
	print_container(vector.begin(), vector.end());

	std::cout << std::endl;
	std::cout << "10" << std::endl;
	vector.resize(10);
	fillRandom(&vector[0], vector.size());
	sort<std::vector< double >,  AccessVectorBrackets< double > >(vector);
	print_container(vector.begin(), vector.end());

	std::cout << std::endl;
	std::cout << "25" << std::endl;
	vector.resize(25);
	fillRandom(&vector[0], vector.size());
	sort<std::vector< double >,  AccessVectorBrackets< double > >(vector);
	print_container(vector.begin(), vector.end());

	std::cout << std::endl;
	std::cout << "50" << std::endl;
	vector.resize(50);
	fillRandom(&vector[0], vector.size());
	sort<std::vector< double >,  AccessVectorBrackets< double > >(vector);
	print_container(vector.begin(), vector.end());

	std::cout << std::endl;
	std::cout << "100" << std::endl;
	vector.resize(100);
	fillRandom(&vector[0], vector.size());
	//print_container(vector.begin(), vector.end());
	sort<std::vector< double >,  AccessVectorBrackets< double > >(vector);
	print_container(vector.begin(), vector.end());
}