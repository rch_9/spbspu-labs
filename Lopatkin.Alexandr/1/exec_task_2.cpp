#include "exec_task_2.h"

void exec_task_2(const char * name)//if it's needed to get array as argument or doesn't matter?
{
	std::cout << "---------TASK 2---------\n";
	if (!name)
	{
		std::cerr << "Name pointer = NULL\n";
		return;
	}

	std::ifstream inp_stream(name);
	if (!inp_stream)
	{
		std::cerr << "Can't open file for reading. The file \"" << name << "\" is not accessible\n";
		return;
	}

	inp_stream.seekg(0, inp_stream.end);
	int size = static_cast< int >(inp_stream.tellg());
	inp_stream.seekg(0, inp_stream.beg);
	if ((!size) || (size == -1))
	{
		std::cerr << "File \"" << name << "\" is empty\n";
		return;
	}

	std::string text_array;
	text_array.resize(size);

	inp_stream.read(&text_array[0], size);
	std::vector< char > text_vector(text_array.begin(), text_array.end()); //Maybe assign better?
	std::cout << "Print vector: ";
	print_container(text_vector.begin(), text_vector.end());

	std::cout << "Print array: ";
	print_container(text_array.begin(), text_array.end());
}