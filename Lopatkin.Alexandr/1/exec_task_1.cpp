#include "exec_task_1.h"

void exec_task_1()
{
	std::cout << "---------TASK 1---------\n";
	std::vector< int > vector1;
	vector1.push_back(6);
	vector1.push_back(11);
	vector1.push_back(1);
	vector1.push_back(12);
	std::cout << "Vector1 before sorting with brackets: ";
	print_container(vector1.begin(), vector1.end());
	sort< std::vector< int >, AccessVectorBrackets< int > >(vector1);
	std::cout << "Vector1 after sorting with brackets: ";
	print_container(vector1.begin(), vector1.end());

	std::vector < int > vector2;
	vector2.push_back(6);
	vector2.push_back(11);
	vector2.push_back(1);
	vector2.push_back(12);
	std::cout << "Vector2 before sorting with at(): ";
	print_container(vector2.begin(), vector2.end());
	sort< std::vector< int >, AccessVectorAt< int > >(vector2);
	std::cout << "Vector2 after sorting with at(): ";
	print_container(vector2.begin(), vector2.end());

	Slist< int > slist;
	slist.push_back(12);
	slist.push_back(1);
	slist.push_back(11);
	slist.push_back(6);
	std::cout << "Slist before sorting: ";
	print_container(slist.begin(), slist.end());

	sort< Slist< int >, AccessSlist< int > >(slist);
	std::cout << "Slist after sorting: ";
	print_container(slist.begin(), slist.end());
}