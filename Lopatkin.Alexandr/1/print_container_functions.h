#ifndef PRINT_CONTAINER_FUNCTIONS_H
#define PRINT_CONTAINER_FUNCTIONS_H

#include <iostream>

template < typename T >
inline void print_container(const T & begin, const T & end)
{
	if (begin == end)
	{
		std::cout << "Container is empty\n";
		return;
	}
	T iter = begin;
	while (iter != end)
	{
		std::cout << *iter << "\n";
		++iter;
	}
}

#endif