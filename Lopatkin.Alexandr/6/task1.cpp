#include "task1.h"

void task1Execute()
{
	std::ifstream inp_stream("Text");

	if (!inp_stream)
	{
		std::cout << "File is not accessible" << std::endl;
		return;
	}

	std::vector< std::string > single_word_v;

	while (inp_stream)
	{
		std::string word;
		inp_stream >> word;

		if (std::find(single_word_v.begin(), single_word_v.end(), word) == single_word_v.end())
		{
			single_word_v.push_back(word);
		}
	}

	for (std::vector< std::string >::iterator it = single_word_v.begin(); it != single_word_v.end(); ++it)
	{
		std::cout << *it << " ";
	}

	std::cout << std::endl;
}