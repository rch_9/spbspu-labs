#include "task1.h"
#include "task2.h"

#include <time.h>

int main()
{
	srand(static_cast< size_t >(time(0)));
	task1Execute();
	task2Execute();
	return 0;
}