#include "task2.h"

void task2Execute()
{
	std::vector< Shape > shapes_v(4);
	std::vector< Point > point_v;
	point_v.reserve(shapes_v.size());

	//Fill vector of shapes
	std::for_each(shapes_v.begin(), shapes_v.end(), AssignRandomShape());
	std::copy(shapes_v.begin(), shapes_v.end(), std::ostream_iterator< Shape >(std::cout));

	std::cout << std::endl;
	std::cout << "Total vertixes: " << std::accumulate(shapes_v.begin(), shapes_v.end(), 0, VertexNumCounter()) << std::endl;
	std::cout << "Triangle amount: " << std::count_if(shapes_v.begin(), shapes_v.end(), isTriangle()) << std::endl;
	std::cout << "Square amount: " << std::count_if(shapes_v.begin(), shapes_v.end(), isSquare()) << std::endl;
	std::cout << "Rectangle amount: " << std::count_if(shapes_v.begin(), shapes_v.end(), isRectangle()) << std::endl;
	std::cout << std::endl;

	//Remove pentagon and print vector
	std::cout << std::endl;
	shapes_v.erase(std::remove_if(shapes_v.begin(), shapes_v.end(), IsPentagon()), shapes_v.end());
	std::copy(shapes_v.begin(), shapes_v.end(), std::ostream_iterator< Shape >(std::cout));
	std::cout << std::endl;

	//Make point vector based on shapes vector
	std::cout << std::endl;
	std::transform(shapes_v.begin(), shapes_v.end(), std::back_inserter(point_v), GetRandomPoint());
	std::copy(shapes_v.begin(), shapes_v.end(), std::ostream_iterator< Shape >(std::cout));
	std::cout << std::endl;

	//Sort vector
	std::cout << std::endl;
	std::sort(shapes_v.begin(), shapes_v.end(), ShapeComparator());
	std::copy(shapes_v.begin(), shapes_v.end(), std::ostream_iterator< Shape >(std::cout));
	std::cout << std::endl;
}