#ifndef TASK2_H
#define TASK2_H

#include "functors.h"
#include "shape.h"

#include <iterator>
#include <algorithm>
#include <iostream>
#include <vector>
#include <numeric>
#include <functional>

void task2Execute();

#endif