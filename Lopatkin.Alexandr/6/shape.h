#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>

struct Point
{
	Point(const int & rx, const int & ry)
		: x(rx)
		, y(ry)
	{}

	int x;
	int y;

	friend std::ostream & operator <<(std::ostream & out, const Point & point)
	{
		return (out << "[X: " << point.x << " Y " <<  point.y << "]"); 
	}
};

struct Shape
{
	std::vector< Point > vertexes;

	friend std::ostream & operator <<(std::ostream & out, const Shape & shape)
	{
		out << "Vertexes num: " << shape.vertexes.size() << std::endl;

		out << "Points: ";
		for (size_t i = 0; i < shape.vertexes.size(); ++i)
		{
			out << shape.vertexes[i];
		}

		out << std::endl;

		return out;
	}
};

#endif
