#ifndef FUNCTORS_H
#define FUNCTORS_H

#include "shape.h"

#include <iostream>
#include <algorithm>

inline int getSideLength(const Point & a, const Point & b)
{
	return ((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}

struct AssignRandomShape
{
	void operator ()(Shape & shape)
	{
		size_t shapeTypeDefiner = rand() % 4;
		switch (shapeTypeDefiner)
		{
			//Triangle
		case 0: 
			{
				for (size_t i = 0; i < 3; ++i)
				{
					shape.vertexes.push_back(Point::Point(rand() % 200, rand() % 200));
				}
				break;
			}
			//Square
		case 1: 
			{
				size_t length = rand() % 200;
				int x0 = rand() % 200;
				int y0 = rand() % 200;
				shape.vertexes.push_back(Point::Point(x0, y0));
				shape.vertexes.push_back(Point::Point(x0, y0 + length));
				shape.vertexes.push_back(Point::Point(x0 + length, y0 + length));
				shape.vertexes.push_back(Point::Point(x0 + length, y0));
				break;
			}
			//Rectangle
		case 2: 
			{
				for (size_t i = 0; i < 4; ++i)
				{
					shape.vertexes.push_back(Point::Point(rand() % 200, rand() % 200));
				}
				break;
			}
			//Pentagon
		case 3: 
			{
				for (size_t i = 0; i < 5; ++i)
				{
					shape.vertexes.push_back(Point::Point(rand() % 200, rand() % 200));
				}
				break;
			}
		};
	}
};

struct VertexNumCounter
{
	size_t operator ()(int accumulator, const Shape & shape)
	{
		return accumulator + shape.vertexes.size();
	}
};

struct isTriangle
{
	bool operator ()(const Shape & shape) const
	{
		return (shape.vertexes.size() == 3);
	}
};

struct isSquare
{
	bool operator ()(const Shape & shape) const
	{
		if (shape.vertexes.size() == 4)
		{

			std::vector< int > sides(2);
			std::vector< int > diag(2);

			sides[0] = getSideLength(shape.vertexes[0], shape.vertexes[1]);
			sides[1] = getSideLength(shape.vertexes[1], shape.vertexes[2]);
			diag[0] = getSideLength(shape.vertexes[0], shape.vertexes[2]);
			diag[1] = getSideLength(shape.vertexes[1], shape.vertexes[3]);

			return ((sides[0] == sides[1]) && (diag[0] == diag[1]));
		} else
		{
			return false;
		}
	}
};

struct isRectangle
{
	bool operator ()(const Shape & shape) const
	{
		if (shape.vertexes.size() == 4)
		{

			std::vector< int > sides(4);
			std::vector< int > diag(2);

			sides[0] = getSideLength(shape.vertexes[0], shape.vertexes[1]);
			sides[1] = getSideLength(shape.vertexes[1], shape.vertexes[2]);
			sides[2] = getSideLength(shape.vertexes[2], shape.vertexes[3]);
			sides[3] = getSideLength(shape.vertexes[3], shape.vertexes[0]);
			diag[0] = getSideLength(shape.vertexes[0], shape.vertexes[2]);
			diag[1] = getSideLength(shape.vertexes[1], shape.vertexes[3]);

			return (
				(sides[0] == sides[2]) && (sides[1] == sides[3]) && (diag[0] == diag[1])
				&& (sides[0] != sides[1])
				);

		} else
		{
			return false;
		}
	}
};

struct IsPentagon
{
	bool operator ()(const Shape & shape) const
	{
		return (shape.vertexes.size() == 5);
	}
};

struct GetRandomPoint
{
	Point operator ()(const Shape & shape) const
	{
		return shape.vertexes[rand() % shape.vertexes.size()];
	}
};

struct ShapeComparator
{
	bool operator ()(const Shape & rhs, const Shape & lhs) const
	{
		return (
			(rhs.vertexes.size() < lhs.vertexes.size())
			|| (
				(rhs.vertexes.size() == lhs.vertexes.size()) 
				&& (isSquare()(rhs) && !isSquare()(lhs))
				)
			);
	}
};

#endif