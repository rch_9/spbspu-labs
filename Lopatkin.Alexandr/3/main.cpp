#include "QueueWithPriority.h"
#include "PriorityQueueTest.h"
#include "task2.h"

#include <string>
#include <iostream>

int main()
{
	//Test for QueueWithPriority
	testPriorityQueue();
	testAccelerateOp();

	//Test for correct processing task 2
	srand(static_cast< size_t >(time(0)));
	std::list< int > test_list;
	for (size_t i = 0; i < 20; ++i)
	{
		fillRandList(test_list, i);
		std::cout << "Not shuffled list with size = " << i << ":" << std::endl;
		printIntList(test_list.begin(), test_list.end());
		std::cout << "Shuffled list with size = " << i << ":" << std::endl;
		printShuffledList(test_list.begin(), test_list.end());
		test_list.clear();
	}

	return 0;
}