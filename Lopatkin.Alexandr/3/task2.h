#ifndef TASK2_H
#define TASK2_H

#include <list>
#include <iostream>
#include <time.h>

inline void printShuffledList(const std::list< int >::iterator & begin, const std::list< int >::iterator & end)
{
	if (begin == end)
	{
		std::cout << "Container is empty\n";
		return;
	}

	std::list< int >::iterator iter(begin);
	std::list< int >::reverse_iterator rev_iter(end);

	do
	{
		std::cout << *(iter++) << " | ";
		if (iter == rev_iter.base()) //for situations, when 1 element in container or if container's size is even
		{
			return;
		}

		std::cout << *(rev_iter++) << " | ";

	} while (iter != rev_iter.base());

	std::cout << std::endl;
}

inline void fillRandList(std::list< int > & list, size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		list.push_back(rand() % 20 + 1);
	}
}

inline void printIntList(std::list< int >::iterator begin, const std::list< int >::iterator & end)
{
	if (begin == end)
	{
		std::cout << "Container is empty" << std::endl;
	}

	while (begin != end)
	{
		std::cout << *begin << " | ";
		++begin;
	}

	std::cout << std::endl;
}

#endif