#ifndef QUEUEWITHPRIORITY_H
#define QUEUEWITHPRIORITY_H

#include <list> //I think that list works faster when pop or push front operations than deque
#include <iterator>

template < typename T >
class QueueWithPriority
{
public:
	enum ElementPriority
	{
		LOW,
		NORMAL,
		HIGH
	};

	void put(const T & element, ElementPriority priority);

	template < typename Operation >
	void processAndPop(Operation operation); //instead get()

	void Accelerate();

	bool empty() const;

private:
	std::list< T > high_list_;
	std::list< T > normal_list_;
	std::list< T > low_list_;

};

//==========QueueWithPriority implementation=========

template < typename T >
inline void QueueWithPriority< T >::put(const T & element, ElementPriority priority)
{
	switch (priority)
	{
	case HIGH:
		high_list_.push_front(element);
		break;
	case NORMAL:
		normal_list_.push_front(element);
		break;
	case LOW:
		low_list_.push_front(element);
		break;
	}
}

template < typename T >
template < typename Operation >
inline void QueueWithPriority< T >::processAndPop(Operation operation)
{
	T element;

	if (!high_list_.empty())
	{
		element = high_list_.back();
		high_list_.pop_back();
	}
	else if (!normal_list_.empty())
	{
		element = normal_list_.back();
		normal_list_.pop_back();
	} 
	else if (!low_list_.empty())
	{
		element = low_list_.back();
		low_list_.pop_back();
	} else
	{
		return; //Queue is empty
	}

	operation(element);
}

template < typename T >
inline void QueueWithPriority< T >::Accelerate()
{
	if (low_list_.empty())
	{
		return;
	}

	high_list_.splice(high_list_.begin(), low_list_);
}

template < typename T >
inline bool QueueWithPriority< T >::empty() const
{
	return (high_list_.empty() && normal_list_.empty() && low_list_.empty());
}

#endif