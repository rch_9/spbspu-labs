#ifndef FUNCTORS_H
#define FUNCTORS_H

#include "QueueWithPriority.h"
#include <iostream>

template < typename T >
struct print_element
{
	void operator()(const T & element)
	{
		std::cout << "Element: " << element << ::std::endl;
	}
};

#endif