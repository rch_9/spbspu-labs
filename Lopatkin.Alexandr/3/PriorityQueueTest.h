#ifndef PRIORITYQUEUETEST_H
#define PRIORITYQUEUETEST_H

#include "QueueWithPriority.h"
#include "functors.h"

void testPriorityQueue();
void testAccelerateOp();

#endif