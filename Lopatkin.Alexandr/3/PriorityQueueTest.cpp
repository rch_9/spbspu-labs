#include "PriorityQueueTest.h"

void testPriorityQueue()
{
	typedef QueueWithPriority< int > queue_t;

	int test_input_array[6] = {1, 2, 3, 4, 5, 6};
	queue_t::ElementPriority priority_array[6] = {queue_t::LOW, queue_t::NORMAL, queue_t::HIGH, queue_t::HIGH, queue_t::NORMAL, queue_t::LOW};
	QueueWithPriority< int > queue;

	std::cout << "Test priority queue" << std::endl;

	for (size_t i = 0; i < 6; ++i)
	{
		queue.put(test_input_array[i], priority_array[i]);
	}
	for (size_t i = 0; i < 6; ++i)
	{
		queue.processAndPop(print_element< int >());
	}
}

void testAccelerateOp()
{
	typedef QueueWithPriority< int > queue_t;

	int test_input_array[6] = {1, 2, 3, 4, 5, 6};
	queue_t::ElementPriority priority_array[6] = {queue_t::LOW, queue_t::NORMAL, queue_t::HIGH, queue_t::HIGH, queue_t::NORMAL, queue_t::LOW};
	QueueWithPriority< int > queue;

	//std::cout << "Test accelerate operation, before accelerate:" << std::endl;

	for (size_t i = 0; i < 6; ++i)
	{
		queue.put(test_input_array[i], priority_array[i]);
	}

	queue.Accelerate();

	std::cout << "Test accelerate operation, after accelerate:" << std::endl;

	std::cout << "Queue" << std::endl;
	for (size_t i = 0; i < 6; ++i)
	{
		queue.processAndPop(print_element< int >());
	}
}
