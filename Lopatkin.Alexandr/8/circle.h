#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.h"

#include <iostream>

class Circle:
	public Shape
{
public:
	Circle(const int rx, const int ry)
		: Shape(rx, ry)
	{}

	virtual void draw() const
	{
		std::cout << "Shape: Circle(" << "X: " << getCenter().x << " Y: " << getCenter().y << ")" << std::endl;
	}
};

#endif