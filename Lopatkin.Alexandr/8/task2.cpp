#include "task2.h"

void task2Execute()
{
	std::cout << "============Task 2============" << std::endl;

	std::tr1::shared_ptr< Circle > c1(new Circle(1, 6));
	std::tr1::shared_ptr< Triangle > t1(new Triangle(0, 5));
	std::tr1::shared_ptr< Square > s1(new Square(-1, 4));

	std::vector< std::shared_ptr< Shape > > v;

	v.push_back(c1);
	v.push_back(t1);
	v.push_back(s1);

	std::for_each(v.begin(), v.end(), drawShapes());

	std::cout << std::endl;
	std::cout << "After sort left to right" << std::endl;
	std::sort(v.begin(), v.end(), isMoreLeft());
	std::for_each(v.begin(), v.end(), drawShapes());

	std::cout << std::endl;
	std::cout << "After sort right to left" << std::endl;
	std::sort(v.begin(), v.end(), isMoreRight());
	std::for_each(v.begin(), v.end(), drawShapes());

	std::cout << std::endl;
	std::cout << "After sort up to down" << std::endl;
	std::sort(v.begin(), v.end(), isUpper());
	std::for_each(v.begin(), v.end(), drawShapes());

	std::cout << std::endl;
	std::cout << "After sort down to up" << std::endl;
	std::sort(v.begin(), v.end(), isLower());
	std::for_each(v.begin(), v.end(), drawShapes());
}