#ifndef SHAPE_H
#define SHAPE_H

struct Point
{
	Point(const int rx, const int ry)
		: x(rx)
		, y(ry)
	{}

	int x, y;
};

class Shape
{
public:
	Shape(const int rx, const int ry)
		: center_(rx, ry)
	{}

	Point getCenter() const
	{
		return center_;
	}

	void setCenter(const int rx, const int ry)
	{
		center_.x = rx;
		center_.y = ry;
	}

	bool isMoreLeft(const Shape & rShape) const
	{
		return (center_.x < rShape.getCenter().x);
	}

	bool isUpper(const Shape & rShape) const
	{
		return (rShape.getCenter().y < center_.y);
	}

	virtual void draw() const = 0;

protected:
	Point center_;
	~Shape()
	{}
};

#endif