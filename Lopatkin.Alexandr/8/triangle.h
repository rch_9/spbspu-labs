#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "shape.h"

#include <iostream>

class Triangle:
	public Shape
{
public:
	Triangle(const int rx, const int ry)
		: Shape(rx, ry)
	{}

	virtual void draw() const
	{
		std::cout << "Shape: Triangle(" << "X: " << getCenter().x << " Y: " << getCenter().y << ")" << std::endl;
	}
};

#endif