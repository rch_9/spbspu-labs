#ifndef FUNCTORS_H
#define FUNCTORS_H

#include "shape.h"

#define _USE_MATH_DEFINES //need for M_PI

#include <math.h>
#include <iostream>
#include <cstdlib>
#include <memory>

struct PrintDoubleType
{
	void operator ()(const double & elem) const
	{
		std::cout << elem << " " << std::endl;
	}
};

struct isMoreLeft
{
	bool operator ()(const std::shared_ptr< Shape > & lhs, const std::shared_ptr< Shape > & rhs) const
	{
		return lhs->isMoreLeft(*rhs);
	}
};

struct isMoreRight
{
	bool operator ()(const std::shared_ptr< Shape > & lhs, const std::shared_ptr< Shape > & rhs) const
	{
		return rhs->isMoreLeft(*lhs);
	}
};

struct isUpper
{
	bool operator ()(const std::shared_ptr< Shape > & lhs, const std::shared_ptr< Shape > & rhs) const
	{
		return lhs->isUpper(*rhs);
	}
};

struct isLower
{
	bool operator ()(const std::shared_ptr< Shape > & lhs, const std::shared_ptr< Shape > & rhs) const
	{
		return rhs->isUpper(*lhs);
	}
};

struct drawShapes
{
	void operator ()(const std::shared_ptr< Shape > & shape) const
	{
		shape->draw();
	}
};

#endif