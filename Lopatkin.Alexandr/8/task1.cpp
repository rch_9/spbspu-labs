#include "task1.h"

void task1Execute()
{
	std::vector< double > testContainer;

	testContainer.push_back(0.1);
	testContainer.push_back(2.0);
	testContainer.push_back(0);

	std::cout << "================Task 1================" << std::endl;
	//std::for_each(testContainer.begin(), testContainer.end(), PiMultiply());
	std::transform(testContainer.begin(), testContainer.end(), testContainer.begin(), std::bind1st(std::multiplies< double >(), M_PI));
	std::for_each(testContainer.begin(), testContainer.end(), PrintDoubleType());
}
