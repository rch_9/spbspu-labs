#include "task1.h"
#include "task2.h"

int main()
{
	task1Execute();
	task2Execute();
	return 0;
}