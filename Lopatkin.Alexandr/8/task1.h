#ifndef TASK1_H
#define TASK1_H

#include "functors.h"

#include <algorithm>
#include <numeric>
#include <vector>

void task1Execute();

#endif