#ifndef TASK2_H
#define TASK2_H

#include "shape.h"
#include "circle.h"
#include "triangle.h"
#include "square.h"
#include "functors.h"

#include <algorithm>
#include <iostream>
#include <vector>
#include <memory>

void task2Execute();

#endif