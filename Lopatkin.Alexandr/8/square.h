#ifndef SQUARE_H
#define SQUARE_H

#include "shape.h"

#include <iostream>

class Square:
	public Shape
{
public:
	Square(const int rx, const int ry)
		: Shape(rx, ry)
	{}

	virtual void draw() const
	{
		std::cout << "Shape: Square(" << "X: " << getCenter().x << " Y: " << getCenter().y << ")" << std::endl;
	}
};

#endif