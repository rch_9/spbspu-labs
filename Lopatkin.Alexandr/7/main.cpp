#include "functors.h"

#include <algorithm>
#include <numeric>
#include <time.h>
#include <vector>

int main()
{
	srand(time(0));

	std::vector< int > test(10);

	std::generate(test.begin(), test.end(), putRandomInt);
	std::for_each(test.begin(), test.end(), printInt());

	std::for_each(test.begin(), test.end(), GetStatistic()).print();

	return 0;
}