#ifndef FUNCTORS_H
#define FUNCTORS_H

#include <iostream>

struct GetStatistic
{
	GetStatistic()
		: average(0)
		, pos_num(0)
		, neg_num(0)
		, even_sum(0)
		, odd_sum(0)
		, isFirstSet(false)
		, isFirstEqLast(false)
		, count(0)
	{}

	int max;
	int min;
	double average;
	size_t pos_num; //positive counter
	size_t neg_num; //negative counter
	int even_sum;
	int odd_sum;

	bool isFirstSet;
	bool isFirstEqLast;
	int first;
	size_t count;

	void operator ()(const int & elem)
	{
		if (!isFirstSet)
		{
			max = elem;
			min = elem;
			first = elem;
			isFirstSet = true;
		}

		if (elem > max)
		{
			max = elem;
		}

		if (elem < min)
		{
			min = elem;
		}

		if ((elem % 2) == 0)
		{
			even_sum += elem;
		}
		else
		{
			odd_sum += elem;
		}

		if (elem >= 0)
		{
			++pos_num;
		}
		else
		{
			++neg_num;
		}

		 average = (average * count + elem) / (count + 1);
		 ++count;

		if (isFirstSet)
		{
			isFirstEqLast = (first == elem);
		}
	}
	void print()
	{
		std::cout << "Statistic" << std::endl;

		if (!isFirstSet)
		{
			std::cout << "Empty collection" << std::endl;
		}

		std::cout << "Min: " << min<< std::endl;
		std::cout << "Max: " << max << std::endl;
		std::cout << "Average: " << std::fixed << average << std::endl;
		std::cout << "Positive amount: " << pos_num << std::endl;
		std::cout << "Negative amount: " << neg_num << std::endl;
		std::cout << "Even sum: " << even_sum << std::endl;
		std::cout << "Odd sum: " << odd_sum << std::endl;
		std::cout << "First is equal Last: " << std::boolalpha << isFirstEqLast << std::endl;
	}
};

struct printInt
{
	void operator ()(const int & elem) const
	{
		std::cout << elem << std::endl;
	}
};

int putRandomInt()
{
	return (rand() % (500 * 2) - 500);
}

#endif