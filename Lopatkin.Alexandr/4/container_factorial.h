#ifndef CONTAINER_FACTORIAL_H
#define CONTAINER_FACTORIAL_H

#include <iterator>

class ContainerFactorial
{
public:
	class FactorialIterator
		: public std::iterator< std::bidirectional_iterator_tag, size_t >
	{
	public:
		typedef size_t node_t;
		typedef FactorialIterator this_t;

		FactorialIterator();
		FactorialIterator(size_t n);

		bool operator !=(const this_t & rhs) const;
		bool operator ==(const this_t & rhs) const;
		node_t operator *() const;

		this_t & operator ++();
		this_t operator ++(int);
		this_t & operator --();
		this_t operator --(int);
	private:
		size_t factorial(const size_t & n);

		size_t factorial_arg_; //current factorial parametr
		size_t value_; //current factorial value
	};

	typedef size_t value_type;
	typedef FactorialIterator iterator;

	ContainerFactorial();
	ContainerFactorial(const size_t & n);

	iterator begin();
	iterator end();

	const size_t & size() const;

private:
	size_t size_;
};

#endif