#include "phonebook.h"

//==================Phonebook::Record implementation==============

Phonebook::Record::Record(const std::string & name, const std::string & number)
	: name_(name)
	, number_(number)
{

}

std::string Phonebook::Record::getName() const
{
	return name_;
}

std::string Phonebook::Record::getNumber() const
{
	return number_;
}

void Phonebook::Record::setName(const std::string & name)
{
	name_ = name;
}

void Phonebook::Record::setNumber(const std::string & number)
{
	number_ = number;
}

//==================End Phonebook::Record implementation==========

//==================Phonebook implementation==============

Phonebook::iterator Phonebook::begin()
{
	return book_.begin();
}

Phonebook::iterator Phonebook::end()
{
	return book_.end();
}

void Phonebook::show(Phonebook::iterator position) const
{
	if (!book_.empty() && (position != book_.end()))
	{
		std::cout << "Name: " << position->getName() << "; Number: " << position->getNumber() << std::endl;
	} else
	{
		std::cout << "Book is empty" << std::endl;
	}
}

void Phonebook::moveToNextRecord(Phonebook::iterator position)
{
	if (position != book_.end())
	{
		++position;
	}
}

void Phonebook::moveToPrevRecord(Phonebook::iterator position)
{
	if (position != book_.begin())
	{
		--position;
	}
}

void Phonebook::insertAfter(Phonebook::iterator position, const Phonebook::value_type & record)
{
	if (position != book_.end())
	{
		book_.insert(++position, record);
	} else
	{
		book_.insert(position, record);
	}
}

void Phonebook::insertAfter(Phonebook::iterator position, const std::string & name, const std::string & number)
{
	if (position != book_.end())
	{
		book_.insert(++position, Phonebook::value_type(name, number));
	} else
	{
		book_.insert(position, Phonebook::value_type(name, number));
	}
}

void Phonebook::insertBefore(Phonebook::iterator position, const Phonebook::value_type & record)
{
	book_.insert(position, record);
}

void Phonebook::insertBefore(Phonebook::iterator position, const std::string & name, const std::string & number)
{
	book_.insert(position, Phonebook::value_type(name, number));
}

void Phonebook::push_back(const Phonebook::value_type & record)
{
	book_.push_back(record);
}

void Phonebook::push_back(const std::string & name, const std::string & number)
{
	book_.push_back(Phonebook::value_type(name, number));
}

void Phonebook::modifyRecord(Phonebook::iterator position, const Phonebook::value_type & record) //change record
{
	if (position != book_.end())
	{
		position->setName(record.getName());
		position->setNumber(record.getNumber());
	}
}

//==================End Phonebook implementation==========