#ifndef TESTS_H
#define TESTS_H

#include "phonebook.h"
#include "container_factorial.h"
#include <iostream>
#include <vector>

void testPhonebook();
void testFactorialContainer();

#endif