#include "container_factorial.h"

//****************FactorialIterator implementation****************//
size_t ContainerFactorial::FactorialIterator::factorial(const size_t & n)
{
	return (n != 1) ? n * factorial(n - 1) : 1;
}

ContainerFactorial::FactorialIterator::FactorialIterator()
	: factorial_arg_(1)
	,value_(1)
{

}

ContainerFactorial::FactorialIterator::FactorialIterator(size_t n)
	: factorial_arg_(n)
	, value_(factorial(n))
{

}

bool ContainerFactorial::FactorialIterator::operator !=(const FactorialIterator::this_t & rhs) const
{
	return (factorial_arg_ != rhs.factorial_arg_);
}

bool ContainerFactorial::FactorialIterator::operator ==(const FactorialIterator::this_t & rhs) const
{
	return (factorial_arg_ == rhs.factorial_arg_);
}

size_t ContainerFactorial::FactorialIterator::operator *() const
{
	return value_;
}

ContainerFactorial::FactorialIterator & ContainerFactorial::FactorialIterator::operator ++()
{
	value_ *= ++factorial_arg_;
	return *this;
}

ContainerFactorial::FactorialIterator ContainerFactorial::FactorialIterator::operator ++(int)
{
	this_t temp(*this);
	this->operator ++();

	return temp;
}

ContainerFactorial::FactorialIterator & ContainerFactorial::FactorialIterator::operator --()
{
	value_ /= factorial_arg_--;
	return *this;
}

ContainerFactorial::FactorialIterator ContainerFactorial::FactorialIterator::operator --(int)
{
	this_t temp(*this);
	this->operator --();

	return temp;
}

//*************End FactorialIterator implementation****************//


//****************ContainerFactorial implementation****************//

ContainerFactorial::ContainerFactorial()
	: size_(11)
{

}

ContainerFactorial::ContainerFactorial(const size_t & n)
	: size_(n)
{

}

ContainerFactorial::iterator ContainerFactorial::begin()
{
	return iterator();
}

ContainerFactorial::iterator ContainerFactorial::end()
{
	return iterator(size_);
}

//************End ContainerFactorial implementation****************//