#include "tests.h"

void testPhonebook()
{
	Phonebook book;
	Phonebook::Record testRecord("Test", "00");
	Phonebook::iterator position(book.begin());

	//insertAfter
	book.insertAfter(position, "3", "03");
	book.insertAfter(position, testRecord);

	//insertBefore
	book.insertBefore(position, "4", "04");
	book.insertBefore(position, testRecord);

	//push_back operation
	book.push_back("1", "01");
	book.push_back(testRecord);

	for (Phonebook::iterator it = book.begin(); it != book.end(); ++it)
	{
		book.show(it);
		book.moveToNextRecord(it);
	}
	
	//modifyRecord operation
	std::cout << "Change record via modifyRecord" << std::endl;
	position = book.begin();
	book.modifyRecord(position, Phonebook::Record("Changed name", "New Number"));
	book.show(position);
}

void testFactorialContainer()
{
	//Check container
	ContainerFactorial fac_container1;

    for (ContainerFactorial::iterator it = fac_container1.begin(); it != fac_container1.end(); ++it)
    {
        std::cout << (*it) << std::endl;
    }
    std::cout << std::endl;

	//STL check
	ContainerFactorial fac_container2;
	std::vector< size_t > vector(10);

	std::copy(fac_container2.begin(), fac_container2.end(), vector.begin());

	for (std::vector< size_t >::iterator it = vector.begin(); it != vector.end(); ++it)
	{
		std::cout << (*it) << std::endl;
	}
}