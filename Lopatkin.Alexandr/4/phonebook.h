#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <list>
#include <iostream>
#include <string>

class Phonebook
{
public:
	class Record
	{
	public:
		Record(const std::string & name, const std::string & number);

		std::string getName() const;
		std::string getNumber() const;

		void setName(const std::string & name);
		void setNumber(const std::string & number);

	private:
		std::string name_;
		std::string number_;
	};

	typedef Phonebook::Record value_type;
	typedef std::list< value_type > container_type;
	typedef container_type::iterator iterator;

	Phonebook::iterator begin();
	Phonebook::iterator end();

	void show(Phonebook::iterator position) const;

	void Phonebook::moveToNextRecord(Phonebook::iterator position);
	void Phonebook::moveToPrevRecord(Phonebook::iterator position);

	void insertAfter(Phonebook::iterator position, const value_type & record);
	void insertAfter(Phonebook::iterator position, const std::string & name, const std::string & number);
	void insertBefore(Phonebook::iterator position, const value_type & record);
	void insertBefore(Phonebook::iterator position, const std::string & name, const std::string & number);
	void push_back(const value_type & record);
	void push_back(const std::string & name, const std::string & number);

	void modifyRecord(Phonebook::iterator position, const value_type & record); //change record
private:
	container_type book_;
};

#endif