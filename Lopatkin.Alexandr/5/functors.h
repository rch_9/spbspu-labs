#ifndef FUNCTORS_H
#define FUNCTORS_H

#include "datastructtype.h"

#include <iomanip>
#include <iostream>

struct print
{
	void operator ()(const DataStruct & elem) const
	{
		std::cout << "Key1: " << std::setw(3) << elem.key1 << " Key2: " << std::setw(3) << elem.key2 << " Str: " << elem.str << std::endl;
	}
};

#endif