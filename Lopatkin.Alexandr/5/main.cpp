#include "DataStructComparator.h"
#include "functors.h"
#include "datastructtype.h"

#include <algorithm>
#include <ctime>
#include <vector>

int main()
{
	srand(time(0));

	const size_t vect_length = 20;
	const size_t range = 5;
	//std::string table = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	std::vector< std::string > table;
	table.push_back("1st");
	table.push_back("2nd");
	table.push_back("3rd");
	table.push_back("4th");
	table.push_back("5th");
	table.push_back("6th");
	table.push_back("7th");
	table.push_back("8th");
	table.push_back("9th");
	table.push_back("10th");
	
	std::vector< DataStruct > vect;

	for (size_t i = 0; i < vect_length; ++i)
	{
		vect.push_back(
			DataStruct((rand() % (2 * range + 1) - range), (rand() % (2 * range + 1) - range), table[rand() % table.size()])
			);
	}

	std::cout << "Vector before sort:" << std::endl;
	std::for_each(vect.begin(), vect.end(), print());

	std::sort(vect.begin(), vect.end(), DataStructComparator());

	std::cout << "Vector after sort:" << std::endl;
	std::for_each(vect.begin(), vect.end(), print());

	return 0;
}