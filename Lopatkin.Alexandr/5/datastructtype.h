#ifndef DATASTRUCTTYPE_H
#define DATASTRUCTTYPE_H

#include <string>

struct DataStruct
{
	DataStruct(int rKey1, int rKey2, std::string rStr):
		key1(rKey1),
		key2(rKey2),
		str(rStr)
	{}

	int key1;
	int key2;
	std::string str;
};

#endif