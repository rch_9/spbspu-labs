#ifndef DATASTRUCTCOMPARATOR_H
#define DATASTRUCTCOMPARATOR_H

#include "datastructtype.h"

#include <functional>
#include <string>

class DataStructComparator:
	public std::binary_function< DataStruct, DataStruct, bool >
{
public:
	bool operator ()(const DataStruct & left, const DataStruct & right) const
	{
		//return true if some of left elements is less
		return (
			(left.key1 < right.key1)
			|| ((left.key1 == right.key1) && (left.key2 < right.key2))
			|| ((left.key1 == right.key1) && (left.key2 == right.key2) && (left.str.size() < right.str.size()))
		   );
	}
};

#endif