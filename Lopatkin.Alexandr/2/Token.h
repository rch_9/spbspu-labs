#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <iostream>
#include <cctype>

struct Token
{
	Token()
		: isLastPunct(false)
		, isLastSpace(false)
		, end(false)
	{}

	std::string word;
	std::string punct;

	//State Flags
	bool isLastPunct;
	bool isLastSpace;
	bool end;

	friend std::istream & operator >>(std::istream & in, Token & token);
};

#endif