#include "Token.h"

#include <iostream>
#include <cctype>
#include <fstream>
#include <string>
#include <vector>

const char separator = ' ';
const size_t MAX_LENGTH = 40;

void addTokenToVector(std::vector< std::string > & vector, const Token & token)
{
	if (vector.empty())
	{
		vector.push_back(token.word);
		vector.back() += token.punct + " ";
		return;
	}

	if ((vector.back().size() + token.word.size() + token.punct.size()) < MAX_LENGTH)
	{
		vector.back() += token.word + token.punct + " ";
	} else
	{
		vector.push_back(token.word);
		vector.back() += token.punct + " ";
	}
}

void execTask(const char * input_file_name)
{
	if (!input_file_name)
	{
		std::cerr << "Input file name pointer = NULL\n";
		return;
	}
	
	std::vector< std::string > string_vector;

	std::ifstream inp_stream(input_file_name, std::ios::in);

	if (!inp_stream)
	{
		std::cerr << "Can't open file. The file \"" << input_file_name << "\" is not accessible\n";
		return;
	}

	while (inp_stream)
	{
		Token token;

		while (!token.end)
		{
			inp_stream  >> std::noskipws >> token;
		}

		if (token.word.size() > 10)
		{
			token.word = "Vau!!!";
		}

		addTokenToVector(string_vector, token);
	}

	for (std::vector< std::string >::iterator it = string_vector.begin(); it != string_vector.end(); ++it)
	{
		std::cout << "Length: " << it->size() << " Text: " << *it << "\n";
	}
}

int main()
{
	execTask("Data");
	return 0;
}