#include "Token.h"

std::istream & operator >>(std::istream & in, Token & token)
{
	std::istream::sentry sentry(in);

	if (sentry)
	{
		char tmp;
		if (!(in >> tmp))
		{
			token.end = true;
			return in;
		}

		if (std::isalnum(tmp))
		{
			if (token.isLastSpace || token.isLastPunct)
			{
				token.end = true;
				return in.putback(tmp);
			} else
			{
				token.word += tmp;
			}
		}
		else if (std::ispunct(tmp))
		{
			token.isLastPunct = true;
			token.punct += tmp;
		}
		else if(std::isspace(tmp))
		{
			token.isLastSpace = true;
		}
	}
	return in;
}