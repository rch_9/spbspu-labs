#include <iostream>
#include <vector>
#include <string>
#include <stdexcept> 
#include <fstream>

std::string loadfile(const char * file) 
{
	if (file == 0)
	{
		 throw std::invalid_argument("Wrong file name");
	}
	std::ifstream fp;
	fp.open(file);
	if (!fp)
	{
		throw std::runtime_error("Could not open file");
	}
	std::ifstream::pos_type size = 0;
	fp.seekg(0, std::ios::end);
	size = fp.tellg();
	std::string buffer;
	fp.seekg(0, std::ios::beg);
	if (size)
    	{ 
	   buffer.resize(size);
       	   fp.read(&buffer[0], size);
	   buffer.resize(fp.gcount());
    	}
	else
	{
		std::cerr<<"Empty file";
	}
	return buffer;
}

std::vector<std::string> modify (std::string & in) 
{
	std::string alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	std::string punct = ".,!?:;";
	std::vector<std::string> out;
	std::string temp = "";
	bool space = false; 
	for (std::string::iterator it = in.begin(); it != in.end(); it++) 
	{
		if (alph.find(*it) != std::string::npos) {
			if (space) 
			{
				if (temp.length() > 0) 
				{						
					if (temp.length() > 10) 
					{
						temp = "Vau!!!";
					}
					out.push_back(temp);
					temp = "";
				}
				temp += *it;
				space = false;
			}
			else 
			{
				temp += *it;
			}
		}
		else if (punct.find(*it) != std::string::npos) 
		{
			if (temp.length() > 10)
			{
				temp = "Vau!!!";
			}
			temp += *it;
			out.push_back(temp);
			temp = "";
			space = false;
		}
		else 
		{
			space = true;
		}
	}
	if (temp.length() > 0) 
	{
		out.push_back(temp);
	}
	return out;
}

void print_str (std::vector<std::string> vs, unsigned int num) 
{
	unsigned int len = 0;
	for (unsigned int i = 0; i < vs.size(); i++) 
	{
		if (vs[i].size() + len > num) 
		{
			std::cout << "\n";
			len = 0;
		}
		std::cout << vs[i] << ' ';
		len += vs[i].length() + 1;
	}
}

int main(int, char* [])
{
	const char * name = "file.txt";
	std::string file_content;
	try
	{
		file_content = loadfile(name);
		std::vector<std::string> vs;
		vs = modify(file_content);
		print_str(vs, 40);
	}
	catch (const std::exception &e)
	{
		std::cerr<<e.what()<<"\n";
		return 1;
	}
	system("pause");
	return 0;
}
