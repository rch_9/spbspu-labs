#include "lab4_1.h"

PhoneBook::iterator PhoneBook::begin()
{
	return phonebook_list.begin();
}
PhoneBook::iterator PhoneBook::end()
{
	return phonebook_list.end();
}

void PhoneBook::push_back(const PhoneBookRec & rec)
{
	phonebook_list.push_back(rec);
}

void PhoneBook::add(std::string name_, std::string phone_)
{
	phonebook_list.push_back(PhoneBookRec(name_, phone_));
}

PhoneBook::iterator PhoneBook::PhoneBook::insert(iterator it, const PhoneBookRec & rec)
{
	if (phonebook_list.empty())
	{
		throw std::invalid_argument("List is empty");
	}
	else
	{
		it = phonebook_list.insert(it, rec);
		it = phonebook_list.begin();
		return it;
	}
}
PhoneBook::iterator PhoneBook::insert_after(iterator it, const PhoneBookRec & rec)
{
	if (phonebook_list.empty())
	{
		throw std::invalid_argument("List is empty");
	}
	else
	{
		if (it != phonebook_list.end())
		{
			it++;
		}
		it = this->insert(it, rec);
		return it;
	}
}

void PhoneBook::modifyRecord(iterator it, const PhoneBookRec & rec)
{
	if (it != phonebook_list.end())
	{
		*it = rec;
	}
	else
	{
		throw std::invalid_argument("Can't modify record at the end");
	}
}

void PhoneBook::print(iterator it)
{
	if (it == phonebook_list.end())
	{
		std::cout << "End of PhoneBook\n";
		return;
	}
	std::cout << "Current record:\n";
	std::cout << "Name: " << (*it).name << " Phone: " << (*it).phone << "\n";
}

void PhoneBook::print_all()
{
	for (iterator it = phonebook_list.begin(); it != phonebook_list.end(); it++)
	{
		std::cout << "Name: " << (*it).name << " Phone: " << (*it).phone << "\n";
	}
}


PhoneBookControl::PhoneBookControl(const PhoneBook & pb_) :
	pb(pb_)
{}

void PhoneBookControl::run()
{
	PhoneBook::iterator current = pb.begin();
	char command = ' ';
	while (command != '0')
	{
		pb.print(current);
		print_commands();
		std::cin >> command;
		switch (command)
		{
		case '2':
		{	
			current++;
			if (current == pb.end())
			{
				std::cout << "End of phonebook\n";
				current--;
			}
			break;
		}
		case '1':
		{
			if (current != pb.begin())
			{
				current--;
			}
			break;
		}
		case '5':
		{
			current = pb.begin();
			break;
		}
		case '4':
		{
			for (int i = 0; i < 3; i++)
			{
				current++;
				if (current == pb.end())
				{
					std::cout << "End of phonebook\n";
					current--;
					break;
				}

			}			
			break;
		}
		case '3':
		{
			for (int i = 0; i < 3; i++)
			{
				if (current != pb.begin())
				{
					current--;
				}
			}
			break;
		}
		case 'p':
		{
			pb.print_all();
			break;
		}
		case '7':
		{

			try
			{
				current = pb.insert(current, get_name_phone());
			}
			catch (const std::exception &e)
			{
				std::cerr << e.what() << "\n";
			}
			break;
		}
		case '8':
		{
			try
			{
				current = pb.insert_after(current, get_name_phone());
			}
			catch (const std::exception &e)
			{
				std::cerr << e.what() << "\n";
			}
			break;
		}
		case '6':
		{
			pb.push_back(get_name_phone());
			break;
		}
		case '9':
		{
			pb.modifyRecord(current, get_name_phone());
			pb.print_all();
			break;
		}
		default:
		{
			std::cout << "Enter existing command\n";
		}
		}
	}
}
void PhoneBookControl::print_commands()
{
	std::cout << "\nAvailable commands:\n";
	std::cout << " 0 - exit\n";
	std::cout << " 1/2 - move to previous/next record\n";
	std::cout << " 3/4 - move to previous/next 3 records\n";
	std::cout << " 5 - move to the begin of phonebook\n";
	std::cout << " 6 - add a record to phonebook\n";
	std::cout << " 7/8 - insert a record before/after current\n";
	std::cout << " 9 - modify current record of phonebook\n";
	std::cout << " p - print all phonebook\n";

}
PhoneBook::PhoneBookRec PhoneBookControl::get_name_phone()
{
	std::string name_, phone_;
	std::cout << "\nEnter name:";
	std::cin >> name_;
	std::cout << "\nEnter phone number:";
	std::cin >> phone_;
	return PhoneBook::PhoneBookRec(name_, phone_);
}

