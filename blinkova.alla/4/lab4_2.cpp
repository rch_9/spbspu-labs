#include "lab4_2.h"

Factorial::FIterator::FIterator(int current_) :
	current(current_)
{
	int f = 1;
	for (int i = 1; i <= current_; i++)
	{
		f *= i;
	}
	fact = f;
}
Factorial::FIterator& Factorial::FIterator::operator =(const FIterator& rhs)
{
	if (this != &rhs)
	{
		current = rhs.current;
		fact = rhs.fact;
	}
	return *this;
}
Factorial::FIterator& Factorial::FIterator::operator ++()
{
	++current;
	fact *= current;
	return *this;
}
Factorial::FIterator& Factorial::FIterator::operator --()
{
	fact /= current;
	--current;
	return *this;
}
Factorial::FIterator& Factorial::FIterator::operator +(int i)
{
	for (int j = 1; j <= i; j++)
	{
		++current;
		fact *= current;
	}
	return *this;
}
Factorial::FIterator& Factorial::FIterator::operator -(int i)
{
	for (int j = 1; j <= i; j++)
	{
		--current;
		fact /= current;
	}
	return *this;
}
Factorial::FIterator Factorial::FIterator::operator ++(int)
{
	FIterator temp(current);
	++current;
	fact *= current;
	return temp;
}
Factorial::FIterator Factorial::FIterator::operator --(int)
{
	FIterator temp(current);
	--current;
	fact /= current;
	return temp;
}
int Factorial::FIterator::operator *() const
{
	if (current > size || current < 1)
	{
		throw std::runtime_error("Incorrect data");
	}
	else
	{
		return fact;
	}
}

bool Factorial::FIterator::operator ==(const FIterator& rhs) const
{
	return current == rhs.current;
}

bool Factorial::FIterator::operator !=(const FIterator& rhs) const
{
	return  current != rhs.current;
}

Factorial::FIterator Factorial::begin()
{
	return FIterator(1);
}

Factorial::FIterator Factorial::end()
{
	return FIterator(size + 1);
}