#ifndef LAB4_1
#define LAB4_1

#include <iostream>
#include <list>
#include <iterator>
#include <string>
class PhoneBook
{
public:
	struct PhoneBookRec
	{
		PhoneBookRec(std::string name_, std::string phone_) :
			name(name_),
			phone(phone_)
		{}
		std::string name;
		std::string phone;
	};

	typedef std::list<PhoneBookRec>::iterator iterator;

	iterator begin();
	iterator end();
	void push_back(const PhoneBookRec & rec);
	void add(std::string name_, std::string phone_);
	iterator insert(iterator it, const PhoneBookRec & rec);
	iterator insert_after(iterator it, const PhoneBookRec & rec);
	void modifyRecord(iterator it, const PhoneBookRec & rec);
	void print(iterator it);
	void print_all();
private:
	std::list<PhoneBookRec> phonebook_list;
};

class PhoneBookControl
{
public:
	PhoneBookControl(const PhoneBook & pb_);
	void run();
	void print_commands();
	PhoneBook::PhoneBookRec get_name_phone();
private:
	PhoneBook pb;
};
#endif