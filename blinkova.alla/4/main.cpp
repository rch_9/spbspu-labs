#include "lab4_1.h"
#include "lab4_2.h"

int main(int, char * []) 
{
	//Task 1
	PhoneBook pb;
	pb.add("Ivanov", "98347602389");			
	pb.add("Petrov", "96876096765");
	pb.add("Maksimov", "97658745563");
	pb.print_all();
	try
	{
		PhoneBookControl pb_control(pb);      
		pb_control.run();
	}
	catch (const std::exception & e)
	{
		std::cerr << e.what();
		return 1;
	}
	//Task 2
	Factorial f;
	for (Factorial::FIterator it = f.begin(); it != f.end(); it++)
	{
		std::cout << *it << "\n";
	}
    std::vector<int> vec(f.begin(), f.end());
	std::cout << "Vector contains:";
	for (int i = 0; i < 10; ++i)
	{
		std::cout << ' ' << vec[i];
	}
	std::cout << '\n';
	return 0;
}

