#ifndef LAB4_2
#define LAB4_2

#include <iostream>
#include <vector>
#include <stdexcept> 
#include <cstddef> 

class Factorial
{
public:
	class FIterator :public std::iterator<std::bidirectional_iterator_tag, int>
	{
	public:
		typedef int value_type;
		typedef int * pointer;
		typedef int & reference;
		typedef std::bidirectional_iterator_tag iterator_category;
		typedef ptrdiff_t difference_type;
		FIterator(int current_);
		FIterator& operator =(const FIterator& rhs);
		FIterator& operator ++();
		FIterator& operator --();
		FIterator& operator +(int i);
		FIterator& operator -(int i);
		FIterator operator ++(int);
		FIterator operator --(int);
		int operator *() const;
		bool operator ==(const FIterator& rhs) const;
		bool operator !=(const FIterator& rhs) const;
	private:
		int current;
		int fact;
	};
	FIterator begin();
	FIterator end();
private:
	const static int size = 10;
};
#endif