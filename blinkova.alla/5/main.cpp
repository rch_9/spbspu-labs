#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <iterator>

struct DataStruct
{	
	int key1;
	int key2;
	std::string str;
	friend std::ostream & operator << (std::ostream & out, const DataStruct & rhs);
};

std::ostream & operator << (std::ostream & out, const DataStruct & rhs)
{
	out << rhs.key1 << " " << rhs.key2 << " " << rhs.str << "\n";
	return out;
}

std::string table[10] = {
	"a",
	"aa",
	"bbb",
	"cccc",
	"abcdef",
	"ffffff",
	"abcdefgh",
	"abcdefghi",
	"abcdefghij",
	"abcdefghijk"
};

int gen_rand(int a, int b)
{
	return rand() % (b - a) + a;
}

DataStruct gen_struct()
{
	DataStruct d;
	d.key1 = gen_rand(-5, 5);
	d.str = table[gen_rand(0,9)];
	d.key2 = gen_rand(-5, 5);
	return d;
}

bool sort(DataStruct & lhs, DataStruct & rhs)
{
	if (lhs.key1 != rhs.key1)
	{
		return lhs.key1 < rhs.key1;
	}
	else if (lhs.key2 != rhs.key2)
	{
		return lhs.key2 < rhs.key2;
	}
	else
	{
		return lhs.str.size() < rhs.str.size();
	}
}
int main(int, char *[])
{
	std::vector<DataStruct> vec;
	for (int i = 0; i < 10; i++)
	{
		vec.push_back(gen_struct());
	}
	std::ostream_iterator<DataStruct> out_it(std::cout);
	std::cout << "Unsorted vector:\n";
	std::copy(vec.begin(), vec.end(), out_it);
	std::sort(vec.begin(), vec.end(), sort);
	std::cout << "Sorted vector:\n";
	std::copy(vec.begin(), vec.end(), out_it);
	return 0;
}