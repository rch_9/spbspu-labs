#ifndef LAB6_1
#define LAB6_1

#include <algorithm>
#include <iterator>
#include <fstream>
#include <stdexcept>
#include <vector>
#include <string>

std::string loadfile(const char * file);
std::vector<std::string> format(std::string in);

#endif
