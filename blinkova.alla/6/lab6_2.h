#ifndef LAB6_2
#define Lab6_2

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cmath>

struct Point
{
	int x, y;
};

struct Shape
{
	std::vector <Point> vertexes;
};

int gen_rand(int a, int b);
Point gen_point();
Shape gen_shape();
void print_point(const Point & p);
void print_shape(const Shape & sh);
int ver_sum(int x, const Shape & y);
bool is_triangle(const Shape & sh);
bool is_square(const Shape & sh);
bool is_rectangle(const Shape & sh);
bool is_pentagon(const Shape & sh);
Point get_point(const Shape & sh);
bool sort_shapes(Shape & lhs, Shape & rhs);

#endif
