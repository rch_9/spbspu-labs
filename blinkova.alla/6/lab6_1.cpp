#include "lab6_1.h"

std::string loadfile(const char * file)
{
	if (file == 0)
	{
		throw std::invalid_argument("Wrong file name");
	}
	std::ifstream fp;
	fp.open(file);
	if (!fp)
	{
		throw std::runtime_error("Could not open file");
	}
	std::string buffer((std::istreambuf_iterator<char>(fp)), (std::istreambuf_iterator<char>()));
	return buffer;
}

std::vector<std::string> format(std::string in)
{
	std::vector<std::string> vs;
	std::string temp;
	std::string divider(" \t\r\n");

	for (std::string::iterator it = in.begin(); it != in.end(); it++)
	{
		if (divider.find(*it) == std::string::npos)
		{
			temp += *it;
		}
		else if (temp.size() > 0)	
		{
			vs.push_back(temp);
			temp = "";
		}
	}
	if (temp.size() > 0) 
	{
		vs.push_back(temp);
	}
	return vs;
}
