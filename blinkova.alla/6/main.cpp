#include "lab6_1.h"
#include "lab6_2.h"
#include <iostream>
#include <numeric>

int main (int, char * []) 
{
	//Task 1
	try
	{
		std::string s = loadfile("file.txt");
		std::vector<std::string> vec = format(s);
		
		std::sort(vec.begin(), vec.end());
		std::vector<std::string>::iterator it = std::unique(vec.begin(), vec.end());
		it = vec.erase(it, vec.end());

		std::ostream_iterator<std::string> out_it(std::cout, "\n");
		std::copy(vec.begin(), vec.end(), out_it);
		std::cout << "\n";
	}
	catch (const std::exception & e)
	{
		std::cerr << e.what() << "\n";
	}
	//Task 2
	std::vector<Shape> vs(10);
	std::generate(vs.begin(), vs.end(), gen_shape);
	std::cout << "Shapes:\n";
	std::for_each(vs.begin(), vs.end(), print_shape);
	std::cout << "\n";

	int num = std::accumulate(vs.begin(), vs.end(), 0, ver_sum);
	std::cout << "Number of vertexes: " << num << "\n\n";

	int num_tri = std::count_if(vs.begin(), vs.end(), is_triangle);
	std::cout << "Number of triangles: " << num_tri << "\n";
	int num_squ = std::count_if(vs.begin(), vs.end(), is_square);
	std::cout << "Number of squares: " << num_squ << "\n";
	int num_rec = std::count_if(vs.begin(), vs.end(), is_rectangle);
	std::cout << "Number of rectangles: " << num_rec;
	std::cout << "\n\n";

	std::cout << "Without pentagons:\n";
	std::vector<Shape>::iterator it = std::remove_if(vs.begin(), vs.end(), is_pentagon);
	it = vs.erase(it, vs.end());
	std::for_each(vs.begin(), vs.end(), print_shape);
	std::cout << "\n";

	std::vector<Point> vp(vs.size());
	std::transform(vs.begin(), vs.end(), vp.begin(), get_point);
	std::cout << "Vector of points: ";
	std::for_each(vp.begin(), vp.end(), print_point);
	std::cout << "\n\n";

	std::cout << "Sorted shapes:\n";
	std::sort(vs.begin(), vs.end(), sort_shapes);
	std::for_each(vs.begin(), vs.end(), print_shape);
	std::cout << "\n";

	return 0;
} 



