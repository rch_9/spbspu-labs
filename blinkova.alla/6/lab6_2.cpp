#include "lab6_2.h"

int gen_rand(int a, int b)
{
	return rand() % (b - a) + a;
}

Point gen_point()
{
	Point p;
	p.x = gen_rand(-10, 11);
	p.y = gen_rand(-10, 11);
	return p;
}

Shape gen_shape()
{
	Shape sh;
	int type = gen_rand(0, 4);
	switch (type)
	{
	case 0:
	{
		std::vector<Point> vec(3);
		std::generate(vec.begin(), vec.end(), gen_point);
		sh.vertexes = vec;
		break;
	}
	case 1:
	{
		std::vector<Point> vec;
		Point p = gen_point();
		int len = gen_rand(1, 11);
		vec.push_back(p);
		p.x += len;
		vec.push_back(p);
		p.y += len;
		vec.push_back(p);
		p.x -= len;
		vec.push_back(p);
		sh.vertexes = vec;
		break;
	}
	case 2:
	{
		std::vector<Point> vec;
		Point p = gen_point();
		int len1 = gen_rand(1, 11);
		int len2 = gen_rand(1, 11);
		while (len2 == len1)
		{
			len2 = gen_rand(1, 11);
		}
		vec.push_back(p);
		p.x += len1;
		vec.push_back(p);
		p.y += len2;
		vec.push_back(p);
		p.x -= len1;
		vec.push_back(p);
		sh.vertexes = vec;
		break;
	}
	case 3:
	{
		std::vector<Point> vec(5);
		std::generate(vec.begin(), vec.end(), gen_point);
		sh.vertexes = vec;
		break;
	}
	}
	return sh;
}

void print_point(const Point & p)
{
	std::cout << "(" << p.x << "," << p.y << ") ";
}

void print_shape(const Shape & sh)
{
	switch(sh.vertexes.size())
	{
	case (3) :
	{
		std::cout <<"Triangle: ";
		break;
	}
	case (4) :
	{
		if (is_square(sh))
		{
			std::cout << "Square: ";
		}
		else
		{
			std::cout << "Rectangle: ";
		}
		break;
	}
	case (5) :
	{
		std::cout << "Pentagon: ";
		break;
	}
	}
	std::for_each(sh.vertexes.begin(), sh.vertexes.end(), print_point);
	std::cout << "\n";
}

int ver_sum(int x, const Shape & y)
{
	return x + y.vertexes.size();
}

bool is_triangle(const Shape & sh)
{
	return sh.vertexes.size() == 3;
}

bool is_square(const Shape & sh)
{
	if (sh.vertexes.size() == 4)
	{
		return abs(sh.vertexes[1].x - sh.vertexes[0].x) == abs(sh.vertexes[0].y - sh.vertexes[3].y);
	} 
	else
	{
		return false;
	}
}

bool is_rectangle(const Shape & sh)
{
	if (sh.vertexes.size() == 4)
	{
		return !(is_square(sh));
	}
	else
	{
		return false;
	}
}

bool is_pentagon(const Shape & sh)
{
	return sh.vertexes.size() == 5;
}

Point get_point(const Shape & sh)
{
	return sh.vertexes[gen_rand(0, sh.vertexes.size())];
}

bool sort_shapes(Shape & lhs, Shape & rhs)
{
	if ((lhs.vertexes.size() == 4) && (rhs.vertexes.size() == 4))
	{
		return ((is_square(lhs)) && (is_rectangle(rhs)));
	}
	else
	{
		return lhs.vertexes.size() < rhs.vertexes.size();
	}
}