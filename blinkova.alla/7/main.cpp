#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <climits>

int gen_rand() 
{
	return (rand() % 200) - 100;
}

struct MyFunctor
{
public:
	MyFunctor():
		first(0), 
		max(INT_MIN), 
		min(INT_MAX), 
		average(0), 
		count(0),
		count_positive(0), 
		count_negative(0), 
		sum_odd(0),
		sum_even(0)
	{}
	void operator() (const int & x)
	{
		if (!count) 
		{
			first = x;
		}
		count++;
		if (x > max)
		{
			max = x;
		}
		if (x < min)
		{
			min = x;
		}
		if (x > 0)
		{
			count_positive++;
		}
		if (x < 0)
		{
			count_negative++;
		}
		if (x % 2 == 0)
		{
			sum_even += x;
		}
		else
		{
			sum_odd += x;
		}
		average = (sum_even + sum_odd) / count;
		first_last = (first == x);
	}

	void print()
	{
		if (count)
		{
			std::cout << "Max: " << max << "\n";
			std::cout << "Min: " << min << "\n";
			std::cout << "Average: " << average << "\n";
			std::cout << "Positive count: " << count_positive << "\n";
			std::cout << "Negative count: " << count_negative << "\n";
			std::cout << "Sum odd: " << sum_odd << "\n";
			std::cout << "Sum even: " << sum_even << "\n";
			std::cout << "First is equal to last: " << first_last << "\n";
		}
	}
private:
	int first, max, min, count, count_positive, count_negative, sum_odd, sum_even;
	double average;
	bool first_last;
};

int main(int, char * []) 
{
	int n = 20;
	std::vector<int> vec(n);
	std::generate(vec.begin(), vec.end(), gen_rand);
	
	std::copy(vec.begin(), vec.end(), std::ostream_iterator<int> (std::cout, " "));
	std::cout << "\n";

	MyFunctor func = std::for_each(vec.begin(), vec.end(), MyFunctor());
	func.print();
	return 0;

}
