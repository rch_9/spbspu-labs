#include "lab1_4.h"

void fillRandom(double * array, int size)
{
	if (array == 0)
	{
		std::cerr << ("Empty array");

	}
	for (int i = 0; i < size; i++)
	{
		array[i] = 2 * ((double)rand() / (RAND_MAX)) - 1;
	}
}