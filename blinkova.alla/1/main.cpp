#include "lab1_1.h"
#include "lab1_4.h"

void removal(std::vector<int> & myvec);
void read_file(const char * file);

int main(int, char *[])
{
	//Task 1
	std::vector<int> vec1;
	for (int i = 9; i > 0; i--)
	{
		vec1.push_back(i);
	}
	std::cout << "Unsorted vector:\n";
	print_vector(vec1);
	sort< policy_vector_index<int> >(vec1);
	std::cout << "Sorted vector:\n";
	print_vector(vec1);
	std::vector<int> vec2;
	for (int i = 15; i > 0; i--)
	{
		vec2.push_back(i);
	}
	std::cout << "Unsorted vector:\n";
	print_vector(vec2);
	sort< policy_vector_at<int> >(vec2);
	std::cout << "Sorted vector:\n";
	print_vector(vec2);
	
	slist<int> list1;
	for (int i = 0; i < 5; i++)
	{
		list1.add(i);
	}
	std::cout << "Unsorted list:\n";
	slist<int>::slist_iterator it = list1.begin();
	while (it != list1.end())
	{
		std::cout << *it << " ";
		it++;
	}
	std::cout << "\n";
	sort< policy_list<int> >(list1);
	std::cout << "Sorted list:\n";
	it = list1.begin();
	while (it != list1.end())
	{
		std::cout << *it << " ";
		it++;
	}
	//Task 2
	const char * name = "file.txt";
	try
	{
		read_file(name);
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << "\n";
		return 1;
	}
	//Task 3
	std::vector<int> v;
	int a;
	std::cout << "Enter the sequence of numbers:\n";
	do
	{
		std::cin >> a;
		if (!std::cin.good())
		{
			std::cerr << "Error occured while reading numbers\n";
			return 1;
		}
		if (a)
		{
			v.push_back(a);
		}
		else break;
	} 
	while (a);
	removal(v);
	std::cout << "Result:\n";
	print_vector(v);
	//Task 4
	const int size[] = { 5, 10, 25, 50, 100 };
	for (int i = 0; i<5; i++)
	{
		std::vector<double> vec(size[i]);
		fillRandom(& vec[0], size[i]);
		std::cout << "Unsorted array:\n";
		print_vector(vec);
		std::cout << "Sorted array:\n";
		sort< policy_vector_at<double> >(vec);
		print_vector(vec);
	}
	return 0;
}
