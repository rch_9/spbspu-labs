#ifndef SLIST
#define SLIST
#include <assert.h>

template <typename T>
class slist
{
private:
	struct node_t
	{
		T value;
		node_t * next;
		node_t(T val, node_t * next_);
	};
	node_t * head;
	slist(const slist &);
	slist & operator =(const slist &);
public:
	class slist_iterator :
		public std::iterator<std::forward_iterator_tag, T>
	{
	public:
		typedef node_t node_type;
		typedef slist_iterator this_type;
		slist_iterator();
		slist_iterator(node_type * slist);
		bool operator ==(const this_type & rhs) const;
		bool operator !=(const this_type & rhs) const;
		T & operator *() const;
		T * operator ->() const;
		this_type & operator ++();
		this_type operator ++(int);
		friend void destroy_list(slist_iterator list);
	private:
		node_type * current_;
	};

	typedef slist_iterator iterator;
	typedef T value_type;
	slist() :
		head(0)
	{};
	 ~slist();
	 slist_iterator begin();
	 slist_iterator end();
	 void add(T value);
 };

template<typename T>
slist<T>::node_t::node_t(T val, node_t * next_) :
	value(val),
	next(next_)
{}

template <typename T>
slist<T>::slist_iterator::slist_iterator() :
	current_(0)
{}

template <typename T>
slist<T>::slist_iterator::slist_iterator(node_type * slist) :
	current_(slist)
 {}

template <typename T>
bool slist<T>::slist_iterator::operator==(const slist_iterator & rhs) const
{
	return current_ == rhs.current_;
}

template <typename T>
bool slist<T>::slist_iterator::operator !=(const slist_iterator & rhs) const
{
	return current_ != rhs.current_;
}

template <typename T>
T & slist<T>::slist_iterator::operator *() const
{
	assert(current_ != 0);
	return current_->value;
}

template <typename T>
T * slist<T>::slist_iterator::operator ->() const
{
	assert(current_ != 0);
	return &current_->value;
}

template <typename T>
typename slist<T>::slist_iterator & slist<T>::slist_iterator::operator ++()
{
	assert(current_ != 0);
	current_ = current_->next;
	return *this;
}

template <typename T>
typename slist<T>::slist_iterator slist<T>::slist_iterator::operator ++(int)
{
	this_type temp(*this);
	this->operator ++();
	return temp;
}

template <typename T>
void slist<T>::add(T value)
{
	node_t * temp = new node_t(value, head);
	head = temp;
}
template <typename T>
slist<T>::~slist()
 {
	 while (head != 0)
	 {
		node_t * temp = head->next;
		delete head;
		head = temp;
	}
 }

template < typename T >
typename slist<T>::slist_iterator slist<T>::begin()
{
	return head;
}

template < typename T >
typename slist<T>::slist_iterator slist<T>::end()
{
	return 0;
}
#endif