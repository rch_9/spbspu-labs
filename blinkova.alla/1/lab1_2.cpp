#include <vector>
#include <iostream>
#include <stdexcept> 
#include <fstream>

void read_file(const char * file)
{
	if (file == 0)
	{
		throw std::invalid_argument("Wrong file name");
	}
	std::ifstream fp;
	fp.open(file, std::ios::in);
	if (!fp)
	{
		throw std::runtime_error("Could not open file");
	}
	std::ifstream::pos_type size = 0;
	fp.seekg(0, std::ios::end);
	size = fp.tellg();
	std::string buffer;
	fp.seekg(0, std::ios::beg);
	if (size)
	{
		buffer.resize(size);
		fp.read(&buffer[0], size);
		buffer.resize(fp.gcount());
	}
	else
	{
		std::cerr << "Empty file";
	}
	std::vector<char> myvec(buffer.begin(), buffer.end());
	std::cout << "\n";
	for (unsigned int i = 0; i<myvec.size(); i++)
	{
		std::cout << myvec[i];
	}
	std::cout << "\n";
}

