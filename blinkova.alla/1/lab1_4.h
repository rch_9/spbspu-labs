#ifndef LAB1_4
#define LAB1_4
#include <iostream>
#include <cstdlib>
#include <vector>

template <typename T>
void print_vector(std::vector<T> & v)
{
	for (unsigned int i = 0; i<v.size(); i++)
	{
		std::cout << v[i] << ' ';
	}
	std::cout << "\n";
}

void fillRandom(double * array, int size);

#endif

