#ifndef LAB1_1
#define LAB1_1
#include <iostream>
#include <vector>
#include "slist.h"

template <typename T1>
struct policy_vector_index
{
	typedef unsigned int mover;
	typedef std::vector<T1> container_type;
	static mover begin(container_type &)
	{
		return 0;
	}
	static mover end(container_type & c)
	{
		return c.size();
	}
	static T1 & get_element(container_type & c, mover pos)
	{
		return c[pos];
	}
};
template <typename T1>
struct policy_vector_at
{
	typedef unsigned int mover;
	typedef std::vector<T1> container_type;
	static mover begin(container_type &)
	{
		return 0;
	}
	static mover end(container_type & c)
	{
		return c.size();
	}
	static T1 & get_element(container_type & c, mover pos)
	{
		return c.at(pos);
	}
};

template <typename T1>
struct policy_list
{
	typedef  slist<T1> container_type;
	typedef  typename slist<T1>::iterator mover;
	static mover begin(container_type & c)
	{
		return c.begin();
	}
	static mover end(container_type & c)
	{
		return c.end();
	}
	static T1 & get_element(container_type &, mover pos)
	{
		return *pos;
	}
};

template <typename Policy, typename T>
void sort(T& container)
{
	for (typename Policy::mover i = Policy::begin(container); i != Policy::end(container); i++)
	{
		for (typename Policy::mover j = i; j != Policy::end(container); j++)
		{
			if (Policy::get_element(container, i) > Policy::get_element(container, j))
			{
				std::swap(Policy::get_element(container, i), Policy::get_element(container, j));
			}
		}
	}
}
#endif