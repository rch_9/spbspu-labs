#ifndef LAB3_1
#define LAB3_1

#include <list>
#include <string>
#include <iostream>
#include <stdexcept>

template<typename T>
class QueueWithPriority
{
public:
	enum ElementPriority
	{
		LOW,
		NORMAL,
		HIGH
	};
	void Put(const T & el, const ElementPriority & priority);
	void Get(T & el);
	void Accelerate();
	void Print();
private:
	std::list<T> qlist_high;
	std::list<T> qlist_normal;
	std::list<T> qlist_low;
	void Print_list(const std::list<T> & l, const std::string & priority);
};

template<typename T>
void QueueWithPriority<T>::Put(const T & el, const ElementPriority & priority)
{
		switch (priority)
		{
		case (LOW) : 
		{
			qlist_low.push_front(el);
			break;
		}
		case (NORMAL) : 
		{
			qlist_normal.push_front(el);
			break;
		}
		case (HIGH) : 
		{
			qlist_high.push_front(el);
			break;
		}
		}
}

template<typename T>
void QueueWithPriority<T>::Print()
{
	if ((qlist_high.empty()) && (qlist_normal.empty()) && (qlist_low.empty()))
	{
		std::cout << "Queue is empty\n";
	}
	else
	{
		Print_list(qlist_high, "HIGH");
		Print_list(qlist_normal,"NORMAL");
		Print_list(qlist_low, "LOW");
	}
}

template<typename T>
void QueueWithPriority<T>::Print_list(const std::list<T> & l, const std::string & priority)
{
	for (typename std::list<T>::const_reverse_iterator it = l.rbegin(); it != l.rend(); it++)
		{
			std::cout << (*it) << " - " << priority << "\n";
		}
}

template<typename T>
void QueueWithPriority<T>::Accelerate()
{
	if (!qlist_low.empty())
	{
		qlist_high.splice(qlist_high.begin(), qlist_low);
	}
}

template<typename T>
void QueueWithPriority<T>::Get(T & el)
{
	if ((qlist_high.empty()) && (qlist_normal.empty()) && (qlist_low.empty()))
	{
		throw std::runtime_error("Queue is empty");
	}
	if (!qlist_high.empty())
	{
		el = qlist_high.back();
		qlist_high.pop_back();
	}
	else if (!qlist_normal.empty())
	{
		el = qlist_normal.back();
		qlist_normal.pop_back();
	}
	else
	{
		el = qlist_low.back();
		qlist_low.pop_back();
	}
}

#endif
