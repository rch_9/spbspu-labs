#ifndef LAB3_2
#define LAB3_2
#include <list>
int gen_rand(int a, int b);
std::list<int> create_list(int size);
void print_list(std::list<int> & l);
void print_list_2(std::list<int> & l);
#endif