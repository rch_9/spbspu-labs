#include "lab3_1.h"
#include "lab3_2.h"
#include <list>
#include <string>
#include <iostream>
#include <stdexcept>

struct QueueElement
{
public:
	QueueElement(const std::string & name_);
	friend std::ostream& operator << (std::ostream& ostr, const QueueElement& el);
private:
	std::string name;
};

QueueElement::QueueElement(const std::string & name_) :
	name(name_)
{}
std::ostream& operator<<(std::ostream& ostr, const QueueElement& el)
{
	ostr << el.name;
	return ostr;
}

int main(int, char * []) 
{
	//Task 1
	QueueWithPriority<QueueElement> qwp;
	QueueElement el1("el_1");
	QueueElement el2("el_2");
	qwp.Put(el1, QueueWithPriority<QueueElement>::LOW);
	qwp.Put(el2, QueueWithPriority<QueueElement>::NORMAL);

	QueueElement qe("qe");
	qwp.Get(qe);
	std::cout << "Element from queue:"<<qe<<"\n";
	try 
	{
		qwp.Get(qe);
		qwp.Get(qe);
	}
	catch (const std::exception &e) 
	{
		std::cerr<<e.what()<<"\n";
	}
	QueueElement el3("el_3");
	QueueElement el4("el_4");
	QueueElement el5("el_5");
	QueueElement el6("el_6");
	QueueElement el7("el_7");
	QueueElement el8("el_8");
	QueueElement el9("el_9");

	qwp.Put(el1, QueueWithPriority<QueueElement>::LOW);
	qwp.Put(el2, QueueWithPriority<QueueElement>::NORMAL);
	qwp.Put(el3, QueueWithPriority<QueueElement>::LOW);
	qwp.Put(el4, QueueWithPriority<QueueElement>::HIGH);
	qwp.Put(el5, QueueWithPriority<QueueElement>::LOW);
	qwp.Put(el6, QueueWithPriority<QueueElement>::HIGH);
	qwp.Put(el7, QueueWithPriority<QueueElement>::NORMAL);
	qwp.Put(el8, QueueWithPriority<QueueElement>::NORMAL);
	qwp.Put(el9, QueueWithPriority<QueueElement>::HIGH);

	std::cout << "Queue:\n";
	qwp.Print();
	
	qwp.Accelerate();
	std::cout << "After acceleration:\n";
	qwp.Print();

	//Task 2
	int size[] = { 0, 1, 2, 3, 4, 5, 7, 14 };
	std::list<int> list_int;
	for (int i = 0; i<8; i++)
	{
		list_int = create_list(size[i]);
		print_list(list_int);
		print_list_2(list_int);
	}
	return 0;
}