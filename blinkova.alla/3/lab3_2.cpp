#include <iostream>
#include <list>
#include <cstdlib>

int gen_rand(int a, int b) 
{
	return rand() % (b - a) + a;
}

std::list<int> create_list(int size) 
{
	std::list<int> l;
	for (int i = 0; i<size; i++) 
	{
		l.push_back(gen_rand(1, 20));
	}
	return l;
}

void print_list(std::list<int> & l) 
{
	std::cout << "List:\n";
	if (l.empty())
	{
		std::cout << "List is empty\n";
	}
	else
	{
		for (std::list<int>::iterator it = l.begin(); it != l.end(); it++)
		{
			std::cout << *it << " ";
		}
		std::cout << "\n";
	}
}

void print_list_2(std::list<int> & l)
{
	std::cout << "List from head and tail:\n";
	if (l.empty())
	{
		std::cout << "List is empty\n";
	}
	else
	{
		std::list<int>::iterator it = l.begin();
		std::list<int>::iterator rit = l.end();
		while (it!=rit)
		{
			std::cout << *it << " ";
			if ((++it) == rit)
			{
				break;
			}
			rit--;
			if (it==rit)
			{
				std::cout << (*rit) << " ";
				break;
			}
			std::cout << *rit << " ";
		}
		std::cout<<"\n";
	}	
}
