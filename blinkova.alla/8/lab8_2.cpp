#include "lab8_2.h"
#include <iostream>

int gen_rand_int(int a, int b)
{
	return rand() % (b - a) + a;
}

Point gen_point()
{
	Point p;
	p.x = gen_rand_int(-10, 11);
	p.y = gen_rand_int(-10, 11);
	return p;
}

Shape::Shape(Point center_)
{
	center.x = center_.x;
	center.y = center_.y;
}

bool Shape::IsMoreLeft(const Shape & rhs)
{
	return center.x < rhs.center.x;
}

bool Shape::IsUpper(const Shape & rhs)
{
	return center.y > rhs.center.y;
}

void Shape::Draw()
{}

void Circle::Draw()
{
	std::cout << "Circle : (" << center.x << "," << center.y << ")\n";
}

void Triangle::Draw()
{
	std::cout << "Triangle : (" << center.x << "," << center.y << ")\n";
}

void Square::Draw()
{
	std::cout << "Square : (" << center.x << "," << center.y << ")\n";
}

boost::shared_ptr<Shape> gen_shape()
{
	boost::shared_ptr<Shape> shp;
	int type = gen_rand_int(0, 3);
	switch (type)
	{
	case 0:
		shp.reset(new Circle(gen_point()));
		break;
	case 1:
		shp.reset(new Triangle(gen_point()));
		break;
	case 2:
		shp.reset(new Square(gen_point()));
		break;
	}
	return shp;
}

void print_shape(const boost::shared_ptr<Shape> & shp)
{
	shp->Draw();
}

bool compare_shapes(const boost::shared_ptr<Shape> & lhs, const boost::shared_ptr<Shape> & rhs, bool type)
{
	if (type)
	{
		return lhs->IsMoreLeft(*rhs);
	}
	else
	{
		return lhs->IsUpper(*rhs);
	}
};
