#include "lab8_2.h"

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>

double gen_rand();

double gen_rand()
{
	return (rand() % 200) / 100.;
}

int main(int, char * []) 
{
	//Task 1
	std::vector<double> v(10);
	std::generate(v.begin(), v.end(), gen_rand);
	std::ostream_iterator<double> it_out(std::cout, " ");
	std::copy(v.begin(), v.end(), it_out);
	std::cout << "\n";

	std::transform(v.begin(), v.end(), v.begin(), std::bind2nd(std::multiplies<double>(), acos(-1.0)));
	
	std::cout << "After multiplucation:\n";
	std::copy(v.begin(), v.end(), it_out);
	std::cout << "\n";
	
	//Task 2
	std::vector<boost::shared_ptr<Shape>> vec(10);
	std::generate(vec.begin(), vec.end(), gen_shape);
	std::for_each(vec.begin(), vec.end(), print_shape);
	std::cout << "\n";

	std::cout << "Left to right\n";
	std::sort(vec.begin(), vec.end(), boost::bind(compare_shapes, _1, _2, true));
	std::for_each(vec.begin(), vec.end(), print_shape);
	std::cout << "\n";

	std::cout << "Right to left\n";
	std::sort(vec.begin(), vec.end(), boost::bind(compare_shapes, _2, _1, true));
	std::for_each(vec.begin(), vec.end(), print_shape);
	std::cout << "\n";

	std::cout << "Upper to lower\n";
	std::sort(vec.begin(), vec.end(), boost::bind(compare_shapes, _1, _2, false));
	std::for_each(vec.begin(), vec.end(), print_shape);
	std::cout << "\n";

	std::cout << "Lower to upper\n";
	std::sort(vec.begin(), vec.end(), boost::bind(compare_shapes, _2, _1, false));
	std::for_each(vec.begin(), vec.end(), print_shape);

	return 0;
}
