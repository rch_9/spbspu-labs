#ifndef LAB8_2
#define LAB8_2

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

struct Point
{
	int x, y;
};

class Shape
{
public:
	Shape(Point center_);
	bool IsMoreLeft(const Shape & rhs);
	bool IsUpper(const Shape & rhs);
	virtual void Draw();
protected:
	Point center;
};

class Circle : public Shape
{
public:
	Circle(Point center_) : Shape(center_)
	{};
	void Draw();
};

class Triangle : public Shape
{
public:
	Triangle(Point center_) : Shape(center_)
	{};
	void Draw();
};

class Square : public Shape
{
public:
	Square(Point center_) : Shape(center_)
	{};
	void Draw();
};

int gen_rand_int(int a, int b);
Point gen_point();
boost::shared_ptr<Shape> gen_shape();
void print_shape(const boost::shared_ptr<Shape> & shp);
bool compare_shapes(const boost::shared_ptr<Shape> & lhs, const boost::shared_ptr<Shape> & rhs, bool type);

#endif
