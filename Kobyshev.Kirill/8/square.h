#ifndef SQUARE_DECLARATION
#define SQUARE_DECLARATION
#include "shape.h"

class square : public shape
{
	public:
	square(void);
	square(int, int);
	virtual void draw(void) const;
};

#endif