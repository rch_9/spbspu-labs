#ifndef ADAPTERS_DECLARATION
#define ADAPTERS_DECLARATION
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>
#include "circle.h"
#include "triangle.h"
#include "square.h"

typedef boost::shared_ptr < shape > shape_ptr;

struct fill_random_adapter
	: std::unary_function< void, shape_ptr >
{
	shape_ptr operator()(void);
};

struct is_more_left_adapter
	: std::binary_function< shape_ptr, shape_ptr, bool >
{
	bool operator()(const shape_ptr &, const shape_ptr &);
};

struct is_upper_adapter
	: std::binary_function< shape_ptr, shape_ptr, bool >
{
	bool operator()(const shape_ptr &, const shape_ptr &);
};

struct draw_adapter
	: std::unary_function< shape_ptr, void >
{
	void operator()(const shape_ptr &);
}; 

#endif