#include <ctime>
#include <vector>
#include <boost/math/constants/constants.hpp>
#include "nums_list_func.h"
#include "adapters.h"

int main(int argc, char** argv) 
{
	//p1
	srand(time(0));
	std::list< double > nums(5);
	std::generate(nums.begin(), nums.end(), randomizator());
	out(nums);
	std::transform(nums.begin(), nums.end(), nums.begin(),
		std::bind1st(std::multiplies< double >(), 
		boost::math::constants::pi< double >()));
	out(nums);
	
	//p2
	std::cout << "Begin:" << std::endl;
	std::vector< shape_ptr > shapes(5);
	std::generate(shapes.begin(), shapes.end(), fill_random_adapter());
	std::for_each(shapes.begin(), shapes.end(), draw_adapter());
	std::cout << "Sort from left to right:" << std::endl;
	std::sort(shapes.begin(), shapes.end(),
		boost::bind(is_more_left_adapter(), _1, _2));
	std::for_each(shapes.begin(), shapes.end(), draw_adapter());
	std::cout << "Sort from right to left:" << std::endl;
	std::sort(shapes.begin(), shapes.end(),
		boost::bind(is_more_left_adapter(), _2, _1));
	std::for_each(shapes.begin(), shapes.end(), draw_adapter());
	std::cout << "Sort from up to down:" << std::endl;
	std::sort(shapes.begin(), shapes.end(),
		boost::bind(is_upper_adapter(), _1, _2));
	std::for_each(shapes.begin(), shapes.end(), draw_adapter());
	std::cout << "Sort from down to up:" << std::endl;
	std::sort(shapes.begin(), shapes.end(),
		boost::bind(is_upper_adapter(), _2, _1));
	std::for_each(shapes.begin(), shapes.end(), draw_adapter());
	return 0;
}