#ifndef NUMS_LIST_FUNC_DEFINITION
#define NUMS_LIST_FUNC_DEFINITION
#include "nums_list_func.h"

double randomizator::operator()(void)
{
	return (((double)rand() * 100 ) / RAND_MAX); //from 0 to 100
}

void out(const std::list< double > & out_list)
{
	std::copy(out_list.begin(), out_list.end(), std::ostream_iterator< double >(std::cout," "));
	std::cout << std::endl;
}

#endif