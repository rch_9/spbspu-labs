#ifndef SHAPE_DEFINITION
#define SHAPE_DEFINITION
#include "shape.h"

shape::shape(void) : x(0), y(0) {}
shape::shape(int x_, int y_) : x(x_), y(y_) {}

bool shape::is_more_left(const shape & comp_shape)
{
	return x < comp_shape.x;
}

bool shape::is_upper(const shape & comp_shape)
{
	return y > comp_shape.y;
}

void shape::draw(void) const
{
	std::cout << "Shape: " << x << " " << y << std::endl;
}

#endif