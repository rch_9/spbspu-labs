#ifndef NUMS_LIST_FUNC_DECLARATION
#define NUMS_LIST_FUNC_DECLARATION
#include <list>
#include <algorithm>
#include <iterator>
#include <iostream>

struct randomizator
	: std::unary_function< double, void >
{
	double operator()(void);
};

void out(const std::list< double > & out_list);

#endif