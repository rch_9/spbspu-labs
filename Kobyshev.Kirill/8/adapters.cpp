#ifndef ADAPTERS_DEFINITION
#define ADAPTERS_DEFINITION
#include "adapters.h"

shape_ptr fill_random_adapter::operator()(void)
{
	switch(rand() % 3)
	{
		case 0:
			return boost::make_shared< circle >(circle(rand() % 100, rand() % 100));
 			break;
		case 1:
			return boost::make_shared< triangle >(triangle(rand() % 100, rand() % 100));
			break;
		case 2:
			return boost::make_shared< square >(square(rand() % 100, rand() % 100));
			break;
	}
}

bool is_more_left_adapter::operator()(const shape_ptr & a, 
	const shape_ptr & b)
{
	return a->is_more_left(*b);
}

bool is_upper_adapter::operator()(const shape_ptr & a,
	const shape_ptr & b)
{
	return a->is_upper(*b);
}

void draw_adapter::operator()(const shape_ptr & out_shape)
{
	out_shape->draw();
}

#endif