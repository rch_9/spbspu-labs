#ifndef CIRCLE_DEFINITION
#define CIRCLE_DEFINITION
#include "circle.h"

circle::circle(void) : shape() {}
circle::circle(int x_, int y_) : shape(x_, y_) {}

void circle::draw(void) const
{
	std::cout << "Circle: " << x << " " << y << std::endl;
}

#endif