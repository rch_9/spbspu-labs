#ifndef CIRCLE_DECLARATION
#define CIRCLE_DECLARATION
#include "shape.h"

class circle : public shape
{
	public:
	circle(void);
	circle(int, int);
	virtual void draw(void) const;	
};

#endif