#ifndef TRIANGLE_DEFINITION
#define TRIANGLE_DEFINITION
#include "triangle.h"

triangle::triangle(void) : shape() {}
triangle::triangle(int x_, int y_) : shape(x_, y_) {}

void triangle::draw(void) const
{
	std::cout << "Triangle: " << x << " " << y << std::endl;
}

#endif