#ifndef TRIANGLE_DECLARATION
#define TRIANGLE_DECLARATION
#include "shape.h"

class triangle : public shape
{
	public:
	triangle(void);
	triangle(int, int);
	virtual void draw(void) const;
};

#endif