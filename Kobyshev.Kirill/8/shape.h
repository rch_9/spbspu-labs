#ifndef SHAPE_DECLARATION
#define SHAPE_DECLARATION
#include <iostream>

class shape
{
	public:
	shape(void);
	shape(int, int);
	bool is_more_left(const shape &);
	bool is_upper(const shape &);
	virtual void draw(void) const = 0;
	
	protected:
	int x, y;
	virtual ~shape(void);
};

#endif