#ifndef SQUARE_DEFINITION
#define SQUARE_DEFINITION
#include "square.h"

square::square(void) : shape() {}
square::square(int x_, int y_) : shape(x_, y_) {}

shape::~shape(void) {};

void square::draw(void) const
{
	std::cout << "Square: " << x << " " << y << std::endl;
}

#endif