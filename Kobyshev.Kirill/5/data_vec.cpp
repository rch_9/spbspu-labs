#ifndef DATAVEC_DEFINITION
#define DATAVEC_DEFINITION
#include "data_vec.h"

data_struct randomizator::operator()(const std::vector< std::string > & table)
{
	data_struct random_struct =
	{
		rand()%11 - 5,
		rand()%11 - 5,
		table[rand() % table.size()]	
	};
	return random_struct;
}

void data_struct_writer::operator()(const data_struct & out_struct)
{
	std::cout << out_struct.key1 << " "
		<< out_struct.key2 << " "
		<< out_struct.str << std::endl;
}

void out(const data_vec & out_data)
{
	std::cout << "DataVec:" << std::endl;
	std::for_each(out_data.begin(), out_data.end(), data_struct_writer());
}

bool comparator::operator()(const data_struct & first, const data_struct & second)
{
	if (first.key1 == second.key1)
	{
		return ((first.key2 == second.key2 && first.str.length() < second.str.length()) ||
				(first.key2 < second.key2));
	}
	else
	{
		return (first.key1 < second.key1);
	}
}

void sort(data_vec & sort_data)	
{
	std::sort(sort_data.begin(), sort_data.end(), comparator());
}

#endif