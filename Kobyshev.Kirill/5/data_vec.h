#ifndef DATAVEC_DECLARATION
#define DATAVEC_DECLARATION
#include <vector>
#include <iostream>
#include <algorithm>
#include "data_struct.h"

typedef std::vector< data_struct > data_vec;
void out(const data_vec & out_data);
void sort(data_vec & sort_data);	

struct comparator
	: std::binary_function < data_struct, data_struct, bool >
{
	bool operator()(const data_struct &, const data_struct &);
};

struct data_struct_writer
	: std::unary_function < data_struct, void >
{
	void operator()(const data_struct &);
};

struct randomizator
	: std::unary_function < std::vector< std::string >, data_struct >
{
	data_struct operator()(const std::vector< std::string > &);
};

#endif