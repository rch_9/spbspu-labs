#ifndef DATASTRUCT_DECLARATION
#define DATASTRUCT_DECLARATION
#include <string>

struct data_struct
{
	int key1;
	int key2;
	std::string str;
};

#endif
