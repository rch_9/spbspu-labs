#include <boost/bind.hpp>
#include <ctime>
#include "data_vec.h"

int main(int argc, char** argv) 
{
	srand(time(0));
	std::vector< std::string > data_table;
	data_table.push_back("one");
	data_table.push_back("two");
	data_table.push_back("three");
	data_table.push_back("four");
	data_table.push_back("five");
	data_table.push_back("six");
	data_table.push_back("seven");
	data_table.push_back("eight");
	data_table.push_back("nine");
	data_table.push_back("ten");
	data_vec data;
	data.resize(100);
	std::generate(data.begin(), data.end(), 
		boost::bind(randomizator(), data_table));
	out(data);
	sort(data);
	out(data);
	
	return 0;
}