#include <exception>
#include <algorithm>
#include <ctime>
#include "Statistics.h"
#include "fillRandom.h"

int main(int argc, char** argv) 
{
	try
	{
		srand(time(0));
		std::vector< int > numbers(1000);
		std::for_each(numbers.begin(), numbers.end(), fillRandom);
		NumInf inf = std::for_each(numbers.begin(), numbers.end(), 
			NumInf());
		inf.out();
	}
	catch(std::exception & e)
	{
		std::cout << "Exception type: " << e.what() << std::endl;
	}
	return 0;
}