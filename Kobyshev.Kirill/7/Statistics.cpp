#ifndef STATISTICS_DEFINITION
#define STATISTICS_DEFINITION
#include "Statistics.h"

NumInf::NumInf(void) : initialized(false) {}

void NumInf::operator()(const int num)
	{
		if(num > 500 || num < -500)
		{
			throw std::runtime_error("Incorrect value - " + num);
		}
		if(!initialized)
		{
			first = num;
			sum = num;
			max = num;
			min = num;
			avg = num;
			pos = 0;
			neg = 0;
			odd = 0;
			even = 0;
			firEqLast = false;
			initialized = true;
		}
		else
		{
			if(num > max)
			{
				max = num;
			}
			if(num < min)
			{
				min = num;
			}
			sum += num;
			avg = (double)sum/(odd + even);
			
			if(num == first)
			{
				firEqLast = true;
			}
			else
			{
				firEqLast = false;
			}
		}
		
		if(num > 0)
		{
			pos++;
		}
		
		if(num < 0)
		{
			neg++;
		}
		
		if(num % 2 == 0)
		{
			even++;
		}
		else
		{
			odd++;
		}
	}

void NumInf::out(void)
{
	std::cout << "max: " << max << std::endl
			  << "min: " << min << std::endl
			  << "avg: " << avg << std::endl
			  << "pos: " << pos << std::endl
			  << "neg: " << neg << std::endl
			  << "odd: " << odd << std::endl
			  << "even:" << even << std::endl
			  << "first == last: " << firEqLast << std::endl;
}
#endif