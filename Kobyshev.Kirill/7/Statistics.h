#ifndef STATISTICS_DECLARATION
#define STATISTICS_DECLARATION
#include <iostream>
#include <stdexcept>

class NumInf
{
	public:
		int max, min, pos, neg, odd, even;
		double avg;
		bool firEqLast;
		NumInf(void);
		void operator()(const int num);
		void out(void);
		
	private:
		int first, sum;
		bool initialized;
};

#endif