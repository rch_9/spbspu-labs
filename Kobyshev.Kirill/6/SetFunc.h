#ifndef SETFUNC_DECLARATION
#define SETFUNC_DECLARATION
#include <fstream>
#include <iterator>
#include <string>
#include <set>
#include <stdexcept>
#include <iostream>
#include <algorithm>

void read_file_to_set(std::set< std::string > &, const char *);
void out_set(const std::set< std::string > &);

#endif