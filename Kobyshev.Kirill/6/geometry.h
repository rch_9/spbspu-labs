#ifndef GEOMETRY_DECLARATION
#define GEOMETRY_DECLARATION
#include <vector>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <boost/bind.hpp>

const int max_margin = 100;

struct shape
{
	struct point
	{
		int x, y;
	};
	std::vector< point > vertexes;
	int type(void) const;
	void push_back_coords(int, int);
};

struct point_randomizator
	: std::unary_function< void, shape::point >
{	
	shape::point operator()(void);
};

struct shape_randomizator
	: std::unary_function< void, shape >
{	
	shape operator()(void);
};

struct point_from_shape_randomizator
	: std::unary_function< shape, shape::point >
{	
	shape::point operator()(const shape &);
};

struct point_writer
	: std::unary_function< shape::point, void >
{
	void operator()(const shape::point &);
};

struct shape_writer
	: std::unary_function< shape, void >
{
	void operator()(const shape &);
};

struct shape_typizator
	: std::binary_function< std::vector< shape >, unsigned int, bool >
{
	bool operator()(const shape &, unsigned int);
};

struct shape_comparator
	: std::binary_function< shape, shape, bool >
{
	bool operator()(const shape &, const shape &);
};

int sum_vertexes_num(const std::vector< shape > &);
int sum_type_num(const std::vector< shape > &, int);
void out_points(const std::vector< shape::point > &);
void out_shapes(const std::vector < shape > &);
void delete_shape(std::vector< shape > &, int);
void get_points_from_shapes(const std::vector< shape > &, std::vector< shape::point > &);

#endif