#ifndef SETFUNC_DEFINITION
#define SETFUNC_DEFINITION
#include "SetFunc.h"

void read_file_to_set(std::set< std::string > & input, const char * filename)
{
	if (filename)
	{
		std::ifstream file(filename);
		if(!file)
		{
			throw std::runtime_error("File wasn't opened");
		}
		std::copy(
			std::istream_iterator< std::string >(file), 
			std::istream_iterator< std::string >(), 
			std::inserter(input, input.begin())
		);
		if(!file.eof())
		{
			throw std::runtime_error("Status of reading file - not eof");
		}
	}
	else
	{
		throw std::runtime_error("Name of file is empty");
	}
}

void out_set(const std::set< std::string > & output)
{
	std::copy(output.begin(), output.end(), 
		std::ostream_iterator< std::string >(std::cout, " "));
	std::cout << std::endl;
}

#endif