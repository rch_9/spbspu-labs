#include <exception>
#include <ctime>
#include "SetFunc.h"
#include "geometry.h"

int main(int argc, char** argv) 
{
	try
	{
		//first part
		std::set< std::string > dictionary;
		read_file_to_set(dictionary,(char *)"file.txt");
		out_set(dictionary);
		//second part
		srand(time(0));
		std::vector< shape > shapes;
		shapes.resize(10);
		std::generate(shapes.begin(), shapes.end(), shape_randomizator());
		out_shapes(shapes);
		std::cout << "Points: " << sum_vertexes_num(shapes) << std::endl;
		out_shapes(shapes);
		std::cout << "Triangles: " << sum_type_num(shapes, 0) << std::endl
			<< "Squares: " << sum_type_num(shapes, 1) << std::endl
			<< "Rectangles: " << sum_type_num(shapes, 2) << std::endl
			<< "Pentagons: " << sum_type_num(shapes, 3) << std::endl;
		out_shapes(shapes);
		delete_shape(shapes, 3);
		out_shapes(shapes);
		std::vector< shape::point > points;
		get_points_from_shapes(shapes, points);
		out_points(points);
		out_shapes(shapes);
		std::sort(shapes.begin(), shapes.end(), shape_comparator());
		out_shapes(shapes);
	}
	catch(std::exception & e)
	{
		std::cout << "Exception type: " << e.what() << std::endl;
	}
	return 0;
}