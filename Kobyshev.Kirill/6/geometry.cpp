#ifndef GEOMETRY_DEFINITION
#define GEOMETRY_DEFINITION
#include "geometry.h"

void shape::push_back_coords(int x, int y)
{
	shape::point temp = {x, y};
	vertexes.push_back(temp);
}

shape::point point_randomizator::operator()(void)
{
	shape::point random_point =
	{
		rand() % (2 * max_margin) - max_margin, 
		rand() % (2 * max_margin) - max_margin 
	};
	return random_point;
}

shape shape_randomizator::operator()(void)
{
	shape result_shape;
	int length, lengthX, lengthY;
	shape::point start_point;
	point_randomizator random_point;
	switch(rand() % 4)
	{
		case 0:
			result_shape.vertexes.resize(3);
			std::generate(result_shape.vertexes.begin(),result_shape.vertexes.end(),
				point_randomizator());
			break;
		case 1:
			start_point = random_point();
			length = rand() % (max_margin - std::max(start_point.x, start_point.y));
			result_shape.push_back_coords(start_point.x, start_point.y);
			result_shape.push_back_coords(start_point.x + length, start_point.y);
			result_shape.push_back_coords(start_point.x + length, start_point.y + length);
			result_shape.push_back_coords(start_point.x, start_point.y + length);
			break;
		case 2:
			start_point = random_point();
			lengthX = rand() % (max_margin - start_point.x);
			lengthY = rand() % (max_margin - start_point.y);
			result_shape.push_back_coords(start_point.x, start_point.y);
			result_shape.push_back_coords(start_point.x + lengthX, start_point.y);
			result_shape.push_back_coords(start_point.x + lengthX, start_point.y + lengthY);
			result_shape.push_back_coords(start_point.x, start_point.y + lengthY);
			break;
		case 3:
			result_shape.vertexes.resize(5);
			std::generate(result_shape.vertexes.begin(),result_shape.vertexes.end(),
				point_randomizator());
			break;
	}
	
	return result_shape;
}

bool shape_typizator::operator()(const shape & out_shape, unsigned int comp_type)
{
	int left, right, down, up;
	if (comp_type > 5)
	{
		throw std::runtime_error("Unknown comp_type");
	}
	switch(out_shape.vertexes.size())
	{
		case 3:
			return comp_type == 0;
			break;
		case 4:
			left = out_shape.vertexes[3].y - out_shape.vertexes[0].y;
			right = out_shape.vertexes[2].y - out_shape.vertexes[1].y;
			down = out_shape.vertexes[1].x - out_shape.vertexes[0].x;
			up = out_shape.vertexes[2].x - out_shape.vertexes[3].x;
			if 
			(
				right == left &&
				down == up
			)
			{
				if (right == down)
				{
					return comp_type == 1;
				}
				else
				{
					return comp_type == 2;
				}
			}
			else
			{
				throw std::runtime_error("Unknown type of comp_shape");
			}
			break;
		case 5:
			return comp_type == 3;
			break;
		default:
			throw std::runtime_error("Unknown type of comp_shape");
	}
}

int shape::type(void) const
{
	shape_typizator comp_type;
	for (int res_type = 0; res_type < 4; res_type++)
	{
		if (comp_type(*this, res_type))
		{
			return res_type;
		}
	}
	throw std::runtime_error("Error in shape::type()");
}

void point_writer::operator()(const shape::point & out_point)
{
	std::cout << "{" << out_point.x << "," << out_point.y << "} ";
}

void shape_writer::operator()(const shape & out_shape)
{
	switch(out_shape.type())
	{
		case 0:
			std::cout << "Triangle: ";
			break;
		case 1:
			std::cout << "Square: ";
			break;
		case 2:
			std::cout << "Rectangle: ";
			break;
		case 3:
			std::cout << "Pentagon: ";
			break;
	}
	std::for_each(out_shape.vertexes.begin(), out_shape.vertexes.end(),
		point_writer());
	std::cout << std::endl;
}

int sum_type_num(const std::vector< shape > & shapes, int type)
{
	return std::count_if(shapes.begin(), shapes.end(), 
		boost::bind(shape_typizator(), _1, type));
}

int sum_vertexes_num(const std::vector< shape > & shapes)
{
	return sum_type_num(shapes, 0) * 3
		+ sum_type_num(shapes, 1) * 4
		+ sum_type_num(shapes, 2) * 4
		+ sum_type_num(shapes, 3) * 5;
}

void delete_shape(std::vector< shape > & shapes, int type)
{
	int sum_type = sum_type_num(shapes, type);
	std::remove_if(shapes.begin(), shapes.end(),
		boost::bind(shape_typizator(), _1, type));
	shapes.resize(shapes.size() - sum_type);
}

shape::point point_from_shape_randomizator::operator()(const shape & some_shape)
{
	return some_shape.vertexes[rand() % some_shape.vertexes.size()];
}

void get_points_from_shapes(const std::vector< shape > & shapes, 
	std::vector< shape::point > & points)
{
	points.resize(shapes.size());
	std::transform(shapes.begin(), shapes.end(), points.begin(), 
		point_from_shape_randomizator());
}

bool shape_comparator::operator()(const shape & a, const shape & b)
{
	return a.type() < b.type();
}

void out_points(const std::vector< shape::point > & points)
{
	std::cout << "=====================" << std::endl;
	std::for_each(points.begin(), points.end(), point_writer());
	std::cout << std::endl;
}

void out_shapes(const std::vector < shape > & shapes)
{
	std::cout << "=====================" << std::endl;
	std::for_each(shapes.begin(), shapes.end(), shape_writer());
}

#endif