#ifndef PARSING_DEFINITION
#define PARSING_DEFINITION
#include "format.h"

std::string parse_token(std::ifstream & reader)
{
	std::string result;
	char last_char;
	do 
	{
		last_char = reader.get();
	} while (isspace(last_char));

	if (ispunct(last_char))
	{
		result.push_back(last_char);
		return result;
	}

	int word_size = 0;
	while (isalnum(last_char))
	{
		result.push_back(last_char);
		last_char = reader.get();
		word_size++;
	}

	if (ispunct(last_char))
	{
		reader.unget();
	}

	if (word_size > 10)
	{
		return "vau!";
	}

	return result;
}

void format_tokens(token_vec & text)
{
	int line_size = 0;
	for (unsigned int current = 0; current < text.size(); current++)
	{
		if (line_size > 40)
		{
			text.insert(text.begin() + current - 1, "\n");
			line_size = 0;
		}

		if (isalnum(text[current][0]))
		{
			if (line_size != 0)
			{
				text.insert(text.begin() + current, " ");
				line_size++;
				current++;
			}
			line_size += text[current].size();
		}

		if(ispunct(text[current][0]))
		{
			line_size++;
		}
	}
}

#endif