#include <exception>
#include <stdexcept>
#include "format.h"

int main(int argv, char ** argc)
{
	try
	{
		token_vec text;
		std::ifstream file("file.txt");
		if (!file)
		{
			throw std::runtime_error("Error of opening file.");
		}
		while (file.good())
		{
			text.push_back(parse_token(file));
		}
		if (file.fail())
		{
			std::cout << "Warning: Status of reading file is fail." << std::endl;
		}
		if (file.bad())
		{
			throw std::runtime_error("Status of reading file is bad.");
		}
		if (!file.eof())
		{
			throw std::runtime_error("Status of reading file is not eof.");
		}
		format_tokens(text);
		for (token_vec::iterator out_it = text.begin(); out_it != text.end(); out_it++)
		{
			std::cout << *out_it;
		}
		return 0;
	}
	catch (std::exception & e)
	{
		std::cout << "Exception type: " << e.what();
	}
}