#ifndef PARSING_DECLARATION
#define PARSING_DECLARATION
#include <string>
#include <fstream>
#include <iostream>
#include <vector>

typedef std::vector< std::string > token_vec;
std::string parse_token(std::ifstream &);
void format_tokens(token_vec &);

#endif