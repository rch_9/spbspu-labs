#include "datastruct.hpp"
#include "support.hpp"

#include <iostream>
#include <algorithm>        //sort, for_each
#include <string>
#include <vector>
#include <ctime>            //time
#include <cstdlib>          //srand, rand
#include <iterator>

std::string strs[] = {"aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaaa", "aaaaaaaaaa", "aaaaaaaaaaa"};

int main()
{    
    srand(time(0));
    std::vector<Data> dataContainer(10);

    std::for_each(dataContainer.begin(), dataContainer.end(), RandomData());
    std::for_each(dataContainer.begin(), dataContainer.end(), PrintData());
    
    std::sort(dataContainer.begin(), dataContainer.end());
    std::cout << "\tSorted:\n";
    std::for_each(dataContainer.begin(), dataContainer.end(), PrintData());

    return 0;
}
