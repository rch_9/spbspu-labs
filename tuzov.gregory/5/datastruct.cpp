#include "datastruct.hpp"

#include <string>
#include  <iostream>

Data::Data() :
    key1_(0),
    key2_(0),
    str_("")
{
}

Data::Data(const int key1, const int key2, const std::string str) :
    key1_(key1),
    key2_(key2),
    str_(str)
{
}

Data::Data(const Data & rhs)
{
    this->key1_ = rhs.key1_;
    this->key2_ = rhs.key2_;
    this->str_ = rhs.str_;
}

Data::~Data()
{    
}

int Data::getKey1() const
{
    return key1_;
}

int Data::getKey2() const
{
    return key2_;
}

std::string Data::get() const
{
    return str_;
}

bool Data::operator <(const Data & rhs) const
{
    if(this->key1_ == rhs.key1_)
    {
        if(this->key2_ == rhs.key2_)
        {
            return this->str_.length() < rhs.str_.length();
        }
        return this->key2_ < rhs.key2_;
    }
    return this->key1_ < rhs.key1_;
}

void Data::operator =(const Data & rhs)
{
    this->key1_ = rhs.key1_;
    this->key2_ = rhs.key2_;
    this->str_  = rhs.str_;
}