#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>
#include <iterator>

class Data
{
public:	
	Data();
    Data(const int, const int, const std::string);            
    Data(const Data & rhs);
    ~Data();
    bool operator <(const Data &) const;    
    void operator =(const Data &);    		//=> destr, copy constr    
    int getKey1() const;
    int getKey2() const;
    std::string get() const;

private:
    int key1_;
    int key2_;
    std::string str_;
};

#endif