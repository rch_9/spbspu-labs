#include "support.hpp"

extern std::string strs[];

void PrintData::operator ()(const Data & d) 
{
  	std::cout << d.getKey1() << "\n"
         	  << d.getKey2() << "\n"
         	  << d.get() << "\n";
}

void RandomData::operator ()(Data & d)
{
    d = Data (rand() % 11 - 5,
              rand() % 11 - 5,
              strs[rand() % 10]);    
}
