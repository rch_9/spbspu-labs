#ifndef SUPPORT_HPP
#define SUPPORT_HPP

#include "datastruct.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>          //srand, rand

struct PrintData
{
	void operator ()(const Data & d);	
};

struct RandomData
{
    void operator ()(Data & d);    
};

#endif