#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <list>
#include <stdexcept>
#include <memory>

template <typename T>
class PriorityQueue
{
public:
    enum ElementPriority
    {
        PRIORITY_LOW,
        PRIORITY_NORMAL,
        PRIORITY_HIGH
    };
    void put(const T & elem, const ElementPriority & priority);
    std::shared_ptr<T> get();
    void accelerate();
    bool empty();
private:
    std::list<T> high, normal, low;
};

template<typename T>
void PriorityQueue<T>::put(const T & elem, const ElementPriority & priority)
{
    switch(priority)
    {
    case(PRIORITY_LOW) :
    {
        low.push_back(elem);
        break;
    }
    case(PRIORITY_NORMAL) :
    {
        normal.push_back(elem);
        break;
    }
    case(PRIORITY_HIGH) :
    {
        high.push_back(elem);
    }
    }
}

template<typename T>
std::shared_ptr<T> PriorityQueue<T>::get()
{
    if(this->empty())
    {
        throw std::out_of_range("\nTry to get from empty priorityQueue\n");
    }

    std::shared_ptr<T> smPtr;
    if(!high.empty())
    {
        smPtr = std::make_shared<T>(high.back());
        high.pop_back();
    }
    else if(!normal.empty())
    {
        smPtr = std::make_shared<T>(normal.back());
        normal.pop_back();
    }
    else
    {
        smPtr = std::make_shared<T>(low.back());
        low.pop_back();
    }
    return smPtr;
}

template<typename T>
void PriorityQueue<T>::accelerate()
{
    if(!low.empty())
    {
        high.splice(high.begin(), low);
    }
}

template<typename T>
bool PriorityQueue<T>::empty()
{
    return low.empty() && normal.empty() && high.empty();
}

#endif // PRIORITYQUEUE_H
