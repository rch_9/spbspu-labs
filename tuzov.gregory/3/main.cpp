#include "priorityqueue.hpp"
#include "task2.hpp"

#include <iostream>
#include <stdexcept>
#include <string>

int main()
{
    try
    {
        //task1........................................................................
        PriorityQueue<std::string> MyPriorityQueue;

        MyPriorityQueue.put(std::string("low-1"), PriorityQueue<std::string>::PRIORITY_LOW);
        MyPriorityQueue.put(std::string("low-2"), PriorityQueue<std::string>::PRIORITY_LOW);
        MyPriorityQueue.put(std::string("normal-1"), PriorityQueue<std::string>::PRIORITY_NORMAL);
        MyPriorityQueue.put(std::string("normal-2"), PriorityQueue<std::string>::PRIORITY_NORMAL);
        MyPriorityQueue.put(std::string("high-1"), PriorityQueue<std::string>::PRIORITY_HIGH);
        MyPriorityQueue.put(std::string("normal-3"), PriorityQueue<std::string>::PRIORITY_NORMAL);
        MyPriorityQueue.put(std::string("high-2"), PriorityQueue<std::string>::PRIORITY_HIGH);
        MyPriorityQueue.put(std::string("low-3"), PriorityQueue<std::string>::PRIORITY_LOW);

        MyPriorityQueue.accelerate();

        while(!MyPriorityQueue.empty())
        {
            std::cout << *MyPriorityQueue.get() << std::endl;
        }

        //task2.........................................................................

        for(unsigned i = 0; i < 15; ++i)
        {
            std::list<int> MyList;
            std::cout << "List size = " << i << ":" << "\n";
            fillListRandom(MyList, i);
            for(std::list<int>::iterator it = MyList.begin(); it != MyList.end(); ++it)
            {
                std::cout << *it << " ";
            }
            std::cout << std::endl;

            std::list<int> firstEndOrderList;
            formFirstEndOrderList(MyList.begin(), MyList.end(), firstEndOrderList);
            for(std::list<int>::iterator it = firstEndOrderList.begin(); it != firstEndOrderList.end(); ++it)
            {
                std::cout << *it << " ";
            }
            std::cout << std::endl;
        }        
    }
    catch(std::exception & e)
    {
        std::cerr << "exception caught" << e.what() << std::endl;
    }
    return 0;
}
