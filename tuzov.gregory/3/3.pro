TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    task2.cpp

HEADERS += \
    task2.hpp \
    priorityqueue.hpp

