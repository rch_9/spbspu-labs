#include "task2.hpp"

#include <stdexcept>
#include <ctime>        //time()
#include <cstdlib>      //rand(), srand()
#include <list>

void fillListRandom(std::list<int> & l, unsigned size)
{    
    srand(time(0));
    for(unsigned i = 0; i < size; ++i)
    {
        l.push_back(rand() % 20 + 1);
    }    
}

void formFirstEndOrderList(std::list<int>::iterator begin, std::list<int>::iterator end, std::list<int> & firstEndOrderList)
{    
    if(begin == end)
    {
        return;
    }
    firstEndOrderList.push_back(*begin);
    if(--end != begin)
    {
        firstEndOrderList.push_back(*end);
    }
    else
    {
        return;
    }
    if(++begin != end)
    {
        formFirstEndOrderList(begin, end, firstEndOrderList);
    }

}
