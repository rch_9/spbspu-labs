#ifndef TASK2_H
#define TASK2_H

#include <list>

void fillListRandom(std::list<int> &, unsigned);
void formFirstEndOrderList(std::list<int>::iterator, std::list<int>::iterator, std::list<int> &);

#endif // TASK2_H
