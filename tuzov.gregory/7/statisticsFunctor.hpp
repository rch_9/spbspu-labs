#ifndef STATISTICS_FUNCTOR_HPP
#define STATISTICS_FUNCTOR_HPP

#include <cstddef>

class StatFunct
{
public:
	StatFunct();
	void print() const;
	void operator ()(int);
private:	
	int max_, min_;
	double average_;	
	int numPositive_, numNegative_;
	int sumOdd_, sumEven_;
	bool cmpFirstLast_;
	size_t num_;						//for counting average_
	int first_;							//for cmpFirstLast_
};

#endif