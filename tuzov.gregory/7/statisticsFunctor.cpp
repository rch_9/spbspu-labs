#include <limits>       // std::numeric_limits
#include <iostream>

#include "statisticsFunctor.hpp"

StatFunct::StatFunct() :
	max_(std::numeric_limits<int>::min()),			
	min_(std::numeric_limits<int>::max()),
	average_(0),
	numPositive_(0),
	numNegative_(0),
	sumOdd_(0),
	sumEven_(0),
	cmpFirstLast_(false),
	num_(0),
	first_(0)
{	
}

void StatFunct::print()	const	//add ostream
{
	std::cout << "MAX: " << max_ << std::endl
			  << "MIN: " << min_ << std::endl
			  << "AVERAGE: " << average_ << std::endl
			  << "NUMBER POSITIVE: " << numPositive_ << std::endl
			  << "NUMBER NEGATIVE: " << numNegative_ << std::endl
			  << "SUM ODD: " << sumOdd_ << std::endl
			  << "SUM EVEN: " << sumEven_ << std::endl
			  << "FIRST EQUAL LAST: " << (cmpFirstLast_ == 0 ? "false" : "true") << std::endl;
}

void StatFunct::operator ()(int cur)
{
	first_ = (num_ == 0) ? cur : first_;
	++num_;

	if(cur > max_)
	{
		max_ = cur;
	}
	if(cur < min_)
	{
		min_ = cur;
	}
	if(cur > 0)
	{
		++numPositive_;
	}
	if(cur < 0)
	{
		++numNegative_;
	}
	if(cur % 2 == 0)
	{
		sumEven_ += cur;
	}		
	else
	{
		sumOdd_ += cur;
	}
	
	cmpFirstLast_ = (first_ == cur);
	average_ = (sumEven_ + sumOdd_) / num_;
}