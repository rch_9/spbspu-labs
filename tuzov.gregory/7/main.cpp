#include <cstdlib>		//srand, rand
#include <ctime>		//time
#include <vector>
#include <algorithm>
#include <iterator>		//ostream_iterator
#include <iostream>

#include "statisticsFunctor.hpp"

struct randInLimits
{
	void operator ()(int & cur)
	{
		cur = (rand() % 1000) - 500;
	}
};

int main()
{
	srand(time(0));
	std::vector<int> v(7);	
	StatFunct funct;

	std::for_each(v.begin(), v.end(), randInLimits());
	std::copy(v.begin(), v.end(), std::ostream_iterator<int> (std::cout, " "));

	funct = std::for_each(v.begin(), v.end(), StatFunct());
	std::cout << std::endl;	
	funct.print();	

	return 0;
}