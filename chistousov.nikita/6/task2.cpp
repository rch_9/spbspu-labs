#include "task2.h"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <cstdlib>
#include <ctime>

typedef std::vector<Shape>::iterator iterator;

Shape generateShape()
{
    int gen_type = rand() % 4; // Four types
    Shape shape(gen_type);
    return shape;
}

bool isTriangle(const Shape & shape)
{
    return shape.getNum() == 3;
}

bool isSquare(const Shape & shape)
{
    if (shape.getNum() == 4)
    {
        unsigned int vertical_side = shape.getVertexes()[2].y - shape.getVertexes()[0].y;
        unsigned int horizontal_side = shape.getVertexes()[1].x - shape.getVertexes()[0].x;
        return vertical_side == horizontal_side;
    }
    return false;
}

bool isRectangle(const Shape & shape)
{
    if (shape.getNum() == 4)
    {
        return !isSquare(shape);
    }
    return false;
}

bool isPentagon(const Shape & shape)
{
    return shape.getNum() == 5;
}

void countShapes(const std::vector<Shape> & shapes)
{
    int triangle_count = 0, square_count = 0,
        rectangle_count = 0, pentagon_count = 0;

    triangle_count = std::count_if(shapes.begin(), shapes.end(), isTriangle);
    square_count = std::count_if(shapes.begin(), shapes.end(), isSquare);
    rectangle_count = std::count_if(shapes.begin(), shapes.end(), isRectangle);
    pentagon_count = std::count_if(shapes.begin(), shapes.end(), isPentagon);

    std::cout << "Triangles: " << triangle_count << std::endl;
    std::cout << "Squares: " << square_count << std::endl;
    std::cout << "Rectangles: " << rectangle_count << std::endl;
    std::cout << "Pentagons: " << pentagon_count << std::endl;
}

int sumVertexes(int sum, const Shape & shape)
{
    return sum + shape.getNum();
}

int countVertexes(const std::vector<Shape> & shapes)
{
    return std::accumulate(shapes.begin(), shapes.end(), 0, sumVertexes);
}

Point getPoint(const Shape & shape)
{
    return shape.getVertexes()[0];
}

std::vector<Point> createVertexVector(const std::vector<Shape> & shapes)
{
    std::vector<Point> vertexes(shapes.size());
    std::transform(shapes.begin(), shapes.end(), vertexes.begin(), getPoint);
    return vertexes;
}

bool sortCondition(const Shape & lhs, const Shape & rhs)
{
    if (lhs.getNum() < rhs.getNum())
    {
        return true;
    }
    else if (lhs.getNum() == rhs.getNum())
    {
        return (isSquare(lhs) && !isSquare(rhs));
    }
    return false;
}

int main()
{
    srand(time(0));

    std::vector<Shape> test_vector(shapes_num);

    std::generate(test_vector.begin(), test_vector.end(), generateShape);
    std::copy(test_vector.begin(), test_vector.end(), std::ostream_iterator<Shape>(std::cout, " "));

    std::cout << std::endl;
    std::cout << "***************" << std::endl;
    std::cout << std::endl;

    countShapes(test_vector);

    std::cout << std::endl;
    std::cout << "***************" << std::endl;
    std::cout << std::endl;

    std::cout << "Sum of vertexes: " << countVertexes(test_vector);

    std::cout << std::endl;
    std::cout << "***************" << std::endl;
    std::cout << std::endl;

    test_vector.erase(std::remove_if(test_vector.begin(), test_vector.end(), isPentagon), test_vector.end());
    std::copy(test_vector.begin(), test_vector.end(), std::ostream_iterator<Shape>(std::cout, " "));

    std::cout << std::endl;
    std::cout << "***************" << std::endl;
    std::cout << std::endl;

    std::sort(test_vector.begin(), test_vector.end(), sortCondition);
    std::copy(test_vector.begin(), test_vector.end(), std::ostream_iterator<Shape>(std::cout, " "));

    std::cout << std::endl;
    std::cout << "***************" << std::endl;
    std::cout << std::endl;

    std::vector<Point> test_point_vector = createVertexVector(test_vector);
    std::copy(test_point_vector.begin(), test_point_vector.end(), std::ostream_iterator<Point>(std::cout, " | "));

    return 0;
}
