#ifndef TASK_TWO_H
#define TASK_TWO_H

#include "shape.h"
#include <vector>

const int shapes_num = 10;

Shape generateShape();
bool isTriangle(const Shape & shape);
bool isSquare(const Shape & shape);
bool isRectangle(const Shape & shape);
bool isPentagon(const Shape & shape);
void countShapes(const std::vector<Shape> & shapes);
int sumVertexes(int sum, const Shape & shape);
int countVertexes(const std::vector<Shape> & shapes);
void erasePentagons(std::vector<Shape> & shapes);
Point getPoint(const Shape & shape);
std::vector<Point> createVertexVector(const std::vector<Shape> & shapes);
bool sortCondition(const Shape & lhs, const Shape & rhs);
void sortVector(std::vector<Shape> & shapes);

#endif //TASK_TWO_H
