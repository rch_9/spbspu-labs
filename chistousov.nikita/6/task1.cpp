#include <fstream>
#include <iostream>
#include <algorithm>
#include <set>
#include <string>
#include <iterator>

int main()
{
    std::ifstream in("text.txt");
    if (!in)
    {
        std::cerr << "Couldn't open input stream!" << std::endl;;
        return -1;
    }

    std::set<std::string> words;
    std::copy(std::istream_iterator<std::string>(in), std::istream_iterator<std::string>(), std::inserter(words, words.begin()));

    if (words.empty())
    {
        std::cout << "There are no words!" << std::endl;
    }
    else
    {
        std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(std::cout, " "));
    }
    return 0;
}
