#ifndef SHAPE_H
#define SHAPE_H

#include <vector>
#include <iostream>

struct Point
{
    int x, y;
};

class Shape
{
public:
    Shape();
    Shape(int gen_type);

    std::vector<Point> getVertexes() const;
    int getNum() const;
    
    void addPoint(int xx, int yy);

private:
    std::vector<Point> vertexes;
};

std::ostream & operator<<(std::ostream & os, const Point & point);
std::ostream & operator<<(std::ostream & os, const Shape & shape);

#endif
