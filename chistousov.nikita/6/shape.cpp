#include "shape.h"
#include <algorithm>
#include <cstdlib>

std::ostream & operator<<(std::ostream & os, const Point & point)
{
    return os << "x = " << point.x << " y = "<< point.y;
}

Shape::Shape() :
    vertexes(0)
{
}

Shape::Shape(int gen_type)
{
    int xx = 0, yy = 0, sidex = 0, sidey = 0;
    switch (gen_type)
    {
        case 0: // Triangle
            for (int i = 0; i < 3; ++i)
            {
                xx = rand();
                yy = rand();
                addPoint(xx, yy);
            }
            break;

        case 1: // Square
            xx = rand();
            yy = rand();
            sidex = rand();
            addPoint(xx, yy);
            addPoint(xx + sidex, yy);
            addPoint(xx, yy + sidex);
            addPoint(xx + sidex, yy + sidex);
            break;

        case 2: // Rectangle
            xx = rand();
            yy = rand();
            sidex = rand();
            sidey = rand();
            addPoint(xx, yy);
            addPoint(xx + sidex, yy);
            addPoint(xx, yy + sidey);
            addPoint(xx + sidex, yy + sidey);
            break;

        case 3: // Pentagon
            for (int i = 0; i < 5; ++i)
            {
                xx = rand();
                yy = rand();
                addPoint(xx, yy);
            }
            break;
    }
}

std::vector<Point> Shape::getVertexes() const
{
    return vertexes;
}

int Shape::getNum() const
{
    return vertexes.size();
}

void Shape::addPoint(int xx, int yy)
{
    Point point = {xx, yy};
    vertexes.push_back(point);
}

std::ostream & operator<<(std::ostream & os, const Shape & shape)
{
    switch (shape.getNum())
    {
        case 3:
            return os << "Triangle";
        case 4:
            if ((shape.getVertexes()[2].y - shape.getVertexes()[0].y) ==
                (shape.getVertexes()[1].x - shape.getVertexes()[0].x))
            {
                return os << "Square";
            }
            return os << "Rectangle";
        case 5:
            return os << "Pentagon";
    }
    return os;
}
