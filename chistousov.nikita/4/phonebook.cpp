#include "phonebook.h"
#include <iostream>
#include <iterator>

Phonebook::Record::Record(std::string _name, std::string _phone_number) :
    name(_name),
    phone_number(_phone_number)
{
}

const std::string & Phonebook::Record::getName() const
{
    return name;
}

const std::string & Phonebook::Record::getNumber() const
{
    return phone_number;
}

void Phonebook::pushBack(const Phonebook::Record & record)
{
    phonebook.push_back(record);
}

void Phonebook::pushBack(const std::string _name, const std::string _phone_number)
{
    phonebook.push_back(Record(_name, _phone_number));
}

void Phonebook::modifyRecord(Phonebook::iterator_t it, const Phonebook::Record & record)
{
    *(it) = record;
}

Phonebook::iterator_t Phonebook::insertBefore(Phonebook::iterator_t it, const Phonebook::Record & record)
{
    return phonebook.insert(it, record);
}

Phonebook::iterator_t Phonebook::insertAfter(Phonebook::iterator_t it, const Phonebook::Record & record)
{
    return phonebook.insert(++it, record);
}

Phonebook::iterator_t Phonebook::end()
{
    return phonebook.end();
}

Phonebook::iterator_t Phonebook::begin()
{
    return phonebook.begin();
}

std::ostream & operator<<(std::ostream & os, const Phonebook::Record & record)
{
    return os << "Name: " << record.getName() << " Number: " << record.getNumber() << "\n";
}
