#ifndef FACTORIAL_CONTAINER_H
#define FACTORIAL_CONTAINER_H

#include <iterator>

class FactorialContainer
{
public:
    class iterator : public std::iterator<std::bidirectional_iterator_tag, int>
    {
    public:
        iterator();
        iterator(int n);

        int operator*() const;
        iterator & operator++();
        iterator & operator--();
        iterator operator++(int);
        iterator operator--(int);
        bool operator==(const iterator & some) const;
        bool operator!=(const iterator & some) const;

        int factorial(unsigned int n);

    private:
        unsigned int current_n;
        int current_value;
    };

    FactorialContainer();
    
    iterator begin();
    iterator end();
};

#endif
