#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <vector>
#include <string>
#include <iostream>

class Phonebook
{
public:
    class Record
    {
    public:
        Record(std::string _name, std::string _phone_number);

        const std::string & getName() const;
        const std::string & getNumber() const;

    private:
        std::string name, phone_number;
    };

    typedef std::vector<Phonebook::Record> container_t;
    typedef container_t::iterator iterator_t;
    
    void pushBack(const Record & record);
    void pushBack(const std::string _name, const std::string _phone_number);
    void modifyRecord(iterator_t it, const Record & record);

    iterator_t insertBefore(iterator_t it, const Record & record);
    iterator_t insertAfter(iterator_t it, const Record & record);
    iterator_t begin();
    iterator_t end();

private:
    container_t phonebook;

};

std::ostream & operator<<(std::ostream & os, const Phonebook::Record & record);

#endif
