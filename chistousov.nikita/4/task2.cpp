#define BOOST_TEST_MODULE TasTwoTest

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include "factorial_container.h"
#include <algorithm>
#include <vector>

BOOST_AUTO_TEST_CASE(get_factorial_test)
{
    FactorialContainer test_container;

    for (FactorialContainer::iterator it = test_container.begin(); it != test_container.end(); ++it)
    {
        std::cout << *(it) << std::endl;
    }
    std::cout << "\n";
}

BOOST_AUTO_TEST_CASE(stl_copy_test)
{
    FactorialContainer test_container;
    std::vector<int> test_vector(10);

    std::copy(test_container.begin(), test_container.end(), test_vector.begin());

    for (std::vector<int>::iterator it = test_vector.begin(); it != test_vector.end(); ++it)
    {
        std::cout << *(it) << std::endl;
    }
    std::cout << "\n";
}
