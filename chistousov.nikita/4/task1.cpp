#define BOOST_TEST_MODULE TaskOneTest

#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>

#include "phonebook.h"
#include <algorithm>

BOOST_AUTO_TEST_CASE(pushBack_test)
{
    Phonebook test_phonebook;

    test_phonebook.pushBack("Nikita Chistousov", "123-456-789");
    test_phonebook.pushBack("Swald Johansson", "8-800-555-35-35");
    test_phonebook.pushBack("Thomas Mueller", "13-85-00");
    test_phonebook.pushBack("Barack Obama", "420-420");
    test_phonebook.pushBack("Elite Guy", "13-37-00");

    std::copy(test_phonebook.begin(), test_phonebook.end(), std::ostream_iterator<Phonebook::Record>(std::cout));
    std::cout << "\n";
}

BOOST_AUTO_TEST_CASE(modify_test)
{
    Phonebook test_phonebook;

    test_phonebook.pushBack("Nikita Chistousov", "123-456-789");
    test_phonebook.pushBack("Swald Johansson", "8-800-555-35-35");
    test_phonebook.pushBack("Thomas Mueller", "13-85-00");
    test_phonebook.pushBack("Barack Obama", "420-420");
    test_phonebook.pushBack("Elite Guy", "13-37-00");

    test_phonebook.modifyRecord(test_phonebook.begin(), Phonebook::Record("Mr. Freeman", "666-666"));
    test_phonebook.modifyRecord(test_phonebook.begin() + 2, Phonebook::Record("Robert Lewandowski", "222-333"));

    std::copy(test_phonebook.begin(), test_phonebook.end(), std::ostream_iterator<Phonebook::Record>(std::cout));
    std::cout << "\n";
}

BOOST_AUTO_TEST_CASE(insertion_test)
{
    Phonebook test_phonebook;

    test_phonebook.pushBack("Vladimir Putin", "000-000-000");
    test_phonebook.insertAfter(test_phonebook.begin(), Phonebook::Record("Angela Merkel", "222-222-222"));
    test_phonebook.insertBefore(test_phonebook.begin(), Phonebook::Record("Barack Obama", "111-111-111"));

    std::copy(test_phonebook.begin(), test_phonebook.end(), std::ostream_iterator<Phonebook::Record>(std::cout));
    std::cout << "\n";
}
