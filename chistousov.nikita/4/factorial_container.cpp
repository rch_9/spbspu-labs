#include "factorial_container.h"
#include <iostream>

FactorialContainer::iterator::iterator() :
    current_n(1),
    current_value(1)
{
}

FactorialContainer::iterator::iterator(int n) :
    current_n(n),
    current_value(factorial(n))
{
}

int FactorialContainer::iterator::operator*() const
{
    return current_value;
}

FactorialContainer::iterator & FactorialContainer::iterator::operator++()
{
    ++current_n;
    current_value *= current_n;
    return *(this);
}

FactorialContainer::iterator & FactorialContainer::iterator::operator--()
{
    current_value /= current_n;
    --current_n;
    return *(this);
}

FactorialContainer::iterator FactorialContainer::iterator::operator++(int)
{
    iterator temp(*(this));
    this->operator++();
    return temp;
}

FactorialContainer::iterator FactorialContainer::iterator::operator--(int)
{
    iterator temp(*(this));
    this->operator--();
    return temp;
}

bool FactorialContainer::iterator::operator==(const iterator & some) const
{
    return (current_n == some.current_n && current_value == some.current_value);
}

bool FactorialContainer::iterator::operator!=(const iterator & some) const
{
    return (current_n != some.current_n || current_value != some.current_value);
}

int FactorialContainer::iterator::factorial(unsigned int n)
{
    return n == 0 ? 1 : n * factorial(n - 1);
}

FactorialContainer::FactorialContainer()
{
}

FactorialContainer::iterator FactorialContainer::begin()
{
    return iterator();
}

FactorialContainer::iterator FactorialContainer::end()
{
    return iterator(11);
}
