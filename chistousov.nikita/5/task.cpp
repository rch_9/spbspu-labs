#include "task.h"
#include <iostream>
#include <cstdlib>

void fillVector(ds_vector & v)
{
    DataStruct ds;
    for (size_t i = 0; i < 10; ++i)
    {
        ds.key1 = lower_gen_bound + (rand() % static_cast<int>(upper_gen_bound - lower_gen_bound + 1));
        ds.key2 = lower_gen_bound + (rand() % static_cast<int>(upper_gen_bound - lower_gen_bound + 1));
        ds.str = table[rand() % table_size];
        v.push_back(ds);
    }
}

void printVector(const ds_vector & v)
{
    for (size_t i = 0; i < v.size(); ++i)
    {
        std::cout << "Key 1: " << v[i].key1 << " Key 2: " << v[i].key2 << " String : '" << v[i].str << "'" << std::endl;
    }
}

bool compareElements(const DataStruct & ds1, const DataStruct & ds2)
{
    return ((ds1.key1 < ds2.key1) ||
           ((ds1.key1 == ds2.key1) && (ds1.key2 < ds2.key2)) ||
           ((ds1.key1 == ds2.key1) && (ds1.key2 == ds2.key2) && (ds1.str.size() < ds2.str.size())));
}
