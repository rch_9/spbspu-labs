#include "task.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <algorithm>

int main()
{
    srand(time(0));

    ds_vector test_vector;

    fillVector(test_vector);
    printVector(test_vector);

    std::cout << "***************" << std::endl;

    std::sort(test_vector.begin(), test_vector.end(), compareElements);
    printVector(test_vector);

    return 0;
}
