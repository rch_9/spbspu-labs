#ifndef TASK_H
#define TASK_H

#include <string>
#include <vector>

struct DataStruct
{
    int         key1;
    int         key2;
    std::string str;
};

typedef std::vector<DataStruct> ds_vector;

const int lower_gen_bound = -5;
const int upper_gen_bound = 5;
const int table_size = 10;

const std::string table[] =
{
    "Education",
    "is",
    "the",
    "best",
    "provision",
    "for",
    "journey",
    "to",
    "old",
    "age"
};

void fillVector(ds_vector & v);
void printVector(const ds_vector & v);
bool compareElements(const DataStruct & ds1, const DataStruct & ds2);

#endif //TASK_H
