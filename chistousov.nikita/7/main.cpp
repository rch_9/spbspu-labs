#include "statistic.h"
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>

const int min = -500;
const int max = 500;
const int max_range = 100;

std::vector<int> generateVector()
{
    std::vector<int> storage(0);
    int i = 0, current = 0;

    int range = rand() % max_range;

    while (i != range)
    {
        current = min + (rand() % (max - min + 1));
        storage.push_back(current);
        ++i;
    }

    return storage;
}

int main()
{
    srand(time(0));

    std::vector<int> test_vector = generateVector();

    for (std::vector<int>::iterator it = test_vector.begin(); it != test_vector.end(); ++it)
    {
        std::cout << *(it) << " ";
    }
    std::cout << std::endl;

    Statistic stat_functor;

    stat_functor = std::for_each(test_vector.begin(), test_vector.end(), stat_functor);

    stat_functor.printData();

    return 0;
}
