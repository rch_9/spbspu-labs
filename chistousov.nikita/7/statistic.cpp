#include "statistic.h"
#include <iostream>

Statistic::Statistic() :
    minimum(0),
    maximum(0),
    average(0),
    pos_count(0),
    neg_count(0),
    odd_sum(0),
    even_sum(0),
    first_last_equal(false),
    count(0),
    first_value(0)
{
}

void Statistic::operator()(const int & x)
{
    if (count == 0)
    {
        first_value = x;
        maximum = first_value;
        minimum = first_value;
    }

    if (x > maximum)
    {
        maximum = x;
    }
    else if (x < minimum)
    {
        minimum = x;
    }

    average = (average * count + x) / (count + 1);
    ++count;

    if (x > 0)
    {
        ++pos_count;
    }
    else if (x < 0)
    {
        ++neg_count;
    }

    if (x % 2 == 0)
    {
        even_sum += x;
    }
    else
    {
        odd_sum += x;
    }

    if (x == first_value)
    {
        first_last_equal = true;
    }
    else
    {
        first_last_equal = false;
    }
}

void Statistic::printData()
{
    std::cout << "Minimum value is: " << minimum << std::endl;
    std::cout << "Maximum value is: " << maximum << std::endl;
    std::cout << "Average value is: " << average << std::endl;
    std::cout << "Positive numbers: " << pos_count << std::endl;
    std::cout << "Negative numbers: " << neg_count << std::endl;
    std::cout << "The sum of even numbers: " << even_sum << std::endl;
    std::cout << "The sum of odd numbers: " << odd_sum << std::endl;
    if (first_last_equal)
    {
        std::cout << "First and last value are equal" << std::endl;
    }
    else
    {
        std::cout << "First and last value ar not equal" << std::endl;
    }
}
