#ifndef STATISTIC_H
#define STATISTIC_H

class Statistic
{
public:
    Statistic();

    void operator()(const int & x);
    void printData();

private:
    int minimum, maximum;
    double average;
    int pos_count, neg_count;
    int odd_sum, even_sum;
    bool first_last_equal;
    int count;
    int first_value;
};

#endif // STATISTIC_H
