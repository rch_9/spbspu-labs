#include "statistics.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
#include <ostream>
#include <iterator>

#define size_vector 10

class generator
{
public:
	int operator()()
	{
		return rand() % 1001 - 500;
	}
};

int main(int, char *[])
{
	srand(static_cast<unsigned int>(time(0)));

	std::vector<int> v(size_vector);
	Statistics stats;

	std::generate(v.begin(), v.end(), generator());
	v.push_back(v.at(0));
	std::copy(v.begin(), v.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	stats = std::for_each(v.begin(), v.end(), stats);
	std::cout << stats << std::endl;


}