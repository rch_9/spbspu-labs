#ifndef STATISTICS_HPP
#define STATISTICS_HPP
#include <iostream>

class Statistics
{
public:
	Statistics():
		average(0.0),
		amountOfPositive(0),
		amountOfNegative(0),
		sumOfOdd(0),
		sumOfEven(0),
		feql(false),
		count(0)

	{}

	friend std::ostream & operator << (std::ostream &, Statistics &);

	void operator()(const int & );


private:
	int max;
	int min;
	double average;
	size_t amountOfPositive;
	size_t amountOfNegative;
	int sumOfOdd;
	int sumOfEven;
	bool feql; //First EQual Last

	size_t count; //for the average
	int first;
	//int last;
};

void Statistics::operator()(const int & current)
{
	if(count == 0)
	{
		first = current;
		max = current;
		min = current;
	}

	if(max <= current)
	{
		max = current;
	}

	if(current <= min)
	{
		min = current;
	}

	if(current % 2 == 0)
	{
		sumOfEven+= current;
	}
	else
	{
		sumOfOdd+= current;
	}

	if(current > 0)
	{
		++amountOfPositive;
	}
	else if(current < 0)
	{
		++amountOfNegative;
	}

	average = static_cast<double>((average * count + current))/ static_cast<double>((count+1));

	feql = (first == current);

	++count;
}

std::ostream & operator << (std::ostream & os, Statistics & data)
{
	os << "Statistics:"
	   << "\nMaximal item: " << data.max
	   << "\nMinimal item: " << data.min
	   << "\nAverage: " << data.average
	   << "\nAmount of positive items: " << data.amountOfPositive
	   << "\nAmount of negative items: " << data.amountOfNegative
	   << "\nSum of odd items: " << data.sumOfOdd
	   << "\nSumm of even items: " << data.sumOfEven
	   << (data.feql ? "\nFirst item is equal last" : "\nFirst is not equal last");
	return os;
}

#endif