#include <iostream>
#include <stdexcept>
#include <vector>
#include <string>
#include <fstream>

#define max_string_length 10
#define lengthOfString 40

void openFile(const char* fileName, std::ifstream &file)
{

	if(!fileName)
	{
		throw std::invalid_argument("File name is not correct!");
	}

	file.open(fileName, std::ifstream::in);

	if(!file)
	{
		throw std::runtime_error("Error! File can't open!");
	}
}

std::vector<std::string> splitStream(std::ifstream & file)
{
	if(!file)
	{
		throw std::invalid_argument("File has not received!");
	}

	std::vector<std::string> out;
	std::string temp = "";
	while(!file.eof() && file.good())
	{
		char c;
		file.get(c);
		if(isalnum(c) )
		{
			temp += c;
		}
		else if(ispunct(c))
		{
			if(!temp.empty())
			{
				out.push_back(temp);
			}
			temp=c;
			out.push_back(temp);
			temp.clear();
		}
		else if(isspace(c))
		{
			if(!temp.empty())
			{
				out.push_back(temp);
				temp.clear();
			}
		}
	}
	if(!temp.empty())
	{
		out.push_back(temp);
	}

	return out;
}


void vauReplace(std::vector<std::string> &v)
{
	for (std::vector<std::string>::iterator it = v.begin(); it != v.end(); ++it)
	{
		if (it->size() > max_string_length)
		{
			*it = "Vau!!!";
		}
	}
}

void formatText(std::vector<std::string> &v)
{
	std::string space = " ";
	std::string temp = "";
	std::vector<std::string> vtemp;
	for(std::vector<std::string>::iterator it =  v.begin(); it != v.end(); ++it)
	{
		if(temp.size() + it->size() + space.size() < lengthOfString)
		{
			if(!ispunct(it->operator[](0)) && !temp.empty())
			{
				temp.append(space);
			}
			temp.append(*it);
		}
		else
		{
			vtemp.push_back(temp);
			temp.clear();
			temp.append(*it);
		}
	}
	if(temp.size() < lengthOfString)
	{
		vtemp.push_back(temp);
	}
	v.clear();
	v = vtemp;
}


int main(int, char* [])
{

	std::ifstream file;
	const char* fileName="file.txt";

	try
	{
		openFile(fileName, file);
		std::vector<std::string> str = splitStream(file);
 		vauReplace(str);
		formatText(str);

		for(std::vector<std::string>::iterator it = str.begin(); it != str.end(); ++it)
		{
			std::cout << *it << std::endl;
		}

	}
	catch(std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
