#include "phoneBook.hpp"

PhoneBook::Record::Record(std::string name_, std::string number_) :
	name(name_),
	number(number_)
{}

std::string PhoneBook::Record::getName() const
{
	return name;
}

std::string PhoneBook::Record::getNumber() const
{
	return number;
}

void PhoneBook::Record::setName(const std::string &name_)
{
	name = name_;
}

void PhoneBook::Record::setNumber(const std::string &number_)
{
	number = number_;
}

void PhoneBook::goNext()
{
	if(!book.empty())
	{
		++current;
		if(current == book.end())
		{
			std::cerr << "Next record doesn't exist! Now the first record is current!" << std::endl;
			current = book.begin();
		}
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}

}

void PhoneBook::goPrev()
{
	if(!book.empty())
	{
		//--current;
		if(--current == book.end())
		{
			std::cerr << "Previous record doesn't exist! Now the first record is current!" << std::endl;
			current = book.begin();
		}
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}
}

void PhoneBook::insAfter(std::string name_, std::string number_)
{
	if(!book.empty())
	{
		book.insert(++current, Record(name_, number_));
		current = PhoneBook::begin();
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}
}

void PhoneBook::insAfter(value_type &record)
{
	if(!book.empty())
	{
		book.insert(++current, record);
		current = PhoneBook::begin();
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}
}

void PhoneBook::insBefore(std::string name_, std::string number_)
{
	if(!book.empty())
	{
		book.insert(current, Record(name_,number_));
		current = PhoneBook::begin();
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}
}

void PhoneBook::insBefore(value_type &record)
{
	if(!book.empty())
	{
		book.insert(current, record);
		current = PhoneBook::begin();
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}
}

void PhoneBook::insToEnd(std::string name_, std::string number_)
{
	book.push_back(Record(name_, number_));
	current = PhoneBook::begin();
}

void PhoneBook::insToEnd(value_type &record)
{
	book.push_back(record);
	current = PhoneBook::begin();
}

void PhoneBook::moveN(int n)
{
	iterator temp = current;
	for(size_t i =0; i < std::abs(n); ++i)
	{
		if(n > 0)
		{
			++current;
		}
		else
		{
			-- current;
		}
		if(current == book.end())
		{
			current = temp;
			throw std::runtime_error("Out of range!");
		}
	}

}

void PhoneBook::modifyCurrent(std::string newName, std::string newNumber)
{
	current->setName(newName);
	current->setNumber(newNumber);
}

PhoneBook::Container::iterator PhoneBook::begin()
{
	return book.begin();
}

PhoneBook::Container::iterator PhoneBook::end()
{
	return book.end();
}

std::ostream& operator<< (std::ostream& os, PhoneBook::Record rec)
{
	os << rec.getName() << " " << rec.getNumber();
	return os;
}

void PhoneBook::print()
{
	if(!book.empty())
	{
		for(iterator it = PhoneBook::begin(); it != PhoneBook::end(); ++it)
		{
			std::cout << *it << std::endl;
		}
	}
	else
	{
		std::cerr << "PhoneBook is empty!" << std::endl;
	}
}

PhoneBook::Record PhoneBook::showCurrent() const
{
	std::cout << *current << std::endl;
	return  *current;
}

PhoneBook::iterator PhoneBook::getIterator()
{
	return current;
}