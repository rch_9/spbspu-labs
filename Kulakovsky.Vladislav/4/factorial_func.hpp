#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <cstddef>

size_t factorial(size_t n);

#endif