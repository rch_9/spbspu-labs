#include "phoneBook.hpp"
#include "fact_container.hpp"
#include <iostream>

int main(int, char **)
{
	PhoneBook test;
	test.print();
	test.insToEnd("vlad", "79213412354");
	test.insToEnd("vasya", "79213469874");
	test.insToEnd("dasd", "5454");
	test.insAfter("111", "949495");
	test.goNext();
	//test.goPrev();
	PhoneBook::Record rec("igor", "5454");
	test.insAfter(rec);
	test.insBefore("denis", "949495");
	test.modifyCurrent("denis", "9848448");
	test.moveN(2);
	test.showCurrent();


	test.print();

	std::cout << std::endl;

	FactContainer ftest(10);

	for (FactContainer::iterator it = ftest.begin(); it != ftest.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
}