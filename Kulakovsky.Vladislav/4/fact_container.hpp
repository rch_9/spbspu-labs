#ifndef FACT_CONTAINER_HPP
#define FACT_CONTAINER_HPP

#include "factorial_func.hpp"
#include <iterator>
#include <iostream>

class FactContainer
{
public:
	class iterator:
		public std::iterator<std::bidirectional_iterator_tag, unsigned int>
	{
	public:

		iterator();
		iterator(size_t n, size_t value_);

		bool operator==(const iterator &rhs) const;
	    bool operator!=(const iterator &rhs) const;

	    size_t operator*() const;

	    iterator operator ++();
		iterator operator ++(int);
		iterator operator --();
		iterator operator --(int);
	private:
		size_t position;
		size_t value;
	};

	typedef size_t value_type;

	FactContainer();
	FactContainer(size_t n);

	iterator begin();
	iterator end();
private:
	size_t size;
};





#endif