#include "fact_container.hpp"

FactContainer::FactContainer() :
	size(0)
{}

FactContainer::FactContainer(size_t n) :
	size(n)
{}

FactContainer::iterator FactContainer::begin()
{
	return iterator(0,factorial(0));
}

FactContainer::iterator FactContainer::end()
{
	return iterator(size+1, factorial(size+1));
}


FactContainer::iterator::iterator() :
	position(0),
	value(1)
{}

FactContainer::iterator::iterator(size_t n, size_t value_) :
	position(n),
	value(value_)
{}

bool FactContainer::iterator::operator ==(const iterator &rhs) const
{
	return position == rhs.position;
}

bool FactContainer::iterator::operator !=(const iterator &rhs) const
{
	return position != rhs.position;
}

size_t FactContainer::iterator::operator*() const
{
	return value;
}

FactContainer::iterator FactContainer::iterator::operator ++()
{
	++position;
	if (position <= 1)
	{
		value = 1;
	}
	else
	{
		value = value * position;
	}
	return *this;
}

FactContainer::iterator FactContainer::iterator::operator ++(int)
{
	iterator temp(*this);
	++(*this);
	return temp;
}

FactContainer::iterator FactContainer::iterator::operator --()
{
	if (position > 0)
	{
		--position;
		if (position <= 1)
		{
			value = 1;
		}
		else
		{
			value = value / (position + 1);
		}
	}
	else
	{
		std::cerr << "Error! Iterator is on the previous position." << std::endl;
	}
	return *this;
}

FactContainer::iterator FactContainer::iterator::operator --(int)
{
	iterator temp(*this);
	--(*this);
	return temp;
}
