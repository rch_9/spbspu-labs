#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>
#include <stdexcept>
#include <cmath>
#include <iostream>

class PhoneBook
{
public:

	class Record
	{
	public:
		Record(std::string, std::string);
		std::string getName() const;
		std::string getNumber() const;
		void setName(const std::string &newName);
		void setNumber(const std::string &newNumber);
	private:
		std::string name;
		std::string number;
	};

	typedef PhoneBook::Record value_type;
	typedef std::list<value_type> Container;
	typedef Container::iterator iterator;

	iterator begin();
	iterator end();

	Record showCurrent() const;
	iterator getIterator();
	void goNext();
	void goPrev();
	void insAfter(std::string name_, std::string phoneNumber_);
	void insAfter(value_type &record);
	void insBefore(std::string name_, std::string phoneNumber_);
	void insBefore(value_type &record);
	void modifyCurrent(std::string name_, std::string phoneNumber_);
	void modifyCurrent(value_type &record);
	void insToEnd(std::string name_, std::string phoneNumber_);
	void insToEnd(value_type &record);
	void moveN(int n);
	void print();

	friend std::ostream& operator<< (std::ostream&, value_type);

private:
	Container book;
	iterator current;
};


#endif