#include "factorial_func.hpp"

size_t factorial(size_t n)
{
	return n > 1 ? n*factorial(n-1) : 1;
}