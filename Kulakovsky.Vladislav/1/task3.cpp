#include "policy.hpp"
#include <iostream>
#include <vector>
#include <stdexcept>

void modifyVector(std::vector<int> &collection)
{
	if(collection.empty())
	{
		throw std::runtime_error("collection is empty");
	}
	if(collection.back() == 1)
	{
		for(std::vector<int>::iterator it = collection.begin(); it != collection.end(); )
		{
			if(!(*it % 2))
			{
				it = collection.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
	else if(collection.back() == 2)
	{
		for (std::vector<int>::iterator it = collection.begin(); it != collection.end(); ++it) 
		{
			if(!(*it % 3))
			{
				for(int i =0; i < 3; ++i)
				{
					it=collection.insert(++it, 1);	//its corresponds iso 03
				}
			}
		}
	}
}

int main(int, char* [])
{
	std::vector<int> vec;
	FillVector(vec);
	printCollection<PolicyByIndex>(vec);
	modifyVector(vec);
	printCollection<PolicyByIndex>(vec);

	return 0;
}
