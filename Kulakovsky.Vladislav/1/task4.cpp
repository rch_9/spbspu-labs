#include "policy.hpp"
#include <iostream>
#include <ctime> //srand
#include <cstdlib> //rand

void fillRandom(double * array, size_t size)
{
	if(!array)
	{
		throw std::invalid_argument("Array is empty");
	}

	for(size_t i=0; i < size; ++i)
	{
		array[i] = ((static_cast<double>(rand())/ (RAND_MAX) * 2.0) - 1.0);
	}
}


void fillVectorOfRandomNumbers(size_t size)
{
	std::vector<double> v(size);
	fillRandom(&v[0], size);
	printCollection<PolicyByIndex>(v);
	selectionSort<PolicyByIndex>(v);
	printCollection<PolicyByIndex>(v);
}

int main(int, char* [])
{
	srand(time(0));

	int values[] = {5,10,25,50,100};

	try
	{
		for(size_t i=0; i<(sizeof(values)/sizeof(int)); ++i)
		{
			fillVectorOfRandomNumbers(values[i]);
		}
	}
	catch(std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
}