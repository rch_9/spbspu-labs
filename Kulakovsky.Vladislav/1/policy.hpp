#ifndef POLICY_HPP
#define POLICY_HPP

#include "my_forward_list.hpp"
#include <iostream>
#include <vector>
#include <stdexcept>


template <typename Container>
struct PolicyByIndex
{
	typedef typename Container::size_type pointer;
	typedef typename Container::value_type value;

	static pointer begin(Container &)
	{
		return 0;
	}

	static pointer end(Container &container)
	{
		return container.size();

	}

	static value & getValue(Container &container, pointer index)
	{
		if(index > end(container)-1)
		{
			throw std::runtime_error("wrong index");
		}
		return container[index];
	}
};

template <typename Container>
struct PolicyByAt
{
	typedef typename Container::size_type pointer;
	typedef typename Container::value_type value;

	static pointer begin(Container &)
	{
		return 0;
	}

	static pointer end(Container &container)
	{
		return container.size();
	}

	static value & getValue(Container &container, pointer index)
	{
		return container.at(index);

	}
};

template <typename Container>
struct  PolicyByIterator
{
	typedef typename Container::iterator pointer;
	typedef typename Container::value_type value;

	static pointer begin(Container &container)
	{
		return container.begin();
	}

	static pointer end(Container &container)
	{
		return container.end();
	}

	static value & getValue(Container &container, pointer it)
	{
		if(begin(container) == end(container))
		{
			throw std::runtime_error("Container is empty");
		}
		return *it;
	}
};

////////////////
template < template <typename > class Policy, typename T> void selectionSort(T &collection)
{
	typedef Policy<T> MyPolicy;
	typedef typename MyPolicy::pointer ptr;

	for(ptr i = MyPolicy::begin(collection); i != MyPolicy::end(collection); ++i)
	{
		ptr min = i;
		for(ptr j = i; j != MyPolicy::end(collection); ++j)
		{
			if (MyPolicy::getValue(collection, j) < MyPolicy::getValue(collection, min))
			{
				min = j;
			}
		}
		if(min != i)
		{
			std::swap(MyPolicy::getValue(collection, min), MyPolicy::getValue(collection, i));
		}

	}
}

template <typename T> void FillVector(std::vector<T> &name)
{
	T num = 1;
	while(num)
	{
		std::cin >> num;
		if(!std::cin.good())
		{
			throw std::runtime_error("Erore while reading! You should put only numbers!");
		}
		if(num)
		{
			name.push_back(num);
		}
	}
}


template < template <typename> class Policy, typename T> void printCollection(T &collection)
{
	typedef Policy <T> MyPolicy;
	typedef typename MyPolicy::pointer ptr;

	std::cout << "It's begin of collection\n";
	for(ptr i = MyPolicy::begin(collection); i != MyPolicy::end(collection); ++i)
	{
		std::cout << MyPolicy::getValue(collection, i) << " ";
	}
	std::cout << "\nIt's end of collection" << std::endl;
}
#endif //POLICY_HPP
