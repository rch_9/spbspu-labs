#include "policy.hpp"
#include <stdexcept> //std::exeption
#include <vector> // vector
#include <cstdio> // fopen, fread
#include <boost/shared_ptr.hpp> //shared_ptr
#include <boost/shared_array.hpp> //shared_array


class fileCloser
{
public:
    void operator()(FILE* file)
    {
        if(file != 0)
        {
            fclose(file);
        }
    }
};


void readToArrayFromFileAndConvertToVector(const char* fileName, std::vector<char> &v)
{
	if(fileName == NULL)
	{
		throw std::invalid_argument("File name is empty");
	}

 	boost::shared_ptr<FILE> file(fopen(fileName, "r"), fileCloser());

	if(!file.get())
	{
		throw std::invalid_argument("File has not opened!");
	}

	fseek (file.get(), 0, SEEK_END);
	size_t  size = ftell(file.get());
	rewind(file.get());

	if(size == 0)
	{
		throw std::runtime_error("File is empty");
	}

	boost::shared_array<char> array(new char[size]);

	while(!feof(file.get()))
	{
		fread(array.get(), sizeof(char), size ,file.get());
	}

		v.assign(array.get(), array.get() + size);
}



int main(int , char *[])
{
	const char* file1="file.txt";
	std::vector<char> v;

	try
	{
		readToArrayFromFileAndConvertToVector(file1, v);
		printCollection<PolicyByIndex>(v);
	}
	catch(std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
