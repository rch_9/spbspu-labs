#ifndef MY_FORWARD_LIST
#define MY_FORWARD_LIST

#include <iterator> //std::forward_iterator_tag
#include <cassert>	//assert

template <typename T>
class forward_list
{
public:

	struct node_t
	{
		T value;
		node_t * next;

		node_t(const T& value_);
	};

	class iterator :
		public std::iterator<std::forward_iterator_tag, T>
	{
	public:
		typedef node_t node_type;
		typedef iterator this_type;

		iterator();
		iterator(node_type * fwlist);

		bool operator ==(const this_type & rhs) const;
		bool operator !=(const this_type & rhs) const;

		T & operator *() const;
		T * operator ->() const;

		this_type & operator ++();
		this_type operator ++(int);

	private:
		node_type * current_;
	};

	typedef T value_type;
	typedef size_t size_type;

	forward_list();
	forward_list(const T& value_);
	~forward_list();

	void push_front(const T& value_);
	bool empty();
	iterator begin() const;
	iterator end() const;

private:
	node_t * head;
};


template <typename T>
inline forward_list<T>::node_t::node_t(const T& value_) :
	value(value_),
	next(0)
{}

template <typename T>
inline forward_list<T>::iterator::iterator(node_type * fwlist) :
	current_(fwlist)
{}

template <typename T>
inline bool forward_list<T>::iterator::operator==(const iterator & rhs) const
{
	return current_ == rhs.current_;
}

template <typename T>
inline bool forward_list<T>::iterator::operator !=(const iterator & rhs) const
{
	return current_ != rhs.current_;
}

template <typename T>
inline T & forward_list<T>::iterator::operator *() const
{
	assert(current_ != 0);

	return current_->value;
}

template <typename T>
inline T * forward_list<T>::iterator::operator ->() const
{
	assert(current_ != 0);

	return &current_->value;
}

template <typename T>
inline typename forward_list<T>::iterator & forward_list<T>::iterator::operator ++()
{
	assert(current_ != 0);

	current_ = current_->next;

	return *this;
}

template <typename T>
inline typename forward_list<T>::iterator forward_list<T>::iterator::operator ++(int)
{
	this_type temp(*this);

	this->operator ++();

	return temp;
}

template <typename T>
inline forward_list<T>::forward_list() :
	head(0)
{}

template <typename T>
inline forward_list<T>::forward_list(const T& value_):
	head(0)
{
	node_t * tmp = new node_t(value_);
	tmp->next = head;
	head = tmp;
}

template <typename T>
inline void forward_list<T>::push_front(const T& value_)
{
	node_t * tmp = new node_t(value_);
    tmp->next = head;
    head = tmp;
}

template <typename T>
inline bool forward_list<T>::empty()
{
	return !head;
}


template <typename T>
inline forward_list<T>::~forward_list()
{
	node_t * tmp;
	while (head != 0)
	{
		tmp = head;
		head = head->next;
		delete tmp;
	}
}

template <typename T>
inline typename forward_list<T>::iterator forward_list<T>::forward_list<T>::begin() const
{
	return head;
}

template <typename T>
inline typename forward_list<T>::iterator forward_list<T>::forward_list<T>::end() const
{
	return 0;
}



#endif //MY_FORWARD_LIST
