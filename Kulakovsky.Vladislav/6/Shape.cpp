#include "Shape.hpp"

Shape::Point::Point(int x_, int y_):
	x(x_),
	y(y_)
{}

Shape::Shape(const std::vector<Point> & vertexes_):
	vertexes(vertexes_)
{}

void Shape::addPoint(const Point & point)
{
	vertexes.push_back(point);
}

void Shape::addPoint(int x_, int y_)
{

	vertexes.push_back(Point(x_, y_));
}

bool Shape::isSquare() const
{
	int x1 = vertexes[0].x;
	int x2 = vertexes[1].x;
	int x3 = vertexes[2].x;
	int x4 = vertexes[3].x;
	int y1 = vertexes[0].y;
	int y2 = vertexes[1].y;
	int y3 = vertexes[2].y;
	int y4 = vertexes[3].y;
	double dist1 = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
	double dist2 = sqrt(pow(x3 - x1, 2) + pow(y3 - y1, 2));
	double dist3 = sqrt(pow(x4 - x1, 2) + pow(y4 - y1, 2));
	return (dist1 == dist2 || dist1 == dist3 || dist2 == dist3);
};