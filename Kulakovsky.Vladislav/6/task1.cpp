#include <iostream>
#include <set>
#include <iterator>
#include <algorithm>
#include <string>
#include <fstream>
#include <stdexcept>


int main(int, char* [])
{
	std::ifstream file("file.txt");
	if(!file)
	{
		std::cerr << "File has not been opened!" << std::endl;
		return 1;
	}
	std::set<std::string> mySet;

	std::copy((std::istream_iterator<std::string>(file)), (std::istream_iterator<std::string>()), std::inserter(mySet, mySet.begin()));

	if (file.bad())
		{
			std::cerr << "Error while reading!" << std::endl;
			return 1;
		}

	std::copy(mySet.begin(), mySet.end(), std::ostream_iterator<std::string>(std::cout, "\n"));

	return 0;
}