#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <vector>
#include <ostream>
#include <cmath>


struct Shape
{

	struct Point
	{
		int x,y;
		Point(int x_, int y_);
	};

	std::vector<Point> vertexes;

	Shape();
	Shape(const std::vector<Point> & vertexes_);

	void addPoint(const Point & point);
	void addPoint(int x_, int y_);
    bool isSquare() const;

};


#endif