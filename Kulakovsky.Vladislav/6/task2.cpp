#include "Shape.hpp"
#include "functors.hpp"
#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm>

int main(int, char * [])
{
	srand(static_cast<unsigned int>(time(0)));

	std::vector<Shape> test;
	test.reserve(10);

	std::generate_n(std::back_inserter(test), 10, GenerateShape());
	std::for_each(test.begin(), test.end(), Print());
	std::cout << "\n" << std::endl;

	std::cout << std::endl;
	SumOfAllVertexes testSum = std::for_each(test.begin(), test.end(), SumOfAllVertexes());
	std::cout << testSum.getSum() << std::endl;
	std::cout << "\n" << std::endl;

	CountTSR tsr = std::for_each(test.begin(), test.end(), CountTSR());
	std::cout << "amount of triangles: " << tsr.triangValue() << "\namount of squares: "
			  << tsr.squareValue() << "\namount of rectangles: " << tsr.rectValue() << std::endl;
	std::cout << "\n" << std::endl;

	test.erase(std::remove_if(test.begin(), test.end(), IsPentagon()), test.end());
	 std::for_each(test.begin(), test.end(), Print());
    std::cout << "\n" << std::endl;

	std::vector<Shape::Point> v(test.size(), Shape::Point(0,0));
	std::transform(test.begin(), test.end(), v.begin(), InsertPoint());
	std::for_each(v.begin(), v.end(), PrintPoints());
	std::cout << "\n" << std::endl;

	std::sort(test.begin(), test.end(), CompareShapes());
	std::for_each(test.begin(), test.end(), Print());
    std::cout << "\n" << std::endl;

}
