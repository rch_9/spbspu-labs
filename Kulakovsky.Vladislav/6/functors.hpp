#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include "Shape.hpp"
#include <iostream>
#include <cstdlib>

class Print
{
public:
    void operator()(const Shape & shape)
    {
        std::cout << "< " << shape.vertexes.size() << " ";

        switch (shape.vertexes.size())
        {
	        case 3:
	        {
	        	std::cout << "Triang";
	            break;
	        }
	        case 4:
	        {
	        	if(shape.isSquare())
	        	{
	        		std::cout << "Square";
	        	}
	        	else
	        	{
	        		std::cout << "Rect";
	        	}
	            break;
	        }
	        case 5:
	        {
	        	std::cout << "Penta";
	            break;
	        }
	        default: break;
        }

        std::cout << " > ";
    }
};

class PrintPoints
{
public:
	void operator()(const Shape::Point & point) const
	{
		std::cout << "( " <<point.x << " , " << point.y << ") ";
	}
};

class GenerateShape
{
public:
	Shape operator()()
	{

		std::vector<Shape::Point> points;

		size_t numVertexes = rand() % 4;

		switch(numVertexes)
		{
			case 0:
			{
				for(size_t i = 0; i < 3; ++i)
				{
					points.push_back(Shape::Point(rand() % 100, rand() % 100));
				}
				break;
			}
			case 1:
			{
				int lengthSide = rand() % 100;
				int x = rand() % 100;
				int y = rand() % 100;
				points.push_back(Shape::Point(x,y));
				points.push_back(Shape::Point(x+lengthSide,y));
				points.push_back(Shape::Point(x+lengthSide,y+lengthSide));
				points.push_back(Shape::Point(x, y+lengthSide));
				break;
			}
			case 2:
			{
				int x0 = rand() % 100;
				int y0 = rand() % 100;
				int x1 = rand() % 100;
				int y1 = rand() % 100;
				points.push_back(Shape::Point(x0,y0));
				points.push_back(Shape::Point(x1,y0));
				points.push_back(Shape::Point(x1,y1));
				points.push_back(Shape::Point(x0,y1));
				break;
			}
			case 3:
			{
				for(size_t i = 0; i < 5; ++i)
				{
					points.push_back(Shape::Point(rand() % 100, rand() % 100));
				}
				break;
			}
		}
		return Shape(points);
	}
};

class CompareShapes
{
public:
	bool operator()(const Shape & lhs, const Shape & rhs) const
	{
		return lhs.vertexes.size() < rhs.vertexes.size();
	}
};

class SumOfAllVertexes
{
public:
	SumOfAllVertexes():
		sum(0)
	{}

	void operator()(const Shape & shape)
	{
		sum += shape.vertexes.size();
	}

	size_t getSum() const
	{
		return sum;
	}
private:
	size_t sum;
};

class IsPentagon
{
public:
	bool operator()(Shape & shape) const
	{
		return shape.vertexes.size() == 5;
	}
};

class InsertPoint
{
public:
	Shape::Point operator()(Shape & shape) const
	{
		return shape.vertexes[0];
	}

};

class CountTSR //count Trinangle, Square, Rectangle
{
public:
    CountTSR():
    	triang(0),
    	square(0),
    	rect(0)
    {}

    void operator()(const Shape & shape)
    {
        if(shape.vertexes.size() == 3)
        {
            ++triang;
        }

        if(shape.vertexes.size() == 4 && shape.isSquare())
        {
        	++square;
        }
        else if(shape.vertexes.size() == 4)
        {
        	++rect;
        }
    }

    size_t triangValue() const
    {
        return triang;
    }

    size_t squareValue() const
    {
    	return square;
    }

    size_t rectValue()	const
    {
    	return rect;
    }

private:
    size_t triang;
    size_t square;
    size_t rect;
};

#endif