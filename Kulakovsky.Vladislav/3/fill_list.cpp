#include "fill_list.hpp"
#include <iostream>
#include <ctime>

int main(int, char* [])
{
	srand(static_cast<unsigned int> (time(0)));

	std::list<int> test;

	int values[] = {0,1,2,3,4,5,7,14};

	for(size_t i = 0; i<(sizeof(values)/sizeof(int)) ; ++i)
	{
		fillListRandomNumbers(test, values[i]);
		printStraight(test);
		printListFromEdgesToCenter(test);
	}


	return 0;
}