#include "queue.hpp"



int main(int ,char* [])
{

	typedef QueueWithPriority<int> queue;

	queue test;

	test.Put(10,queue::HIGH);
	test.Put(40, queue::NORMAL);
	test.Put(15, queue::LOW);
	test.Put(20, queue::NORMAL);
	test.Put(100, queue::HIGH);
	test.Put(1, queue::LOW);

	test.Print();
	std::cout << std::endl;

	test.Accelerate();

	test.Print();

	test.Get();

	test.Print();


return 0;
}