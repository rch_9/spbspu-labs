#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <iostream>
#include <memory>

template <typename T>
class QueueWithPriority
{
public:
	enum Priority
	{
		LOW,
		NORMAL,
		HIGH
	};

	class Item
	{
	public:

		Item(const T& item);
		T getItem() const;

	private:
		T item_;

	};

	inline void Put(const T& item, Priority prty);
	inline void Accelerate();
	inline std::auto_ptr<T> Get();
	//inline T Get();
	void Print();



private:
	typedef typename std::list<QueueWithPriority::Item> listWithPRTY;

	listWithPRTY HIGH_QUEUE_PRTY;
	listWithPRTY NORMAL_QUEUE_PRTY;
	listWithPRTY LOW_QUEUE_PRTY;
};

template <typename T>
QueueWithPriority<T>::Item::Item(const T& item):
	item_(item)
{}

template <typename T>
T QueueWithPriority<T>::Item::getItem() const
{
	return this->item_;
}

template <typename T>
inline void QueueWithPriority<T>::Put(const T& item, QueueWithPriority::Priority prty)
{
	switch (prty)
	{
		case QueueWithPriority::HIGH :
		{
			HIGH_QUEUE_PRTY.push_back(item);
			break;
		}
		case QueueWithPriority::NORMAL :
		{
			NORMAL_QUEUE_PRTY.push_back(item);
			break;
		}
		case QueueWithPriority::LOW :
		{
			LOW_QUEUE_PRTY.push_back(item);
			break;
		}
	}
}

template <typename T>
inline void QueueWithPriority<T>::Accelerate()
{
	if(!LOW_QUEUE_PRTY.empty())
	{
		HIGH_QUEUE_PRTY.splice(HIGH_QUEUE_PRTY.end(), LOW_QUEUE_PRTY);
	}
}

template <typename T>
inline std::auto_ptr<T> QueueWithPriority<T>::Get()
{

	if(!HIGH_QUEUE_PRTY.empty())
	{
		std::auto_ptr<T> temp(new T(HIGH_QUEUE_PRTY.front().getItem()));
		HIGH_QUEUE_PRTY.pop_front();
		return temp;
	}
	else if(!NORMAL_QUEUE_PRTY.empty())
	{
		std::auto_ptr<T> temp(new T (NORMAL_QUEUE_PRTY.front().getItem()));
		NORMAL_QUEUE_PRTY.pop_front();
		return temp;
	}
	else if(!LOW_QUEUE_PRTY.empty())
	{
		std::auto_ptr<T> temp(new T (LOW_QUEUE_PRTY.front().getItem()));
		LOW_QUEUE_PRTY.pop_front();
		return temp;
	}
	else
	{
		 std::cerr << "Queue is empty" << std::endl;
		 return static_cast< std::auto_ptr<T> >(0);
	}

}

template <typename T>
void QueueWithPriority<T>::Print()
{
	if(!HIGH_QUEUE_PRTY.empty())
	{
		std::cout << "HIGH PRIORITY: ";

		for(typename QueueWithPriority::listWithPRTY::iterator it = HIGH_QUEUE_PRTY.begin(); it!=HIGH_QUEUE_PRTY.end(); ++it)
		{
			std::cout << it->getItem() << " ";
		}

		std::cout << std::endl;
	}
	else
	{
		std::cout << "HIGH PRIORITY: EMPTY" << std::endl;
	}

	if(!NORMAL_QUEUE_PRTY.empty())
	{
		std::cout << "NORMAL PRIORITY: " ;

		for(typename QueueWithPriority::listWithPRTY::iterator it = NORMAL_QUEUE_PRTY.begin(); it!=NORMAL_QUEUE_PRTY.end(); ++it)
		{
			std::cout << it->getItem() << " ";
		}

		std::cout << std::endl;
	}
	else
	{
		std::cout << "NORMAL PRIORITY: EMPTY" << std::endl;
	}

	if(!LOW_QUEUE_PRTY.empty())
	{
		std::cout << "LOW PRIORITY: ";

		for(typename QueueWithPriority::listWithPRTY::iterator it = LOW_QUEUE_PRTY.begin(); it!=LOW_QUEUE_PRTY.end(); ++it)
		{
			std::cout << it->getItem() <<  " ";
		}

		std::cout << std::endl;
	}
	else
	{
		std::cout << "LOW PRIORITY: EMPTY" << std::endl;
	}
}

#endif //QUEUE_HPP