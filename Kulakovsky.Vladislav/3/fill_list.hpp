#ifndef FILL_LIST_HPP
#define FILL_LIST_HPP

#include <iostream>
#include <list>
#include <cstdlib>

#define max_size 15
#define max_rand 20

void fillListRandomNumbers(std::list<int> &myList, size_t size)
{

	if(size == 0)
	{
		std::cout << "Size of list is zero" << std::endl;
		return;
	}

	if(size > max_size)
	{
		std::cerr << "Size is not correnct" << std::endl;
		return;
	}
	if(!myList.empty())
	{
		std::cerr << "List is not empty and it will be cleared!" << std::endl;
		myList.clear();
	}
	for(size_t i=0; i<size; ++i)
	{

		myList.push_back(1 + rand() % max_rand);
	}
}

template <typename T>
void printStraight(std::list<T> &myList)
{
	if(!myList.empty())
	{
		for(typename std::list<T>::iterator i = myList.begin(); i != myList.end(); ++i)
		{
			std::cout << *i << " ";
		}
		std::cout << std::endl;
	}
}

template <typename T>
void printListFromEdgesToCenter(std::list<T> &myList)
{
	if(!myList.empty())
	{
		for (typename std::list<T>::iterator i = myList.begin(), j = myList.end(); i != j; )
		{
			std::cout << *i << " ";
			++i;
			if(i==j)
			{
				break;
			}
			else
			{
				--j;
				if(i==j)
				{
					std::cout << *j << " ";
					break;
				}
				else
				{
					std::cout << *j << " ";
				}
			}
		}
		std::cout << std::endl;
	}
}

#endif //FILL_LIST_HPP