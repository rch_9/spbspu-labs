#include "Shape.hpp"

Shape::Shape(int x_, int y_):
	x(x_),
	y(y_)
{}

Shape::~Shape()
{}

bool Shape::isMoreLeft(const Shape & shape) const
{
	return x < shape.x;
}

bool Shape::isUpper(const Shape & shape) const
{
	return y > shape.y;
}