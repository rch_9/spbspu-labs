#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include "Shape.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "Square.hpp"

#include <boost/shared_ptr.hpp>

typedef boost::shared_ptr< Shape > shape_shared_ptr;

class DrawShape
{
public:
	void operator()(const shape_shared_ptr & shape) const
	{
		shape->draw();
	}
};

class SortLeftToRight
{
public:
	typedef bool result_type;

	bool operator()(const shape_shared_ptr & shape1, const shape_shared_ptr & shape2)
	{
		return shape1->isMoreLeft(*shape2);
	}
};

class SortRightToLeft
{
public:
	typedef bool result_type;
	bool operator()(const shape_shared_ptr & shape1, const shape_shared_ptr & shape2)
	{
		return shape2->isMoreLeft(*shape1);
	}
};

class SortUpToDown
{
public:
	typedef bool result_type;
	bool operator()(const shape_shared_ptr & shape1, const shape_shared_ptr & shape2)
	{
		return shape1->isUpper(*shape2);
	}
};

class SortDownToUp
{
public:
	typedef bool result_type;
	bool operator()(const shape_shared_ptr & shape1, const shape_shared_ptr & shape2)
	{
		return shape2->isUpper(*shape1);
	}
};

class FillRand
{
public:
	shape_shared_ptr operator()()
	{
		int shape_type = rand() % 3;
		switch (shape_type)
		{
			case 0:
			{
				return shape_shared_ptr(new Circle(rand() % 10, rand() % 10));
				break;
			}
			case 1:
			{
				return shape_shared_ptr(new Triangle(rand() % 10, rand() % 10));
				break;
			}
			case 2:
			{
				return shape_shared_ptr(new Square(rand() % 10, rand() % 10));
				break;
			}
			default: assert(!"Something go wrong! But we know about this.");
		}
	}
};

#endif