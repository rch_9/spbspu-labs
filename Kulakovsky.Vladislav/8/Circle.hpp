#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "Shape.hpp"
#include <iostream>

class Circle:
	public Shape
{
public:
	Circle(int, int);

	void draw() const;
};

#endif