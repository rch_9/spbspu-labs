#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "Shape.hpp"
#include <iostream>

class Triangle:
	public Shape
{
public:
	Triangle(int, int);

	void draw() const;
};

#endif