#include "Square.hpp"

Square::Square(int x_, int y_):
	Shape(x_, y_)
{}

void Square::draw() const
{
	std::cout << "Square: < " << x << " , " << y << " >" << std::endl;
}