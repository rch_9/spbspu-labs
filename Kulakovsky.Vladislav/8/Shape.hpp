#ifndef SHAPE_HPP
#define SHAPE_HPP

class Shape
{
public:
	Shape(int, int);
	virtual ~Shape();

	bool isMoreLeft(const Shape&) const;
	bool isUpper(const Shape&) const;

	virtual void draw() const = 0;

protected:
	int x, y;
};

#endif