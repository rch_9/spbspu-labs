#include "Shape.hpp"
#include "Circle.hpp"
#include "Triangle.hpp"
#include "Square.hpp"
#include "Functors.hpp"
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

typedef boost::shared_ptr<Shape> shape_shared_ptr;
typedef std::vector<shape_shared_ptr> shared_vector;

void fill_random(std::vector<shape_shared_ptr> & v, size_t size);

int main(int, char *[])
{
	srand(static_cast<unsigned int>(time(0)));

	shared_vector v;

	v.reserve(10);

	std::generate_n(std::back_inserter(v), 10, FillRand());

	std::cout << "Initial data:" << std::endl;
	std::for_each(v.begin(), v.end(), DrawShape());
	std::cout << "\n " << std::endl;

	std::cout << "Left to right:" << std::endl;
	std::sort(v.begin(), v.end(), SortLeftToRight());
	std::for_each(v.begin(), v.end(), DrawShape());
	std::cout  << "\n "<< std::endl;

	std::cout << "Right to left:" << std::endl;
	std::sort(v.begin(), v.end(), SortRightToLeft());
	std::for_each(v.begin(), v.end(), DrawShape());
	std::cout << "\n " << std::endl;

	std::cout << "Up to down:" << std::endl;
	std::sort(v.begin(), v.end(), SortUpToDown());
	std::for_each(v.begin(), v.end(), DrawShape());
	std::cout << "\n " << std::endl;

	std::cout << "Down to up:" << std::endl;
	std::sort(v.begin(), v.end(), SortDownToUp());
	std::for_each(v.begin(), v.end(), DrawShape());
	std::cout << "\n " << std::endl;

	return 0;
}
