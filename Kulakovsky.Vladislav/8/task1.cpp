#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <boost/bind.hpp>
#include <iterator>

class randomizer
{
public:
    double operator()()
    {
        return (rand() % 100) * 1.0;
    }
};

int main(int, char *[])
{
	srand(static_cast<double>(time(0)));
	std::vector<double> v(5);

	std::generate(v.begin(), v.end(), randomizer());

	std::transform(v.begin(), v.end(), v.begin(),
			boost::bind(std::multiplies< double >(), _1, M_PI));

	std::copy(v.begin(), v.end(), std::ostream_iterator<double>(std::cout, " || "));
	std::cout << std::endl;

	return 0;
}

