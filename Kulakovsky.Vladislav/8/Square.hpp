#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "Shape.hpp"
#include <iostream>

class Square:
	public Shape
{
public:

	Square(int, int);

	void draw() const;
};

#endif