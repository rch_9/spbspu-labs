#include "DataStruct.hpp"
#include "Comparator.hpp"
#include "StructGenerator.hpp"
#include <iostream>
#include <ctime>
#include <algorithm>
#include <string>
#include <vector>
#include <iterator>



int main(int, char* [])
{
	typedef std::vector<DataStruct>::iterator iter;

	srand(static_cast<unsigned int>(time(0)));

	const size_t size = static_cast<size_t>(sizeof(list)/sizeof(std::string));

	std::vector<DataStruct> v;
	v.reserve(size);

	std::generate_n(std::back_inserter(v), size, StructGenerator());

	std::copy(v.begin(), v.end(), std::ostream_iterator<DataStruct>(std::cout));
	std::sort(v.begin(), v.end(), Comparator());
	std::cout << "------------------------------------------------------------" << std::endl;
	std::copy(v.begin(), v.end(), std::ostream_iterator<DataStruct>(std::cout));

	return 0;


}