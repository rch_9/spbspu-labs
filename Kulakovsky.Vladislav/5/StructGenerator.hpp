#ifndef STRUCTGENERATOR_HPP
#define STRUCTGENERATOR_HPP

#include "DataStruct.hpp"

std::string list[] = {
	"first",
	"second",
	"third",
	"fourth",
	"fifth",
	"sixth",
	"seventh",
	"eighth",
	"ninth",
	"tenth"
};

class StructGenerator:
	public std::unary_function<DataStruct, DataStruct>
{
public:
	DataStruct operator ()()
	{
		return DataStruct(
			rand() % 11 - 5,
			rand() % 11 - 5,
			list[rand() % 10]
		);
	}
};

#endif