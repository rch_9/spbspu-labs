#include "DataStruct.hpp"

DataStruct::DataStruct(int key1_, int key2_, std::string str_):
	key1(key1_),
	key2(key2_),
	str(str_)
{}

std::ostream & operator<<(std::ostream &os, const DataStruct &data)
{
	os << "key1: " <<  std::setw(2) << data.key1 << " key2: " << std::setw(2) << data.key2 << " str: " << data.str << std::endl;
	return os;
}