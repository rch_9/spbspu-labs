#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <iostream>
#include <iomanip>
#include <cstdlib>

struct DataStruct
{
	DataStruct(int key1_, int key2_, std::string str_);
	int key1;
	int key2;
	std::string str;
};

std::ostream & operator<<(std::ostream &os, const DataStruct &data);

#endif