#ifndef COMPARATOR_HPP
#define COMPARATOR_HPP

#include "DataStruct.hpp"

class Comparator:
	public std::binary_function <DataStruct, DataStruct, bool>
{
public:
	bool operator()(const DataStruct &lhs, const DataStruct &rhs) const
	{
		return ((lhs.key1 < rhs.key1) ||
				((lhs.key1 == rhs.key1) && (lhs.key2 < rhs.key2)) ||
				((lhs.key1 == rhs.key1) && (lhs.key2 == rhs.key2) && (lhs.str.length() < rhs.str.length())));

	}
};

#endif