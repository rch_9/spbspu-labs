#ifndef SORT_H
#define SORT_H

#include <vector>

#include "datastruct.h"

namespace lab_5
{

    bool compare_data(const DataStruct & a, const DataStruct & b);

    void sort_vector(std::vector<DataStruct> & data);

}

#endif //SORT_H
