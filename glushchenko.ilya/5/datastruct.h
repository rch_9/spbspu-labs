#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include <string>

namespace lab_5
{

    struct DataStruct
    {

        int key1;
        int key2;
        std::string str;

        DataStruct(const int & key1_, const int & key2_, const std::string & str_) :
            key1(key1_),
            key2(key2_),
            str(str_)
        {
        }

    };


}

#endif // DATASTRUCT_H
