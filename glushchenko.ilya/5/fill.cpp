#include "fill.h"

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <algorithm>
#include <math.h>

int lab_5::rand_in_range(const int & min, const int & max)
{
    if (min > max + 1)
    {
        throw std::invalid_argument("random_in_range min > max");
    }

    if (max + 1 + min > RAND_MAX)
    {
        throw std::invalid_argument("random_in_range range is to big");
    }

    return rand() % (max + 1 + std::abs(min)) - std::abs(min);
}

lab_5::DataStruct lab_5::make_rand(const std::vector<std::string> & table)
{
    if (table.empty())
    {
        throw std::invalid_argument("fill_random_stuct is empty!");
    }

    std::string str = table[rand_in_range(0, table.size() - 1)];
    return DataStruct(rand_in_range(-5, 5), rand_in_range(-5, 5), str);
}


struct lab_5::fill_rand
{
    const std::vector<std::string> & table;

    fill_rand(const std::vector<std::string> & _table)
        : table(_table)
    {
    }

    DataStruct operator()()
    {
        if (table.empty())
        {
            throw std::invalid_argument("fill_rand() table is empty");
        }

        return make_rand(table);
    }
};


void lab_5::fill_vector(std::vector<DataStruct> & data, const std::vector<std::string> & table, const std::size_t & size)
{
    if (size == 0)
    {
        throw std::invalid_argument("fill_vector size == 0");
    }

    if (table.empty())
    {
        throw std::invalid_argument("fill_vector table is empty");
    }

    data.resize(size, DataStruct(0, 0, ""));
    std::generate(data.begin(), data.end(), fill_rand(table));
}
