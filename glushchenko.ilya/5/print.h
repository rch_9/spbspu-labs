#ifndef PRINT_H
#define PRINT_H

#include <vector>

#include "datastruct.h"

namespace lab_5
{

    void print_struct(const DataStruct & data);

    void print_vector(std::vector<DataStruct> & data);

}

#endif // PRINT_H
