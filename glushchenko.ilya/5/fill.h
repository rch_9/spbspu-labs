#ifndef FILL_H
#define FILL_H

#include <vector>
#include <string>

#include "datastruct.h"

namespace lab_5
{

    int rand_in_range(const int & min, const int & max);

    DataStruct make_rand(const std::vector<std::string> & table);

    struct fill_rand;

    void fill_vector(
        std::vector<DataStruct> & data,
        const std::vector<std::string> & table,
        const std::size_t & size
    );
}

#endif //FILL_H

