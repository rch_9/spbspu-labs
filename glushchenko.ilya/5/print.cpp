#include "print.h"

#include <iostream>
#include <algorithm>
#include <stdexcept>

#include <boost/io/ios_state.hpp>

void lab_5::print_struct(const DataStruct & data)
{
    std::cout << data.key1 << " " << data.key2 <<  " " << data.str << std::endl;
}

void lab_5::print_vector(std::vector<DataStruct> & data)
{
    if (data.empty())
    {
        throw std::invalid_argument("print_vector data is empty!");
    }

    boost::io::ios_all_saver ias(std::cout);

    std::cout.setf(std::ios::showpos);
    std::for_each(data.begin(), data.end(), print_struct);
}


