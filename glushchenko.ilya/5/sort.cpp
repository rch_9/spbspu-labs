#include "sort.h"

#include <stdexcept>
#include <algorithm>

bool lab_5::compare_data(const DataStruct & a, const DataStruct & b)
{
    if (a.key1 != b.key1)
    {
        return a.key1 < b.key1;
    }

    if (a.key2 != b.key2)
    {
        return a.key2 < b.key2;
    }

    return a.str.size() < b.str.size();
}

void lab_5::sort_vector(std::vector<DataStruct> & data)
{
    if (data.empty())
    {
        throw std::invalid_argument("print_vector data is empty!");
    }

    std::sort(data.begin(), data.end(), compare_data);
}

