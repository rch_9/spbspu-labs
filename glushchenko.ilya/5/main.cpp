#include <iostream>

#include <vector>
#include <string>

#include "datastruct.h"
#include "fill.h"
#include "sort.h"
#include "print.h"

int main()
{
    std::vector<lab_5::DataStruct> data;
    std::vector<std::string> table(10, "");

    lab_5::fill_vector(data, table, 10);
    lab_5::print_vector(data);
    std::cout << "---------------------------\n";
    lab_5::sort_vector(data);
    lab_5::print_vector(data);

    return 0;
}
