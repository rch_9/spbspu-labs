#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <limits>
#include <algorithm>

void fill_random(double* array, std::size_t size)
{
    if (array == NULL)
    {
        throw std::runtime_error("Null array");
    }

    for (size_t i = 0; i < size; i++)
    {
        array[i] = (double(rand()) / RAND_MAX) * 2.0f - 1.0f;
    }
}

void makeTestFile(
        const char* filename,
        const size_t length,
        const size_t packet_size,
        const bool verbose = false)
{
    if (filename == NULL)
    {
        throw std::invalid_argument("makeTestFile filename is nullptr");
    }

    std::ofstream out(filename, std::ios::out | std::ios::binary);

    if(!out)
    {
        if (verbose)
        {
            std::cerr << "Cannot open file: " << filename;
        }
    }
    else
    {
        for (size_t i = 0; i < length; i++)
        {
            std::vector<double> test_data(packet_size, 0);
            fill_random(&test_data[0], test_data.size());

            for (size_t j = 0; j < test_data.size(); j++)
            {
                test_data[j] += (j + 1) * 100;

                if (verbose)
                {
                    std::cout << test_data[j] << " ";
                    if (j+1 == test_data.size())
                    {
                        std::cout << std::endl;
                    }
                }
            }

            out.write(
                reinterpret_cast<char*>(test_data.data()),
                sizeof(double)*test_data.size()
            );
        }
    }
}

void readFile(
        const char* filename,
        std::vector<double>& average_val,
        std::vector<double>& error,
        const bool verbose = false
    )
{
    size_t data_length = 1;
    std::vector<double> max_val(average_val.size(), std::numeric_limits<double>::min());
    std::vector<double> min_val(average_val.size(), std::numeric_limits<double>::max());

    std::ifstream in(filename, std::ios::in | std::ios::binary);
    if (in.is_open())
    {
        if (verbose)
        {
            std::cout << std::endl;
        }

        while(in)
        {
            std::vector<double> data(average_val.size(), 0);
            in.read(reinterpret_cast<char*>(data.data()), sizeof(double)*data.size());

            //Initialize data if it's a first reading
            if (in.tellg() == sizeof(double)*data.size()) {
                average_val = data;
                if (verbose)
                {
                    for (size_t i = 0; i < data.size(); i++)
                    {
                        std::cout << data[i] << " ";
                    }
                }
            }
            else {
                //Check if read correct amount of data
                if (in.gcount() == sizeof(double)*data.size())
                {
                    data_length++;

                    for (size_t i = 0; i < data.size(); i++)
                    {
                        //Calculate average
                        average_val[i] += data[i];

                        if (data[i] > max_val[i])
                        {
                            max_val[i] = data[i];
                        }
                        else if (min_val[i] > data[i])
                        {
                            min_val[i] = data[i];
                        }

                        if (verbose)
                        {
                            std::cout << data[i] << " ";
                        }
                    }
                }
            }

            if (verbose)
            {
                std::cout << std::endl;
            }
        }

        //Calculate error and average values
        for (size_t i = 0; i < error.size(); i++)
        {
            error[i] = (max_val[i] - min_val[i]) / 2.0f;
            average_val[i] /= data_length;
        }

        if (verbose)
        {
            std::cout << "Average values: \n";
            for (size_t i = 0; i < average_val.size(); i++)
            {
                std::cout << average_val[i] << " ";
            }
            std::cout << std::endl;

            std::cout << "Errors: \n";
            for (size_t i = 0; i < error.size(); i++)
            {
                std::cout << error[i] << " ";
            }
            std::cout << std::endl;
        }
    }
    else
    {
        if (verbose)
        {
            std::cerr << "Cannot open file: " << filename;
        }
    }
}

int main()
{
    size_t length = 10;
    size_t packet_size = 5;
    std::cout << "Packet size: ";
    std::cin >> packet_size;
    makeTestFile("data.bin", length, packet_size, true);

    std::vector<double> average_val(packet_size, 0);
    std::vector<double> error(packet_size, 0);
    readFile("data.bin", average_val, error, true);

    return 0;
}
