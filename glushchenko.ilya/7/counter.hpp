#ifndef COUNTER_H
#define COUNTER_H

template < typename T >
class Counter
{
public:
    Counter() :
        max_element(0),
        min_element(0),
        total_size(0),
        positive_size(0),
        negative_size(0),
        total_sum(0),
        odd_sum(0),
        even_sum(0),
        first_element(0),
        last_element(0)
    {
    }

    Counter operator()(T & element)
    {
        if (total_size == 0)
        {
            init(element);
        }

        inc_sizes(element);

        inc_sums(element);

        inc_range(element);

        last_element = element;
    }

    T get_max()
    {
        return max_element;
    }

    T get_min()
    {
        return min_element;
    }

    T get_average()
    {
        return total_size > 0 ? total_sum / total_size : 0;
    }

    std::size_t get_positive_size()
    {
        return positive_size;
    }

    std::size_t get_negative_size()
    {
        return negative_size;
    }

    T get_odd_sum()
    {
        return odd_sum;
    }

    T get_even_sum()
    {
        return even_sum;
    }

    bool last_first_equal()
    {
        return total_size > 1 ? first_element == last_element : false;
    }

private:
    T max_element;
    T min_element;

    std::size_t total_size;
    std::size_t positive_size;
    std::size_t negative_size;

    T total_sum;
    T odd_sum;
    T even_sum;

    T first_element;
    T last_element;

    void init(const T & element)
    {
        max_element = element;
        min_element = element;
        first_element = element;
    }

    void inc_sizes(const T & element)
    {
        ++total_size;

        if (element >= 0)
        {
            ++positive_size;
        }
        else
        {
            ++negative_size;
        }
    }

    void inc_sums(const T & element)
    {
        total_sum += element;

        if (element % 2 == 0)
        {
            even_sum += element;
        }
        else
        {
            odd_sum += element;
        }
    }

    void inc_range(const T & element)
    {
        if (element > max_element)
        {
            max_element = element;
        }
        if (element < min_element)
        {
            min_element = element;
        }
    }
};

#endif //COUNTER_H
