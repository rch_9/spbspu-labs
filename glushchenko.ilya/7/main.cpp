#include <vector>
#include <cstdlib>
#include <algorithm>

#include "counter.hpp"

int make_random()
{
    return (rand() % 1000) - 500;
}

std::vector<int> make_random_vector(const std::size_t & size)
{
    std::vector<int> data(size, 0);
    std::generate(data.begin(), data.end(), make_random);
    return data;
}

int main()
{
    std::vector<int> rand_data = make_random_vector(10);
    Counter<int> counter = std::for_each(rand_data.begin(), rand_data.end(), Counter<int>());

    return 0;
}
