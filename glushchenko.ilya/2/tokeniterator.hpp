#ifndef TOKENITERATOR_H
#define TOKENITERATOR_H

#include <iterator>
#include <stdexcept>
#include <boost/shared_ptr.hpp>

#include "token.h"

template < typename StreamT >
class TokenIterator
{

public:
    typedef boost::shared_ptr<StreamT> istream_ptr;

    TokenIterator()
    {
    }

    TokenIterator(istream_ptr & stream_)
    {
        if (!stream_)
        {
            throw std::runtime_error("stream is a nullptr!");
        }

        stream = stream_;

        operator ++();
    }

    TokenIterator(const TokenIterator & iterator)
    {
        stream = iterator.stream;
        current_token = iterator.current_token;
    }

    friend bool operator!=(const TokenIterator & a, const TokenIterator & b)
    {        
        if (!a.stream && !b.stream)
        {
            return false;
        }

        if (a.stream && b.stream )
        {
            return !(a.stream->eof() == b.stream->eof());
        }

        if (a.stream)
        {
            return !a.stream->eof();
        }
        else
        {
            return !b.stream->eof();
        }
    }

    TokenIterator & operator++()
    {
        if (!stream)
        {
            throw std::runtime_error("Bad stream!");
        }

        if (stream->eof())
        {
            throw std::runtime_error("Stream eof!");
        }

        if (next_valid_char())
        {
            current_token = make_token();
        }
        else
        {
            while (!stream->eof())
            {
                std::string tmp;
                *stream >> tmp;
            }
        }
    }

    Token operator* () const
    {
        if (!stream || stream->eof())
        {
            throw std::runtime_error("Cannot get value!");
        }

        return current_token;
    }

private:
    istream_ptr stream;

    Token current_token;

    bool next_valid_char()
    {
        if (!stream)
        {
            throw std::runtime_error("stream is a nullptr!");
        }

        std::istreambuf_iterator<char> buf_iter(*stream.get());

        while (buf_iter != std::istreambuf_iterator<char>()
               && !std::isalnum(*buf_iter)
               && !ispunct(*buf_iter))
        {
            ++buf_iter;
        }

        return buf_iter != std::istreambuf_iterator<char>();
    }

    Token make_token()
    {
        if (!stream)
        {
            throw std::runtime_error("stream is a nullptr!");
        }

        std::istreambuf_iterator<char> buf_iter(*stream.get());

        Token::Type type = std::isalnum(*buf_iter) ? Token::WORD : Token::PUNCT;
        std::string value;

        if (type == Token::WORD)
        {
            while (buf_iter != std::istreambuf_iterator<char>() && std::isalnum(*buf_iter))
            {
                value += *buf_iter;
                ++buf_iter;
            }
        }
        else if (type == Token::PUNCT)
        {
            value = *buf_iter;
            ++buf_iter;
        }
        else
        {
            throw std::runtime_error("Unknown token type!");
        }

        return Token(type, value);
    }

    bool ispunct( const char & symbol)
    {
        std::string allowed_punct(".,!?:;");
        return allowed_punct.find(symbol) != std::string::npos;
    }

};

#endif //TOKENITERATOR_H
