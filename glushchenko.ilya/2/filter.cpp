#include "filter.h"

#include <stdexcept>
#include <fstream>
#include <string>

#include "tokeniterator.hpp"
#include "token.h"

#define MAX_LINE_LENGTH 40
#define MAX_WORD_SIZE 10
#define SPACE_SIZE 1

template < typename StreamT >
void write_current_token(
        Token & current_token,
        std::size_t & current_line_size,
        StreamT & out)
{
    if (current_token.type == Token::WORD)
    {
        //Remove long words
        if (current_token.value.size() > MAX_WORD_SIZE)
        {
            current_token.value = "Vau!!!";
        }

        //Go to a new line if neccessary
        const std::size_t new_line_size = SPACE_SIZE + current_token.value.size();
        if (current_line_size > 0 && current_line_size + new_line_size > MAX_LINE_LENGTH)
        {
            out << "\n";
            current_line_size = 0;
        }

        //Insert space
        if (current_line_size > 0)
        {
            out << " ";
            ++current_line_size;
        }
    }

    out << current_token.value;
    current_line_size +=current_token.value.size();
}


void filter::make_filtred_copy(const char * src, const char * dst)
{
    if (src != 0 && dst != 0)
    {
        boost::shared_ptr<std::ifstream> istream_ptr(new std::ifstream(src));
        std::ofstream out(dst);

        if (istream_ptr->is_open() && out.is_open())
        {
            TokenIterator<std::ifstream> it(istream_ptr);
            Token current_token = *it;
            Token prev_token = current_token;
            std::size_t current_line_size = 0;
            write_current_token(current_token, current_line_size, out);
            ++it;

            while (it != TokenIterator<std::ifstream>())
            {
                current_token = *it;

                if (!(prev_token.type == Token::PUNCT && current_token.type == Token::PUNCT))
                {
                    write_current_token(current_token, current_line_size, out);
                    prev_token = current_token;
                }

                ++it;
            }
        }
    }
    else
    {
        throw std::invalid_argument("Filename is a nullptr!");
    }
}

