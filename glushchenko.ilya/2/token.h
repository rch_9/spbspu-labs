#ifndef TOKEN_H
#define TOKEN_H

#include <string>

struct Token
{
    enum Type { WORD, PUNCT };
    Type type;
    std::string value;

    Token() :
        type(WORD)
    {
    }

    Token(const Type & type_, const std::string & value_) :
        type(type_),
        value(value_)
    {
    }
};

#endif //TOKEN_H
