#include "shape.h"

Shape::Shape(const PointXY & center_) :
    center(center_)
{
}

Shape & Shape::operator=(const Shape & other)
{
    if (this != &other)
    {
        center = other.center;
    }

    return *this;
}

PointXY Shape::getCenter() const
{
    return center;
}

bool Shape::isMoreLeft(const Shape & shape) const
{
    return center.x < shape.center.x;
}

bool Shape::isUpper(const Shape & shape) const
{
    return center.y > shape.center.y;
}
