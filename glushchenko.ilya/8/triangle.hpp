#ifndef TRIANGLE
#define TRIANGLE

#include <iostream>

#include "shape.h"

class Triangle : public Shape
{
public:
    Triangle(const PointXY & center) : Shape(center)
    {
    }

    Triangle(const Triangle & other) : Shape(other)
    {
    }

    void Draw() const
    {
        std::cout << "Triangle! Center:" << center.x << ";"  << center.y << "\n";
    }
};

#endif //TRIANGLE
