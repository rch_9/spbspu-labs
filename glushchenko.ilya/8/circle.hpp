#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>

#include "shape.h"

class Circle : public Shape
{
public:
    Circle(const PointXY & center) : Shape(center)
    {
    }

    Circle(const Circle & other) : Shape(other)
    {
    }

    void Draw() const
    {
        std::cout << "Circle! Center:" << center.x << ";"  << center.y << "\n";
    }
};

#endif //CIRCLE_H
