#include <iostream>
#include <vector>
#include <boost/shared_ptr.hpp>

#include "shape.h"
#include "shapesutility.hpp"
#include "triangle.hpp"
#include "square.hpp"
#include "circle.hpp"


int main()
{
    std::vector< boost::shared_ptr< Shape > > total(9);
    std::generate(total.begin(), total.begin() + 3, make_rand_shape_ptr<Circle>);
    std::generate(total.begin() + 3, total.begin() + 6, make_rand_shape_ptr<Triangle>);
    std::generate(total.begin() + 6, total.end(), make_rand_shape_ptr<Square>);

    std::cout << "Src:\n";
    std::for_each(total.begin(), total.end(), draw_shape());

    std::cout << "Lt:\n";
    std::sort(total.begin(), total.end(), compare_shapes_lt());
    std::for_each(total.begin(), total.end(), draw_shape());

    std::cout << "Rt:\n";
    std::sort(total.begin(), total.end(), compare_shapes_rt());
    std::for_each(total.begin(), total.end(), draw_shape());

    std::cout << "Lower then:\n";
    std::sort(total.begin(), total.end(), compare_shapes_lower_then());
    std::for_each(total.begin(), total.end(), draw_shape());

    std::cout << "Higher then:\n";
    std::sort(total.begin(), total.end(), compare_shapes_higet_then());
    std::for_each(total.begin(), total.end(), draw_shape());

    return 0;
}
