#ifndef SHAPESUTILITY_H
#define SHAPESUTILITY_H

#include <cstdlib>
#include <algorithm>
#include <boost/shared_ptr.hpp>

#include "shape.h"
#include "pointxy.hpp"

#define RAND_X_RANGE_MIN -50
#define RAND_X_RANGE_MAX  50
#define RAND_Y_RANGE_MIN -50
#define RAND_Y_RANGE_MAX  50

PointXY make_random_point()
{
    float x = rand() % RAND_X_RANGE_MAX + RAND_X_RANGE_MIN;
    float y = rand() % RAND_Y_RANGE_MAX + RAND_Y_RANGE_MIN;

    return PointXY(x, y);
}

template < typename ShapeT >
boost::shared_ptr<ShapeT> make_rand_shape_ptr()
{
    return boost::shared_ptr<ShapeT>( new ShapeT(make_random_point()) );
}

struct draw_shape
{
    void operator ()(boost::shared_ptr<Shape> & shape_ptr)
    {
        shape_ptr->Draw();
    }
};

struct compare_shapes_lt
{
    bool operator ()(const boost::shared_ptr<Shape> & a, const boost::shared_ptr<Shape> & b)
    {
        return a->isMoreLeft(*b.get());
    }
};

struct compare_shapes_rt
{
    bool operator ()(const boost::shared_ptr<Shape> & a, const boost::shared_ptr<Shape> & b)
    {
        return !a->isMoreLeft(*b.get());
    }
};

struct compare_shapes_higet_then
{
    bool operator ()(const boost::shared_ptr<Shape> & a, const boost::shared_ptr<Shape> & b)
    {
        return a->isUpper(*b.get());
    }
};

struct compare_shapes_lower_then
{
    bool operator ()(const boost::shared_ptr<Shape> & a, const boost::shared_ptr<Shape> & b)
    {
        return !a->isUpper(*b.get());
    }
};

#endif //SHAPESUTILITY_H
