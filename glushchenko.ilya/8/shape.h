#ifndef SHAPES_H
#define SHAPES_H

#include "pointxy.hpp"

class Shape
{
public:
    Shape(const PointXY & center_);

    Shape & operator = (const Shape & other);

    PointXY getCenter() const;

    bool isMoreLeft(const Shape & shape) const;

    bool isUpper(const Shape & shape) const;

    virtual void Draw() const {}

protected:
    PointXY center;

    virtual ~Shape() {}

};


#endif //SHAPES_H
