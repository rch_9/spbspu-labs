#ifndef SQUARE_H
#define SQUARE_H

#include <iostream>

#include "shape.h"

class Square : public Shape
{
public:
    Square(const PointXY & center) : Shape(center)
    {
    }

    Square(const Square & other) : Shape(other)
    {
    }

    void Draw() const
    {
        std::cout << "Square! Center:" << center.x << ";"  << center.y << "\n";
    }
};

#endif //SQUARE_H
