#ifndef POINTXY_H
#define POINTXY_H

struct PointXY
{
    float x;
    float y;

    PointXY() : x(0), y(0)
    {
    }

    PointXY(const float & x_, const float & y_) :
        x(x_), y(y_)
    {
    }

    PointXY(const PointXY & point) :
        x(point.x), y(point.y)
    {
    }
};

#endif //POINTXY_H
