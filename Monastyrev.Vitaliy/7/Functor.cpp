#include "Functor.h"
#include <iostream>

Functor::Functor() : average(0),
	                 positivenumber(0),
                     negativenumber(0),
                     evensum(0),
                     oddsum(0),
                     firstlastcomp(false),
	                 firstnumflag(false),
	                 numcount(0),
	                 sum(0)
{};

void Functor::operator()(const int & value)
{
	++numcount;
	sum += value;
	if (firstnumflag == false)
	{
		firstnum = value;
		max = value;
		min = value;
		firstlastcomp = true;
		firstnumflag = true;
	}
	if (value > max)
	{
		max = value;
	}
	if (value < min)
	{
		min = value;
	}
	if (value > 0)
	{
		++positivenumber;
	}
	else
	{
		++negativenumber;
	}
	average = sum / numcount;
	if (value % 2 == 0)
	{
		evensum += value;
	}
	else
	{
		oddsum += value;
	}
	if (firstnum == value)
	{
		firstlastcomp = true;
	}
	else
	{
		firstlastcomp = false;
	}
};

void Functor::ShowStat()
{
	if (firstnumflag == 0)
	{
		std::cerr << "Empty";
		return;
	}
	else
	{
		std::cout << "max " << max << "\n"
			<< "min " << min << "\n"
			<< "average " << average << "\n"
			<< "number of positive numbers " << positivenumber << "\n"
			<< "number of negative numbers " << negativenumber << "\n"
			<< "sum of even numbers " << evensum << "\n"
			<< "sum of odd numbers " << oddsum << "\n"
			<< "comparing the first and last number " << firstlastcomp << "\n";
	}
};

int Functor::getmax()
{
	return max;
};

int Functor::getmin()
{
	return min;
};

double Functor::getaverage()
{
	return average;
};

int Functor::getpositivenumber()
{
	return positivenumber;
};

int Functor::getnegativenumber()
{
	return negativenumber;
};

int Functor::getevensum()
{
	return evensum;
};

int Functor::getoddsum()
{
	return oddsum;
};

bool Functor::getfirstlastcomp()
{
	return firstlastcomp;
};