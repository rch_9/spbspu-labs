#ifndef FUNCTOR
#define FUNCTOR

class Functor
{
private:
	int max;
	int min;
	double average;
	int positivenumber;
	int negativenumber;
	int evensum;
	int oddsum;
	bool firstlastcomp;
	bool firstnumflag;
	int firstnum;
	int numcount;
	int sum;
public:
	Functor();
	void operator() (const int & value);
	void ShowStat();
	int getmax();
	int getmin();
	double getaverage();
	int getpositivenumber();
	int getnegativenumber();
	int getevensum();
	int getoddsum();
	bool getfirstlastcomp();
};

#endif