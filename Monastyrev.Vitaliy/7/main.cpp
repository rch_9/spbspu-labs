#define BOOST_TEST_MODULE TESTS

#include "Functor.h"
#include <boost/test/included/unit_test.hpp>
#include <vector>
#include <algorithm>

BOOST_AUTO_TEST_CASE(PutandGet)
{
	bool flag = false;
	Functor functor;
	std::vector<int> vect;
	vect.push_back(21);
	vect.push_back(72);
	vect.push_back(46);
	vect.push_back(-2);
	vect.push_back(-5);
	vect.push_back(22);
	vect.push_back(12);
	vect.push_back(21);
	functor = std::for_each(vect.begin(), vect.end(), functor);
	functor.ShowStat();
	if ((functor.getmax() != 72) || (functor.getmin() != -5) ||
		(functor.getaverage() != 23) || (functor.getpositivenumber() != 6) ||
		(functor.getnegativenumber() != 2) || (functor.getevensum() != 150) ||
		(functor.getoddsum() != 37) || (functor.getfirstlastcomp() != true))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Functor doesn't work");
}