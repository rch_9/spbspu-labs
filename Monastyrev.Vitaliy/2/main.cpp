#include "FuncForStrAndVec.h"
#include "Parser.h"
#include <iostream>

int main(int /*argc*/, char* /**argv*/[])
{
	char filename[] = "testtext2.txt";
	size_t max = 10;
	size_t stringsize = 40;
	std::string vauword = "Vau!!!";
	Parser pars(max, stringsize, vauword);
	pars.PrettyReading(filename);
	std::vector<std::string> v = pars.GetVec();
	ShowVector(v);

	return 0;
}