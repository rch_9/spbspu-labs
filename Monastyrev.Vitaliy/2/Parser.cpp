#include "Parser.h"
#include <iostream>
#include <cctype> 
#include <fstream> 
#include <stdexcept>

Parser::token_t::token_t(const typet & type_, const std::string & name_) : name(name_), type(type_)
{}

void Parser::PutList(const typet & type_, const std::string & name_)
{
	name_.size() > max ? ls.push_back(token_t(type_, vauword)) : ls.push_back(token_t(type_, name_));
}

Parser::Parser(const size_t & max_, const size_t & stringsize_, const std::string & vauword_)

{
	if ((stringsize_ <= max_) || (max_ < vauword_.size()))
	{
		throw std::invalid_argument("Invalid argument");
	}
	else
	{
		max = max_;
		stringsize = stringsize_ + 1;
		vauword = vauword_;
	}
}

void Parser::Separate()
{
	std::string str;
	size_t maxsizeflag = 0;
	for (myit it = ls.begin(); it != ls.end(); ++it)
	{
		if (str.size() + it->name.size() < stringsize)
		{
			str.insert(str.size(), it->name);
		}
		else
		{
			vec.push_back(str);
			str.clear();
			str.insert(str.size(), it->name);
		}
		if (it->type == punct)
		{
			if (str.size() + 1 < stringsize)
			{
				str.insert(str.size(), " ");
			}
		}
		if (it->type == word)
		{
			it++;
			if (it != ls.end())
			{
				if ((it->type != punct) && (str.size() + 1 < stringsize))
				{
					str.insert(str.size(), " ");
				}
			}
			it--;
		}
	}
	if (!str.empty())
	{
		vec.push_back(str);
	}
}

void Parser::PrettyReading(const char * file)
{
	if (!file)
	{
		std::cerr << "ERROR: can not find file" << "\n";
		return;
	}
	std::ifstream stream(file, std::ios::in);
	if (!stream)
	{
		std::cerr << "ERROR: file don't open" << "\n";
		return;
	}
	char ch;
	std::string arraychar;
	std::string symb;
	while (!stream.eof())
	{
		stream.get(ch);
		if (!stream.good() && !stream.eof())
		{
			std::cerr << "ERROR: problem with file reading";
			vec.clear();
			ls.clear();
			return;
		}
		if (isspace(ch))
		{
			if (!arraychar.empty())
			{
				PutList(word, arraychar);
				arraychar.clear();
			}
		}
		if (ispunct(ch) && !stream.eof())
		{
			if (!arraychar.empty())
			{
				PutList(word, arraychar);
				arraychar.clear();
			}
			symb.push_back(ch);
			PutList(punct, symb);
			symb.clear();
		}
		if (!ispunct(ch) && !isspace(ch) && !stream.eof())
		{
			arraychar.push_back(ch);
		}
	}
	if (!arraychar.empty())
	{
		PutList(word, arraychar);
		arraychar.clear();
	}
	Separate();
}

const std::vector<std::string> Parser::GetVec()
{
	return vec;
}
