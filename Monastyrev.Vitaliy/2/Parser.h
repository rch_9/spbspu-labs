#ifndef PARSER
#define PARSER

#include <list>
#include <vector>
#include <string>

class Parser
{
private:
	size_t max;
	size_t stringsize;
	std::string vauword;
	std::vector<std::string> vec;
	enum typet
	{
		word,
		punct
	};
	class token_t
	{
	public:
		std::string name;
		typet type;
		token_t(const typet & type_, const std::string & name_);
	};
	std::list<token_t> ls;
	typedef std::list<token_t>::iterator myit;
	void PutList(const typet & type_, const std::string & name_);
	void Separate();
public:
	Parser(const size_t & max_, const size_t & stringsize_, const std::string & vauch_);
	void PrettyReading(const char * file);
	const std::vector<std::string> GetVec();
};

#endif