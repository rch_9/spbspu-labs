#ifndef DATA
#define DATA

#include <string>
#include <iostream>

struct DataStruct
{
	int key1;
	int key2;
	std::string str;
	DataStruct(int key1_, int key2_, const std::string & str_);
	friend std::ostream & operator << (std::ostream & sentry, const DataStruct & dat);
};

#endif