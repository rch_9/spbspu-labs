#ifndef FUNCFORLAB5
#define FUNCFORLAB5

#include "DataStruct.h"
#include <vector>
#include <string>

int Interval(int x, int y);
bool DataCompare(const DataStruct &el1, const DataStruct &el2);

#endif