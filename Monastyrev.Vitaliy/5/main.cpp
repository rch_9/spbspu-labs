#include "DataStruct.h"
#include "FuncForLab5.h"
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <iterator>

int main(int /*argc*/, char* /*argv*/[])
{
	srand(static_cast<unsigned>(time(0)));
	std::string str[] = 
	{ 
		"str1", 
		"str_2",
		"str__3", 
		"str4", 
		"str_5",
		"str__6", 
		"str7", 
		"str_8", 
		"str__9", 
		"str10" 
	};
	std::vector<DataStruct> datavect;
	for (int i = 0; i != 10; ++i)
	{
		datavect.push_back(DataStruct(
			Interval(-5, 6), 
			Interval(-5, 6), 
			str[rand() % 10]));
	}
	std::cout << "BEFORE: \n";
	std::copy(datavect.begin(), datavect.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));
	std::sort(datavect.begin(), datavect.end(), DataCompare);
	std::cout << "AFTER: \n";
	std::copy(datavect.begin(), datavect.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));

	return 0;
}