#include "DataStruct.h"
#include "FuncForLab5.h"

DataStruct::DataStruct(int key1_, int key2_, const std::string & str_) : key1(key1_), key2(key2_), str(str_)
{}

std::ostream & operator << (std::ostream & sentry, const DataStruct & dat)
{
	sentry << dat.key1 << ' ' << dat.key2 << ' ' << dat.str;
	return sentry;
}
