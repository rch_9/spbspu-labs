#include "FuncForLab5.h"
#include <vector>
#include <iostream>
#include <cstdlib>

int Interval(int x, int y)
{
	return ((y - x)*(rand()) / RAND_MAX) + x;
}

bool DataCompare(const DataStruct &el1, const DataStruct &el2)
{
	return (el1.key1 < el2.key1) || (el1.key1 == el2.key1 && el1.key2 < el2.key2) ||
		(el1.key1 == el2.key1 && el1.key2 == el2.key2 && el1.str.size() < el2.str.size());
}
