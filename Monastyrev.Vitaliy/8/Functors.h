#ifndef FUNCTOR
#define FUNCTOR

#include "Shape.h"
#include <boost/shared_ptr.hpp>

class Random 
{
public:
	double operator()();
};

class GenerateShape
{
public:
	boost::shared_ptr<Shape> operator()();
};

class Compare : public std::binary_function<boost::shared_ptr<Shape>, boost::shared_ptr<Shape>, bool>
{
public:
	bool operator()(const boost::shared_ptr<Shape> & el1, const boost::shared_ptr<Shape> & el2, bool type);
};

class Print
{
public:
	void operator()(const boost::shared_ptr<Shape> & el);
};

#endif