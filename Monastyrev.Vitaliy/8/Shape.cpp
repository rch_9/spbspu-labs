#include "Shape.h"
#include <iostream>

Shape::Shape(int x_, int y_) :x(x_), y(y_)
{};

bool Shape::IsMoreLeft(const Shape & el) const
{
	return x > el.x;
}

bool Shape::IsMoreUpper(const Shape & el) const
{
	 return y > el.y;
}

void Shape::Draw() const
{};

void Circle::Draw() const
{
	std::cout << "Circle " << "x " << x << " y " << y << "\n";
};

void Triangle::Draw() const
{
	std::cout << "Triangle " << "x " << x << " y " << y << "\n";
};

void Square::Draw() const
{
	std::cout << "Square " << "x " << x << " y " << y << "\n";
};

