#include "Functors.h"
#include <stdexcept>

bool Compare:: operator()(const boost::shared_ptr<Shape> & el1, const boost::shared_ptr<Shape> & el2, bool type)
{
	return type ? el1->IsMoreLeft(*el2) : el1->IsMoreUpper(*el2);
}

void Print:: operator()(const boost::shared_ptr<Shape> & el)
{
	el->Draw();
}

double Random::operator()()
{
	return double(rand() % 10000) / 100;
}

boost::shared_ptr<Shape> GenerateShape::operator()()
{
		int number = rand() % 3;
		switch (number)
		{
		case 0:
		{
			Circle el(rand() % 100, rand() % 100);
			boost::shared_ptr<Shape> elptr(new Circle(el));
			return elptr;
		}
		case 1:
		{
			Triangle el(rand() % 100, rand() % 100);
			boost::shared_ptr<Shape> elptr(new Triangle(el));
			return elptr;
		}
		case 2:
		{
			Square el(rand() % 100, rand() % 100);
			boost::shared_ptr<Shape> elptr(new Square(el));
			return elptr;
		}
		default:
		{
			throw std::runtime_error("runtime_error");
		}
		}
	return boost::shared_ptr<Shape>();
}