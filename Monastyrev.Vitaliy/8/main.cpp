#include "Shape.h"
#include "Functors.h"
#include <boost/bind.hpp>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <iterator>
#include <cmath>


int main(int /*argc*/, char* /*argv*/[])
{
	///////////TASK1///////////
	std::vector<double> vect(10);
	srand(static_cast<unsigned int>(time(0)));
	std::generate(vect.begin(), vect.end(), Random());
	std::cout << "Before: \n";
	std::copy(vect.begin(), vect.end(), std::ostream_iterator<double>(std::cout, "\n"));
	std::transform(vect.begin(), vect.end(), vect.begin(), bind1st(std::multiplies<double>(), acos(-1)));
	std::cout << "After: \n";
	std::copy(vect.begin(), vect.end(), std::ostream_iterator<double>(std::cout, "\n"));
	///////////TASK2///////////
	std::vector<boost::shared_ptr<Shape>> shapevect(10);
	std::generate(shapevect.begin(), shapevect.end(), GenerateShape());
	std::cout << "Right \n";
	std::sort(shapevect.begin(), shapevect.end(), boost::bind(Compare(), _1, _2, true));
	std::for_each(shapevect.begin(), shapevect.end(), Print());
	std::cout << "Left \n";
	std::sort(shapevect.begin(), shapevect.end(), boost::bind(Compare(), _2, _1, true));
	std::for_each(shapevect.begin(), shapevect.end(), Print());
	std::cout << "Up \n";
	std::sort(shapevect.begin(), shapevect.end(), boost::bind(Compare(), _1, _2, false));
	std::for_each(shapevect.begin(), shapevect.end(), Print());
	std::cout << "Down \n";
	std::sort(shapevect.begin(), shapevect.end(), boost::bind(Compare(), _2, _1, false));
	std::for_each(shapevect.begin(), shapevect.end(), Print());

	return 0;
}

