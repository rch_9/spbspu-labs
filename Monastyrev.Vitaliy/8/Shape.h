#ifndef SHAPE
#define SHAPE

#include <vector>

class Shape
{
protected:
	int x, y;
public:
	Shape(int x_, int y_);
	bool IsMoreLeft(const Shape & el) const;
	bool IsMoreUpper(const Shape & el) const;
	virtual void Draw() const;
};

class Circle : public Shape
{
public:
	Circle(int x_, int y_) : Shape(x_, y_)
	{};
	void Draw() const;
};

class Triangle : public Shape
{
public:
	Triangle(int x_, int y_) : Shape(x_, y_)
	{};
	void Draw() const;
};

class Square : public Shape
{
public:
	Square(int x_, int y_) : Shape(x_, y_)
	{};
	void Draw() const;
};

#endif 