#include "PhoneBook.h"
#include <iostream>
#include <stdexcept>

PhoneBook::Entry::Entry(std::string name_, std::string number_)
{
	if ((name_.empty()) || (number_.empty()))
	{
		throw std::invalid_argument("Error: Invalid argument");
	}
	else
	{
		name = name_;
		number = number_;
	}
};

PhoneBook::iterator PhoneBook::Show(iterator it)
{
	if (it != list.end())
	{
		std::cout << it->name << "\n";
		std::cout << it->number << "\n";
	}
	else
	{
		throw std::out_of_range("List is empty");
	}
	return it;
};

PhoneBook::iterator PhoneBook::Insert(insert insert_, iterator & it, Entry & entry_)
{
	if ((it == list.begin()) && (it == list.end()) && (insert_ != END))
	{
		std::cerr << "List is empty \n";
		return list.begin();
	}
	switch (insert_)
	{
	case (PREV) :
	{
		list.insert(it, entry_);
		break;
	}
	case (NEXT) :
	{
		if (it != list.end())
		{
			list.insert(++it, entry_);
		}
		break;
	}
	case (CURRENT) :
	{
		it->name = entry_.name;
		it->number = entry_.number;
		break;
	}
	case (END) :
	{
		list.push_back(entry_);
		break;
	}
	default:
	{
		std::cerr << "Error: Invalid argument \n";
	}
	}
	return list.begin();
};

PhoneBook::iterator PhoneBook::Begin()
{
	return list.begin();
};

PhoneBook::iterator PhoneBook::End()
{
	return list.end();
};

void PhoneBook::ForwardJump(unsigned int move, iterator & it)
{
	for (unsigned int i = 0; i != move + 1; ++i)
	{
		if (it == list.end())
		{
			throw std::invalid_argument("Error: Invalid argument");
			it = list.begin();
		}
		else
		{
			++it;
		}
	}
};

void PhoneBook::BackJump(unsigned int move, iterator & it)
{
	for (unsigned int i = 0; i != move + 1; ++i)
	{
		if (it == list.begin())
		{
			throw std::invalid_argument("Error: Invalid argument");
		}
		else
		{
			--it;
		}
	}
};