#include <vector> 
#include <cassert> 

class Factorial
{
private:
	const static int factoriallimit = 10;
public:
	class Iterator :
		public std::iterator<std::bidirectional_iterator_tag, int>
	{
	public:
		typedef Iterator this_type;
		Iterator(int currentpos_);
		bool operator ==(const this_type & rhs) const;
		bool operator !=(const this_type & rhs) const;
		int operator *() const;
		this_type & operator ++();
		this_type operator ++(int);
		this_type & operator --();
		this_type operator --(int);
	private:
		int currentpos;
		int maxposition;
		int factorial;
	};
	Iterator begin() const;
	Iterator end() const;
};

#endif