#include "Factorial.h"
#include <stdexcept>

static int GetFactorial(int factorial)
{
	int value = 1;
	for (int count = 1; count <= factorial; ++count)
	{
		value *= count;
	}
	return value;
};

Factorial::Iterator::Iterator(int currentpos_) : currentpos(currentpos_), maxposition(factoriallimit)
{
	factorial = GetFactorial(currentpos_);
};

bool Factorial::Iterator::operator ==(const this_type & rhs) const
{
	return factorial == rhs.factorial;
};

bool Factorial::Iterator::operator !=(const this_type & rhs) const
{
	return factorial != rhs.factorial;
};

int Factorial::Iterator::operator *() const
{
	if ((currentpos > maxposition) || (currentpos < 1))
	{
		throw std::out_of_range("Error: End of list");
	}
	return factorial;
};

Factorial::Iterator & Factorial::Iterator::operator ++()
{
	if (currentpos == maxposition + 1)
	{
		throw std::out_of_range("Error: End of list");
	}
	++currentpos;
	factorial *= currentpos;
	return *this;
};

Factorial::Iterator Factorial::Iterator::operator ++(int)
{
	this_type temp(*this);
	this->operator ++();
	return temp;
};

Factorial::Iterator & Factorial::Iterator::operator --()
{
	if (currentpos == 1)
	{
		throw std::out_of_range("Error: End of list");
	}
	factorial /= currentpos;
	--currentpos;
	return *this;
};

Factorial::Iterator Factorial::Iterator::operator --(int)
{
	this_type temp(*this);
	this->operator --();
	return temp;
};

Factorial::Iterator Factorial::begin() const
{
	return Iterator(1);
};

Factorial::Iterator Factorial::end() const
{
	return Iterator(factoriallimit + 1);
};
