#define BOOST_TEST_MODULE TESTS

#include <boost/test/included/unit_test.hpp>
#include "PhoneBook.h"
#include "Factorial.h"

BOOST_AUTO_TEST_CASE(Show)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	myphonebook.Show(it);
	if ((it->name != "Gena") || (it->number != "892134503"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function Show doesn't work");
}

BOOST_AUTO_TEST_CASE(Next)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	PhoneBook::Entry el2("Roman", "892434212");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	it = myphonebook.Insert(PhoneBook::END, it, el2);
	++it;
	myphonebook.Show(it);
	if ((it->name != "Roman") || (it->number != "892434212"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Next doesn't work");
}

BOOST_AUTO_TEST_CASE(Prev)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	PhoneBook::Entry el2("Roman", "892434212");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	it = myphonebook.Insert(PhoneBook::END, it, el2);
	++it;
	--it;
	myphonebook.Show(it);
	if ((it->name != "Gena") || (it->number != "892134503"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Prev doesn't work");
}

BOOST_AUTO_TEST_CASE(Insert)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	PhoneBook::Entry el2("Roman", "892434212");
	PhoneBook::Entry el3("Ilia", "892485711");
	PhoneBook::Entry el4("John", "892439574");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	it = myphonebook.Insert(PhoneBook::CURRENT, it, el2);
	it = myphonebook.Insert(PhoneBook::PREV, it, el3);
	++it;
	it = myphonebook.Insert(PhoneBook::NEXT, it, el4);
	myphonebook.Show(it);
	if ((it->name != "Ilia") || (it->number != "892485711"))
	{
		flag = true;
	}
	++it;
	myphonebook.Show(it);
	if ((it->name != "Roman") || (it->number != "892434212"))
	{
		flag = true;
	}
	++it;
	myphonebook.Show(it);
	if ((it->name != "John") || (it->number != "892439574"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function Insert doesn't work");
}

BOOST_AUTO_TEST_CASE(Insert2)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	PhoneBook::Entry el2("Roman", "892434212");
	PhoneBook::Entry el3("Ilia", "892485711");
	PhoneBook::Entry el4("John", "892439574");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	it = myphonebook.Insert(PhoneBook::END, it, el2);
	it = myphonebook.Insert(PhoneBook::END, it, el3);
	++it;
	++it;
	it = myphonebook.Insert(PhoneBook::PREV, it, el4);
	myphonebook.Show(it);
	it = myphonebook.Begin();
	if ((it->name != "Gena") || (it->number != "892134503"))
	{
		flag = true;
	}
	++it;
	++it;
	if ((it->name != "John") || (it->number != "892439574"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function Insert doesn't work");
}

BOOST_AUTO_TEST_CASE(ForwardJump)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	PhoneBook::Entry el2("Roman", "892434212");
	PhoneBook::Entry el3("Ilia", "892485711");
	PhoneBook::Entry el4("John", "892439574");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	it = myphonebook.Insert(PhoneBook::CURRENT, it, el2);
	it = myphonebook.Insert(PhoneBook::PREV, it, el3);
	++it;
	it = myphonebook.Insert(PhoneBook::NEXT, it, el4);
	myphonebook.ForwardJump(1, it);
    myphonebook.Show(it);
	if ((it->name != "John") || (it->number != "892439574"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function ForwardJump doesn't work");
}

BOOST_AUTO_TEST_CASE(BackJump)
{
	bool flag = false;
	PhoneBook myphonebook;
	PhoneBook::iterator it = myphonebook.Begin();
	PhoneBook::Entry el1("Gena", "892134503");
	PhoneBook::Entry el2("Roman", "892434212");
	PhoneBook::Entry el3("Ilia", "892485711");
	PhoneBook::Entry el4("John", "892439574");
	it = myphonebook.Insert(PhoneBook::END, it, el1);
	it = myphonebook.Insert(PhoneBook::CURRENT, it, el2);
	it = myphonebook.Insert(PhoneBook::PREV, it, el3);
	++it;
	it = myphonebook.Insert(PhoneBook::NEXT, it, el4);
	++it;
	++it;
	myphonebook.BackJump(1, it);
	myphonebook.Show(it);
	if ((it->name != "Ilia") || (it->number != "892485711"))
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function BackJump doesn't work");
}

BOOST_AUTO_TEST_CASE(Factorial_begin)
{
	bool flag = false;
	Factorial el;
	Factorial::Iterator it = el.begin();
	if (*it != 1)
	{
		flag = true;
	}
	it++;
	if (*it != 2)
	{
		flag = true;
	}
	it++;
	if (*it != 6)  
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function begin doesn't work");
}

BOOST_AUTO_TEST_CASE(Factorial_end)
{
	bool flag = false;
	Factorial el;
	Factorial::Iterator it = el.end();
	--it;
	if (*it != 3628800)
	{
		flag = true;
	}
	it--;
	if (*it != 362880)
	{
		std::cout << *it;
		flag = true;
	}
	it--;
	if (*it != 40320) 
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function end doesn't work");
}

BOOST_AUTO_TEST_CASE(STL_test)
{
	bool flag = false;
	Factorial el;
	Factorial::Iterator it = el.begin();
	Factorial::Iterator it2 = el.end();
	std::vector<int> vect;
	vect.push_back(0);
	vect.push_back(0);
	vect.push_back(0);
	vect.push_back(0);
	vect.push_back(0);	
	vect.push_back(0);
	vect.push_back(0);
	vect.push_back(0);
	vect.push_back(0);	
	vect.push_back(0);
	vect.push_back(0);	
	std::vector<int>::iterator vectit = vect.begin();
	std::copy(el.begin(), el.end(), vectit);
	if (*vectit != 1)
	{
		flag = true;
	}
	vectit++;
	if (*vectit != 2)
	{
		flag = true;
	}
	vectit++;
	if (*vectit != 6) 
	{
		flag = true;
	}
	BOOST_TEST(flag == false);
}

