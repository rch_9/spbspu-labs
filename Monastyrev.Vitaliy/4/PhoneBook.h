#ifndef PHONEBOOK
#define PHONEBOOK

#include <list>
#include <string>

class PhoneBook
{
public:
	enum insert { PREV, NEXT, CURRENT, END };
	class Entry
	{
	public:
		std::string name;
		std::string number;
		Entry(std::string name_, std::string number_);
	};
	typedef std::list<Entry> entrylist;
	typedef entrylist::iterator iterator;
private:
	entrylist list;
public:
	iterator Show(iterator it);
	iterator Insert(insert insert_, iterator & it, Entry & entry_);
	iterator Begin();
	iterator End();
	void ForwardJump(unsigned int move, iterator & it);
	void BackJump(unsigned int move, iterator & it);
};

#endif