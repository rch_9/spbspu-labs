#ifndef RANDOMFUNC
#define RANDOMFUNC

#include <list>

template <typename T> //returns a random number from a given interval
T Interval(T x, T y);
void FillRandom(std::list<int> &list, int size, int begin, int end); //fill the array with random numbers

#endif