#include "Print.h"
#include <iostream>

void PrettyPrint(std::list<int> & list)
{
	if (list.empty())
	{
		return;
	}
	std::list<int>::iterator it1 = list.begin();
	std::list<int>::iterator it2 = list.end();
	--it2;
	while (it1 != it2)
	{
		std::cout << *it1 << " ";
		if (it1 != it2)
		{
			std::cout << *it2 << " ";
		}
		else
		{
			return;
		}
		++it1;
		if (it1 == it2)
		{
			return;
		}
		--it2;
		if (it1 == it2)
		{
			std::cout << *it2 << " ";
			return;
		}
	}
}