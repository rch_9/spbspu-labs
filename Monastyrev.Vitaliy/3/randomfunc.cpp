#include "RandomFunc.h"
#include <ctime>
#include <cstdlib>

template <typename T>	 //returns a random number from a given interval
T Interval(T x, T y)
{
	return ((y - x)*(static_cast<T>(rand()) / RAND_MAX)) + x;
}

void FillRandom(std::list<int> &list, int size, int begin, int end)   //fill the array with random numbers
{
	if ((size <= 0) || (begin >= end))
	{
		throw std::invalid_argument("Invaled argument");
	}
	if (!list.empty())
	{
		list.clear();
	}
	int i = 0;
	while (i < size)
	{
		list.push_back(static_cast<int>(Interval<double>(begin, end)));
		i++;
	}
}
