#define BOOST_TEST_MODULE TESTS

#include "queue.h"
#include "randomfunc.h"
#include "Print.h"
#include <boost/test/included/unit_test.hpp>
#include <ctime>
#include <cstdlib>

BOOST_AUTO_TEST_CASE(PutandGet)
{
	bool flag = false;
	MyElement newel1("test1");
	MyElement newel2("test2");
	MyElement newel3("test3");
	MyElement newel4("test4");
	MyElement newel5("test5");
	QueueWithPriority<MyElement> q;
	q.Put(newel1, QueueWithPriority<MyElement>::LOW);
	q.Put(newel2, QueueWithPriority<MyElement>::HIGH);
	q.Put(newel3, QueueWithPriority<MyElement>::HIGH);
	q.Put(newel4, QueueWithPriority<MyElement>::NORMAL);
	q.Put(newel5, QueueWithPriority<MyElement>::LOW);
	std::auto_ptr<MyElement> el;
	el = q.Get();
	if (el->GetName() != "test2")
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test3")
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test4")
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test1")
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test5")
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function Get or Put doesn't work");
}

BOOST_AUTO_TEST_CASE(Accelerate)
{
	bool flag = false;
	MyElement newel1("test1");
	MyElement newel2("test2");
	MyElement newel3("test3");
	MyElement newel4("test4");
	MyElement newel5("test5");
	QueueWithPriority<MyElement> q;
	q.Put(newel1, QueueWithPriority<MyElement>::NORMAL);
	q.Put(newel2, QueueWithPriority<MyElement>::LOW);
	q.Put(newel3, QueueWithPriority<MyElement>::HIGH);
	q.Put(newel4, QueueWithPriority<MyElement>::LOW);
	q.Put(newel5, QueueWithPriority<MyElement>::HIGH);
	q.Accelerate();
	std::auto_ptr<MyElement> el;
	el = q.Get();
	if (el->GetName() != "test3") 
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test5") 
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test2") 
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test4")
	{
		flag = true;
	}
	el = q.Get();
	if (el->GetName() != "test1") 
	{
		flag = true;
	}
	BOOST_TEST(flag == false, "Function Accelerate doesn't work");
}

BOOST_AUTO_TEST_CASE(FillRandomFunc)
{
	bool flag = false;
	std::list<int> list;
	std::list<int>::iterator it = list.begin();
	srand((unsigned)time(0));
	FillRandom(list, 100, 1, 20);
	it = list.begin();
	while (it != list.end())
	{
		if ((*it > 20) || (*it < 1))
		{
			flag = true;
		}
		it++;
	}
	BOOST_TEST(flag == false, "Function FillRandom doesn't work");
}

BOOST_AUTO_TEST_CASE(PrettyPrintFunc)
{
	bool flag = false;
	char ch;
	std::list<int> list;
	std::list<int>::iterator it = list.begin();
	int mass[8] = { 0, 1, 2, 3, 4, 5, 7, 14 };
	for (unsigned int i = 0; i != 8; ++i)
	{
		if (i != 0)
		{
			FillRandom(list, mass[i], 1, 20);
		}
		it = list.begin();
		while (it != list.end())
		{
			std::cout << *it << " ";
			it++;
		}
		std::cout << "\n";
		PrettyPrint(list);
		std::cout << "\n";
		std::cout << "All right? y/n \n";
		std::cin >> ch;
		if (ch == 'n')
		{
			flag = true;
		}
	}
	BOOST_TEST(flag == false, "Function PrettyPrint doesn't work");
}