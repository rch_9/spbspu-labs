#ifndef QUEUE
#define QUEUE

#include <string>
#include <list>
#include <memory>
#include <stdexcept>

class MyElement
{
private:
	std::string name;
public:
	MyElement();
	MyElement(std::string name_);
	~MyElement();
	std::string GetName();
};

template <typename T>
class QueueWithPriority
{
private:
	std::list<T> lowlist;
	std::list<T> normallist;
	std::list<T> highlist;
public:
	enum ElementPriority { LOW, NORMAL, HIGH };
	void Put(T & element_, ElementPriority elementpriority_)
	{
		switch (elementpriority_)
		{
		case LOW:
		{
			lowlist.push_back(element_);
			break;
		}
		case NORMAL:
		{
			normallist.push_back(element_);
			break;
		}
		case HIGH:
		{
			highlist.push_back(element_);
			break;
		}
		default:
		{
			throw std::invalid_argument("Invalid argument");
		}
		}
	}
	std::auto_ptr<T> Get()
	{
		std::auto_ptr<T> element;
		if (!highlist.empty())
		{
			element.reset(new T(highlist.front()));
			highlist.pop_front();
		}
		else
		if (!normallist.empty())
		{
			element.reset(new T(normallist.front()));
			normallist.pop_front();
		}
		else
		if (!lowlist.empty())
		{
			element.reset(new T(lowlist.front()));
			lowlist.pop_front();
		}
		else
		{
			throw std::out_of_range("List is empty");
		}
		return element;
	}
	void Accelerate()
	{
		if (!lowlist.empty())
		{
			highlist.splice(highlist.end(), lowlist);
		}
	}
};

#endif
