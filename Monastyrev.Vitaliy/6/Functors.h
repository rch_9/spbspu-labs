#ifndef FUNCTORS
#define FUNCTORS

#include "Figure.h"

class Vertexes : std::binary_function<int, Shape, int>
{
public:
	int operator() (size_t sum, const Shape & el);
};

class IsRectangle
{
public:
	bool operator() (const Shape & shape);
};

class IsSquare
{
public:
	bool operator() (const Shape & shape);
};

class IsTriangle
{
public:
	bool operator() (const Shape & shape);
};

class IsPentagon
{
public:
	bool operator() (const Shape & shape);
};

class GetPoint
{
public:
	Shape::Point operator() (const Shape & shape);
};

class Compare
{
public:
	bool operator() (const Shape & el1, const Shape & el2);
};

class GenerateShape
{
public:
	Shape operator() ();
};

#endif