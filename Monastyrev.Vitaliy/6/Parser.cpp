#include "Parser.h"
#include <fstream> 
#include <iostream>
#include <string>

Parser::Parser()
{}

void Parser::PrettyReading(const char * file)
{
	if (!file)
	{
		std::cerr << "ERROR: can not find file" << "\n";
		return;
	}
	std::ifstream stream(file, std::ios::in);
	if (!stream)
	{
		std::cerr << "ERROR: file don't open" << "\n";
		return;
	}
	std::string arraych;
	while (!stream.eof())
	{
		stream >> arraych;
		if (!stream.good() && !stream.eof())
		{
			std::cerr << "ERROR: problem with file reading";
			vect.clear();
			return;
		}
		if (std::find(vect.begin(), vect.end(), arraych) == vect.end())
		{
			vect.push_back(arraych);
		}
		arraych.clear();
	}
}

const std::vector<std::string> Parser::GetVect()
{
	return vect;
}
