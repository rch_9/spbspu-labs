#ifndef PARSER
#define PARSER

#include <vector>

class Parser
{
private:
	std::vector<std::string> vect;
public:
	Parser();
	void PrettyReading(const char * file);
	const std::vector<std::string> GetVect();
};

#endif