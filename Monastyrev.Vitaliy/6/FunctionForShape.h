#ifndef FUNCTIONSFORSHAPE
#define FUNCTIONSFORSHAPE

#include "Figure.h"

bool IsOrthogonality(const Shape::Point & el1, const Shape::Point & el2, const Shape::Point & el3);
int SideLength(const Shape::Point & el1, const Shape::Point & el2);

#endif