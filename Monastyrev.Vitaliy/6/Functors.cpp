#include "Functors.h"
#include "FunctionsForShape.h"
#include <stdexcept>

int Vertexes::operator()(size_t sum, const Shape & shape)
{
	return static_cast<int>(sum + shape.GetSize());
}

bool IsRectangle::operator()(const Shape & shape)
{
	int count1 = 0;
	int count2 = 1;
	int count3 = 2;
	for (int count = 0; count != 3; ++count)  //check parallelism
	{
		if (!IsOrthogonality(shape.GetVertexes()[count1],
			shape.GetVertexes()[count2],
			shape.GetVertexes()[count3]))
		{
			return false;
		}
		++count1; ++count2; ++count3;
		if (count3 == 4)
		{
			count3 = 0;
		}
	}
	return !(SideLength(shape.GetVertexes()[0], shape.GetVertexes()[1]) != SideLength(shape.GetVertexes()[3], shape.GetVertexes()[2]) ||
		SideLength(shape.GetVertexes()[1], shape.GetVertexes()[2]) != SideLength(shape.GetVertexes()[0], shape.GetVertexes()[3]));
	//check the equality of opposite sides
}

bool IsSquare::operator()(const Shape & shape)
{
	if (IsRectangle()(shape))
	{
		return (SideLength(shape.GetVertexes()[0], shape.GetVertexes()[1]) == SideLength(shape.GetVertexes()[1], shape.GetVertexes()[2]) ||
			SideLength(shape.GetVertexes()[2], shape.GetVertexes()[3]) == SideLength(shape.GetVertexes()[0], shape.GetVertexes()[3]));
		//check square
	}
	else
	{
		return false;
	}
	return false;
}

bool IsTriangle::operator()(const Shape & shape)
{
	return (shape.GetSize() == 3);
}

bool IsPentagon::operator()(const Shape & shape)
{
	std::vector<Shape::Point> vertexes = shape.GetVertexes();
	return (shape.GetSize() == 5);
}

Shape::Point GetPoint::operator()(const Shape & shape)
{
	return shape.GetVertexes()[0];
}

bool Compare::operator()(const Shape & el1, const Shape & el2)
{
	if ((el1.GetSize() == 4) && (el2.GetSize() == 4))
	{
		return (IsSquare()(el1) && !IsSquare()(el2));
	}
	else
	{
		return el1.GetSize() < el2.GetSize();
	}
}

Shape GenerateShape::operator()()
{
	{
		int number = rand() % 4;
		std::vector<Shape::Point> vertexes;
		int delta1 = rand() % 100;
		int delta2 = rand() % 100;
		int side1 = rand() % 100;
		int side2 = rand() % 100;
		switch (number)
		{
		case 0:
		{
			for (unsigned int count = 0; count != 3; ++count)
			{
				Shape::Point point(rand() % 100, rand() % 100);
				vertexes.push_back(point);
			}
			Shape shape(vertexes);
			return shape;
		}
		case 1:
		{
			Shape::Point point1(side1, side2);
			Shape::Point point2(side1 + delta1, side2);
			Shape::Point point3(side1 + delta1, side2 + delta2);
			Shape::Point point4(side1, side2 + delta2);
			vertexes.push_back(point1);
			vertexes.push_back(point2);
			vertexes.push_back(point3);
			vertexes.push_back(point4);
			Shape shape(vertexes);
			return shape;
		}
		case 2:
		{
			Shape::Point point1(side1, side2);
			Shape::Point point2(side1 + delta1, side2);
			Shape::Point point3(side1 + delta1, side2 + delta1);
			Shape::Point point4(side1, side2 + delta1);
			vertexes.push_back(point1);
			vertexes.push_back(point2);
			vertexes.push_back(point3);
			vertexes.push_back(point4);
			Shape shape(vertexes);
			return shape;
		}
		case 3:
		{
			for (unsigned int count = 0; count != 5; ++count)
			{
				Shape::Point point(rand() % 100, rand() % 100);
				vertexes.push_back(point);
			}
			Shape shape(vertexes);
			return shape;
		}
		default:
		{
			throw std::runtime_error("runtime error");
		}
		}
	}
}
