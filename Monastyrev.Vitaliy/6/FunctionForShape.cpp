#include "FunctionsForShape.h"

bool IsOrthogonality(const Shape::Point & el1, const Shape::Point & el2, const Shape::Point & el3)
{
	return ((el2.GetX() - el1.GetX())*(el3.GetX() - el2.GetX()) + (el2.GetY() - el1.GetY())*(el3.GetY() - el2.GetY()) == 0);
}

int SideLength(const Shape::Point & el1, const Shape::Point & el2)
{
	return (el2.GetX() - el1.GetX())*(el2.GetX() - el1.GetX()) + (el2.GetY() - el1.GetY())*(el2.GetY() - el1.GetY());
}