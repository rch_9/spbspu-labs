#include "Figure.h"
#include "Functors.h"
#include <iterator>
#include <stdexcept>

Shape::Point::Point(int x_, int y_) : x(x_), y(y_)
{}

const int Shape::Point::GetX() const
{
	return x;
}

const int Shape::Point::GetY() const
{
	return y;
}

Shape::Shape(const std::vector<Point> & vertexes_)
{
	size_t size = vertexes_.size();
	if (size < 3 && size > 5)
	{
		throw std::invalid_argument("Invalid argument");
	}
	vertexes = vertexes_;
	if ((size == 4) && (!IsRectangle()(*this)))
	{
		vertexes.clear();
		throw std::invalid_argument("Invalid argument");
	}
}

std::ostream & operator <<(std::ostream & os, const Shape::Point & point)
{
	os << "(" << point.x << " " << point.y << ") ";
	return os;
}

std::ostream & operator <<(std::ostream & os, const Shape & shape)
{
	size_t var = shape.vertexes.size();
	switch (var)
	{
	case (3) :
	{
		os << "TRIANGLE ";
		break;
	}
	case (4) :
	{
		if (IsSquare()(shape))
		{
			os << "SQUARE ";
		}
		else
		{
			if (IsRectangle()(shape))
			{
				os << "RECTANGLE ";
			}
			else
			{
				throw std::invalid_argument("Invalid argument");
			}
		}
	
		break;
	}
	case (5) :
	{
		os << "PENTAGON ";
		break;
	}
	default:
	{
		throw std::invalid_argument("Invalid argument");
		break;
	}
	}
	std::copy(shape.vertexes.begin(), shape.vertexes.end(), std::ostream_iterator<Shape::Point>(os));
	return os;
}

const std::vector<Shape::Point> Shape::GetVertexes() const
{
	return vertexes;
}

const size_t Shape::GetSize() const
{
	return vertexes.size();
}
