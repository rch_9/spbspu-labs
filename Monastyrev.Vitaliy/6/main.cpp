#include "Parser.h"
#include "Figure.h"
#include "Functors.h"
#include <ctime>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>

int main(int /*argc*/, char* /**argv*/[])
{
	////////////TASK1////////////
	char filename[] = "testtext.txt";
	Parser pars;
	pars.PrettyReading(filename);
	std::vector<std::string> vect = pars.GetVect();
	std::copy(vect.begin(), vect.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
	////////////TASK2////////////
	std::vector<Shape::Point> point;
	std::vector<Shape> shapevect(10, Shape(point));
	srand(static_cast<unsigned int>(time(0)));
	std::generate(shapevect.begin(), shapevect.end(), GenerateShape());
	std::copy(shapevect.begin(), shapevect.end(), std::ostream_iterator<Shape>(std::cout, "\n"));
	std::cout << "Number of vertices: " << std::accumulate(shapevect.begin(), shapevect.end(), 0, Vertexes()) << "\n";
	std::cout << "Number of triangle: " << std::count_if(shapevect.begin(), shapevect.end(), IsTriangle()) << "\n";
	std::cout << "Number of rectangle: " << std::count_if(shapevect.begin(), shapevect.end(), IsRectangle()) << "\n";
	std::cout << "Number of square: " << std::count_if(shapevect.begin(), shapevect.end(), IsSquare()) << "\n";
	shapevect.erase(std::remove_if(shapevect.begin(), shapevect.end(), IsPentagon()), shapevect.end());
	std::cout << "After remove \n";
	std::copy(shapevect.begin(), shapevect.end(), std::ostream_iterator<Shape>(std::cout, "\n"));
	std::vector<Shape::Point> pointvect(shapevect.size(), Shape::Point(0, 0));
	std::transform(shapevect.begin(), shapevect.end(), pointvect.begin(), GetPoint());
	std::cout << "Point Vector \n";
	std::copy(pointvect.begin(), pointvect.end(), std::ostream_iterator<Shape::Point>(std::cout, "\n"));
	std::sort(shapevect.begin(), shapevect.end(), Compare());
	std::cout << "After sort \n";
	std::copy(shapevect.begin(), shapevect.end(), std::ostream_iterator<Shape>(std::cout, "\n"));

	return 0;
}