#ifndef FIGURE
#define FIGURE

#include <vector>
#include <iostream>

class Shape
{
public:
	class Point
	{
	private:
		int x, y;
	public:
		Point(int x_, int y_);
		const int GetX() const;
		const int GetY() const;
		friend std::ostream & operator <<(std::ostream & os, const Shape::Point & point);
	};
	Shape(const std::vector<Point> & vertexes_);
	const std::vector<Point> GetVertexes() const;
	const size_t GetSize() const;
	friend std::ostream & operator <<(std::ostream & os, const Shape & shape);
private:
	std::vector<Point> vertexes;
};

#endif