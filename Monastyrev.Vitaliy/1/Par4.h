#ifndef PAR4
#define PAR4
#include <iostream>
#include <vector>
	
double Interval(double x, double y);	 //returns a random number from a given interval
void FillRandom(double * array, int size);   //fill the array with random numbers
template <typename T>
void ShowVector(std::vector <T> Vec)		//show vector
{
	int k = 0;
	while (k != Vec.size())
	{
		std::cout << Vec[k] << "\n";
		k++;
	}
	std::cout << "\n";
};

#endif
