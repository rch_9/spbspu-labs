#ifndef PAR1
#define PAR1
#include <vector>
#include "ForwardList.h"

template <typename T>
struct VectorIndStrat					//first strategy
{
	typedef typename T::size_type index;
	typedef typename T::value_type data;
	static data & Get(T &Data, index count)
	{
		return Data[count];
	}
	static index End(T &Data)
	{
		return Data.size();
	}
	static index Begin(T &Data)
	{
		return 0;
	}
};

template <typename T>
struct VectorAtStrat			//second strategy
{
	typedef typename T::size_type index;
	typedef typename T::value_type data;
	static data & Get(T &Data, index count)
	{
		return Data.at(count);
	}
	static index End(T &Data)
	{
		return Data.size();
	}
	static index Begin(T &Data)
	{
		return 0;
	}
};

template <typename T>
struct ForwardLsIterStrat	//third strategy
{
	typedef typename T::iterator index;
	typedef typename T::value_type data;
	static data & Get(T &Data, index count)
	{
		return *count;
	}
	static index End(T &Data)
	{
		return Data.end();
	}
	static index Begin(T &Data)
	{
		return Data.begin();
	}
};

template <template <typename> class Sorting, typename T>
void Sort(T  &Data)				//basic sorting algorithm
{
	if (Data.begin() == Data.end())
	{
		return;
	}
	typename Sorting<T>::index end = Sorting<T>::End(Data);
	typename Sorting<T>::index count1 = Sorting<T>::Begin(Data);
	typename Sorting<T>::index count2 = Sorting<T>::Begin(Data);
	while (count1 != end)
	{
		count2 = Sorting<T>::Begin(Data);
		while (count2 != end)
		{
			if (Sorting<T>::Get(Data, count1) > Sorting<T>::Get(Data, count2))
			{
				std::swap(Sorting<T>::Get(Data, count1), Sorting<T>::Get(Data, count2));
			}
			count2++;
		}
		count1++;
	}
}

#endif