/*This program does the following:
1)Reads information from a text file "testtext.txt" and displays this information on the monitor screen
2)Prompts you to enter the numbers for the third paragraph of laboratory work and outputs the result
3)Sorts a random 5, 10, 25, 50, 100 numbers by strategy pattern from paragraph 1 a)*/
#include "Par1.h"
#include "Par2.h"
#include "Par3.h"
#include "Par4.h"
#include <ctime>
#include <cstdlib>

int main(int /*argc*/, char* /*argv*/[])
{
	std::vector <char> vectortask2;
	vectortask2 = Reading("testtext.txt");
	for (unsigned int i = 0; i < vectortask2.size(); ++i)
	{
		std::cout << vectortask2[i];
	}
	std::cout << "\n";
	JobWithVector();
	std::cout << "\n";
	srand((unsigned)time(0));
	size_t sizearray[5] = { 5, 10, 25, 50, 100 };
	for (unsigned int count = 0; count < 5; ++count)
	{
		std::vector<double> vec(sizearray[count]);
		FillRandom(&vec[0], sizearray[count]);
		Sort<VectorIndStrat>(vec);
		ShowVector(vec);
	}

	return 0;
}