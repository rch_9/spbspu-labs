#include "Par2.h"
#include <fstream>
#include <iostream>

std::vector<char> Reading(const char * file)
{
	if (!file)
	{
		std::cerr << "ERROR: don't find file" << "\n";

		return std::vector<char>();
	}
	std::ifstream stream(file, std::ios::in);
	if (!stream)
	{
		std::cerr << "ERROR: file don't open" << "\n";

		return std::vector<char>();
	}
	std::vector<char> v((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	
	return v;
}