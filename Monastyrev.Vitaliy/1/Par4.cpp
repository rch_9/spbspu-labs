#include "Par4.h"
 
double Interval(double x, double y)	//returns a random number from a given interval
{
	return ((y - x)*(static_cast<double>(rand()) / RAND_MAX)) + x;
}

void FillRandom(double * array, int size)   //fill the array with random numbers
{
	int i = 0;
	while (i < size)
	{
		array[i] = Interval(-1.0, 1.0);
		i++;
	}
}