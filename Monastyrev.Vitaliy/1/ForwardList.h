#ifndef FORWARDLIST
#define FORWARDLIST
#include <cassert>  
#include <iterator>

template <class T>
class ForwardList
{
private:
	ForwardList(const ForwardList &);
	ForwardList & operator =(const ForwardList &);
	struct node_t
	{
		T value;
		node_t * next;
		inline node_t(const T & _value) :next(0), value(_value) 
		{}
		inline ~node_t()
		{};
	};
	node_t * Beg;
	node_t * End;
public:
	class iterator :
		public std::iterator<std::forward_iterator_tag, T>
	{
	public:
		typedef node_t node_type;
		typedef iterator this_type;
		inline iterator() :current_(0)
		{};
		inline iterator(node_type * slist) :current_(slist)
		{};
		inline bool operator ==(const this_type & rhs) const
		{
			return current_ == rhs.current_;
		};
		inline bool operator !=(const this_type & rhs) const
		{
			return current_ != rhs.current_;
		};
		inline T & operator *() const
		{
			assert(current_ != 0);

			return current_->value;
		};
		inline T * operator ->() const
		{
			assert(current_ != 0);

			return &current_->value;
		};
		inline this_type & operator ++()
		{
			assert(current_ != 0);
			current_ = current_->next;

			return *this;
		};
		inline this_type operator ++(int)
		{
			this_type temp(*this);
			this->operator ++();

			return temp;
		};
	private:
		node_type * current_;
	};
public:
	typedef T value_type;
	inline ForwardList(): Beg(0), End(0)
	{}
	inline void push_back(T & value)
	{
		node_t *nd = new node_t(value);
		if (Beg == 0)
		{
			Beg = End = nd;
		}
		else
		{
			End->next = nd;
			End = nd;
		}
		End->next = 0;
	}
	inline void push_front(T & value) 
	{
		node_t *nd = new node_t(value);
		nd->next = Beg;
		Beg = nd;
	}
	inline ~ForwardList(void)
	{
		node_t *cur, *_next;
		cur = Beg;
		while (cur != 0)
		{
			_next = cur->next;
			delete cur;
			cur = _next;
		}
	}
	inline iterator begin(void) const
	{
		return Beg;
	}
	inline iterator end(void) const
	{
		return 0;
	}
};

#endif