#include "Par3.h"
#include <iostream>
#include <vector>
#include <iterator>

void JobWithVector()
{
	std::vector<int> vect;
	int enteredvalue = 1;
	std::cout << "Enter numbers \n";
	while (enteredvalue)
	{
		std::cin >> enteredvalue;
		if (!std::cin.good())
		{
			std::cerr << "ERROR: incorrect input";
			return;
		}
		if (enteredvalue)
		{
			vect.push_back(enteredvalue);
		}
	}
	if (vect.empty())
	{
		return;
	}
	if (vect.back() == 1)
	{
		std::vector<int>::iterator it = vect.begin();
		while (it < vect.end())
		{
			if (*it % 2 == 0)
			{
				it = vect.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
	else
	{
		if (vect.back() == 2)
		{
			std::vector<int>::iterator it = vect.begin();
			for (it; it < vect.end(); ++it)
			{
				if (*it % 3 == 0)
				{
					it = vect.insert(it + 1, 3, 1);
				}
			}
		}
	}
	for (std::vector<int>::iterator it = vect.begin(); it < vect.end(); ++it)
	{
		std::cout << *it << " ";
	}
}