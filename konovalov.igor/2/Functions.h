#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "States.h"
#include <stdexcept>

const int N = 40;	//for strlen
typedef std::vector<std::string>::iterator iterator;

void MakeVau(std::string &str)
{
	if (str.size() > 10) {
		str = "Vau!!!";
	}
}

void Initialize(const char *fname,std::vector<std::string> &Vector)
{
	if (!fname) {
		throw std::invalid_argument("Invalid filename.\n");
	}

	Context context;
	std::ifstream fin(fname);
	if (fin) {
		std::cout << "Working...\n\n";
		while (!fin.eof()) {
			char buffer[21] = {};
			int i = 0;
			while (i<20) {
				context.handle(fin, &buffer[i]);		//make an action and go to the next state
				if (context.CheckEnd() == 0) {			//if not END -
					if (context.CheckNext()) {			// and ReadWord - add and iterate i for array buffer
						++i;
					}
				}
				else {
					++i;							//if END - break cycle
					break;
				}
			}
			buffer[i] = '\0';				//cuase it's an array of chars
			Vector.push_back(buffer);
			MakeVau(Vector.back());			//make Vau here
			context.Reset();			//go to Begin State
		}
	}
	Vector.pop_back();
}

void SpliceStrings(std::vector<std::string> &Vector)
{
	for (iterator iter = Vector.begin() + 1; iter != Vector.end(); ++iter) {
		iterator prev = iter - 1;
		if (ispunct((*iter)[0]) && prev->length() < N) {		//if it's a punct token and there's enough place
			*prev += *iter;				//then write to that string
		} else if (prev->length() < N - iter->length()) {	//or it's a word and checko for place with space
			*prev += " " + *iter;				//then write to that string + space
		}
		else {
			continue;
		}
		Vector.erase(iter);
		iter = prev;
	}
}
