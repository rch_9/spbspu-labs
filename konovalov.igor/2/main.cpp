#include <iterator>
#include "States.h"
#include "Functions.h"
#include <vector>
#include <string>

int main()
{
	try {
		std::vector<std::string> Vector;
		Initialize("input.txt", Vector);
		SpliceStrings(Vector);
		std::copy(Vector.begin(), Vector.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
	}
	catch (const std::exception &e) {
		std::cerr << "Mistake: " << e.what();
		throw;
	}

	return 0;
}