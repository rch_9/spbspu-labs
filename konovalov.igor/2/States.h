#ifndef STATES_H
#define STATES_H

#include <iostream>
#include <fstream>
#include <memory>

class State;
class Begin;
class ReadChar;
class ReturnPunct;
class ReadWord;
class WordEnd;
class End;

class Context
{
public:
	Context();

	void handle(std::ifstream &, char *);		//will change current State;
	bool CheckEnd();
	bool CheckNext();
	void Reset();
private:
	std::auto_ptr<State> CurrentState;
	char buf;
};

class State
{
public:
	State(bool, bool);

	friend class Context;

	virtual State * execute(std::ifstream &, char *, char &) = 0;
	bool end, next;

	virtual ~State();
};

class Begin :
	public State
{
public:
	Begin();
	virtual State * execute(std::ifstream &, char *, char &);		//readinf from file
};

class ReadChar :
	public State
{
public:
	ReadChar();
	virtual State * execute(std::ifstream &, char *, char &);		//readinf from file
};

class ReturnPunct :
	public State
{
public:
	ReturnPunct();
	virtual State * execute(std::ifstream &, char *, char &);
};

class End :
	public State
{
public:
	End();
	virtual State * execute(std::ifstream &, char *, char &);
};

class ReadWord :
	public State
{
public:
	ReadWord();
	virtual State * execute(std::ifstream &, char *, char &);
};

class WordEnd :
	public State
{
public:
	WordEnd();
	virtual State * execute(std::ifstream &, char *, char &);
};


///CONTEXT////
Context::Context()
	: CurrentState(new Begin())
{};

void Context::handle(std::ifstream &in, char *token)
{
	CurrentState.reset(CurrentState->execute(in,token,buf));
}

bool Context::CheckEnd()
{
	return CurrentState->end;
}

bool Context::CheckNext()
{
	return CurrentState->next;
}

void Context::Reset()
{
	CurrentState.reset(new Begin());
}


///STATE////

State::State(bool _end, bool _next)
	:end(_end),
	next(_next)
{};

State::~State()
{};


///BEGIN////
Begin::Begin()
	:State(false,false)
{};

State * Begin::execute(std::ifstream &, char *, char &)
{
	return new ReadChar();
}


///READCHAR////
ReadChar::ReadChar()
	: State(false,false)
{};

State * ReadChar::execute(std::ifstream &in, char *token, char &buf)
{
	in.get(buf);
	if (in.eof()) {
		return new End();
	}
	if (isspace(buf)) {
		return new ReadChar();
	}
	if (ispunct(buf)) {
		return new ReturnPunct();
	}
	if (isalnum(buf)) {
		*token = buf;
		return new ReadWord();
	}
	else {
		std::cerr << "Error!";
		return new End();
	}
}


///RETURNPUNCT////
ReturnPunct::ReturnPunct()
	: State(false,false)
{};

State * ReturnPunct::execute(std::ifstream &, char *token, char &buf)
{
	*token = buf;
	return new End();
}


///END////
End::End()
	: State(true,false)
{};

State * End::execute(std::ifstream &, char *, char &)
{
	return new Begin();
}


///READWORD////
ReadWord::ReadWord()
	: State(false,true)
{};

State * ReadWord::execute(std::ifstream &in, char *token, char &buf)
{
	in.get(buf);
	if (isspace(buf) || in.eof()) {
		return new End();
	}
	if (ispunct(buf)) {
		return new WordEnd();
	}
	if (isalnum(buf)) {
		*token = buf;
		return new ReadWord();
	}
	else {
		std::cerr << "Error!";
		return new End();
	}
}


///WORDEND////
WordEnd::WordEnd()
	: State(false,false)
{};

State * WordEnd::execute(std::ifstream &in, char *, char &)
{
	in.unget();
	return new End();
}


#endif
