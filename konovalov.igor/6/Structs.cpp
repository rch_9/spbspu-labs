#include "Structs.h"
#include <vector>
#include <iostream>
#include <cmath>

//-----konig----2015----------------//
//-----Point-------Impl------------//
//------------------spbspu---------//

Point::Point(int x, int y)
	: _x(x),
	_y(y)
{};

std::ostream & operator << (std::ostream &out, const Point &point)
{
	out << "x - " << point._x << ", y - " << point._y;
	out << "\n";
	return out;
}


//-----konig----2015----------------//
//-----Shape-------Impl------------//
//------------------spbspu---------//

bool Shape::IsSquare() const
{
    return ( std::abs(vertexes[1]._x - vertexes[2]._x) ==
            std::abs(vertexes[0]._y - vertexes[1]._y) );
}

std::ostream & operator << (std::ostream &out, const Shape &shape)
{
	out << "Shape type: \t";
	switch (shape.vertexes.size())
	{
	case 3:
		out << "Triangle.\n";
			break;
	case 4:
		if (shape.IsSquare()) {
			out << "Square.\n";
		}
		else {
			out << "Rectangle.\n";
		}
		break;
	case 5:
		out << "Pentagon.\n";
		break;
	default:
		out << "unknown type.\n";
	}
	for (size_t i = 0; i < shape.vertexes.size(); ++i) {
		out << "Point " << i+1 << ": ";
		out << shape.vertexes[i];
	}
	out << "\n";
	return out;
};


//-----konig----2015----------------//
//-----Counts------Impl------------//
//------------------spbspu---------//

std::ostream & operator << (std::ostream &out, const Counts &counts)
{
	out << "Num of triagles - " << counts.triangle_num << "\n";
	out << "Num of rectangles - " << counts.rectangle_num << "\n";
	out << "Num of squares - " << counts.square_num << "\n";
	out << "Num of points - " << counts.points_num << "\n\n";
	return out;
}
