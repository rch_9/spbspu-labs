#include "Functions.h"
#include <algorithm>
#include <numeric>
#include <vector>


void Fill(std::vector<Shape> & ShapeVector)
{
	std::generate(ShapeVector.begin(), ShapeVector.end(), myGenerator());
}

void Count(const std::vector<Shape> &Shapevector, Counts &counts)
{
	counts = std::accumulate(Shapevector.begin(), Shapevector.end(), counts, Counter());
}

void DeletePentagons(std::vector<Shape> &Shapevector)
{
	Shapevector.erase(std::remove_if(Shapevector.begin(), Shapevector.end(), Deleter()), Shapevector.end());
}

void CreatePointVector(std::vector<Point> &PointVector, const std::vector<Shape> &ShapeVector)
{
	for (std::vector<Shape>::const_iterator iter = ShapeVector.begin(); iter != ShapeVector.end(); ++iter) {
		PointVector.push_back(iter->vertexes[rand() % (iter->vertexes.size() - 1)]);
	}
}

void Sort(std::vector<Shape> &ShapeVector)
{
	std::sort(ShapeVector.begin(), ShapeVector.end(), Comparator());
}

