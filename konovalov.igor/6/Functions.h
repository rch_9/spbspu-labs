#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "Structs.h"
#include <vector>
#include <iostream>
#include <iterator>
#include <cstdlib>


struct myGenerator:
    std::unary_function < Shape, void >
{
	Shape operator () (void)
	{
		Shape rhs;
		size_t vertex_type = rand() % 3;		//0 - triangle, 1 - rectangle, 2 - pentagon, 3 - square
		switch (vertex_type) {
		case 0:
		case 2:
		{
			for (size_t i = 0; i < vertex_type + 3; ++i) {
				rhs.vertexes.push_back(Point(rand() % 15, rand() % 15));
			}
			break;
		}
		case 1:
		{
			rhs.vertexes.push_back(Point(rand() % 15, rand() % 15));
			rhs.vertexes.push_back(Point(rhs.vertexes[0]._x, rand() % 10));
			rhs.vertexes.push_back(Point(rand() % 15, rhs.vertexes[1]._y));
			rhs.vertexes.push_back(Point(rhs.vertexes[2]._x, rhs.vertexes[0]._y));
			break;
		}
		case 3:
		{
			size_t side = rand()%20 + 1, x0 = rand()%10, y0 = rand()%10;
			rhs.vertexes.push_back(Point(x0, y0));
			rhs.vertexes.push_back(Point(x0, y0 + side));
			rhs.vertexes.push_back(Point(x0 + side, y0 + side));
			rhs.vertexes.push_back(Point(x0 + side, y0));
			break;
		}
		}
		return rhs;
	}
};

void Fill(std::vector<Shape> & ShapeVector);


struct Counter:
	std::binary_function<Counts,Shape,Counts>
{
	Counts operator() (Counts &counts, const Shape &shape)		//doesn't work correctly!!!
	{
		counts.points_num += shape.vertexes.size();
		switch (shape.vertexes.size()) {
		case 3:
			counts.triangle_num++;
			break;
		case 4:
			if (shape.IsSquare()) {
				counts.square_num++;
			}
			else {
				counts.rectangle_num++;
			}
			break;
		default:
			break;
		}
		return counts;
	}
};

void Count(const std::vector<Shape> &Shapevector, Counts &counts);


struct Deleter :			//this deleter find a pentagon trait in figures
	std::unary_function < Shape, bool >
{
	bool operator () (const Shape &shape)
	{
		return shape.vertexes.size() == 5;
	}
};

void DeletePentagons(std::vector<Shape> &Shapevector);
void CreatePointVector(std::vector<Point> &PointVector, const std::vector<Shape> &ShapeVector);

struct Comparator :
	std::binary_function < Shape, Shape, bool >
{
	bool operator () (const Shape &lhs, const Shape &rhs)		//it may be that rhs is the next of current iterator
	{
		if ( ( lhs.vertexes.size() < rhs.vertexes.size() ) ||
            ( ( lhs.vertexes.size() == rhs.vertexes.size() ) &&  ( lhs.IsSquare() && !rhs.IsSquare() ) ) ){
			return true;
		}
		else {
			return false;
		}
	}
};

void Sort(std::vector<Shape> &ShapeVector);

template <typename T>
void Print(const std::vector<T> &Shapevector)
{
	std::copy(Shapevector.begin(),Shapevector.end(),std::ostream_iterator<T>(std::cout));
	std::cout << std::endl;
}


#endif
