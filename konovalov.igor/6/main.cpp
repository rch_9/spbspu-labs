#define _SCL_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <set>
#include <iterator>

#include <vector>
#include "Structs.h"
#include "Functions.h"
#include <ctime>
#include <cstdlib>

int main()
{
	srand((int)time(0u));
	try {
		//-------TASK_1-------//
		std::ifstream fin("input.txt");
		std::set<std::string> StringSet;

		if (fin) {
			std::copy(std::istream_iterator<std::string>(fin),
				(std::istream_iterator<std::string>()), std::inserter(StringSet, StringSet.begin()));
			if (!fin.good() && !fin.eof()) {
				std::cerr << "Error of reading file!!";
				return 1;
			}
			std::cout << "TASK 1:\n\n";
			std::copy(StringSet.begin(), StringSet.end(), std::ostream_iterator<std::string>(std::cout, " "));
			std::cout << "\n\n";
		}


		//-------TASK_2-----//
		std::vector<Shape> myShapeVector(10);
		Counts Counts_1,Counts_2;		//1 is before deleting pentagons, 2 is after
		std::vector<Point> myPointVector;
		std::cout << "TASK 2:\n\n";

		//Fill and print shapeVector
		Fill(myShapeVector);
		Print(myShapeVector);

		//Count points and nums of different kinds of figures and print the information
		Count(myShapeVector, Counts_1);
		std::cout << Counts_1;

		//Delete pentagons from ShapeVector and print it
		DeletePentagons(myShapeVector);
		Print(myShapeVector);

		//Make a PointVector from current ShapeVector and print it
		CreatePointVector(myPointVector, myShapeVector);
		Print(myPointVector);

		//Sort Vector, couns points again and print
		Sort(myShapeVector);
		Count(myShapeVector, Counts_2);
		Print(myShapeVector);
		std::cout << Counts_2;
	}
	catch (const std::exception &e) {
		std::cerr << e.what();
		return 1;
	}

	return 0;
}
