#ifndef STRUCTS_H
#define STRUCTS_H

#include <vector>
#include <cstdlib>
#include <iostream>

struct Point
{
	int _x, _y;
	Point(int, int);
};

struct Shape
{
	std::vector<Point> vertexes;
	bool IsSquare() const;
};

struct Counts
{
	size_t triangle_num;
	size_t rectangle_num;
	size_t square_num;
	size_t points_num;
};

std::ostream & operator << (std::ostream &out, const Point &point);
std::ostream & operator << (std::ostream &out, const Shape &shape);
std::ostream & operator << (std::ostream &out, const Counts &counts);

#endif
