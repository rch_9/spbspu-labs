#include "Functor.h"

#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <ctime>


int get_random();
void Fill(std::vector<int> & Vector);

int main()
{
	srand((int)time(0u));
	std::vector<int> myVector_15(15), myVector_1(1), myVector_0;
	Functor myFunctor_15, myFunctor_1, myFunctor_0;

	Fill(myVector_15);
	Fill(myVector_1);
	Fill(myVector_0);

	std::copy(myVector_15.begin(), myVector_15.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;
	std::copy(myVector_1.begin(), myVector_1.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;
	std::copy(myVector_0.begin(), myVector_0.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	myFunctor_15 = std::for_each(myVector_15.begin(), myVector_15.end(), Functor());
	myFunctor_1 = std::for_each(myVector_1.begin(), myVector_1.end(), Functor());
	myFunctor_0 = std::for_each(myVector_0.begin(), myVector_0.end(), Functor());

	std::cout << myFunctor_15;
	std::cout << myFunctor_1;
	std::cout << myFunctor_0;

	return 0;
}

int get_random()
{
	return rand() % (1001) - 500;
}

void Fill(std::vector<int> & Vector)
{
	std::generate(Vector.begin(), Vector.end(), get_random);
}
