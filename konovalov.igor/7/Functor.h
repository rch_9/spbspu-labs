#ifndef FUNCTOR_H
#define FUNCTOR_H

#include <iostream>
#include <iomanip>

struct Functor
{
	Functor();
	void operator () (const int &value)
	{
	    if (_is_first) {
            max_value = value;
            min_value = value;
	    } else {
            if (value > max_value) {
                max_value = value;
            }
            if (value < min_value) {
                min_value = value;
            }
        }
		if (value > 0) {
			positive_num++;
		}
		else if (value < 0) {
			negative_num++;
		}
		if (value % 2 == 0) {
			even_sum += value;
		}
		else {
			odd_sum += value;
		}

		count++;
		_is_first = false;
		if (count == 1) {
			first = value;
		}
		first_equal_last = (first == value);

		average = (float)(odd_sum + even_sum) / count;
	}
	inline friend std::ostream & operator << (std::ostream &, const Functor &);

private:
	int max_value, min_value, odd_sum, even_sum;
	float average;
	unsigned int positive_num, negative_num;
	bool first_equal_last;

	bool _is_first;		//after processing with first element will false
	unsigned int count;
	int first;
};


//-----konig----2015----------------//
//-----Functor-----Impl------------//
//------------------spbspu---------//


Functor::Functor()
	: max_value(0), min_value(0),
	odd_sum(0), even_sum(0),
	average(0), positive_num(0), negative_num(0),
	first_equal_last(false),
	_is_first(true), count(0), first(0)
{};

inline std::ostream & operator << (std::ostream & out, const Functor &func)
{
	if (func._is_first) {
		out << "Empty sequence.\n\n";
		return out;
	}

	out << "Statistics: \n";
	out << "max - " << func.max_value << " min - " << func.min_value;
	out << "\naverage - " << std::setprecision(3) << func.average;
	out << "\npositive_num - " << func.positive_num << " negative_num - " << func.negative_num;
	out << "\neven_sum - " << func.even_sum << " odd_sum - " << func.odd_sum;
	out << "\nfirst equal last - " << std::boolalpha << func.first_equal_last;
	out << "\n\n";
	return out;
}

#endif
