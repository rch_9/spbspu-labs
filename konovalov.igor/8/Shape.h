#ifndef SHAPE_H
#define SHAPE_H

#include "boost\shared_ptr.hpp"

class Shape
{
public:
	Shape(int x, int y);

	inline bool IsMoreLeft(const boost::shared_ptr<const Shape> & rhs) const;
	inline bool IsUpper(const boost::shared_ptr<const Shape> & rhs) const;

	virtual void Draw() const = 0;
	friend std::ostream & operator << (std::ostream &, const Shape &);

protected:
	virtual ~Shape()
	{};
	int _x, _y;
};


inline bool Shape::IsMoreLeft(const boost::shared_ptr<const Shape> & rhs) const
{
	return _x < rhs.get()->_x;
};

inline bool Shape::IsUpper(const boost::shared_ptr<const Shape> & rhs) const
{
	return _y > rhs.get()->_y;
}

#endif
