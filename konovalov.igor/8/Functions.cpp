#include "Figures.h"
#include "Shape.h"
#include "Functions.h"
#include <vector>
#include "boost\shared_ptr.hpp"
#include "boost\bind.hpp"
#include <algorithm>
#include <iostream>

void Fill(std::vector<pointer> &vector)
{
	std::generate(vector.begin(),vector.end(),Generator());
}

void Print(std::vector<pointer> &vector)
{
	std::for_each(vector.begin(), vector.end(), Printer());
	std::cout << "\n\n";
}

void Sort_and_Print(std::vector<pointer> &vector)
{
	std::cout << "\tLeft->Right:\n";
	std::sort(vector.begin(), vector.end(), boost::bind(Comparator(), _1, _2, true));
	Print(vector);

	std::cout << "\tRight->Left:\n";
	std::sort(vector.begin(), vector.end(), boost::bind(Comparator(), _2, _1, true));
	Print(vector);

	std::cout << "\tUp->Down:\n";
	std::sort(vector.begin(), vector.end(), boost::bind(Comparator(), _1, _2, false));
	Print(vector);

	std::cout << "\tDown->Up:\n";
	std::sort(vector.begin(), vector.end(), boost::bind(Comparator(), _2, _1, false));
	Print(vector);
}

