#ifndef FIGURES_H
#define FIGURES_H

#include "Shape.h"

class Circle :
	public Shape
{
public:
	Circle(int x, int y);

	void Draw() const;
};

class Triangle :
	public Shape
{
public:
	Triangle(int x, int y);

	void Draw() const;
};

class Square :
	public Shape
{
public:
	Square(int x, int y);

	void Draw() const;
};


#endif
