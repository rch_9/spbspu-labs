#include <algorithm>
#include <vector>
#include <iostream>
#include <iterator>
#include <functional>
#include <stdexcept>

#include "Figures.h"
#include "Functions.h"
#include "Shape.h"
#include <ctime>
#include <cmath>


int main()
{
	try {
		srand((int)time(0u));
		//----TASK_1------//
		std::vector<double> myvector;
		double i = 1.234;
		for (size_t j = 0; j < 7; j++, i *= 2) {
			myvector.push_back(i);
		}

		std::cout << "\tTASK 1:\n\n";
		std::copy(myvector.begin(), myvector.end(), std::ostream_iterator<double>(std::cout, " "));
		std::cout << "\n\n";
		std::transform(myvector.begin(), myvector.end(), myvector.begin(), std::bind2nd(std::multiplies<double>(), M_PI));
		std::copy(myvector.begin(), myvector.end(), std::ostream_iterator<double>(std::cout, " "));
		std::cout << "\n\n";

		//----TASK_2------//
		std::vector<pointer> ShapeVector(10);

		std::cout << "\tTASK 2: \n\n";
		Fill(ShapeVector);
		Print(ShapeVector);
		Sort_and_Print(ShapeVector);
	}
	catch (const std::exception &e) {
		std::cerr << e.what();
		return 1;
	}

	return 0;
}
