#include "Shape.h"


//-----konig----2015----------------//
//-----Shape-------Impl------------//
//------------------spbspu---------//


Shape::Shape(int x, int y)
	: _x(x), _y(y)
{};

std::ostream & operator << (std::ostream &out, const Shape &shape)
{
	shape.Draw();
	return out;
}
