#include <iostream>
#include <iomanip>
#include "Shape.h"
#include "Figures.h"


//-----konig----2015----------------//
//-----Circle------Impl------------//
//------------------spbspu---------//


Circle::Circle(int x, int y)
	: Shape(x, y)
{};

void Circle::Draw() const
{
	std::cout << "Circle:   [ " << std::setw(2) << _x << " , " << std::setw(2) << _y << " ]\n";
}


//-----konig----2015----------------//
//-----Triangle----Impl------------//
//------------------spbspu---------//


Triangle::Triangle(int x, int y)
	: Shape(x, y)
{};

void Triangle::Draw() const
{
	std::cout << "Triangle: [ " << std::setw(2) << _x << " , " << std::setw(2) << _y << " ]\n";
}


//-----konig----2015----------------//
//-----Square------Impl------------//
//------------------spbspu---------//


Square::Square(int x, int y)
	: Shape(x, y)
{};

void Square::Draw() const
{
	std::cout << "Square:   [ " << std::setw(2) << _x << " , " << std::setw(2) << _y << " ]\n";
}
