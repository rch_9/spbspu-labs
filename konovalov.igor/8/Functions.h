#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cstdlib>
#include "Shape.h"
#include <vector>
#include "boost\shared_ptr.hpp"
#include <iostream>

typedef boost::shared_ptr<Shape> pointer;

struct Generator :
	std::unary_function < pointer, void >
{
	pointer operator () (void)
	{
	    pointer ptr;
		int type = rand() % 3;
		switch (type) {
		case 0:
			ptr.reset(new Circle(rand() % 21, rand() % 21));
			break;
		case 1:
			ptr.reset(new Triangle(rand() % 21, rand() % 21));
			break;
		case 2:
			ptr.reset(new Square(rand() % 21, rand() % 21));
			break;
		default:
			std::cerr << "Bad operation - exit.\n";
		}
		return ptr;
	}
};

void Fill(std::vector<pointer> &vector);


struct Printer:
	std::unary_function < pointer , void >
{
	void operator () (const pointer &ptr)
	{
		ptr->Draw();
	}
};

void Print(std::vector<pointer> &vector);

struct Comparator :
	std::binary_function < pointer, pointer, bool >
{
	bool operator () (const pointer &lhs, const pointer &rhs, bool sort_type)
	{
		if (sort_type) {
			return lhs->IsMoreLeft(rhs);
		}
		else {
			return lhs->IsUpper(rhs);
		}
	}
};

void Sort_and_Print(std::vector<pointer> &vector);

#endif
