#include "PhonebooK.h"
#include <iostream>

int main()
{
    Phonebook myPBook;

    myPBook.Push_Back(Phonebook::value_type("Slava","89110293412"));
    myPBook.Push_Back(Phonebook::value_type("Denis","89012354723"));

    Phonebook::iterator iter1 = myPBook.begin();
    std::cout << "--from iter1 at begin: \n";
    std::cout << *iter1;

    iter1++;
    std::cout << "--from iter1 at 2nd element: \n";
    std::cout << *iter1;

    myPBook.Push_Back(Phonebook::value_type("Aleksey","89441234567"));

    Phonebook::iterator iter2 = myPBook.end();
    std::cout << "--from iter2 at last element: \n";
    std::cout << *(--iter2);

    std::cout << "--print all elements of Pbook: \n";
    myPBook.PrintAll();

    //let's modify!!
    std::cout << "Modify Current - was: \n" << *iter1;
    myPBook.modifyRecord(Phonebook::value_type("Someone","89000000000"),iter1);
    std::cout << "Now: \n" << *iter1;

    std::cout << "InsertBefore: \n";
    myPBook.modifyRecord(Phonebook::value_type("Stas","89234325623"),iter2,Phonebook::Before);

    std::cout << "--print all elements of Pbook: \n";
    myPBook.PrintAll();

    std::cout << "InsertAfter: \n";
    myPBook.modifyRecord(Phonebook::value_type("David","89125461232"),iter1,Phonebook::After);

    std::cout << "--print all elements of Pbook: \n";
    myPBook.PrintAll();

    return 0;
}
