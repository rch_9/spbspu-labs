#include "PhonebooK.h"
#include <string>
#include <iostream>
#include <stdexcept>

///----This is a PRECORD IMPL------//


Phonebook::PRecord::PRecord(const std::string &name, const std::string &number)
	: _name(name),
	_number(number)
{};

bool Phonebook::PRecord::operator != (const Phonebook::PRecord & Rec)
{
	if (this->_name != Rec._name || this->_number != Rec._number) {
		return true;
	}
	return false;
}

std::ostream & operator << (std::ostream &out, const Phonebook::PRecord & Rec)
{
    out << "Name: " << Rec._name << std::endl;
	out << "Number: " << Rec._number << std::endl << std::endl;
	return out;
}


///----This is a PHONEBOOK IMPL-----//


Phonebook::iterator Phonebook::begin()
{
	return PbooK.begin();
};

Phonebook::iterator Phonebook::end()
{
	return PbooK.end();
};

void Phonebook::modifyRecord(Phonebook::value_type newRec, Phonebook::iterator pCurrRec, Phonebook::insert_type insert)
{
	if (PbooK.empty()) {
		throw std::runtime_error("Can't modify: storage is empty.");
	}

	switch (insert) {
	case Curr:
	{
		pCurrRec->_name = newRec._name;
		pCurrRec->_number = newRec._number;
		break;
	}
	case Before:
	{
		PbooK.insert(pCurrRec, newRec);
		break;
	}
	case After:
	{
	    if (pCurrRec != PbooK.end()) {
            PbooK.insert(++pCurrRec, newRec);
            break;
	    }
	}
	default:
	{
		throw std::invalid_argument("Incorrect insert_type.");
	}
	}
};

void Phonebook::Push_Back(Phonebook::value_type newRec)
{
	PbooK.push_back(newRec);
};

void Phonebook::PrintAll()
{
    for (Phonebook::iterator iter = PbooK.begin(); iter != PbooK.end(); ++iter) {
        std::cout << *iter;
    }
}
