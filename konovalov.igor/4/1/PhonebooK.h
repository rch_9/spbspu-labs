#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <list>
#include <string>

class Phonebook
{
private:
	struct PRecord;
public:
	typedef PRecord value_type;
	typedef std::list<value_type> Storage;
	typedef Storage::iterator iterator;

	typedef enum {
		Curr,
		Before,
		After
	} insert_type;

private:

	Storage PbooK;

public:

	iterator begin();
    iterator end();

	void modifyRecord(value_type, iterator, insert_type = Curr);
	void Push_Back(value_type);

	friend std::ostream & operator << (std::ostream &, const Phonebook::PRecord &);
	void PrintAll();
};


struct Phonebook::PRecord
{
	std::string _name;
	std::string _number;
	PRecord(const std::string &name, const std::string &number);
	bool operator != (const PRecord &);
};

#endif
