#ifndef CONTAINER_H
#define CONTAINER_H

#include <vector>
#include <iterator>

class Container
{
private:

	static int _end_pos;        //mark on the last element of vector
	static std::vector<int> FactVector;

public:

	void InitializeContainer();

	class Iterator;

	Iterator begin() const;
	Iterator end() const;

};


class Container::Iterator :
	public std::iterator<std::bidirectional_iterator_tag, int>
{
public:
	typedef int value_type;
	typedef int * pointer;
	typedef int & reference;
	typedef const int & const_reference;
	typedef Iterator this_type;
	typedef Iterator & this_type_reference;
	typedef const Iterator & this_type_const_reference;

	Iterator();
	explicit Iterator(const_reference curr_pos);
	~Iterator();

	bool operator == (this_type_const_reference);
	bool operator != (this_type_const_reference);

	this_type_reference operator ++();
	this_type_reference operator --();
	this_type operator ++(int i);
	this_type operator --(int i);

	reference operator *();

private:
	int _current_pos;
};

#endif
