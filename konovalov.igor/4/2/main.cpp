#include <iostream>
#include <algorithm>
#include <vector>
#include "Container.h"

int main()
{
	typedef std::vector<int>::iterator iter_vect;
	typedef Container::Iterator Citer;

	try {
		Container myContainer;
		myContainer.InitializeContainer();

		for (Citer iter = myContainer.begin(); iter != myContainer.end(); ++iter) {
			std::cout << *iter << "  ";
		}
		std::cout << std::endl;

		std::vector<int> myVect_1(myContainer.begin(), myContainer.end());

		for (iter_vect iter = myVect_1.begin(); iter != myVect_1.end(); ++iter) {
			std::cout << *iter << "  ";
		}
		std::cout << std::endl;

		Container myContainer2;
		myContainer2.InitializeContainer();
		std::vector<int> myVect_2(10);		//will contain 10 values from 1! to 10!
		iter_vect VITER = myVect_2.begin();

		std::copy(myContainer2.begin(), myContainer2.end(), VITER);

		for (iter_vect iter = myVect_2.begin(); iter != myVect_2.end(); ++iter) {
			std::cout << *iter << "  ";
		}
	}
	catch (const std::exception &e) {
		std::cerr << "Mistake: " << e.what();
		return 1;
	}

	std::cout << std::endl;

	return 0;
}
