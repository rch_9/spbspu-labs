#ifndef COMPILEFACTS_H
#define COMPILEFACTS_H

#ifndef VECTOR
#define VECTOR

template<const int ValuePosition>
struct initFactValue
{
	static const int CurrentValue = ValuePosition*initFactValue<ValuePosition - 1>::CurrentValue;
	static void factUp(std::vector<int> &vector)
	{
		vector[ValuePosition] = CurrentValue;
		initFactValue<ValuePosition - 1>::factUp(vector);
	};
};

template <>
struct initFactValue<0>		//just end of the 0!
{
	static const int CurrentValue = 1;
	static void factUp(std::vector<int> &)
	{};
};

#endif
#endif
