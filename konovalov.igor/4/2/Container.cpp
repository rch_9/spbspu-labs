#include "Container.h"
#include "CompileFacts.h"
#include <stdexcept>
#include <cmath>


//----HERE IS AN ITERATOR IMPLEMENTATION-----//

Container::Iterator::Iterator()
	: _current_pos(0)
{};

Container::Iterator::~Iterator()
{
	_current_pos = 0;
};

Container::Iterator::Iterator(Container::Iterator::const_reference curr_pos)
	: _current_pos(curr_pos)
{};

bool Container::Iterator::operator == (Container::Iterator::this_type_const_reference rhs)
{
	return _current_pos == rhs._current_pos;
};

bool Container::Iterator::operator != (Container::Iterator::this_type_const_reference rhs)
{
	return _current_pos != rhs._current_pos;
};

Container::Iterator::this_type_reference Container::Iterator::operator ++()
{
	if (_current_pos > Container::_end_pos) {
		throw std::out_of_range("Out of range at PREincrement.");
	}

	++_current_pos;
	return *this;
};

Container::Iterator::this_type_reference Container::Iterator::operator --()
{
	if (_current_pos < 1) {
		throw std::out_of_range("Out of range at PREdecrement.");
	}

	--_current_pos;
	return *this;
}

Container::Iterator::this_type Container::Iterator::operator ++(int )
{
	Iterator temp(_current_pos);
	if (_current_pos > Container::_end_pos) {	//Container::_end_pos
		throw std::out_of_range("Out of range at POSTincrement.");
	}

	++_current_pos;
	return temp;
}

Container::Iterator::this_type Container::Iterator::operator --(int )
{
	Iterator temp(_current_pos);
	if (_current_pos < 1) {			//see above
		throw std::out_of_range("Out of range at POSTdecrement.");
	}

	--_current_pos;
	return temp;
}

Container::Iterator::reference Container::Iterator::operator *()
{
	if (_current_pos > Container::_end_pos || _current_pos < 1) {	//see above
		throw std::out_of_range("Can't get a value: out of range.");
	}

	return Container::FactVector[_current_pos];
}


//----HERE IS A CONTAINTER IMPLEMENTATION-----//

int Container::_end_pos = 0;
std::vector<int> Container::FactVector;

void Container::InitializeContainer()
{
	_end_pos = 10;
	FactVector.resize(_end_pos + 1);

	FactVector[0] = 1;
	initFactValue<10>::factUp(FactVector);
}

Container::Iterator Container::begin() const
{
	return Iterator(1);
}

Container::Iterator Container::end() const
{
	return Iterator(_end_pos + 1);
}
