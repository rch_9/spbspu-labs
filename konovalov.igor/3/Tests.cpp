#include <fstream>
#include <memory>
#include "QueueWithPriority.h"
#include "Tests.h"

typedef QueueWithPriority<QueueElem> QWP;

bool DataInputTest(std::ofstream &fout)
{
	fout << "--Testing for correct input of queue elements--" << std::endl;
	QWP Queue;
	QueueElem Q2("Be a Human", 265);
	QueueElem Q3("Raise Your Memory", 789);
	QueueElem Q4("Jump of Freedom", 903);

	Queue.Put(QueueElem("C++03 standart",1260));
	Queue.Put(Q2, QWP::High);
	Queue.Put(Q3, QWP::Normal);
	Queue.Put(Q4, QWP::High);

	if (Queue.Size() == 4) {
		fout << "Test for input has been correctly done.\n\n";
		return true;
	}
	else {
		fout << "!!ERROR during input test: 4 elements have been input in queue, but correcty " \
			<< Queue.Size() << "\n\n";
		return false;
	}
}

bool DataOutputTest(std::ofstream &fout)
{
	fout << "--Testing for correct output of queue elements--" << std::endl;
	QWP Queue;
	QueueElem Q1("C++03 Standart", 1260);
	QueueElem Q2("Be a Human", 265);
	QueueElem Q3("Raise Your Memory", 789);

	Queue.Put(Q1);
	Queue.Put(Q2, QWP::High);
	Queue.Put(Q3, QWP::Normal);

	std::auto_ptr<QueueElem> p1;
	p1 = Queue.Get();
	std::auto_ptr<QueueElem> p2;
	p2 = Queue.Get();
	std::auto_ptr<QueueElem> p3;
	p3 = Queue.Get();

	if (Queue.Size()) {
		fout << "!!ERROR during output test: " << Queue.Size() << " element(s) has(ve) been output instead 3.\n\n";
		return false;
	}
	if (!p1.get() || !p2.get() || !p3.get()) {
		fout << "!!ERROR during output test: element(s) was(were) lost.\n\n";
		return false;
	}
	if (*p1 != Q2 || *p2 != Q3 || *p3 != Q1) {
		fout << "!!ERROR during output test: invalid data output.\n\n";
		return false;
	}
	fout << "Test for output has been correctly done.\n\n";
	return true;
}

bool AccelerateTest(std::ofstream &fout)
{
	fout << "--Testing for correct acceleration in not empty high-priority container--" << std::endl;
	QWP Queue;
	QueueElem Q1("С++03 Standart", 1260);
	QueueElem Q2("Be a Human", 265);
	QueueElem Q3("Raise Your Memory", 789);
	QueueElem Q4("Jump of Freedom", 903);

	Queue.Put(Q1);		//this one has got Low priority
	Queue.Put(Q2, QWP::High);
	Queue.Put(Q3, QWP::Normal);
	Queue.Put(Q4, QWP::High);

	//here we get one high element - Q2
	std::auto_ptr<QueueElem> p1, p2, p3;
	p1 = Queue.Get();
	//so, the high container isn't empty yet, and queue is ready for acceleration
	Queue.Accelerate();

	p2 = Queue.Get();	//should save Q4
	p1 = Queue.Get();	//should save Q1
	p3 = Queue.Get();	//should save Q3

	if (*p1 != Q1 || *p2 != Q4 || *p3 != Q3) {
		fout << "!!ERROR during acceleration test: invalid execution result.\n\n";
		return false;
	}
	else {
		fout << "Acceleration in NOT empty HIGH containter was good.\n\n";
		return true;
	}
}

bool AccelerateToEmptyHighTest(std::ofstream &fout)
{
	fout << "--Testing for correct acceleration in empty high-priority container--" << std::endl;
	QWP Queue;
	QueueElem Q1("C++03 Standart", 1260);
	QueueElem Q2("Be a Human", 265);
	QueueElem Q3("Raise Your Memory", 789);

	Queue.Put(Q1);		//this one has got Low priority
	Queue.Put(Q2, QWP::Low);
	Queue.Put(Q3, QWP::Normal);

	//Accelerate queue with empty HIGH container
	Queue.Accelerate();

	std::auto_ptr<QueueElem> p1, p2, p3;
	p1 = Queue.Get();
	p2 = Queue.Get();
	p3 = Queue.Get();

	if (*p1 != Q1 || *p2 != Q2 || *p3 != Q3) {
		fout << "!!ERROR during acceleration test: invalid execution result.\n\n";
		return false;
	}
	else {
		fout << "Acceleration in EMPTY HIGH containter was good.\n\n";
		return true;
	}
}

bool AccelerateEmptyLowTest(std::ofstream &fout)
{
	fout << "--Testing for correct acceleration of empty low-priority container--" << std::endl;
	QWP Queue;
	QueueElem Q1("C++03 Standart", 1260);
	QueueElem Q2("Be a Human", 265);

	Queue.Put(Q1, QWP::High);
	Queue.Put(Q2, QWP::Normal);

	//Accelerate queue with empty low container
	Queue.Accelerate();

	std::auto_ptr<QueueElem> p1, p2;
	p1 = Queue.Get();
	p2 = Queue.Get();

	if (*p1 != Q1 || *p2 != Q2) {
		fout << "!!ERROR during acceleration test: invalid execution result.\n\n";
		return false;
	}
	else {
		fout << "Acceleration with EMPTY LOW containter was good.\n\n";
		return true;
	}
}

bool GetNullDataTest(std::ofstream &fout)
{
	fout << "--Testing for correct mistake message of getting element from empty queue--" << std::endl;
	QWP Queue;
	std::auto_ptr<QueueElem> p1;

	try {
		p1 = Queue.Get();
	}
	catch (const std::exception &e) {
		fout << e.what();
		fout << "Test of getting data from empty queue was good.\n\n";
		return true;
	}

	if (p1.get()) {
		fout << "!!ERROR during null data getting test: unknown executed data.\n\n";
		return false;
	}
	return true;
}
