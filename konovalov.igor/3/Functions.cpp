#include "Functions.h"
#include <iostream>
#include <cstdlib>

void Fill(std::list<int> &list, size_t size)
{
	//comparison of uint could be 'missed'

	for (size_t i = 0; i < size; i++) {
		list.push_back(int(rand()*(19.) / RAND_MAX + 1));
	}
}

void Print(std::list<int> &list, bool method)	// 0 - in normal way, 1 - with method "first-last.."
{
	typedef std::list<int>::iterator iter;

	if (list.empty()) {
		std::cerr << "Empty list\n";
		return;
	}

	if (!method) {
		for (iter i = list.begin(); i != list.end(); ++i) {
			std::cout << *i << " ";
		}
	}
	else {
		iter i = list.begin();
		iter j = list.end();
		while (i != j) {
			std::cout << *i << " ";
			j--;
			if (i != j) {
				std::cout << *j << " ";
			}
			else {
				break;
			}
			i++;
		}
	}

	std::cout << std::endl;
}


