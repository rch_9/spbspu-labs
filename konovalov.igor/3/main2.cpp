#include <list>
#include <ctime>
#include "Functions.h"

int main()
{
	srand(int(time(0u)));
	std::list<int> list;

	for (size_t i = 0; i <= 15; i++) {
		Fill(list, i);
		Print(list);
		Print(list, 1);
		list.clear();

		if (i == 5) {
			i += 2;
		}
		else if (i == 7) {
			i *= 2;
		}

	}

	return 0;
}
