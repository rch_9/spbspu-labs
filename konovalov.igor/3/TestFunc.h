#ifndef TESTFUNC_H
#define TESTFUNC_H

#include "Tests.h"

static bool (*Test_func[])(std::ofstream &) =
{
	&DataInputTest,
	&DataOutputTest,
	&AccelerateTest,
	&AccelerateToEmptyHighTest,
	&AccelerateEmptyLowTest,
	&GetNullDataTest,
};

#endif // TESTFUNC_H
