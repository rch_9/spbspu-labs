#ifndef QUEUEWITHPRIORITY_H
#define QUEUEWITHPRIORITY_H

#include <deque>
#include <memory>
#include <stdexcept>

template <typename QueueElement>
class QueueWithPriority
{
public:

	typedef enum {
		Low,
		Normal,
		High
	} ElementPriority;

	inline void Put(const QueueElement &, ElementPriority prior = Low);		//with const you can put a temporary Qelem as parameter
	inline std::auto_ptr<QueueElement> Get();

	inline void Accelerate();

	bool Empty() const;
	size_t Size() const;

private:

	std::deque<QueueElement> LowPrior;
	std::deque<QueueElement> NormalPrior;
	std::deque<QueueElement> HighPrior;
};

#endif
