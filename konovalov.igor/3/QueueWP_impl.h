#ifndef QUEUEWP_IMPL_H
#define QUEUEWP_IMPL_H

#include "QueueWithPriority.h"

//-----------QUEUE_WITH_PRIORITY----------//

template <typename QueueElement>
inline void QueueWithPriority<QueueElement>::Put(const QueueElement &element, ElementPriority prior)
{
	switch (prior) {
	case Low:
	{
		LowPrior.push_back(element);
		break;
	}
	case Normal:
	{
		NormalPrior.push_back(element);
		break;
	}
	case High:
	{
		HighPrior.push_back(element);
		break;
	}
	}
}


template <typename QueueElement>
bool QueueWithPriority<QueueElement>::Empty() const
{
	if (LowPrior.empty() && NormalPrior.empty() && HighPrior.empty()) {
		return true;
	}
	return false;
}

template <typename QueueElement>
size_t QueueWithPriority<QueueElement>::Size() const
{
	return HighPrior.size() + NormalPrior.size() + LowPrior.size();
}


template <typename QueueElement>
inline std::auto_ptr<QueueElement> QueueWithPriority<QueueElement>::Get()
{
	std::auto_ptr<QueueElement> element;

	if (Empty()) {
		throw std::runtime_error("Error! Deque is empty.\n");
	}

	if (!HighPrior.empty()) {
		element.reset(new QueueElement(HighPrior.front()));
		HighPrior.pop_front();
	}
	else if (!NormalPrior.empty()) {
		element.reset(new QueueElement(NormalPrior.front()));
		NormalPrior.pop_front();
	}
	else {
		element.reset(new QueueElement(LowPrior.front()));
		LowPrior.pop_front();
	}

	return element;
}


template <typename QueueElement>
inline void QueueWithPriority<QueueElement>::Accelerate()
{
	if (LowPrior.empty()) {
		return;
	}

	if (HighPrior.empty()) {
		std::swap(HighPrior, LowPrior);
	}
	else {
		HighPrior.insert(HighPrior.end(), LowPrior.begin(), LowPrior.end());
		LowPrior.clear();
	}
}

#endif
