#include "QueueWP_impl.h"
#include "Tests.h"
#include "TestFunc.h"
#include <iostream>

int main()
{
	size_t Mistakes = 0;
	std::ofstream out("Log.txt");

	for (size_t j = 0; j < sizeof(Test_func) / sizeof(Test_func[0]); ++j) {
		if (!Test_func[j](out)) {
			++Mistakes;
		}
	}

	std::cout << "\n---\t" << Mistakes << "\t--- error(s) was(were) found. \n\n";

	if (Mistakes) {
		return 1;
	}
	else {
		return 0;
	}
}
