#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <list>
#include <cstdlib>

void Fill(std::list<int> &list, size_t size);
void Print(std::list<int> &list, bool method = 0);

#endif // FUNCTIONS_H
