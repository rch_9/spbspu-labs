#ifndef TESTS_H
#define TESTS_H

#include "QueueWithPriority.h"
#include "QueueWP_impl.h"
#include <fstream>
#include <memory>


struct QueueElem
{
	std::string _title;
	int _pages;
	QueueElem()
		: _pages(0)
	{}
	QueueElem(const std::string &title, int pages)
		: _title(title),
		_pages(pages)
	{}
	bool operator != (QueueElem &QE)
	{
		if (_title != QE._title || _pages != QE._pages) {
			return true;
		}
		return false;
	}
};

bool DataInputTest(std::ofstream &fout);
bool DataOutputTest(std::ofstream &fout);
bool AccelerateTest(std::ofstream &fout);
bool AccelerateToEmptyHighTest(std::ofstream &fout);
bool AccelerateEmptyLowTest(std::ofstream &fout);
bool GetNullDataTest(std::ofstream &fout);

#endif
