#ifndef STRUCTS_H
#define STRUCTS_H

#include <iostream>
#include <string>

static const std::string Table[] = {
	"First string",
	"Give second",
	"Then third",
	"One more string",
	"Information",
	"OOP",
	"5_lab",
	"My algorithm might be the best:)",
	"My name is",
	"Be free"
};

struct DataStruct
{
    DataStruct() :
        key1(0), key2(0)
    {};
    DataStruct(int _key1,int _key2,const std::string &string) :
        key1(_key1), key2(_key2),
        str(string)
    {};
	int key1;
	int key2;
	std::string str;
	inline friend std::ostream & operator << (
        std::ostream &out, const DataStruct & DS);
};

//should this be in other .hpp??
inline std::ostream & operator << (
	std::ostream &out, const DataStruct & DS)
{
	out << "Struct: \n";
	out << "Keys: " << DS.key1 << " " << DS.key2;
	out << "\tStr: " << DS.str << std::endl;
	return out;
}

#endif // STRUCTS_H
