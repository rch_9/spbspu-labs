#ifndef GENERATOR_H
#define GENERATOR_H

#include <string>
#include <cstdlib>
#include "Structs.h"

struct DSgenerator:
    std::unary_function < DataStruct, void >
{
    const DataStruct operator () (void)
    {
        return DataStruct(rand() % 6 - 5, rand() % 6 - 5, Table[rand() % 10]);
    }
};

#endif // GENERATOR_H
