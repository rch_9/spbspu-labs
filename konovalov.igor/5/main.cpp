#include <vector>
#include <ctime>
#include <algorithm>
#include <iterator>
#include <iostream>

#include "Structs.h"
#include "Comparator.h"
#include "Generator.h"

int main()
{
	srand((int)time(0u));
	std::vector<DataStruct> myVector(rand() % 16 + 5);

	std::generate(myVector.begin(),myVector.end(),DSgenerator());

	std::copy(myVector.begin(),myVector.end(),std::ostream_iterator<DataStruct>(std::cout));
	std::cout << std::endl;

	std::sort(myVector.begin(), myVector.end(), Comparator());

	std::copy(myVector.begin(),myVector.end(),std::ostream_iterator<DataStruct>(std::cout));
	std::cout << std::endl;

	return 0;
}
