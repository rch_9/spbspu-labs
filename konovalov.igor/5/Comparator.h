#ifndef COMPARATOR_H
#define COMPARATOR_H

#include <string>
#include "Structs.h"

struct Comparator:
	std::binary_function<DataStruct,DataStruct, bool>
{
	bool operator () (
		const DataStruct & lhs,
		const DataStruct & rhs) const
	{
		if (lhs.key1 != rhs.key1) {
			return lhs.key1 < rhs.key1;
		}
		else if (lhs.key2 != rhs.key2) {
			return lhs.key2 < rhs.key2;
		}
		else {
			return lhs.str.size() < rhs.str.size();
		}
	}
};

#endif
