#include "Sort_Vector_fList.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include <stdexcept>

static void FillRandom(double * array, size_t size);

int main()
{
	srand(int(time(0u)));
	std::vector<double> Vector;

	try {
		for (int sizec = 5, i = 0; i < 5; sizec *= 2, ++i) {
			if (sizec == 20) {
				sizec += 5;
			}
			std::cout << std::endl;
			Vector.resize(sizec);
			FillRandom(&Vector[0], sizec);
			VectorOut<iterPolicy>(Vector); //before sort
			sort<indPolicy>(Vector);
			std::cout << "\nSort: \n";
			VectorOut<iterPolicy>(Vector); //after sort
			Vector.clear();
		}
	}
	catch (const std::exception &e) {
		std::cerr << "Mistake: " << e.what();
		return 1;
	}

	return 0;
}


static void FillRandom(double * array, size_t size)
{
	if (!array) {
		throw std::invalid_argument("Error");
	}

	for (size_t i = 0; i < size; ++i) {
		*(array + i) = rand()*(2.) / RAND_MAX - 1;
	}
}

