#include "boost\shared_array.hpp"
#include "Sort_Vector_fList.h"
#include <vector>
#include <stdexcept>
#include <cstdio>

boost::shared_array<char> FRead(const char *fn, size_t &size)
{
	if (fn == 0) {
		throw std::invalid_argument("Bad filename");
	}
	FILE * f = fopen(fn, "r");
	if (!f) {
		throw std::runtime_error("Error of opening file");
	}

	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f,0,0);

	boost::shared_array<char> Array(static_cast<char*>(malloc(size)),&free);
	//Is it still being UB??

	size_t i = fread(Array.get(),sizeof(char),size,f);
	if (i != size && ferror(f)) {
		fclose(f);
		throw std::runtime_error("Error of reading file.");
	}

	fclose(f);
	return Array;
}

int main()
{
	size_t size = 0;
	boost::shared_array<char> Array;

	try {
		Array = FRead("input.txt",size);
		std::vector<char> Vector(Array.get(), Array.get() + size - 1);		//minus eof 'char'
		VectorOut<indPolicy>(Vector);
	}
	catch (const std::bad_alloc &ba) {
		std::cerr << ba.what() << std::endl;
		return 2;
	}
	catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}
