#ifndef FORWARD_LIST_H
#define FORWARD_LIST_H

#include <iterator>
#include <cassert>

template <typename T>
class Flist
{
private:

	struct node_t
	{
		T _value;
		node_t * _next;
		node_t();
		explicit node_t(const T &);
	};

	node_t * HEAD;

	Flist(const Flist<T> &);
	Flist<T> & operator = (const Flist<T> &);

public:

	//-----C++03----konig--------------------2015//
	//---------iterator-----DECLARATION----//
	//-----------------------------spbspu-------//

	class iterator :
		public std::iterator < std::forward_iterator_tag, T >
	{
	public:
		typedef node_t node_type;
		typedef iterator this_type;
		typedef const this_type & const_reference_tt;

		inline iterator();
		inline iterator(node_type * node_pointer);

		inline bool operator == (const_reference_tt rhs) const;
		inline bool operator != (const_reference_tt rhs) const;

		inline T & operator *() const;
		inline T * operator ->() const;

		inline this_type & operator ++();
		inline this_type operator ++(int);

	private:
		node_type * current_;
	};

	//-----C++03----konig--------------------2015//
	//---------FLIST-------DECLARATION----------//
	//-----------------------------spbspu-------//

	typedef T value_type;

	inline Flist();
	inline ~Flist();

	inline iterator begin() const;
	inline iterator end() const;

	inline void push_back(const value_type &);
	inline bool empty();
	inline void clear();

};


//-----C++03----konig--------------------2015//
//---------NODE_T-----IMPLEMENTATION--------//
//-----------------------------spbspu-------//

template<typename T>
Flist<T>::node_t::node_t()
	: _value(0),
	_next(0)
{}

template<typename T>
Flist<T>::node_t::node_t(const T &value)
	: _value(value),
	_next(0)
{}


//-----C++03----konig--------------------2015//
//-----iterator--------IMPLEMENTATION-//
//-----------------------------spbspu-------//

template<typename T>
inline Flist<T>::iterator::iterator()
	: current_(0)
{}

template<typename T>
inline Flist<T>::iterator::iterator(
	node_type * node_pointer)
	: current_(node_pointer)
{}

template<typename T>
inline bool Flist<T>::iterator::operator == (
	const_reference_tt rhs) const
{
	return current_ == rhs.current_;
}

template<typename T>
inline bool Flist<T>::iterator::operator != (
	const_reference_tt rhs) const
{
	return current_ != rhs.current_;
}

template<typename T>
inline T & Flist<T>::iterator::operator *() const
{
	assert(current_ != 0);

	return current_->_value;
}

template<typename T>
inline T * Flist<T>::iterator::operator ->() const
{
	assert(current_ != 0);

	return &current_->_value;
}

template<typename T>
inline typename Flist<T>::iterator &
Flist<T>::iterator::operator ++()
{
	assert(current_ != 0);
	current_ = current_->_next;
	return *this;
}

template<typename T>
inline typename Flist<T>::iterator::this_type
Flist<T>::iterator::operator ++(int)
{
	this_type temp(*this);
	this->operator++();
	return temp;
}


//-----C++03----konig--------------------2015//
//----------Flist------------IMPLEMENTATION-//
//-----------------------------spbspu-------//

template<typename T>
inline Flist<T>::Flist()
	: HEAD(0)
{}

template<typename T>
inline typename Flist<T>::iterator Flist<T>::begin() const
{
	return iterator(HEAD);
}

template<typename T>
inline typename Flist<T>::iterator Flist<T>::end() const
{
	return iterator(0);
}

template<typename T>
inline bool Flist<T>::empty()
{
	return HEAD == 0;
}

template<typename T>
inline void Flist<T>::push_back(const T & value)
{
	if (!HEAD) {
		HEAD = new node_t(value);
	}
	else {
		node_t * temp = HEAD;
		while (temp->_next) {
			temp = temp->_next;
		}
		temp->_next = new node_t(value);
	}
}

template<typename T>
inline void Flist<T>::clear()
{
	if (HEAD) {
		node_t * temp = HEAD->_next;
		while (HEAD) {
			delete HEAD;
			HEAD = temp;
			if (temp) {
				temp = temp->_next;
			}
		}
	}
}

template<typename T>
inline Flist<T>::~Flist()
{
	clear();
}


#endif
