#include "Sort_Vector_fList.h"
#include <vector>
#include "Forward_list.h"

int main()
{
	try {
		int Arr1[] = { 5, 8, 2, 100, -4, 0, 2 };
		int Arr2[] = { -2, 4, 1, 2, 0, -4, 0 };
		std::vector<int> Vector_m(Arr1, Arr1 + sizeof(Arr1) / sizeof(Arr1[0]));
		std::vector<int> Vector_n(Arr2, Arr2 + sizeof(Arr2) / sizeof(Arr2[0]));
		Flist<int> myFL;

		myFL.push_back(2);
		myFL.push_back(5);
		myFL.push_back(-2);
		myFL.push_back(3);

		//Before:
		VectorOut<iterPolicy>(Vector_m);
		VectorOut<atPolicy>(Vector_n);
		VectorOut<iterPolicy>(myFL);

		//Sorting:
		sort<indPolicy>(Vector_m);
		sort<iterPolicy>(Vector_n);
		sort<iterPolicy>(myFL);

		//After:
		VectorOut<iterPolicy>(Vector_m);
		VectorOut<atPolicy>(Vector_n);
		VectorOut<iterPolicy>(myFL);
	}
	catch (const std::bad_alloc &ba) {
		std::cerr << "Mistake of memory using: " << ba.what();
		return 2;
	}
	catch (const std::exception &e) {
		std::cerr << "Mistake: " << e.what();
		return 1;
	}

	return 0;
}
