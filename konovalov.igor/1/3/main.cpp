#include "Sort_Vector_fList.h"
#include <iostream>
#include <vector>
#include <stdexcept>

template <typename T> void VectorPut(std::vector<T> &Vect)
{
	T temp = 1;
	do {
		if (!(std::cin >> temp)) {
			break;
		}
		if (temp) {
			Vect.push_back(temp);
		}
	} while (temp);
	if (temp && !(std::cin.eof())) {
		throw std::runtime_error("Error of reading");
	}
}

template <typename T> void VectorChange(std::vector<T> &Vect)
{
	typedef typename std::vector<T>::iterator iterator;

	if (Vect.empty()) {
		throw std::invalid_argument("Empty vector.");
	}

	switch (Vect.back()) {

	case 1:
	{
		iterator iter = Vect.begin();

		while (iter != Vect.end()) {
			if (*iter % 2 == 0) {
				iter = Vect.erase(iter);
			}
			else {
				++iter;
			}
		}
		break;
	}

	case 2:

	{
		iterator iter = Vect.begin();
		while (iter != Vect.end()) {
			if (*iter % 3 == 0) {
				iter = Vect.insert(++iter, 3, 1);
			}
			else {
				iter++;
			}
		}
		break;
	}

	default:
	{
		std::cout << "Nothing to change here :(\n";
	}
	//end of switch
	}
}

int main()
{
	std::vector<int> A;

	try {
		VectorPut(A);
		std::cout << std::endl;
		VectorOut<indPolicy>(A);
		VectorChange(A);
		VectorOut<iterPolicy>(A);
	}
	catch (const std::exception &e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}
