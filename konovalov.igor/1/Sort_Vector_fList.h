#ifndef SORT_VECTOR_FLIST_H
#define SORT_VECTOR_FLIST_H

//this header file also includes policies and function - vector_out

#include <iostream>
#include <stdexcept>

template <class Container>
struct indPolicy
{
	typedef typename Container::size_type pointer;
	typedef typename Container::value_type value;

	static pointer begin(Container & )
	{
		return 0;
	}

	static pointer end(Container & _container)
	{
		return _container.size();
	}

	static value & get_data(Container & _container, pointer position)
	{
		if (position > _container.size()) {
			throw std::out_of_range("Out of range.");
		}
		return _container[position];
	}
};


template <typename Container>
struct atPolicy
{
	typedef typename Container::size_type pointer;
	typedef typename Container::value_type value;

	static pointer begin(Container & )
	{
		return 0;
	}

	static pointer end(Container & _container)
	{
		return _container.size();
	}

	static value & get_data(Container & _container, pointer position)
	{
		return _container.at(position);
	}
};

template <typename Container>
struct iterPolicy
{
	typedef typename Container::iterator pointer;
	typedef typename Container::value_type value;

	static pointer begin(Container & _container)
	{
		return _container.begin();
	}

	static pointer end(Container & _container)
	{
		return _container.end();
	}

	static value & get_data(Container & _container, pointer position)
	{
		if (begin(_container) == end(_container)) {
			throw std::runtime_error("Container is empty.");
		}
		return *position;
	}
};


template <template <typename > class Policy, typename Container>
void sort(Container & data)
{
	typedef Policy<Container> policy;
	typedef typename policy::pointer iter;

	for (iter i = policy::begin(data); i != policy::end(data); ++i) {
		for (iter j = i; j != policy::end(data); ++j) {
			if (policy::get_data(data,j) < policy::get_data(data,i)) {
				std::swap(policy::get_data(data,j), policy::get_data(data,i));
			}
		}
	}
}


template <template <typename> class Policy, typename Container>
void VectorOut(Container &massiv)
{
	typedef Policy<Container> policy;
	typedef typename policy::pointer iter;

	if (policy::begin(massiv)==policy::end(massiv)) {
		 throw std::runtime_error("Empty Massiv!");
	}

	for (iter i = policy::begin(massiv); i != policy::end(massiv); ++i) {
		 std::cout << policy::get_data(massiv, i) << " ";
	}
	std::cout << std::endl;
}

#endif
