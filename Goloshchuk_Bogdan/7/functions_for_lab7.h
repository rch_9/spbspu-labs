#ifndef functions_for_lab7_header
#define functions_for_lab7_header

#include <vector>
#include <iterator>

class my_functor
{
public:

    my_functor();

    void operator () (int value);

    int Max();
    int Min();
    double Mean();
    int PosNum();
    int NegNum();
    long long int EvenSum();
    long long int OddSum();
    bool FirstEqLast();

private:

    int max;
    int min;
    long long int sum_odd;
    long long int sum_even;
    int num_pos;
    int num_neg;
    int first;
    int last;
    bool nstart;
    unsigned int counter;

};

void fillRandom( std::vector<int>::iterator first,
std::vector<int>::iterator last, int min, int max);

#endif
