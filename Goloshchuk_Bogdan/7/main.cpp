#include "functions_for_lab7.h"

#include <iostream>
#include <algorithm>
#include <iomanip>

int main(int , char const * [])
{
    try
    {
        my_functor functor;
        std::vector<int> vec(5);

        fillRandom(vec.begin() , vec.end() , -500 , 500 );

        for( std::vector<int>::iterator i=vec.begin() ; i!=vec.end() ; i++ )
        {
            std::cout<< std::setw(4) << *i << ' ';
        }

        std::cout<< '\n' ;

        functor=for_each( vec.begin() , vec.end() , functor );

        std::cout<< "Max: " << functor.Max() << '\n' ;
        std::cout<< "Min: " << functor.Min() << '\n' ;
        std::cout<< "Mean: " << functor.Mean() << '\n' ;
        std::cout<< "PosNum: " << functor.PosNum() << '\n' ;
        std::cout<< "NegNum: " << functor.NegNum() << '\n' ;
        std::cout<< "EvenSum: " << functor.EvenSum() << '\n' ;
        std::cout<< "OddSum: " << functor.OddSum() << '\n' ;
        std::cout<< "FirstEqLast: " << functor.FirstEqLast() << '\n' ;
    }
    
    catch(const std::exception & e)
    {
        std::cerr<<e.what()<<'\n';
    }

    return 0;
}

