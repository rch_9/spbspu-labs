#include "functions_for_lab7.h"

#include <climits>
#include <cstdlib>
#include <ctime>

my_functor:: my_functor()
{
    min=INT_MAX;
    max=INT_MIN;
    sum_even=0;
    sum_odd=0;
    num_pos=0;
    num_neg=0;
    nstart=false;
    counter=0;
}

void my_functor:: operator () (int value)
{

    counter++;

    if(!nstart)
    {
        nstart=true;
        first=value;
    }

    if(value>max)
    {
        max=value;
    }

    if(value<min)
    {
        min=value;
    }

    if(value>0)
    {
        num_pos++;
    }
    else if(value<0)
    {
        num_neg++;
    }

    if( counter % 2 )
    {
        sum_odd+=value;

    }
    else
    {
        sum_even+=value;

    }

    last=value;
}

int my_functor:: Max()
{
    return max;
}

int my_functor:: Min()
{
    return min;
}

double my_functor:: Mean ()
{
    if(!counter)
    {
        return 0;
    }

    return ( (static_cast<double> (sum_odd + sum_even)) /
    (static_cast<double> (counter)) ) ;
}

int my_functor:: PosNum()
{
    return num_pos;
}

int my_functor:: NegNum()
{
    return num_neg;
}

long long int my_functor:: EvenSum()
{
    return sum_even;
}

long long int my_functor:: OddSum()
{
    return sum_odd;
}

bool my_functor:: FirstEqLast()
{
    return first==last;
}

void fillRandom( std::vector<int> ::iterator first,
std::vector<int> ::iterator last, int min, int max)
{
    srand ( time(NULL) );
    for ( ; first!=last ; first++ )
    {
        *first = (( rand()% (max-min+1) ) + min);
    }
}
