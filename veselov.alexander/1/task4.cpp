#include "util.h"
#include "sort.h"
#include <iostream>
#include <vector>
#include <cstdlib>

void fillRandom(double *array, int size)
{
    for (int i = 0; i < size; ++i)
    {
        array[i] = static_cast<double> (rand()) / static_cast<double> (RAND_MAX) * 2.0 - 1.0;
    }

}

void DoTask4()
{
    int vec_sizes[5] = {5, 10, 25, 50, 100};
    
    for (size_t i = 0; i < 5; ++i)
    {
        std::vector<double> Vector(vec_sizes[i], 0);
        fillRandom(&Vector[0], vec_sizes[i]);
        std::cout << "Vector size " << vec_sizes[i] << "\n";
        PrintContainer(Vector, "\n");
        Sort<indexPolicy>(Vector);
        std::cout << "Sorted Vector\n";
        PrintContainer(Vector, "\n");

    }

}
