#include <iostream>
#include <ctime>
#include <cstdlib>

void DoTask1(void);
void DoTask2(void);
void DoTask3(void);
void DoTask4(void);

int main()
{
    srand(time(0));
    int choice = -1;
    do
    {
        std::cin.clear();
        std::cin.sync();

        std::cout << "Enter task number (0 to exit):\n";
        std::cin >> choice;

        if(std::cin.fail())
        {
            std::cerr << "Input Error\n";
            continue;
        }

        switch (choice)
        {
        case 1:
            DoTask1();
            break;
        case 2:
            DoTask2();
            break;
        case 3:
            DoTask3();
            break;
        case 4:
            DoTask4();
            break;

        }

    } while (choice);
    
    return 0;

}
