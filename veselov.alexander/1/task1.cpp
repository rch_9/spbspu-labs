#include "slist.h"
#include "sort.h"
#include "util.h"
#include <vector>
#include <cstdlib>

template <typename T>
void RandomFill(T &Container)
{
    for(int i = 0; i < 10; ++i)
    {
        Container.push_back(rand() % 100);
    }

}

void DoTask1()
{
    std::vector<int> Vector1;
    std::vector<int> Vector2;
    slist<int> List1;

    RandomFill(Vector1);
    RandomFill(Vector2);
    RandomFill(List1);

    std::cout << "Vector 1: \n";
    PrintContainer(Vector1, " ");
    std::cout << "Vector 2: \n";
    PrintContainer(Vector2, " ");
    std::cout << "List: \n";
    PrintContainer(List1, " ");

    std::cout << "Sorting...\n";
    Sort<indexPolicy>(Vector1);
    Sort<atPolicy>(Vector2);
    Sort<listPolicy>(List1);

    std::cout << "Vector 1: \n";
    PrintContainer(Vector1, " ");
    std::cout << "Vector 2: \n";
    PrintContainer(Vector2, " ");
    std::cout << "List: \n";
    PrintContainer(List1, " ");

}
