#include "util.h"
#include <vector>
#include <iostream>

void ModifyVector(std::vector<int> &Vector)
{
    if (Vector.back() == 1)
    {
        for (std::vector<int>::iterator it = Vector.begin(); it != Vector.end(); )
        {
            if (!(*it % 2))
            {
                it = Vector.erase(it);
            }
            else
            {
                ++it;
            }
        }
    }
    else if (Vector.back() == 2)
    {
        size_t i = 0;
        for (std::vector<int>::iterator it = Vector.begin(); it != Vector.end(); )
        {
            if (!(*it % 3))
            {
                Vector.insert(it + 1, 3, 1);
                i += 3;
                it = Vector.begin() + i;
            }
            else
            {
                it++;
                i++;
            }
        }
    }


}

void DoTask3()
{
    std::cout << "Enter vector values (0 to complete)\n";

    std::vector<int> Vector;

    int inputNumber = -1;
    do
    {
        std::cin >> inputNumber;
        if(std::cin.fail())
        {
            std::cout << "Input Error\n";
            return;
        }

        if (inputNumber)
        {
            Vector.push_back(inputNumber);
        }

    } while (inputNumber);

    if (!Vector.empty())
    {
        std::cout << "Vector: \n";
        PrintContainer(Vector, " ");
        ModifyVector(Vector);
        std::cout << "Modified: \n";
        PrintContainer(Vector, " ");
    }
    else
    {
        std::cout << "Vector is empty\n";
    }
    
}
