#ifndef SLIST_H
#define SLIST_H

#include <iostream>
#include <cassert>

template <typename T> class slist
{
private:
    struct node_t
    {
        T value;
        node_t *next;
        node_t(T val) : value(val), next(0)
        {
        }

    };
    node_t *begin_, *end_;
    size_t size_;

public:
    typedef T value_type;

    class iterator : public std::iterator<std::forward_iterator_tag, T>
    {
    public:
        iterator() : current_(0)
        {
        }

        iterator(node_t *node) : current_(node)
        {
        }

        bool operator ==(const iterator &rhs) const
        {
            return current_ == rhs.current_;
        }

        bool operator !=(const iterator &rhs) const
        {
            return current_ != rhs.current_;
        }

        T& operator *() const
        {
            assert(current_ != 0);
            return current_->value;
        }

        T& operator ->() const
        {
            assert(current_ != 0);
            return &current_->value;
        }

        iterator& operator ++()
        {
            assert(current_ != 0);
            current_ = current_->next;
            return *this;
        }

        iterator operator ++(int)
        {
            iterator temp(*this);
            this->operator ++();
            return temp;
        }
        
    private:
        node_t *current_;

    };

    slist() : begin_(0), end_(0), size_(0)
    {
    }
    
    ~slist()
    {
        while(begin_)
        {
            node_t *curr = begin_->next;
            delete begin_;
            begin_ = curr;
        }
    }

    inline size_t size() const
    {
        return size_;
    }

    inline bool empty() const
    {
        return size_ == 0;
    }
    
    void push_back(T val)
    {
        node_t *node = new node_t(val);
        if(!begin_)
        {
            begin_ = end_ = node;
        }
        else
        {
            end_->next = node;
            end_ = node;
        }
        ++size_;

    }
    
    iterator begin()
    {
        return iterator(begin_);
    }

    iterator end()
    {
        return iterator(0);
    }

};

#endif // SLIST_H
