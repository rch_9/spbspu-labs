#ifndef SORT_H
#define SORT_H

#include <vector>

template <typename T>
struct indexPolicy
{
    typedef typename T::size_type i_type;
    typedef typename T::value_type value_type;

    static i_type begin(T &)
    {
        return 0;
    }

    static i_type end(T &Container)
    {
        return Container.size();
    }

    static value_type& getElement(T &Container, i_type i)
    {
        return Container[i];
    }

};

template <typename T>
struct atPolicy
{
    typedef typename T::size_type i_type;
    typedef typename T::value_type value_type;

    static i_type begin(T &)
    {
        return 0;
    }

    static i_type end(T &Container)
    {
        return Container.size();
    }
    
    static value_type& getElement(T &Container, i_type i)
    {
        return Container.at(i);
    }

};

template <typename T>
struct listPolicy
{
    typedef typename T::iterator i_type;
    typedef typename T::value_type value_type;

    static i_type begin(T &Container)
    {
        return Container.begin();
    }

    static i_type end(T &Container)
    {
        return Container.end();
    }
    
    static value_type& getElement(T &, i_type i)
    {
        return (*i);
    }

};

template <template <typename> class Policy, typename T>
void Sort(T &Container)
{
    if (Container.size() < 2) return;

    typedef Policy<T> CurrentPolicy;
    typedef typename CurrentPolicy::i_type i_type;

    for (i_type i = CurrentPolicy::begin(Container); i != CurrentPolicy::end(Container); ++i)
    {
        i_type it_min = i;
        i_type j = i;
        for (++j; j != CurrentPolicy::end(Container); ++j)
        {
            if (CurrentPolicy::getElement(Container, j) < CurrentPolicy::getElement(Container, it_min))
            {
                it_min = j;
            }
        }

        if (it_min != i)
        {
            std::swap(CurrentPolicy::getElement(Container, i), CurrentPolicy::getElement(Container, it_min));
        }
    }

}

#endif // SORT_H
