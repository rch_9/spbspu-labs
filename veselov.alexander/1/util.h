#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <iostream>

template <typename T>
inline void PrintContainer(T &Container, const char* separator = "")
{    
    for (typename T::iterator it = Container.begin(); it != Container.end(); ++it)
    {
        std::cout << *it << separator;
    }
    std::cout << "\n";

}

inline void PrintArray(double *arr, int size)
{
    for (int i = 0; i < size; ++i)
    {
        std::cout << arr[i] << "\n";
    }

}

#endif // UTIL_H
