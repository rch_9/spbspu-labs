#include "util.h"
#include <fstream>
#include <vector>
#include <boost/shared_array.hpp>

void DoTask2()
{
    std::fstream source("data.txt");

    if (!source)
    {
        std::cerr << "File is not accessible\n";
        return;
    }

    source.seekg(0, source.end);
    int size = static_cast<int>(source.tellg());
    source.seekg(0, source.beg);

    boost::shared_array<char> Data(new char[size]);

    source.read(Data.get(), size);
    source.close();

    std::vector<char> assignVector;
    assignVector.assign(Data.get(), Data.get()+size);

    PrintContainer(assignVector);

}
