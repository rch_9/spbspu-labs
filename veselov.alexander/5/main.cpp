#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <iomanip>

const int g_RandRange = 5;
const int g_TableSize = 10;

const std::string stringTable[] = {
        "I",
        "We",
        "You",
        "This",
        "There",
        "Myself",
        "Another",
        "Anything",
        "Something",
        "Everything"
    };

class DataStruct
{
public:
    DataStruct() :
      key1(rand() % (2 * g_RandRange + 1) - g_RandRange),
      key2(rand() % (2 * g_RandRange + 1) - g_RandRange),
      str(stringTable[rand() % g_TableSize])
    {
    }
    int key1;
    int key2;
    std::string str;

};

struct PrintDataStruct
{
    void operator ()(const DataStruct &data)
    {
        std::cout << "key 1: " << std::setw(2) << data.key1 << " key 2: "
             << std::setw(2) << data.key2 << " str: " << data.str << "\n";
    }

};

struct CompareDataStruct
{
    bool operator ()(const DataStruct &lhs, const DataStruct &rhs)
    {
        return (lhs.key1 < rhs.key1) || ((lhs.key1 == rhs.key1) && (lhs.key2 < rhs.key2))
            || ((lhs.key1 == rhs.key1) && (lhs.key2 == rhs.key2)
            && (lhs.str.length() < rhs.str.length()));
    }

};

int main()
{
    srand(time(NULL));
    std::vector<DataStruct> DSVector;

    for (size_t i = 0; i < 100; ++i)
    {
    	DSVector.push_back(DataStruct());
    }

    std::for_each(DSVector.begin(), DSVector.end(), PrintDataStruct());
    std::sort(DSVector.begin(), DSVector.end(), CompareDataStruct());
    std::cout << std::endl;
    std::for_each(DSVector.begin(), DSVector.end(), PrintDataStruct());

    return 0;

}