#ifndef FUNCTORS_H
#define FUNCTORS_H

#include <iostream>
#include <algorithm>

struct RandomNumberGenerator
{
    RandomNumberGenerator(int n) : range(n)
    {
    }
    int operator ()()
    {
        return rand() % (2 * range + 1) - range;
    }
private:
    int range;

};

struct VectorPrinter
{
    void operator ()(int number)
    {
        std::cout << number << "\n";
    }

};

struct StatCollector
{
    StatCollector() :
        first(0),
        max(0),
        min(0),
        average(0.0),
        count(0),
        pos_count(0),
        neg_count(0),
        even_sum(0),
        odd_sum(0),
        outside_equal(false)
    {
    }

    void operator ()(int number)
    {
        if (count == 0)
        {
            first = number;
            max = number;
            min = number;
        }
        
        if (number > max)
        {
            max = number;
        }

        if (number < min)
        {
            min = number;
        }

        average = ((count * average) + number) / (++count);

        if (number > 0)
        {
            ++pos_count;
        }

        if (number < 0)
        {
            ++neg_count;
        }

        if (number % 2 == 0)
        {
            even_sum += number;
        }
        else
        {
            odd_sum += number;
        }

        outside_equal = (number == first);

    }

    void PrintStat()
    {
        std::cout << "Max: " << max << "\n"
            << "Min: " << min << "\n"
            << "Average: " << average << "\n"
            << "Positive count: " << pos_count << "\n"
            << "Negative count: " << neg_count << "\n"
            << "Even sum: " << even_sum << "\n"
            << "Odd sum: " << odd_sum << "\n"
            << (outside_equal ? "Outside is equal" : "Outside is not equal") << "\n";
    }

private:
    int first;
    int max;
    int min;
    double average;
    int count;
    int pos_count;
    int neg_count;
    int even_sum;
    int odd_sum;
    bool outside_equal;

};

#endif // FUNCTORS_H
