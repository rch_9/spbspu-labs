#include "functors.h"
#include <vector>

int main()
{
    std::vector<int> Vector(1000);
    std::generate(Vector.begin(), Vector.end(), RandomNumberGenerator(500));
    std::for_each(Vector.begin(), Vector.end(), VectorPrinter());
    StatCollector collector = std::for_each(Vector.begin(), Vector.end(), collector);

    collector.PrintStat();

    return 0;
}
