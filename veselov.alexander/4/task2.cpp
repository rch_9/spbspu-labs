#include "factorial_container.h"

#include <iostream>
#include <algorithm>
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(iterator_dereference_test)
{
    CFactorialContainer fc_container;

    for (CFactorialContainer::iterator it = fc_container.begin(); it != fc_container.end(); ++it)
    {
        std::cout << (*it) << "\n";
    }

    std::cout << "\n";
    
}

BOOST_AUTO_TEST_CASE(iterator_stl_test)
{
    CFactorialContainer fc_container;
    std::vector<int> Vector(10);

    std::copy(fc_container.begin(), fc_container.end(), Vector.begin());

    for (std::vector<int>::iterator it = Vector.begin(); it != Vector.end(); ++it)
    {
        std::cout << (*it) << "\n";
    }
}
