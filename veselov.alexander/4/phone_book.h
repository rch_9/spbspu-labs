#ifndef PHONE_BOOK_H
#define PHONE_BOOK_H

#include <iterator>
#include <iostream>
#include <vector>
#include <string>

class CPhoneBook
{
public:
    class CRecord
    {
    public:
        CRecord(std::string name, std::string number) :
            name_(name),
            number_(number)
        {
        }
    
        friend std::ostream & operator << (std::ostream &os, const CPhoneBook::CRecord &record);
    private:
        std::string name_;
        std::string number_;
    
    };

    typedef std::vector<CRecord> container_type;
    typedef container_type::iterator iterator;
    
    void push_back(const CRecord &record)
    {
        storage_.push_back(record);
    }

    void push_back(const std::string &name, const std::string &number)
    {
        storage_.push_back(CRecord(name, number));
    }
    
    void modify(iterator it, const CRecord &record)
    {
        *(it) = record;
    }
    
    iterator begin()
    {
        return storage_.begin();
    }
   
    iterator end()
    {
        return storage_.end();
    }
    
    iterator insert_before(iterator it, const CRecord &record)
    {
        return storage_.insert(it, record);
    }

    iterator insert_after(iterator it, const CRecord &record)
    {
        return storage_.insert(++it, record);
    }
    
private:
    container_type storage_;

};

std::ostream & operator << (std::ostream &os, const CPhoneBook::CRecord &record)
{
    return os << "Name: " << record.name_ << " Number: " << record.number_ << "\n";
}

#endif // PHONE_BOOK_H
