#include "phone_book.h"
#include <iostream>

#ifndef _WIN32
#define BOOST_TEST_DYN_LINK
#endif // _WIN32

#define BOOST_TEST_MODULE test4
#include <boost/test/included/unit_test.hpp>


BOOST_AUTO_TEST_CASE(phonebook_test_push_back)
{
    CPhoneBook phonebook;
    phonebook.push_back("IMEI", "*#06#");
    phonebook.push_back("Hair", "555-3298");
    phonebook.push_back("Random Number", "555-1000");
    phonebook.push_back("Emergency Number", "9-1-1");
    
    std::copy(phonebook.begin(), phonebook.end(), std::ostream_iterator<CPhoneBook::CRecord>(std::cout));

    std::cout << "\n";

}

BOOST_AUTO_TEST_CASE(phonebook_test_insert)
{
    CPhoneBook phonebook;
    phonebook.push_back("Record 1", "000-000");
    
    phonebook.insert_after(phonebook.begin(), CPhoneBook::CRecord("Record_After", "222-222"));
    phonebook.insert_before(phonebook.begin(), CPhoneBook::CRecord("Record_Before", "111-111"));

    std::copy(phonebook.begin(), phonebook.end(), std::ostream_iterator<CPhoneBook::CRecord>(std::cout));

    std::cout << "\n";

}

BOOST_AUTO_TEST_CASE(phonebook_modify_test)
{
    CPhoneBook phonebook;
    phonebook.push_back("Record 1", "+7 (800) 000-00-00");
    phonebook.push_back("Record 2", "+7 (800) 010-10-10");
    phonebook.push_back("Record 3", "+7 (800) 001-01-01");
    phonebook.push_back("Record 4", "+7 (800) 220-20-20");
    
    phonebook.modify(phonebook.begin(), CPhoneBook::CRecord("Recard 1 (Modefaied)", "+1(80 0)0 00- 00-01"));
    phonebook.modify(phonebook.begin(), CPhoneBook::CRecord("Record 1 (Modified)", "+1 (800) 000-00-01"));

    phonebook.modify(phonebook.begin() + 2, CPhoneBook::CRecord("Record 3 (Modified)", "+5 (500) 555-55-55"));


    std::copy(phonebook.begin(), phonebook.end(), std::ostream_iterator<CPhoneBook::CRecord>(std::cout));

    std::cout << "\n";

}

