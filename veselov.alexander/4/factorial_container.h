#ifndef FACTORIAL_CONTAINER_H
#define FACTORIAL_CONTAINER_H

#include <iterator>
#include <cassert>

class CFactorialContainer
{
public:
    typedef int value_type;

    class iterator : public std::iterator<std::bidirectional_iterator_tag, value_type>
    {
    public:
        iterator() : arg_(1), value_(1)
        {
        }

        iterator(size_t n) : arg_(n), value_(1)
        {
            for (size_t i = 2; i <= n; ++i)
                value_ *= i;
        }

        value_type operator *() const
        {
            assert(arg_ > 0 && arg_ < 11);
            
            return value_;
        }

        iterator& operator ++()
        {
            assert(arg_ < 11);
            value_ *= ++arg_;

            return *this;
        }

        iterator& operator --()
        {
            assert(arg_ > 0);
            value_ /= arg_--;

            return *this;
        }

        iterator operator ++(int)
        {
            iterator temp(*this);
            this->operator++();

            return temp;
        }

        iterator operator --(int)
        {
            iterator temp(*this);
            this->operator--();

            return temp;
        }

        bool operator ==(const iterator &rhs) const
        {
            return arg_ == rhs.arg_;
        }

        bool operator !=(const iterator &rhs) const
        {
            return arg_ != rhs.arg_;
        }

    private:
        size_t arg_;
        size_t value_;

    };

    CFactorialContainer()
    {
    }
    
    iterator begin()
    {
        return iterator();
    }

    iterator end()
    {
        return iterator(11);
    }

};

#endif // FACTORIAL_CONTAINER_H
