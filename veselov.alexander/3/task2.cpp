#include <list>
#include <boost/test/unit_test.hpp>

void ProcessLists(const std::list<int> &in, std::list<int> &out)
{
    if (in.empty())
    {
        return;
    }

    typedef std::list<int>::const_iterator iterator;
    typedef std::list<int>::const_reverse_iterator reverse_iterator;

    iterator it_left = in.begin();
    reverse_iterator it_right = in.rbegin();
    do
    {
        out.push_back(*(it_left++));
        if (it_left != it_right.base())
        {
            out.push_back(*(it_right++));
        }
    } while (it_left != it_right.base());
}

BOOST_AUTO_TEST_CASE(testSize0)
{
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize1)
{
    const size_t current_size = 1;
    int in_array[current_size] = { 40 };
    int correct_array[current_size] = { 40 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize2)
{
    const size_t current_size = 2;
    int in_array[current_size] = { 40, 28 };
    int correct_array[current_size] = { 40, 28 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize3)
{
    const size_t current_size = 3;
    int in_array[current_size] = { 40, 28, 20 };
    int correct_array[current_size] = { 40, 20, 28 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize4)
{
    const size_t current_size = 4;
    int in_array[current_size] = { 40, 28, 20, 41 };
    int correct_array[current_size] = { 40, 41, 28, 20 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize5)
{
    const size_t current_size = 5;
    int in_array[current_size] = { 40, 28, 20, 41, 67 };
    int correct_array[current_size] = { 40, 67, 28, 41, 20 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize7)
{
    const size_t current_size = 7;
    int in_array[current_size] = { 40, 28, 20, 41, 67, 58, 3 };
    int correct_array[current_size] = { 40, 3, 28, 58, 20, 67, 41 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}

BOOST_AUTO_TEST_CASE(testSize14)
{
    const size_t current_size = 14;
    int in_array[current_size] = { 40, 28, 20, 41, 67, 58, 3, 82, 65, 77, 81, 90, 74, 93 };
    int correct_array[current_size] = { 40, 93, 28, 74, 20, 90, 41, 81, 67, 77, 58, 65, 3, 82 };
    std::list<int> list_in;
    std::list<int> list_out;
    std::list<int> list_correct;

    list_in.assign(in_array, in_array+current_size);
    list_correct.assign(correct_array, correct_array+current_size);

    ProcessLists(list_in, list_out);

    BOOST_CHECK_EQUAL_COLLECTIONS(list_out.begin(), list_out.end(), list_correct.begin(), list_correct.end());

}
