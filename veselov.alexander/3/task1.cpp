#define BOOST_TEST_MODULE testTask3

#ifndef _WIN32
#define BOOST_TEST_DYN_LINK
#endif // _WIN32

#include "priority_queue.h"
#include <boost/test/unit_test.hpp>

typedef QueueWithPriority<int> Queue;

BOOST_AUTO_TEST_CASE(correctPriorityOutputTest)
{
    int inputValues[10] = {41, 34, 69, 78, 62, 5, 81, 61, 95, 27};
    Queue::ElementPriority priorities[10] = {Queue::HIGH, Queue::NORMAL, Queue::NORMAL, Queue::LOW, Queue::HIGH, Queue::HIGH, Queue::LOW, Queue::HIGH, Queue::HIGH, Queue::LOW};
    int correctResult[10] = {41, 62, 5, 61, 95, 34, 69, 78, 81, 27};
    
    int result[10];

    Queue queue;
    for (int i = 0; i < 10; ++i)
    {
        queue.push(inputValues[i], priorities[i]);
    }

    for (int i = 0; i < 10; ++i)
    {
        result[i] = *queue.get();
    }

    BOOST_CHECK_EQUAL_COLLECTIONS(result, result+10, correctResult, correctResult+10);

}

BOOST_AUTO_TEST_CASE(correctAccelerateTest)
{
    int inputValues[10] = {41, 34, 69, 78, 62, 5, 81, 61, 95, 27};
    Queue::ElementPriority priorities[10] = {Queue::HIGH, Queue::NORMAL, Queue::NORMAL, Queue::LOW, Queue::HIGH, Queue::HIGH, Queue::LOW, Queue::HIGH, Queue::HIGH, Queue::LOW};
    int correctResult[10] = {41, 78, 62, 5, 81, 61, 95, 27, 34, 69};
    
    int result[10];

    Queue queue;
    for (int i = 0; i < 10; ++i)
    {
        queue.push(inputValues[i], priorities[i]);
    }

    queue.Accelerate();

    for (int i = 0; i < 10; ++i)
    {
        result[i] = *queue.get();
    }

    BOOST_CHECK_EQUAL_COLLECTIONS(result, result+10, correctResult, correctResult+10);

}

BOOST_AUTO_TEST_CASE(emptyQueueTest)
{
    Queue queue;

    BOOST_CHECK_THROW(queue.get(), std::out_of_range);

}
