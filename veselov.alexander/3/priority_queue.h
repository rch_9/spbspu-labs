#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#include <list>
#include <stdexcept>
#include <boost/shared_ptr.hpp>

template <typename T>
class QueueWithPriority
{
public:
    enum ElementPriority
    {
        LOW,
        NORMAL,
        HIGH
    };

private:
    struct node_t
    {
        T value;
        ElementPriority priority;

        node_t(T _Val, ElementPriority _Pr) : 
            value(_Val),
            priority(_Pr)
        {
        }

    };
    std::list<node_t> c;

public:
    typedef T value_type;
    typedef std::list<node_t> container_type;
    typedef typename container_type::iterator iterator;

    void push(const value_type &_Val, ElementPriority _Pr)
    {
        c.push_back(node_t(_Val, _Pr));
    }

    boost::shared_ptr<value_type> get()
    {
        iterator it_max = c.begin();

        if (c.empty())
        {
            throw std::out_of_range("Empty Queue");
        }

        for (iterator it = c.begin(); it != c.end(); ++it)
        {
            if (it->priority == HIGH)
            {
                it_max = it;
                break;
            }
            if (it->priority > it_max->priority)
            {
                it_max = it;
            }
        }
        
        boost::shared_ptr<value_type> result(new value_type(it_max->value));
        c.erase(it_max);

        return result;
    }

    void Accelerate()
    {
        for (iterator it = c.begin(); it != c.end(); ++it)
        {
            if (it->priority == LOW)
            {
                it->priority = HIGH;
            }
        }
    }

    bool empty() const
    {
        return c.empty();
    }

    iterator begin()
    {
        return c.begin();
    }

    iterator end()
    {
        return c.end();
    }

};

#endif // PRIORITY_QUEUE_H
