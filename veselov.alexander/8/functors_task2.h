#ifndef FUNCTORS_TASK1_H
#define FUNCTORS_TASK1_H

#include "shapes.h"
#include <boost/shared_ptr.hpp>

#define SHAPE_MAX_COORD 100

struct ShapeGenerator
{
    boost::shared_ptr<Shape> operator () ()
    {
        int xx = rand() % (2 * SHAPE_MAX_COORD + 1) - SHAPE_MAX_COORD;
        int yy = rand() % (2 * SHAPE_MAX_COORD + 1) - SHAPE_MAX_COORD;

        switch(rand() % SHAPE_TYPE_COUNT)
        {
        case 0:
            return boost::shared_ptr<Shape>(new Circle(xx, yy));
        case 1:
            return boost::shared_ptr<Shape>(new Triangle(xx, yy));
        case 2:
            return boost::shared_ptr<Shape>(new Square(xx, yy));
        }
        return boost::shared_ptr<Shape>();

    }

};

struct ShapeDrawer
{
    void operator () (const boost::shared_ptr<Shape> &current)
    {
        current->Draw();
    }

};

struct ShapeComparatorX
{
    typedef bool result_type;
    bool operator () (const boost::shared_ptr<Shape> &lhs, const boost::shared_ptr<Shape> &rhs)
    {
        return lhs->IsMoreLeft(*rhs);
    }

};

struct ShapeComparatorY
{
    typedef bool result_type;
    bool operator () (const boost::shared_ptr<Shape> &lhs, const boost::shared_ptr<Shape> &rhs)
    {
        return lhs->IsMoreUpper(*rhs);
    }

};


#endif // FUNCTORS_TASK1_H
