#include "shapes.h"
#include "functors_task2.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

void DoTask2()
{
    std::vector<boost::shared_ptr<Shape> > ShapeVector(20);
    std::generate(ShapeVector.begin(), ShapeVector.end(), ShapeGenerator());

    std::cout << "Unsorted shapes:\n";
    std::for_each(ShapeVector.begin(), ShapeVector.end(), ShapeDrawer());

    std::sort(ShapeVector.begin(), ShapeVector.end(), boost::bind(ShapeComparatorX(), _1, _2));
    std::cout << "Sorted left-to-right:\n";
    std::for_each(ShapeVector.begin(), ShapeVector.end(), ShapeDrawer());

    std::sort(ShapeVector.begin(), ShapeVector.end(), boost::bind(ShapeComparatorX(), _2, _1));
    std::cout << "Sorted right-to-left:\n";
    std::for_each(ShapeVector.begin(), ShapeVector.end(), ShapeDrawer());
    
    std::sort(ShapeVector.begin(), ShapeVector.end(), boost::bind(ShapeComparatorY(), _1, _2));
    std::cout << "Sorted up-to-down:\n";
    std::for_each(ShapeVector.begin(), ShapeVector.end(), ShapeDrawer());

    std::sort(ShapeVector.begin(), ShapeVector.end(), boost::bind(ShapeComparatorY(), _2, _1));
    std::cout << "Sorted down-to-up:\n";
    std::for_each(ShapeVector.begin(), ShapeVector.end(), ShapeDrawer());

}
