#ifndef SHAPES_H
#define SHAPES_H

#include <iostream>
#include <iomanip>

#define SHAPE_TYPE_COUNT 3

class Shape
{
public:
    Shape(int x, int y) : x_(x), y_(y)
    {
    }
    bool IsMoreLeft(const Shape &other) const
    {
        return x_ < other.x_;
    }
    bool IsMoreUpper(const Shape &other) const
    {
        return y_ > other.y_;
    }
    int getX() const
    {
        return x_;
    }
    int getY() const
    {
        return y_;
    }

    virtual void Draw() const = 0;

private:
    int x_, y_;

};

class Circle : public Shape
{
public:
    Circle(int x, int y) : Shape(x, y)
    {
    }
    virtual void Draw() const
    {
        std::cout << "Circle   at (" << std::setw(3)
            << getX() << ", " << std::setw(3) << getY() << ")" << std::endl;
    }

};

class Triangle : public Shape
{
public:
    Triangle(int x, int y) : Shape(x, y)
    {
    }
    virtual void Draw() const
    {
        std::cout << "Triangle at (" << std::setw(3)
            << getX() << ", " << std::setw(3) << getY() << ")" << std::endl;
    }

};

class Square : public Shape
{
public:
    Square(int x, int y) : Shape(x, y)
    {
    }
    virtual void Draw() const
    {
        std::cout << "Square   at (" << std::setw(3)
            << getX() << ", " << std::setw(3) << getY() << ")" << std::endl;
    }

};


#endif // SHAPES_H
