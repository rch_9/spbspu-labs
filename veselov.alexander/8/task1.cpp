#include "functors_task1.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>

#define MATH_PI 3.141592653589793

void DoTask1()
{
    std::vector<double> Values(10);

    std::generate(Values.begin(), Values.end(), VectorGenerator());
    std::cout << "Before:" << std::endl;
    std::for_each(Values.begin(), Values.end(), VectorPrinter());
    std::transform(Values.begin(), Values.end(), Values.begin(), boost::bind(std::multiplies<double>(), _1, MATH_PI));
    std::cout << "After:" << std::endl;
    std::for_each(Values.begin(), Values.end(), VectorPrinter());

}
