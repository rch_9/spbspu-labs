#ifndef FUNCTORS_TASK1_H
#define FUNCTORS_TASK1_H

#include <iostream>
#include <cstdlib>

struct VectorGenerator
{
    double operator () ()
    {
        return static_cast<double> (rand()) / static_cast<double> (RAND_MAX) * 2.0 - 1.0;
    }

};

struct VectorPrinter
{
    void operator () (double value)
    {
        std::cout << value << std::endl;
    }

};

#endif // FUNCTORS_TASK1_H
