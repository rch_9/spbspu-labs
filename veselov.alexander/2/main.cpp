#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cctype>

static const std::string punct = ".,!?:;";
static const std::string replace_word = "Vau!!!";
typedef std::string::size_type i_type;

bool isWhitespace(char c)
{
    return std::isspace(c) && (c != ' ');
}

bool isPunct(char c)
{
    // return std::ispunct(c); // Sure
    return punct.find(c) != std::string::npos;
}

int main()
{
    std::ifstream input_file("input.txt");

    if (!input_file)
    {
        std::cerr << "File is not accessible\n";
        return 1;
    }

    std::vector<std::string> stringData;
    i_type min_i = 0;
    std::string curr_line, tempstr;
    while (std::getline(input_file, curr_line))
    {
        // Add a space at the end of the line, just in case
        curr_line.insert(curr_line.length(), " ");

        // (a): Erasing tabulation
        for (i_type i = 0; i < curr_line.size(); )
        {
            if (isWhitespace(curr_line[i]))
            {
                curr_line[i] = ' ';
            }
            else
            {
                ++i;
            }
        }

        // (b): Erasing unique spaces
        for (i_type i = 0; i + 1 < curr_line.size(); )
        {
            if (std::isspace(curr_line[i]) && std::isspace(curr_line[i+1]))
            {
                curr_line.erase(i + 1, 1);
            }
            else
            {
                ++i;
            }
        }

        // (c): Erasing spaces between punctuation
        for (i_type i = 1; i < curr_line.size(); ++i)
        {
            if (std::isspace(curr_line[i-1]) && isPunct(curr_line[i]))
            {
                curr_line.erase(i - 1, 1);
            }
        }

        // (d): Adding a space after punctuation
        for (i_type i = 0; i + 1 < curr_line.size(); ++i)
        {
            if (isPunct(curr_line[i]) && !std::isspace(curr_line[i+1]) && !isPunct(curr_line[i+1]))
            {
                curr_line.insert(i + 1, " ");
            }
        }

        // So, delete the first space
        if (std::isspace(curr_line[0]))
        {
            curr_line.erase(0, 1);
        }
        
        // (e): "Vau!!!"
        i_type i = 0, j = 0;

        while(true)
        {
            j = curr_line.find_first_of(" "+punct, i);
            if (j == std::string::npos)
            {
                break;
            }

            if ((j - i) > 10)
            {
                curr_line.replace(i, j - i, replace_word);
                j = i + replace_word.length();
            }
            i = j + 1;

        }

        tempstr += curr_line;

        // Push to vector
        i_type max_i = tempstr.rfind(' ', min_i + 40);
        stringData.push_back(tempstr.substr(min_i, max_i - min_i));
        min_i = max_i + 1;

    }
    
    // Now we consider leftover trash
    while (true)
    {
        i_type max_i = tempstr.rfind(' ', min_i + 40);

        if (min_i > max_i)
        {
            break;
        }

        stringData.push_back(tempstr.substr(min_i, max_i - min_i));
        min_i = max_i + 1;

    }
    for (std::vector<std::string>::iterator it = stringData.begin(); it != stringData.end(); ++it)
    {
        std::cout << (*it) << "\n";
    }

    return 0;

}