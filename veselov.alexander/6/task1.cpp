#include <iostream>
#include <fstream>
#include <set>
#include <iterator>
#include <string>
#include <algorithm>

void DoTask1(const char *filename)
{
    std::ifstream input(filename);

    if (!input)
    {
        std::cerr << "File is not accessible!\n";
        return;
    }

    std::set<std::string> words;
    std::copy(std::istream_iterator<std::string>(input),
        std::istream_iterator<std::string>(), std::inserter(words, words.begin()));

    if (input.bad())
    {
        std::cerr << "Read error\n";
    }

    std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(std::cout, "\n"));

    std::cout << std::endl;

}
