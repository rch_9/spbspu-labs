#include "shapes.h"
#include "shape_functors.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <iterator>

void DoTask2()
{
    std::vector<Shape> ShapeVector(10);
    std::generate(ShapeVector.begin(), ShapeVector.end(), ShapeGenerator());
    std::copy(ShapeVector.begin(), ShapeVector.end(), std::ostream_iterator<Shape>(std::cout));
    
    std::cout << "Total vertex count: "
        << std::accumulate(ShapeVector.begin(), ShapeVector.end(), 0, ShapeVertexAccumulator()) << "\n";

    std::cout << "Triangle count: "
        << std::count_if(ShapeVector.begin(), ShapeVector.end(), TriangleChecker()) << "\n";
    std::cout << "Square count: "
        << std::count_if(ShapeVector.begin(), ShapeVector.end(), SquareChecker()) << "\n";
    std::cout << "Rectangle count: "
        << std::count_if(ShapeVector.begin(), ShapeVector.end(), RectangleChecker()) << "\n";
    std::cout << std::endl;
    
    ShapeVector.erase(std::remove_if(ShapeVector.begin(), ShapeVector.end(), PentagonChecker()), ShapeVector.end());
    
    std::copy(ShapeVector.begin(), ShapeVector.end(), std::ostream_iterator<Shape>(std::cout));

    std::vector<Point> PointVector(ShapeVector.size(), Point(0, 0));
    std::transform(ShapeVector.begin(), ShapeVector.end(), PointVector.begin(), PointGetter());
    std::cout << "Points: \n";
    std::copy(PointVector.begin(), PointVector.end(), std::ostream_iterator<Point>(std::cout));
    std::cout << std::endl;

    std::sort(ShapeVector.begin(), ShapeVector.end(), ShapeComparator());
    std::copy(ShapeVector.begin(), ShapeVector.end(), std::ostream_iterator<Shape>(std::cout));

}
