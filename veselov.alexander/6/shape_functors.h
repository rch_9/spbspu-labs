#ifndef SHAPE_FUNCTORS_H
#define SHAPE_FUNCTORS_H

#include "shapes.h"
#include <iostream>
#include <cmath>
#include <iomanip>
#include <algorithm>

#define MATH_PI   3.14159265358979323846
#define MATH_2PI  6.28318530717958647692
const int g_ShapeCoordRange = 100;
const int g_ShapeMaxSize = 100;

double random_range(double min, double max)
{
    return min + static_cast<double> (rand()) / static_cast<double> (RAND_MAX) * (max - min);
}

struct ShapeGenerator
{
    Shape operator ()()
    {
        // Initial position of shape
        int xx = rand() % (2 * g_ShapeCoordRange + 1) - g_ShapeCoordRange;
        int yy = rand() % (2 * g_ShapeCoordRange + 1) - g_ShapeCoordRange;
        
        double width = random_range(1.0, g_ShapeMaxSize);
        // Regular polygon
        double height = width;
        
        unsigned int vertex_num = rand() % 4 + 2;
        Shape result;
        result.regular = true;

        // Now let 2 will be rectangle
        if (vertex_num == 2)
        {
            vertex_num = 4;
            result.regular = false;
            height = random_range(1.0, g_ShapeMaxSize);
        }
        
        double step = MATH_2PI / vertex_num;
        double angle = (vertex_num == 4) ? MATH_PI / 4 : MATH_PI / 2;
        
        for (size_t i = 0; i < vertex_num; ++i)
        {
            result.vertices.push_back(Point(xx + static_cast<int> (cos(i * step + angle) * width),
                yy + static_cast<int> (sin(i * step + angle) * height)));
        }

        return result;
    }

};


std::ostream & operator<<(std::ostream &os, const Point &current)
{
    return os << "(" << std::setw(4) << current.x << ", " << std::setw(4) << current.y << ")\n";
}

std::ostream & operator<<(std::ostream &os, const Shape &current)
{
        os << "Vertex count: " << current.vertices.size() << "\n";
        for (std::vector<Point>::const_iterator it = current.vertices.begin(); it != current.vertices.end(); ++it)
        {
            os << (*it);
        }

        return os;
}

struct ShapeVertexAccumulator
{
    int operator ()(int accumulator, const Shape &current)
    {
        return accumulator + current.vertices.size();
    }
};

struct TriangleChecker
{
    bool operator ()(const Shape &current)
    {
        return (current.vertices.size() == 3);
    }
};

struct SquareChecker
{
    bool operator ()(const Shape &current)
    {
        return ((current.vertices.size() == 4) && current.regular);
    }
};

struct RectangleChecker
{
    bool operator ()(const Shape &current)
    {
        return ((current.vertices.size() == 4) && !current.regular);
    }
};

struct PentagonChecker
{
    bool operator ()(const Shape &current)
    {
        return (current.vertices.size() == 5);
    }
};

struct PointGetter
{
    Point operator ()(const Shape &current)
    {
        return current.vertices[rand() % current.vertices.size()];
    }
};

struct ShapeComparator
{
    bool operator ()(const Shape &lhs, const Shape &rhs)
    {
        return (lhs.vertices.size() < rhs.vertices.size())
            || ((lhs.vertices.size() == rhs.vertices.size()) && (lhs.regular && !rhs.regular));
    }
};

#endif // SHAPE_FUNCTORS_H
