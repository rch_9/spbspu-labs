#include <cstdlib>
#include <ctime>

void DoTask1(const char *);
void DoTask2();

int main()
{
    srand(time(NULL));
    DoTask1("input.txt");
    DoTask2();

    return 0;
}
