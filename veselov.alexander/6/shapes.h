#ifndef SHAPES_H
#define SHAPES_H

#include <vector>

struct Point
{
    Point(int x_, int y_)
        : x(x_), y(y_)
    {
    }
    int x, y;

};

struct Shape
{
    std::vector<Point> vertices;
    bool regular;

};

#endif // SHAPES_H
