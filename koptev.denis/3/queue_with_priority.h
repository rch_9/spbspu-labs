#ifndef QUEUE_WITH_PRIORITY_H
#define QUEUE_WITH_PRIORITY_H

#include <memory>
#include <list>
#include <stdexcept>

//**************QUEUE_WITH_PRIORITY DECLARATION**************//

template <typename T>
class QueueWithPriority
{
	private:

		std::list<T> HIGH_L;
		std::list<T> NORMAL_L;
		std::list<T> LOW_L;

	public:

		typedef enum
		{
			LOW,
			NORMAL,
			HIGH
		} ElementPriority;

		typedef typename std::list<T>::iterator Q_ITER;

		inline void Put(const T &, ElementPriority);

		inline std::auto_ptr<T> Get();

		inline void Accelerate();

		//******** Can be useful for tests ********//

		inline bool Empty() const;
		inline size_t Size() const;
};

//**************QUEUE_WITH_PRIORITY IMPLEMENTATION**************//

template <typename T>
inline void QueueWithPriority<T>::Put(const T & element_,
                                      ElementPriority prior_)
{
	switch (prior_)
	{
		case HIGH:
		{
			HIGH_L.push_back(element_);
			break;
		}
		case NORMAL:
		{
			NORMAL_L.push_back(element_);
			break;
		}
		case LOW:
		{
			LOW_L.push_back(element_);
			break;
		}
	}
}

template <typename T>
inline std::auto_ptr<T> QueueWithPriority<T>::Get()
{
	std::auto_ptr<T> Extracted;
	if (!HIGH_L.empty())
	{
		Extracted.reset(new T(HIGH_L.front()));
		HIGH_L.pop_front();
	}
	else if (!NORMAL_L.empty())
	{
		Extracted.reset(new T(NORMAL_L.front()));
		NORMAL_L.pop_front();
	}
	else if (!LOW_L.empty())
	{
		Extracted.reset(new T(LOW_L.front()));
		LOW_L.pop_front();
	}
	else
	{
		throw std::runtime_error("<<<Exception: Empty queue!>>>");
	}
	return Extracted;
}

template <typename T>
inline void QueueWithPriority<T>::Accelerate()
{
	HIGH_L.splice(HIGH_L.end(), LOW_L);
}

template <typename T>
inline bool QueueWithPriority<T>::Empty() const
{
	return (HIGH_L.empty() && NORMAL_L.empty() && LOW_L.empty());
}

template <typename T>
inline size_t QueueWithPriority<T>::Size() const
{
	return (HIGH_L.size() + NORMAL_L.size() + LOW_L.size());
}

#endif
