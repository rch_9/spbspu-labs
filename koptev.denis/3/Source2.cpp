#include <iostream>
#include <list>
#include <ctime>
#include <cstdlib>

typedef std::list<int>::iterator ITER;

void fill_list(std::list<int> & list, size_t amount);
void print_list(std::list<int> & list);
void print_list_recursive(ITER left, ITER right);

int main()
{
	srand(time(0));
	std::list<int> list;
	for (size_t i = 0; i < 15; ++i)
	{
		if (i == 6)
		{
			++i;
		}
		if (i == 8)
		{
			i += 6;
		}

		fill_list(list, i);

		if (!list.empty())
		{
			print_list(list);
			print_list_recursive(list.begin(), --list.end());
		}
		else
		{
			std::cout << "Empty list!" << std::endl;
		}
		list.clear();
	}
	return 0;
}

void fill_list(std::list<int> & list, size_t amount)
{
	for (size_t i = 0; i < amount; ++i)
	{
		list.push_back(1 + rand() % 20);
	}
}

void print_list_recursive(ITER left, ITER right)
{
	if (left != right)
	{
		std::cout << *left << " " << *right << " ";
	}
	else
	{
		std::cout << *left << std::endl;
		return;
	}
	if (++left != right)
	{
		print_list_recursive(left, --right);
	}
	else
	{
		std::cout << std::endl;
	}
}

void print_list(std::list<int> & list)
{
	for (ITER it = list.begin(); it != list.end(); ++it)
	{
		std::cout << *it << " ";
	}
	std::cout << std::endl;
}
