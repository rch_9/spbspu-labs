#include <iostream>

#include "queue_with_priority.h"
#include "test_functions.h"

bool (*Tests[])() =
{
    &correct_data_input,
    &correct_data_output,
    &null_data_output,
    &acceleration_test,
    &empty_queue_test
};

int main()
{
    size_t Errors = 0;
    for (size_t i = 0; i < sizeof(Tests) / sizeof(Tests[i]); ++i)
    {
        if (!Tests[i]())
        {
            ++Errors;
        }
    }

    std::cout << "\n----------------------------\n"
              << Errors << " error(s) found" << std::endl;

    if (!Errors)
    {
        return 0;
    }
    else
    {
        return 1;

    }
}
