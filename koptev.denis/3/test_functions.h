#ifndef TEST_FUNCTIONS_H
#define TEST_FUNCTIONS_H

#include <iostream>
#include <memory>

#include "queue_with_priority.h"

struct QE
{
	std::string name;
	int age;
	QE()
	{};
	QE(std::string name_, int age_)
		: name(name_), age(age_)
	{};
};

typedef QueueWithPriority<QE> QUEUE;

void fill_queue(QUEUE & MyQueue)
{
	QE qe1("Jack", 18);
	QE qe2("Nick", 55);
	QE qe3("Ben", 30);
	MyQueue.Put(qe1, QUEUE::HIGH);
	MyQueue.Put(qe2, QUEUE::LOW);
	MyQueue.Put(qe3, QUEUE::NORMAL);
}

bool correct_data_input()
{
	std::cout << "...Testing input with correct data..." << std::endl;
	QUEUE MyQueue;
	fill_queue(MyQueue);
	if (MyQueue.Size() != 3)
	{
		std::cerr << "***ERROR! " << MyQueue.Size() << " elements of 3 were put!***" << std::endl;
		return false;
	}
	else
	{
		std::cout << "___Input is ok___" << std::endl;
		return true;
	}
}

bool correct_data_output()
{
	std::cout << "...Testing output with correct data..." << std::endl;
	QUEUE MyQueue;
	fill_queue(MyQueue);
	std::auto_ptr<QE> p_1;
	p_1 = MyQueue.Get();
	std::auto_ptr<QE> p_3;
	p_3 = MyQueue.Get();
	std::auto_ptr<QE> p_2;
	p_2 = MyQueue.Get();
	if (!MyQueue.Empty())
	{
		std::cerr << "***ERROR! Queue is not empty!****" << std::endl;
		return false;
	}
	if (!p_1.get() || !p_2.get() || !p_3.get())
	{
		std::cerr << "***ERROR! Some of data was lost!***" << std::endl;
		return false;
	}
	if (p_1.get()->name != "Jack" || p_2.get()->name != "Nick" || p_3.get()->name != "Ben")
	{
		std::cerr << "***ERROR! Wrong order of execution!***" << std::endl;
		return false;
	}
	else
	{
		std::cout << "___Output is ok___" << std::endl;
		return true;
	}
}

bool null_data_output()
{
	std::cout << "...Testing output from empty queue..." << std::endl;
	QUEUE MyQueue;
	std::auto_ptr<QE> ptr_1;
	try
	{
        ptr_1 = MyQueue.Get();
	}
	catch(std::runtime_error &exc)
	{
	    std::cerr << exc.what() << std::endl;
	}
	if (!ptr_1.get())
	{
		std::cout << "___Correct! Nullptr was returned___" << std::endl;
		return true;
	}
	else
	{
		std::cerr << "***ERROR! Unexpected executed data!***" << std::endl;
		return false;
	}
}

bool acceleration_test()
{
	std::cout << "...Testing acceleration..." << std::endl;
	QUEUE MyQueue;
	fill_queue(MyQueue);
	MyQueue.Accelerate();
	std::auto_ptr<QE> p_1;
	p_1 = MyQueue.Get();
	std::auto_ptr<QE> p_2;
	p_2 = MyQueue.Get();
	std::auto_ptr<QE> p_3;
	p_3 = MyQueue.Get();
	if (p_1.get()->name != "Jack" || p_2.get()->name != "Nick" || p_3.get()->name != "Ben")
	{
		std::cerr << "***ERROR! Wrong order of execution after acceleration!***" << std::endl;
		return false;
	}
	else
	{
		std::cout << "___Acceleration process is ok___" << std::endl;
		return true;
	}
}

bool empty_queue_test()
{
	std::cout << "...Testing empty queue..." << std::endl;
	QUEUE MyQueue;
	MyQueue.Accelerate();	//Must be no errors or changes
	if (MyQueue.Empty())
	{
		std::cout << "___Queue is empty. Ok___" << std::endl;
		return true;
	}
	else
	{
		std::cerr << "****ERROR! Something's wrong during working with empty queue****"	<< std::endl;
		return false;
	}
}

#endif
