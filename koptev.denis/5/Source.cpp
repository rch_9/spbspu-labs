#include <iostream>
#include <vector>
#include <ctime> // time(0)
#include <cstdlib> // rand()
#include <string> // cout << string
#include <algorithm> // std::copy, std::sort
#include <iomanip> //std::setw()
#include <iterator> // ostream_iterator

std::string table[] = {
	"Hello World!",
	"#include <iostream>",
	"Let it snow!",
	"Some string...",
	"Lab 5",
	"Algorithms",
	"Polytechnic University",
	"Students",
	"return 0",
	"Goodbye World!"
};

struct DataStruct
{
	int key1;
	int key2;
	std::string str;
	friend std::ostream & operator << (std::ostream & o, const DataStruct & DS)
	{
		o << std::setw(3) << DS.key1 << std::setw(3) << DS.key2 << "  " << DS.str << std::endl;
		return o;
	}
};

typedef std::vector<DataStruct>::iterator ITER;

void fill_vector(std::vector<DataStruct> & Vect);
void sort_vector(std::vector<DataStruct> & Vect);

int main()
{
	srand(static_cast<unsigned>(time(0)));
	std::vector<DataStruct> Vect(10);
	fill_vector(Vect);
	std::cout << "------------------\n Initial:\n------------------" << std::endl;
	std::copy(Vect.begin(), Vect.end(), std::ostream_iterator<DataStruct>(std::cout));
	sort_vector(Vect);
	std::cout << "------------------\n Sorted:\n------------------" << std::endl;
	std::copy(Vect.begin(), Vect.end(), std::ostream_iterator<DataStruct>(std::cout));
}

struct generator
	: std::unary_function<DataStruct, void>
{
	void operator () (DataStruct & this_struct)
	{
		this_struct.key1 = 5 - rand() % 11;
		this_struct.key2 = 5 - rand() % 11;
		this_struct.str = table[rand() % 10];
	}
};

void fill_vector(std::vector<DataStruct> & Vect)
{
	std::for_each(Vect.begin(), Vect.end(), generator());
}

struct comparator
	: std::binary_function<DataStruct, DataStruct, bool>
{
	bool operator () (const DataStruct & lhs, const DataStruct & rhs)
	{
		return (lhs.key1 < rhs.key1) ||
			(lhs.key1 == rhs.key1 && lhs.key2 < rhs.key2) ||
			(lhs.key1 == rhs.key1 && lhs.key2 == rhs.key2 && lhs.str.size() < rhs.str.size());
	}
};

void sort_vector(std::vector<DataStruct> & Vect)
{
	std::sort(Vect.begin(), Vect.end(), comparator());
}
