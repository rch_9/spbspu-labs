#include <iostream>
#include <vector>
#include "Set_Sort.h"

void Processor(std::vector<int> &);

int main()
{
	std::cout << "Enter some values. 0 - end of input" << std::endl;
	std::vector<int> V;
	FillContainer<IndexPolicy>(V);
	Print<IndexPolicy>(V);
	Processor(V);
	Print<IndexPolicy>(V);
	return 0;
}

void Processor(std::vector<int> & V)
{
	if (!V.empty()){
		std::vector<int>::iterator i = V.begin();
		switch (V.back()){
				case 1:
					while (i != V.end()){
						if (!(*i % 2)) {
							i = V.erase(i);
						}
						else {
							++i;
						}
					}
					break;
				case 2:
					while (i != V.end()){
						if (!(*i % 3)){
							i = V.insert(i + 1, 3, 1);
						}
						++i;
					}
					break;
				default: return;
		}
	}
}
