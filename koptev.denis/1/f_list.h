#ifndef F_LIST_H
#define F_LIST_H

#include <iterator>
#include <cassert>

//*********F_LIST DECLARATION*********//

template <typename T>
class f_list
{
    private:

        struct node_t
        {
            node_t(const T & value_);
            T value;
            node_t * next;
        };

        node_t * head;

    public:

        //*********ITERATOR DECLARATION*********//

        class iterator :
            public std::iterator<std::forward_iterator_tag, T>
        {
            public:

                typedef node_t node_type;
                typedef iterator this_type;

                inline iterator();
                inline iterator(node_type * p_node);

                inline bool operator ==(const this_type & rhs) const;
                inline bool operator !=(const this_type & rhs) const;

                inline T & operator *() const;
                inline T * operator ->() const;

                inline this_type & operator ++();
                inline this_type operator ++(int);

            private:
                node_type * current_;
        };

        //*********F_LIST METHODS*********//

        typedef T value_type;

        inline f_list();

        inline iterator begin() const;
        inline iterator end() const;

        inline void push_back(const T & Val);

        inline bool empty() const;
};

//*********NODE IMPLEMENTATION*********//

template <typename T>
f_list<T>::node_t::node_t(const T & value_)
    : value(value_), next(0)
{};

//*********ITERATOR IMPLEMENTATION*********//

template <typename T>
inline f_list<T>::iterator::iterator()
    : current_(0)
{};

template <typename T>
inline f_list<T>::iterator::iterator(node_type * p_node)
    : current_(p_node)
{};

template <typename T>
inline bool f_list<T>::iterator::operator ==(const this_type & rhs) const
{
    return current_ == rhs.current_;
};

template <typename T>
inline bool f_list<T>::iterator::operator !=(const this_type & rhs) const
{
    return current_ != rhs.current_;
};

template <typename T>
inline T & f_list<T>::iterator::operator *() const
{
    assert(current_ != 0);
    return current_->value;
};

template <typename T>
inline T * f_list<T>::iterator::operator ->() const
{
    assert(current_ != 0);
    return &current_->value;
};

template <typename T>
inline typename f_list<T>::iterator & f_list<T>::iterator::operator ++()
{
    assert(current_ != 0);
    current_ = current_->next;
    return *this;
};

template <typename T>
inline typename f_list<T>::iterator f_list<T>::iterator::operator ++(int)
{
    this_type temp(*this);
    assert(current_ != 0);
    current_ = current_->next;
    return temp;
};

//*********F_LIST IMPLEMENTATION*********//

template <typename T>
inline f_list<T>::f_list()
    : head(0)
{};

template <typename T>
inline typename f_list<T>::iterator f_list<T>::begin() const
{
    return iterator(head);
};

template <typename T>
inline typename f_list<T>::iterator f_list<T>::end() const
{
    return iterator(0);
};

template <typename T>
inline void f_list<T>::push_back(const T & Val)
{
    if (!head)
    {
        head = new node_t(Val);
    }
    else {
        node_t * tmp = head;
        while (tmp->next)
        {
            tmp = tmp->next;
        }
        tmp->next = new node_t(Val);
    }
};

template <typename T>
inline bool f_list<T>::empty() const
{
    return head == 0;
};

#endif
