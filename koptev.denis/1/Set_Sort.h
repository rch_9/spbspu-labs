#ifndef SET_SORT_H
#define SET_SORT_H

#include <iostream>
#include <stdexcept>

template <typename Container>
struct IndexPolicy
{
	typedef typename Container::size_type marker;
	typedef typename Container::value_type value;

	static marker end(Container & C)
	{
		return C.size();
	}

	static marker begin(Container &)
	{
		return 0;
	}

	static bool empty(Container & C)
	{
		return C.empty();
	}

	static value & get(Container & C, marker pos)
	{
		if (pos >= end(C))
		{
			throw std::invalid_argument("Wrong index");
		}
		return C[pos];
	}
};

template <typename Container>
struct AtPolicy
{
	typedef typename Container::size_type marker;
	typedef typename Container::value_type value;

	static marker end(Container & C)
	{
		return C.size();
	}

	static marker begin(Container &)
	{
		return 0;
	}

	static bool empty(Container & C)
	{
		return C.empty();
	}

	static value & get(Container & C, marker pos)
	{
		return C.at(pos);
	}
};

template <typename Container>
struct IteratorPolicy
{
	typedef typename Container::iterator marker;
	typedef typename Container::value_type value;

	static marker end(Container & C)
	{
		return C.end();
	}

	static marker begin(Container & C)
	{
		return C.begin();
	}

	static bool empty(Container & C)
	{
		return C.empty();
	}

	static value & get(Container & C, marker pos)
	{
		if (begin(C) == end(C))
		{
			throw std::invalid_argument("Empty container");
		}
		return *pos;
	}
};

template <template <typename > class Policy, typename T>
void Sort(T & Coll)
{
	typedef Policy<T> MyPolicy;
	typedef typename Policy<T>::marker ptr;
	if (MyPolicy::empty(Coll))
	{
		return;
	}
	for (ptr i = MyPolicy::begin(Coll); i != MyPolicy::end(Coll); ++i)
	{
		ptr min = i;
		for (ptr j = i; j != MyPolicy::end(Coll); ++j)
		{
			if (MyPolicy::get(Coll, j) < MyPolicy::get(Coll, min))
			{
				min = j;
			}
		}
		if (min != i)
		{
			std::swap(MyPolicy::get(Coll, min), MyPolicy::get(Coll, i));
		}
	}
}

template <template <typename > class Policy, typename T>
void FillContainer(T & Coll)
{
	typename Policy<T>::value num = 1;
	while (num)
	{
		std::cin >> num;
		if (!std::cin.good())
		{
			std::cerr << "Error while reading!" << std::endl;
			std::cin.clear();
			std::cin.get();
			continue;
		}
		if (num)
		{
			Coll.push_back(num);
		}
	}
}

template <template <typename > class Policy, typename T>
void Print(T & Coll)
{
	typedef Policy<T> MyPolicy;
	typedef typename Policy<T>::marker ptr;
	for (ptr i = MyPolicy::begin(Coll); i != MyPolicy::end(Coll); ++i)
	{
		std::cout << MyPolicy::get(Coll, i) << " ";
	}
	std::cout << std::endl;
}

#endif