#include <iostream>
#include <fstream>
#include <vector>

int main()
{
	std::ifstream f("text.txt");
	if (!f){
		std::cerr << "File hasn't been opened!" << std::endl;
		return 1;
	}

	std::vector<char> V( (std::istreambuf_iterator<char>(f)) , (std::istreambuf_iterator<char>()) );

	for (std::vector<char>::iterator iter = V.begin(); iter != V.end(); ++iter){
		std::cout << *iter;
	}

	return 0;
}
