#include <iostream>
#include <vector>

#include "Set_Sort.h"
#include "f_list.h"

int main()
{
	std::vector<int> V1;
	std::cout << "Enter some values for the first vector. 0 - exit value." << std::endl;
	FillContainer<IndexPolicy>(V1);
	Print<IndexPolicy>(V1);
	Sort<IndexPolicy>(V1);
	Print<IndexPolicy>(V1);
	V1.clear();

	std::cout << "Enter some values for the second vector. 0 - exit value." << std::endl;
	FillContainer<AtPolicy>(V1);
	Print<AtPolicy>(V1);
	Sort<AtPolicy>(V1);
	Print<AtPolicy>(V1);

	f_list<int> MyList;
	std::cout << "Enter some values for the forward list. 0 - exit value." << std::endl;
	FillContainer<IteratorPolicy>(MyList);
	Print<IteratorPolicy>(MyList);
	Sort<IteratorPolicy>(MyList);
	Print<IteratorPolicy>(MyList);

	return 0;
}
