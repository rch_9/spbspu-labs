#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "Set_Sort.h"

void fillRandom(double *Array, const size_t & Size);

int main()
{
	srand(time(0));
	std::cout.precision(3);

	for (size_t i = 5; i <= 100; i *= 2){
		if (i == 20) i += 5;
		std::vector<double> V(i);
		fillRandom(&V[0], i);
		Print<IndexPolicy>(V);
		std::cout << "---------------------\nSorted data:" << std::endl;
		Sort<IndexPolicy>(V);
		Print<IndexPolicy>(V);
	}
	return 0;
}

void fillRandom(double *Array, const size_t & Size)
{
	if (!Array){
		throw std::invalid_argument("Empty array");
	}
	for (size_t i = 0; i < Size; ++i){
		Array[i] = 1 - 2.*rand() / RAND_MAX;
	}
}
