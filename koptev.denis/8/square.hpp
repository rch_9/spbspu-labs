/* square.hpp */

#ifndef SQUARE_HPP
#define SQUARE_HPP

#include "shape.hpp"

class Square : public Shape
{
	public:

		Square();
		Square(int, int);

		virtual void draw() const;
};

#endif