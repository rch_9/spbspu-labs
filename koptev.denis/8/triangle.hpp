/* triangle.hpp */

#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "shape.hpp"

class Triangle : public Shape
{
	public:

		Triangle();
		Triangle(int, int);

		virtual void draw() const;
};

#endif