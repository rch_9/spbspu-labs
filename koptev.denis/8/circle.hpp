/* circle.hpp */

#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

class Circle : public Shape
{
	public:

		Circle();
		Circle(int, int);

		virtual void draw() const;
};

#endif