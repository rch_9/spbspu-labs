/* triangle.cpp */

#include "triangle.hpp"
#include <iostream>

Triangle::Triangle()
	: Shape()
{};

Triangle::Triangle(int x_, int y_)
	: Shape(x_, y_)
{};

void Triangle::draw() const
{
	std::cout << "Triangle: [" << x << "," << y << "]" << std::endl;
}