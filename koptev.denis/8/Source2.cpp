/* TASK2. Source2.cpp */

#include "shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>

enum direction
{
	LEFT,
	RIGHT,
	UP,
	DOWN
};

typedef boost::shared_ptr<Shape> shape_ptr;
typedef std::vector<shape_ptr> shape_vect;

void fill_random(shape_vect &);
void print_shapes(const shape_vect &);
void sort_in_dir(shape_vect &, direction);

int main()
{
	srand(static_cast<unsigned>(time(0)));
	shape_vect Vect(10);
	fill_random(Vect);
	print_shapes(Vect);
	std::cout << "\nSorted vector (left): " << std::endl;
	sort_in_dir(Vect, LEFT);
	print_shapes(Vect);
	std::cout << "\nSorted vector (right): " << std::endl;
	sort_in_dir(Vect, RIGHT);
	print_shapes(Vect);
	std::cout << "\nSorted vector (up): " << std::endl;
	sort_in_dir(Vect, UP);
	print_shapes(Vect);
	std::cout << "\nSorted vector (down): " << std::endl;
	sort_in_dir(Vect, DOWN);
	print_shapes(Vect);
	return 0;
}

struct get_random_shape
	: std::unary_function<void, shape_ptr>
{
	shape_ptr operator () ()
	{
		shape_ptr p;
		switch (rand() % 3)
		{
			case 0:
			{
				p.reset(new Circle(rand() % 10, rand() % 10));
				break;
			}
			case 1:
			{
				p.reset(new Triangle(rand() % 10, rand() % 10));
				break;
			}
			case 2:
			{
				p.reset(new Square(rand() % 10, rand() % 10));
			}
		}
		return p;
	}
};

void fill_random(shape_vect & Vect)
{
	std::generate(Vect.begin(), Vect.end(), get_random_shape());
}

struct print_caller
	: std::unary_function<shape_ptr, void>
{
	void operator () (const shape_ptr & this_ptr) const
	{
		this_ptr->draw();
	}
};

void print_shapes(const shape_vect & Vect)
{
	std::for_each(Vect.begin(), Vect.end(), print_caller());
}

struct comparator
	: std::binary_function<shape_ptr, shape_ptr, bool>
{
	bool operator () (const shape_ptr & left, const shape_ptr & right, direction dir)
	{
		switch (dir)
		{
			case LEFT:
			{
				return left->is_more_left(*right);
				break;
			}
			case RIGHT:
			{
				return right->is_more_left(*left);
				break;
			}
			case UP:
			{
				return left->is_upper(*right);
				break;
			}
			case DOWN:
			{
				return right->is_upper(*left);
				break;
			}
			default:
			{
				throw std::invalid_argument("Wrong direction!");
			}
		}
	}
};

void sort_in_dir(shape_vect & Vect, direction dir)
{
	std::sort(Vect.begin(), Vect.end(), boost::bind(comparator(), _1, _2, dir));
}