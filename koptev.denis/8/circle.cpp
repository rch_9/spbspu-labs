/* circle.cpp */

#include "circle.hpp"
#include <iostream>

Circle::Circle()
	: Shape()
{};

Circle::Circle(int x_, int y_)
	: Shape(x_, y_)
{};

void Circle::draw() const
{
	std::cout << "Circle: [" << x << "," << y << "]" << std::endl;
}