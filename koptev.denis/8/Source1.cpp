/* TASK1. Source1.cpp */

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>

void fill_random(std::vector<double> & Vect);
void multiply_pi(std::vector<double> & Vect);

int main()
{
	srand(static_cast<unsigned>(time(0)));
	std::vector<double> Vect(10);
	fill_random(Vect);
	std::copy(Vect.begin(), Vect.end(), std::ostream_iterator<double>(std::cout, " "));
	std::cout << std::endl;
	multiply_pi(Vect);
	std::copy(Vect.begin(), Vect.end(), std::ostream_iterator<double>(std::cout, " "));
	std::cout << std::endl;
	return 0;
};

struct rand_generator
    : std::unary_function<void, double>
{
    double operator()()
    {
        return rand() % 10000 / 1000.;
    }
};

void fill_random(std::vector<double> & Vect)
{
	std::generate(Vect.begin(), Vect.end(), rand_generator());
}

void multiply_pi(std::vector<double> & Vect)
{
	std::transform(Vect.begin(), Vect.end(), Vect.begin(),
                 std::bind2nd(std::multiplies<double>(), M_PI)
    );
}
