/* shape.cpp */

#include "shape.hpp"

Shape::Shape()
	: x(0), y(0)
{}

Shape::Shape(int x_, int y_)
	: x(x_), y(y_)
{}

Shape::~Shape()
{};

bool Shape::is_more_left(const Shape & sh) const
{
	return x < sh.x;
}

bool Shape::is_upper(const Shape & sh) const
{
	return y > sh.y; //depends on how your coordinate system looks like
}
