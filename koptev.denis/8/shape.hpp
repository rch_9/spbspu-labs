/* shape.hpp */

#ifndef SHAPE_HPP
#define SHAPE_HPP

class Shape
{
	public:

		Shape();
		Shape(int, int);
		virtual ~Shape();

		bool is_more_left(const Shape &) const;
		bool is_upper(const Shape &) const;
		virtual void draw() const = 0;

	protected:

		int x, y;
};

#endif