/* square.cpp */

#include "square.hpp"
#include <iostream>

Square::Square()
	: Shape()
{};

Square::Square(int x_, int y_)
	: Shape(x_, y_)
{};

void Square::draw() const
{
	std::cout << "Square: [" << x << "," << y << "]" << std::endl;
}