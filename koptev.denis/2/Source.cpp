#include <iostream>
#include <fstream>
#include <vector>
#include <stdexcept>
#include <string>

typedef std::vector<std::string> VECT;
typedef std::vector<std::string>::iterator ITER;

void f_open(const char *, std::ifstream &);
VECT make_tokens(std::ifstream &);
void add_vau(VECT &);
VECT make_parsed_strings(VECT &, size_t);

int main()
{
    std::ifstream file;
	try
	{
		f_open("text.txt", file);
		VECT Tokens = make_tokens(file);
		add_vau(Tokens);
		VECT Result = make_parsed_strings(Tokens, 40);
		for (ITER it = Result.begin(); it != Result.end(); ++it)
		{
			std::cout << *it << std::endl;
		}
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}
    return 0;
}

void f_open(const char * filename, std::ifstream & file)
{
	if (filename == 0)
	{
		throw std::invalid_argument("f_open: Filename is nullptr");
	}
	file.open(filename);
	if (!file)
	{
		throw std::runtime_error("f_open: Opening error");
	}
}

VECT make_tokens(std::ifstream & file)
{
	if (!file)
	{
		throw std::invalid_argument("make_tokens: File hasn't been opened!");
	}

	VECT Output;
	bool new_token = true;
	while (!file.eof())
	{
		char c;
		file.get(c);
		std::string punct("");
		punct += c;
		if (file.bad())
		{
			std::cerr << "Error while reading" << std::endl;
			break;
		}
		if (ispunct(c))
		{
			Output.push_back(punct);
			new_token = true;
		}
		else if (isspace(c))
		{
			new_token = true;
		}
		else
		{
			if (new_token)
			{
				Output.push_back(punct);
				new_token = false;
				continue;
			}
			Output.back() += c;
		}
	}
	Output.back().resize(Output.back().size() - 1); //To delete eof symbol
	return Output;
}

void add_vau(VECT & Vect)
{
	for (ITER it = Vect.begin(); it != Vect.end(); ++it)
	{
		if (it->size() > 10)
		{
			*it = "Vau!!!";
		}
	}
}

VECT make_parsed_strings(VECT & Tokens, size_t length)
{
	VECT Output;
	Output.push_back(std::string(""));
	for (ITER it = Tokens.begin(); it != Tokens.end(); ++it)
	{
		if (it->size() + Output.back().size() < length)
        // < length, because space or punct must be included
		{
			if (!ispunct(it->operator[](0)) && !Output.back().empty())
			{
				Output.back().append(" ");
			}
			Output.back().append(*it);
		}
		else
		{
			Output.push_back(*it);
		}
	}
	return Output;
}
