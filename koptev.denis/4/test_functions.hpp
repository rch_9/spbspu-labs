/*test_functions.hpp*/

#ifndef TEST_FUNCTIONS_HPP
#define TEST_FUNCTIONS_HPP

#include "test_functions.hpp"
#include "phonebook.hpp"
#include <iostream>
#include <stdexcept>
#include <string>

std::string Nick("Nick");
std::string Jack("Jack");
std::string Ben("Ben");

std::string n_num("+7 911 867-59-94");
std::string j_num("+7 921 914-29-01");
std::string b_num("+7 981 777-62-36");

bool empty_book_test();
bool i_o_test();
bool modify_test();
bool insert_test();

bool(*Tests[])() = {
	&empty_book_test,
	&i_o_test,
	&modify_test,
	&insert_test
};

/* IMPLEMENTATION */

bool empty_book_test()
{
	std::cout << "<<empty_book_test>>" << std::endl;
	Phonebook Book;
	if (Book.begin() != Book.end())
	{
		std::cerr << "* Unexpected error! [empty_book_test]" << std::endl;
		return false;
	}
	try
	{
		Book.insert_after(Book.begin(), Phonebook::Record("Null", "Null"));
	}
	catch (const std::exception & exc)
	{
		std::cerr << "Expected error: " << exc.what() << std::endl;
		return true;
	}
	return false;
}

bool i_o_test()
{
	std::cout << "<<i_o_test>>" << std::endl;
	Phonebook Book;
	Book.push_b(Nick, n_num);
	Book.push_b(Phonebook::Record(Jack, j_num));
	Phonebook::iterator nick = Book.begin();
	Phonebook::iterator jack = --Book.end();
	if (nick->name != Nick || nick->number != n_num ||
		jack->name != Jack || jack->number != j_num)
	{
		std::cerr << "* Unexpected error! [i_o_test]" << std::endl;
		return false;
	}
	return true;
}

bool modify_test()
{
	std::cout << "<<modify_test>>" << std::endl;
	Phonebook Book;
	Book.push_b(Jack, j_num);
	Phonebook::iterator modified = Book.begin();
	Book.modify(modified, Phonebook::Record(Nick, n_num));

	if (modified->name != Nick || modified->number != n_num)
	{
		std::cerr << "* Unexpected error! [modify_test]" << std::endl;
		return false;
	}
	return true;
}

bool insert_test()
{
	std::cout << "<<insert_test>>" << std::endl;
	Phonebook Book;
	Book.push_b(Nick, n_num);
	Phonebook::iterator jack = Book.insert_before(Book.begin(), Phonebook::Record(Jack, j_num));
	if (jack != Book.begin() || jack->name != Jack || jack->number != j_num)
	{
		std::cerr << "* Unexpected error! [insert_test]" << std::endl;
		return false;
	}
	return true;
}

#endif