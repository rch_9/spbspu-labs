#include <iostream>
#include <vector>
#include <algorithm>

#include "f_coll.hpp"

int main()
{
	typedef f_coll::iterator F_ITER;
	typedef std::vector<int>::iterator V_ITER;

	f_coll MyFact(10); // 10 - max factorial value

	for (F_ITER f_it = MyFact.begin(); f_it != MyFact.end(); ++f_it)
	{
		std::cout << *f_it << std::endl;
	}

	std::vector<int> MyVect(MyFact.size());
	V_ITER vect_it = MyVect.begin();

	std::copy(MyFact.begin(), MyFact.end(), vect_it);

	for (V_ITER v_it = MyVect.begin(); v_it != MyVect.end(); ++v_it)
	{
		std::cout << *v_it << std::endl;
	}

	return 0;
}
