#ifndef F_COLL_HPP
#define F_COLL_HPP

#include <iterator>
#include <memory>
#include <iostream>

/* F_COLL DECLARATION */

class f_coll
{
	private:

		int max_pos;

	public:

		/* F_COLL'S ITERATOR DECLARATION */

		class iterator
			: public std::iterator<std::bidirectional_iterator_tag, int>
		{
			public:

				typedef int node_type;
				typedef iterator this_type;

				iterator();
				iterator(int pos_, int val_);

				bool operator ==(const this_type & rhs) const;
				bool operator !=(const this_type & rhs) const;
				iterator operator =(const this_type & iter);

				int operator *() const;

				this_type & operator ++();
				this_type operator ++(int);
				this_type & operator --();
				this_type operator --(int);

			private:

				int pos;
				int val;
		};

		/* F_COLL'S METHODS */

		f_coll();
		f_coll(int);
		iterator begin();
		iterator end();
		size_t size() const;

};

//********ITERATOR IPMLEMENTATION********//

f_coll::iterator::iterator()
	: pos(0), val(0)
{};

f_coll::iterator::iterator(int pos_, int val_)
	: pos(pos_), val(val_)
{};

bool f_coll::iterator::operator ==(const this_type & rhs) const
{
	return rhs.pos == pos;
}

inline bool f_coll::iterator::operator !=(const this_type & rhs) const
{
	return rhs.pos != pos;
}

inline int f_coll::iterator::operator *() const
{
	return val;
}

inline f_coll::iterator::this_type & f_coll::iterator::operator ++()
{
	++pos;
	if (pos <= 1)
	{
		val = 1;
	}
	else
	{
		val = val * (pos);
	}
	return *this;
}

inline f_coll::iterator::this_type f_coll::iterator::operator ++(int)
{
	iterator temp(*this);
	operator++();
	return temp;
}

inline f_coll::iterator::this_type & f_coll::iterator::operator --()
{
	if (pos > 0)
	{
		--pos;
		if (pos <= 1)
		{
			val = 1;
		}
		else
		{
			val = val / (pos + 1);
		}
	}
	else
	{
		std::cerr << "Error! Iterator is on the previous position." << std::endl;
	}
	return *this;
}

inline f_coll::iterator::this_type f_coll::iterator::operator --(int)
{
	iterator temp(*this);
	operator--();
	return temp;
}

inline f_coll::iterator f_coll::iterator::operator =(const this_type & iter)
{
	pos = iter.pos;
	val = iter.val;
	return *this;
}

//********F_COLL IMPLEMENTATION********//

f_coll::f_coll()
	: max_pos(0)
{}

f_coll::f_coll(int max_pos_)
	: max_pos(max_pos_)
{}

f_coll::iterator f_coll::begin()
{
	return iterator();
}

int fact_func(int n)
{
	return n > 1 ? n * fact_func(n - 1) : 1;
}

f_coll::iterator f_coll::end()
{
	return iterator(max_pos + 1, fact_func(max_pos + 1));
}

size_t f_coll::size() const
{
	return max_pos + 1;
}

#endif