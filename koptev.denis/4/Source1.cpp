#include <iostream>
#include "phonebook.hpp"
#include "test_functions.hpp"

int main()
{
	size_t Errors = 0;
	for (size_t i = 0; i < sizeof(Tests) / sizeof(Tests[i]); ++i)
	{
		if (!Tests[i]())
		{
			++Errors;
		}
	}

	std::cout << "\n----------------------------\n"
	          << Errors << " error(s) found" << std::endl;

	if (!Errors)
	{
		return 0;
	}
	else
	{
		return 1;

	}
}
