#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <list>
//#include <vector>	// variable

class Phonebook
{
	public:

		struct Record;
		// typedef std::vector<Record> Container;	 // variable
		typedef std::list<Record> Container;
		typedef Container::iterator iterator;

		iterator begin();
		iterator end();

		iterator insert_before(iterator, const Record &);
		iterator insert_after(iterator, const Record &);
		void modify(iterator, const Record &);
		void push_b(const std::string &, const std::string &);
		void push_b(const Record &);

private:

	Container phonebook;
};

struct Phonebook::Record
{
	std::string name;
	std::string number;
	Record();
	Record(const std::string &, const std::string &);
};

#endif
