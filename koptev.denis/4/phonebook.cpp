/*phonebook.cpp*/

#include "phonebook.hpp"
#include <cmath>
#include <iostream>
#include <stdexcept>

/* RECORD IMPLEMENTATION */

Phonebook::Record::Record(const std::string & name_, const std::string & number_)
: name(name_), number(number_)
{}

/* PHONEBOOK IMPLEMENTATION */

Phonebook::iterator Phonebook::begin()
{
	return phonebook.begin();
}

Phonebook::iterator Phonebook::end()
{
	return phonebook.end();
}

Phonebook::iterator Phonebook::insert_before(iterator iter, const Record & rec)
{
	if (phonebook.empty())
	{
		throw std::invalid_argument("Phonebook is empty!");
	}
	phonebook.insert(iter, rec);
	iter = phonebook.begin();
	return iter;
}

Phonebook::iterator Phonebook::insert_after(iterator iter, const Record & rec)
{
	if (phonebook.empty())
	{
		throw std::invalid_argument("Phonebook is empty!");
	}
	if (iter != phonebook.end())
	{
		++iter;
	}
	return insert_before(iter, rec);
}

void Phonebook::modify(iterator iter, const Record & rec)
{
	if (phonebook.empty())
	{
		std::cerr << "Phonebook is empty!" << std::endl;
		return;
	}
	iter->name = rec.name;
	iter->number = rec.number;
}

void Phonebook::push_b(const std::string & name_, const std::string & number_)
{
	phonebook.push_back(Record(name_, number_));
}

void Phonebook::push_b(const Record & rec)
{
	phonebook.push_back(rec);
}