#include <iostream>
#include <set>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <string>

int main()
{
	std::ifstream file("text.txt");
	if (!file)
	{
		std::cerr << "File hasn't been opened!" << std::endl;
		return 1;
	}
	std::set<std::string> words;
	std::copy(
	    std::istream_iterator<std::string>(file),
	    std::istream_iterator<std::string>(),
	    std::inserter(words, words.begin())
	);
	if (file.bad())
	{
		std::cerr << "Error while reading" << std::endl;
		return 1;
	}
	std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
	return 0;
}
