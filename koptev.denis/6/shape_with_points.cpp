/* shape_with_points.cpp */

#include "shape_with_points.hpp"

Shape::Point::Point()
	: x(0), y(0)
{}

Shape::Point::Point(const int & x_, const int & y_)
	: x(x_), y(y_)
{}

Shape::Shape()
{}

Shape::Shape(const std::vector<Point> & vertexes_)
	: vertexes(vertexes_)
{}

