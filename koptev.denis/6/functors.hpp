/* functors.hpp */

#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include "shape_with_points.hpp"
#include <cstdlib>
#include <functional>
#include <cmath>

/* to create random shape */

struct shape_creator
	: std::unary_function<void, Shape>
{
	Shape operator () ()
	{
		/*
		0 - triangle
		1 - square
		2 - pentagon
		3 - rectangle
		* in this order to make triangles and pentagons in one cycle
		*/
		size_t type = rand() % 4;
		std::vector<Shape::Point> shape_points;
		switch (type)
		{
			case 0:
			case 2:
			{
				for (size_t i = 0; i < type + 3; ++i)
				{
					shape_points.push_back(Shape::Point(rand() % 10, rand() % 10));
				}
				break;
			}
			case 1:
			{	//rand % 10 to make coordinate-numbers smaller
				int side = rand() % 10;
				int x0 = rand() % 10;
				int y0 = rand() % 10;
				shape_points.push_back(Shape::Point(x0, y0));
				shape_points.push_back(Shape::Point(x0 + side, y0));
				shape_points.push_back(Shape::Point(x0 + side, y0 + side));
				shape_points.push_back(Shape::Point(x0, y0 + side));
				break;
			}
			case 3:
			{
				shape_points.push_back(Shape::Point(rand() % 10, rand() % 10));
				shape_points.push_back(Shape::Point(rand() % 10, shape_points[0].y));
				shape_points.push_back(Shape::Point(shape_points[1].x, rand() % 10));
				shape_points.push_back(Shape::Point(shape_points[0].x, shape_points[2].y));
				break;
			}
		}
		return Shape(shape_points);
	}
};

/* for vertexes counter, returns current sum */

struct get_summ
	: std::binary_function<size_t, Shape, size_t>
{
	size_t operator () (size_t prev, const Shape & sh)
	{
		return prev + sh.vertexes.size();
	}
};

/* condition to count all but not pentagons */

struct pentagon_exceptor
	: std::unary_function<Shape, bool>
{
	bool operator () (const Shape & sh)
	{
		return sh.vertexes.size() != 5;
	}
};

/* obviously, condition to delete pentagons */

struct pentagon_deleter
	: std::unary_function<Shape, bool>
{
	bool operator () (const Shape & sh)
	{
		return sh.vertexes.size() == 5;
	}
};

/* to take one point from shape for vector<Point> */

struct transf_in_point
	: std::unary_function<Shape, Shape::Point>
{
	Shape::Point operator () (const Shape & sh)
	{
		return sh.vertexes.at(0);  // zero point on default
	}
};

/* counts length of shape's edge */

double edge_length(const Shape::Point & lhs, const Shape::Point & rhs)
{
	return std::sqrt(std::pow(rhs.x - lhs.x, 2) + std::pow(rhs.y - lhs.y, 2));
}

/* if it is square (using edge_length) */

struct square_detector
	: std::unary_function<Shape, bool>
{
	bool operator () (const Shape & sh)
	{
		return (edge_length(sh.vertexes.at(0), sh.vertexes.at(1)) ==
			edge_length(sh.vertexes.at(1), sh.vertexes.at(2)));
	}
};

/* for sorting shape-vector */

struct sort_condition
	: std::binary_function<Shape, Shape, bool>
{
	bool operator() (const Shape & lhs, const Shape & rhs)
	{
		if (lhs.vertexes.size() == 4 && rhs.vertexes.size() == 4)
		{
			return (square_detector()(lhs) && !square_detector()(rhs));
		}
		else
		{
			return lhs.vertexes.size() < rhs.vertexes.size();
		}
	}
};

#endif