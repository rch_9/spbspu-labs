#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <ctime>

#include "shape_with_points.hpp"
#include "functors.hpp"

void fill_vector(std::vector<Shape> &);
unsigned int count_points(const std::vector<Shape> &);
unsigned int count_figures(const std::vector<Shape> &);
void delete_pentagons(std::vector<Shape> &);
std::vector<Shape::Point> get_points(const std::vector<Shape> &);
void sort_vector(std::vector<Shape> &);
Shape create_random_shape();
template <typename T>
void print_vector(std::vector<T> & Vect);

int main()
{
	static int figure_amount = 5;
	//Fill vector with random figures
	srand(static_cast<unsigned>(time(0)));
	std::vector<Shape> Shapes(figure_amount);
	fill_vector(Shapes);

	//Print this vector
	print_vector(Shapes);

	//Count all vertexes and print amount
	std::cout << "\nAll vertexes: " << count_points(Shapes) << std::endl;

	//Count only triangles, squares and rectangles and print amount
	std::cout << "Triangles, squares and rectangles: " << count_figures(Shapes)
	          << " of " << figure_amount << "\n" << std::endl;

	//Deleting only pentagons and print result
	delete_pentagons(Shapes);
	print_vector(Shapes);

	//Create new vector of shapes' points from initial vector and print it
	std::vector<Shape::Point> Points = get_points(Shapes);
	std::cout << "\nNew vector of points: " << std::endl;
	print_vector(Points);

	//Sort in order: triangles, squares, rectangles. Print results
	sort_vector(Shapes);
	std::cout << "\nSorted vector: " << std::endl;
	print_vector(Shapes);

	return 0;
}

std::ostream & operator << (std::ostream & o, const Shape::Point & point)
{
	o << "(" << point.x << "," << point.y << "); ";
	return o;
}

std::ostream & operator << (std::ostream & o, const Shape & sh)
{
	o << "Number of points: " << sh.vertexes.size() << "  Type: ";
	switch (sh.vertexes.size())
	{
		case 3:
		{
				  std::cout << "TRIANGLE" << std::endl;
				  break;
		}
		case 4:
		{
				  if (square_detector()(sh))
				  {
					  std::cout << "SQUARE" << std::endl;
				  }
				  else
				  {
					  std::cout << "RECTANGLE" << std::endl;
				  }
				  break;
		} 
		case 5:
		{
				  std::cout << "PENTAGON" << std::endl;
				  break;
		}
		default:
		{
				   std::cout << "UNKNOWN FIGURE" << std::endl;
		}
	}
	std::copy(sh.vertexes.begin(), sh.vertexes.end(), std::ostream_iterator<Shape::Point>(o));
	return o;
}

void fill_vector(std::vector<Shape> & Shapes)
{
	std::generate(Shapes.begin(), Shapes.end(), shape_creator());
}

unsigned int count_points(const std::vector<Shape> & Shapes)
{
	return std::accumulate(Shapes.begin(), Shapes.end(), 0, get_summ());
}

unsigned int count_figures(const std::vector<Shape> & Shapes)
{
	return std::count_if(Shapes.begin(), Shapes.end(), pentagon_exceptor());
}

void delete_pentagons(std::vector<Shape> & Shapes)
{
	Shapes.erase(std::remove_if(Shapes.begin(), Shapes.end(), pentagon_deleter()), Shapes.end());
}

std::vector<Shape::Point> get_points(const std::vector<Shape> & Shapes)
{
	std::vector<Shape::Point> Points(Shapes.size());
	std::transform(Shapes.begin(), Shapes.end(), Points.begin(), transf_in_point());
	return Points;
}

void sort_vector(std::vector<Shape> & Shapes)
{
	std::sort(Shapes.begin(), Shapes.end(), sort_condition());
}

template <typename T>
void print_vector(std::vector<T> & Vect)
{
	std::copy(Vect.begin(), Vect.end(), std::ostream_iterator<T>(std::cout, "\n"));
}
