/* shape_with_points.hpp */

#ifndef SHAPE_WITH_POINTS_HPP
#define SHAPE_WITH_POINTS_HPP

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

struct Shape
{
	struct Point
	{
		int x, y;

		Point();
		Point(const int & x_, const int & y_);
	};

	std::vector<Point> vertexes;

	Shape();
	Shape(const std::vector<Point> & vertexes_);
};

#endif
