/* seq_functor.cpp */

#include "seq_functor.hpp"
#include <iostream>

Seq_functor::Seq_functor()
	: max_num(0), min_num(0), average(0), pos_amount(0),
	  neg_amount(0), odd_summ(0), even_summ(0), front_eq_back(false),
	  all_amount(0), front(0)
{};

void Seq_functor::operator () (int curr)
{
	if (all_amount == 0)
	{
		front = curr;
		min_num = curr;
		max_num = curr;
	}

	if (curr > max_num)
	{
		max_num = curr;
	}
	else
		if (curr < min_num)
		{
			min_num = curr;
		}

	average = static_cast<double>((all_amount * average + curr) / (++all_amount));

	if (curr > 0)
	{
		++pos_amount;
	}
	else
		if (curr < 0)
		{
			++neg_amount;
		}

	if (curr % 2 != 0)
	{
		odd_summ += curr;
	}
	else
	{
		even_summ += curr;
	}

	front_eq_back = (front == curr);
};

void Seq_functor::clear()
{
	min_num = INT_MAX;
	max_num = 0 - INT_MAX - 1;
	average = 0;
	pos_amount = 0;
	neg_amount = 0;
	odd_summ = 0;
	even_summ = 0;
	front_eq_back = false;
	all_amount = 0;
	front = 0;
}

std::ostream & operator << (std::ostream & o, Seq_functor & sf)
{
	o << "Sequence statistics:"
	  << "\nMinimal element: " << sf.min_num
	  << "\nMaximal element: " << sf.max_num
	  << "\nAverage: " << sf.average
	  << "\nAmount of positive elements: " << sf.pos_amount
	  << "\nAmount of negative elements: " << sf.neg_amount
	  << "\nSumm of odd elements: " << sf.odd_summ
	  << "\nSumm of even elements: " << sf.even_summ
	  << (sf.front_eq_back ? "\nFront is equal to back" : "\nFront isn't equal to back");
	return o;
}