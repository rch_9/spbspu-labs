#include <vector>
#include <iterator>
#include <algorithm>
#include <cstdlib>
#include <ctime>

#include "seq_functor.hpp"

const int size = 10;
const int try_num = 3;

int generator()
{
	return 500 - rand() % 1001;
}

int main()
{
	srand(static_cast<unsigned>(time(0)));

	for (int i = 0; i < try_num; ++i)
	{
		Seq_functor Analyzer;
		std::vector<int> Vect(size);
		std::generate(Vect.begin(), Vect.end(), generator);
		std::copy(Vect.begin(), Vect.end(), std::ostream_iterator<int>(std::cout, " "));
		std::cout << std::endl;
		Analyzer = std::for_each(Vect.begin(), Vect.end(), Analyzer);
		std::cout << Analyzer << "\n" << std::endl;
	}

	return 0;
}