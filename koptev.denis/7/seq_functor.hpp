/* seq_functor.hpp */

#ifndef SEQ_FUNCTOR_HPP
#define SEQ_FUNCTOR_HPP

#include <iostream>

class Seq_functor
{
	public:

		Seq_functor();
		void operator () (int);
		friend std::ostream & operator << (std::ostream &, Seq_functor &);
		void clear();

	private:

		int max_num;
		int min_num;
		double average;
		int pos_amount;
		int neg_amount;
		int odd_summ;
		int even_summ;
		bool front_eq_back;
		int front;
		int all_amount;
};

#endif
