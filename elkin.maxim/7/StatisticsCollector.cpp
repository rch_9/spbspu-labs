#include "StatisticsCollector.h"

StatisticsCollector::StatisticsCollector():
	maximum(-501),
	minimum(501),
	N(0),
	firstNumber(0),
	lastNumber(0),
	average(0.0),
	positiveNumbersCount(0),
	negativeNumbersCount(0),
	oddNumbersSum(0),
	evenNumbersSum(0),
	areLastAndFirstEqual(false)
	{};

void StatisticsCollector::operator()(const int & number)
{
	if (number > maximum){
		maximum = number;
	}
	if (number < minimum){
		minimum = number;
	}
	average += number;
	if (N == 0){
		firstNumber = number;
	}
	lastNumber = number;
	++N;
	if (number > 0){
		++positiveNumbersCount;
	}
	else if (number < 0){
		++negativeNumbersCount;
	}
	if (number % 2 == 0 && number != 0){
		++evenNumbersSum += number;
	}
	if (number % 2 == 1){
		oddNumbersSum += number;
	}
}

void StatisticsCollector::processTheStatistics(std::vector <int> & numbers)
{
	if (N == 0){
		throw std::invalid_argument("Statistics data is empty\n");
	}
	average /= N;
	if (lastNumber == firstNumber){
		areLastAndFirstEqual = true;
	}
}

void StatisticsCollector::printTheStatistics(std::vector<int> & numbers)
{
	if (numbers.empty()){
		std::cout << "\nNumerical sequence is empty\n";
	}
	else{
		processTheStatistics(numbers);
		std::cout << "\nMaximum of the sequence = " << maximum << std::endl;
		std::cout << "Minimum = " << minimum << std::endl;
		std::cout << "Average = " << average << std::endl;
		std::cout << "Positive numbers count = " << positiveNumbersCount << std::endl;
		std::cout << "Negative numbers count = " << negativeNumbersCount << std::endl;
		std::cout << "Odd numbers sum = " << oddNumbersSum << std::endl;
		std::cout << "Even numbers sum = " << evenNumbersSum << std::endl;
		if (areLastAndFirstEqual){
			std::cout << "Last and first numbers are equal\n";
		}
		else{
			std::cout << "Last and first numbers are not equal\n";
		}
	}
}