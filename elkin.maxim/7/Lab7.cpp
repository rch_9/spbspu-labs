#include "StatisticsCollector.h"

int main(int, char *)
{
	try{
		srand(time(NULL));
		std::vector <int> sequence(10,0);
		std::generate(sequence.begin(), sequence.end(), fillRandom());
		std::cout << "The numerical sequence:\n";
		std::for_each(sequence.begin(), sequence.end(), print());
		StatisticsCollector stat = std::for_each(sequence.begin(), sequence.end(), StatisticsCollector());
		stat.printTheStatistics(sequence);
	}
	catch (const std::invalid_argument & e){
		std::cerr << e.what();
		return 1;
	}
	return 0;
}

