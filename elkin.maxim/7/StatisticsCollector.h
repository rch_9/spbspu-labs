#ifndef STATISTICS_COLLECTOR_H
#define STATISTICS_COLLECTOR_H

#include <algorithm>
#include <iostream>
#include <vector>
#include <random>
#include <time.h>
#include <boost\bind.hpp>


struct fillRandom :
	public std::unary_function<void, int>
{
	int operator()() const
	{
		return rand() % 1001 - 500;
	}
};

struct print :
	public std::unary_function<int, void>
{
	void operator()(int & number) const
	{
		std::cout << number << " ";
	}
};

struct StatisticsCollector :
	public std::unary_function<int, void>
{
private:
	int maximum;
	int minimum;
	int N;
	int firstNumber;
	int lastNumber;
	double average;
	int positiveNumbersCount;
	int negativeNumbersCount;
	int oddNumbersSum;
	int evenNumbersSum;
	bool areLastAndFirstEqual;

public:
	StatisticsCollector();
	void operator()(const int & number);
	void processTheStatistics(std::vector <int> & numbers);
	void printTheStatistics(std::vector<int> & numbers);
};

#endif