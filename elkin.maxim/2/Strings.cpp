#include "Strings.h"

std::vector<std::string> formatFile(const std::string & fileName)
{
	std::vector<std::string> stringsCollection;
	if (fileName.size() == 0){
		throw std::invalid_argument("File name is absent\n");
	}
	std::ifstream file(fileName);
	if (!file){
		throw std::invalid_argument("File is not accessible\n");
	}
	Buffer buffer(stringsCollection);
	while (!file.eof()){
		char current = file.get();
		if (file.gcount() == 0 && !file.eof()){
			throw std::runtime_error("Access to the file was closed\n");
		}
		buffer.addToBuffer(current);
	}
	buffer.addRemains();
	return stringsCollection;
}

void printVectorToFile(const std::string & fileName, std::vector<std::string> & vector)
{
	if (fileName.size() == 0){
		throw std::invalid_argument("File name is absent");
	}
	std::ofstream file(fileName);
	if (!file){
		throw std::invalid_argument("File is not accessible");
	}

	for (int i = 0; i < vector.size(); ++i){
		if (!(file << vector[i])){
			throw std::runtime_error("Access to the file was closed\n");
		}
	}
}

Buffer::Buffer(std::vector<std::string> & _strings):
stringsCollection(_strings)
{}

void Buffer::wordProcess()
{
	if (word.size() > MAX_WORD_SIZE){
		word = "Vau!!!";
	}
}

bool Buffer::isPunctMark(const char ch)
{
	for (int i = 0; i < 6; ++i){
		if (punctuationMarks[i] == ch){
			return true;
		}
	}
	return false;
}

void Buffer::deleteLastSpace()
{
	if (buffer.size() > 1 &&
		buffer[buffer.size() - 1] == ' ' &&
		isalnum(buffer[buffer.size() - 2]) ||
		buffer.size() == 1 && buffer[0] == ' '){
		buffer.erase(--buffer.end());
	}
}

void Buffer::addWordAndSpace()
{
	if (!word.empty()){
		wordProcess();
		std::string temp(word.append(" "));
		addWithCheck(temp);
		word.clear();
	}
}

void Buffer::addPunctMarkAndSpace(const char ch)
{
	if (word.empty()){
		deleteLastSpace();
		std::string temp;
		temp += ch;
		temp += ' ';
		addWithCheck(temp);
	}
	else{
		wordProcess();
		word += ch;
		word += ' ';
		addWithCheck(word);
		word.clear();
	}
}

void Buffer::addRemains()
{
	if (!word.empty()){
		wordProcess();
		addWithCheck(word);
	}
	if (!buffer.empty()){
		stringsCollection.push_back(buffer);
	}
}

void Buffer::addToBuffer(const char ch)
{
	if (isalnum(ch)){
		word += ch;
	}
	else if (ch == ' '){
		addWordAndSpace();
	}
	else if (isPunctMark(ch)) {
		addPunctMarkAndSpace(ch);
	}
}

void Buffer::addWithCheck(const std::string & str)
{
	if (buffer.size() + str.size() > MAX_STRING_SIZE){
		stringsCollection.push_back(buffer);
		buffer = str;
	}
	else{
		buffer += str;
	}
}