#ifndef STRINGS_H
#define STRINGS_H

#include <string>
#include <fstream>
#include <vector>

struct Buffer
{
private:
	const std::string punctuationMarks = ".,!?:;";
	const unsigned int MAX_STRING_SIZE = 40;
	const unsigned int MAX_WORD_SIZE = 10;
	std::string buffer;
	std::vector<std::string> & stringsCollection;
	std::string word;
	
public:
	Buffer(std::vector<std::string> & _strings);
	void addToBuffer(const char ch);
	void addRemains();

private:
	void wordProcess();
	bool isPunctMark(const char ch);
	void deleteLastSpace();
	void addWordAndSpace();
	void addPunctMarkAndSpace(const char ch);
	void addWithCheck(const std::string & str);
};

std::vector<std::string> formatFile(const std::string & fileName);

void printVectorToFile(const std::string & fileName, std::vector<std::string> & vector);

#endif