#include <stdio.h>
#include <iostream>
#include "Strings.h"

int main(int, char*)
{
	try{
		printVectorToFile("input.txt", formatFile("input.txt"));
	}
	catch (const std::invalid_argument & e){
		std::cerr << e.what();
		return 1;
	}
	catch (const std::runtime_error & e){
		std::cerr << e.what();
		return 2;
	}
	return 0;
}

