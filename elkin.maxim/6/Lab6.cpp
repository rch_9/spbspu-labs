#include <set>
#include <string>
#include "Shapes.h"

void printWordsList(const std::string & fileName)
{
	if (fileName.empty()){
		throw std::invalid_argument("Filename is empty\n");
	}
	std::ifstream file(fileName);
	if (!file){
		throw std::invalid_argument("Access to the file is closed\n");
	}
	std::set<std::string> words;
	while (!file.eof()){
		if (file.bad()){
			throw std::runtime_error("Reading file error\n");
		}
		std::copy(std::istream_iterator<std::string>(file),
			std::istream_iterator<std::string>(),
			std::inserter(words, words.begin()));
		if (file.bad()){
			throw std::runtime_error("Reading file error\n");
		}
		std::copy(words.begin(), words.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
	}
}

int main(int, char *)
{
	try{
		//printWordsList("words.txt");
		srand(time(0));
		std::vector<Shape> shapes;
		std::vector<Point> points;
		fillVector(shapes);
		printContainer(shapes);
		getVertexesCount(shapes);
		getShapesCount(shapes);
		deletePentagons(shapes);
		std::cout << "After deleting pentagons:\n";
		printContainer(shapes);
		getCoordinates(points, shapes);
		std::cout << "Vector of vertexes:\n";
		printContainer(points);
		modifyShapesVector(shapes);
		std::cout << "After sorting the vector of shapes:\n";
		printContainer(shapes);

	}
	catch (const std::invalid_argument & e){
		std::cerr << e.what();
		return 1;
	}
	catch (const std::runtime_error & e){
		std::cerr << e.what();
		return 2;
	}
	return 0;
}

