#include "Shapes.h"

void fillVector(std::vector<Shape> & shapes)
{
	std::vector <Point> points;
	for (int i = 0; i < 6; ++i){
		int vertexesCount = rand() % 5 + 1;
		points.clear();
		if (vertexesCount != 4){
			for (int t = 0; t < vertexesCount; ++t){
				Point point;
				point.y = rand() % 21 - 10;
				point.x = rand() % 21 - 10;
				points.push_back(point);
			}
		}
		else {
			int shapeType = rand() % 2 + 1;
			if (shapeType == SQUARES){  
				int side = rand() % 21 - 10;
				int x0 = rand() % 21 - 10;
				int y0 = rand() % 21 - 10;
				Point point;
				point.x = x0;
				point.y = y0;
				points.push_back(point);
				point.x = x0 + side;
				point.y = y0;
				points.push_back(point);
				point.x = x0;
				point.y = y0 + side;
				points.push_back(point);
				point.x = x0 + side;
				point.y = y0 + side;
				points.push_back(point);
			}
			else{		// rectangle
				int side1 = rand() % 21 - 10;
				int side2 = rand() % 21 - 10;
				int x0 = rand() % 21 - 10;
				int y0 = rand() % 21 - 10;
				Point point;
				point.x = x0;
				point.y = y0;
				points.push_back(point);
				point.x = x0 + side1;
				point.y = y0;
				points.push_back(point);
				point.x = x0;
				point.y = y0 + side2;
				points.push_back(point);
				point.x = x0 + side1;
				point.y = y0 + side2;
				points.push_back(point);
			}
		}
		Shape shape;
		shape.vertexes = points;
		shapes.push_back(shape);
	}
}


int getVertexesCount(std::vector<Shape> & shapes)
{
	int N = 0;
	if (shapes.empty()){
		std::cout << "Shapes vector is empty\n";
	}
	else{
		N = std::accumulate(shapes.begin(), shapes.end(),0,
			boost::bind(std::plus<int>(),_1,
			boost::bind(&Shape::getVertexCount,_2)));
		std::cout << "Total vertexes count = " << N << '\n';
	}
	return N;
}

std::vector<int> getShapesCount(const std::vector<Shape> & shapes)
{
	std::vector<int> counts(3,0); // 1 - triangles, 2 - squares, 3 - rectangles
	counts = std::accumulate(shapes.begin(), shapes.end(),
		counts, SumShapes());
	std::cout << "Triangles count = " << counts[TRIANGLES]
		<< " Squares count = " << counts[SQUARES]
		<< " Rectangles count = " << counts[RECTANGLES] << "\n";
	return counts;
}

void deletePentagons(std::vector<Shape> & shapes)
{
	shapes.erase(std::remove_if(shapes.begin(), shapes.end(), isPentagon()), shapes.end());
}

void getCoordinates(std::vector<Point> & points, const std::vector<Shape> & shapes)
{
	std::transform(shapes.begin(), shapes.end(),
		std::back_inserter<std::vector<Point>>(points), boost::bind(&Shape::getPoint,_1));
}

bool Shape::isSquare() const
{
	if (getVertexCount() != 4){
		throw std::invalid_argument("Shape is not a tetragon\n");
	}
	int x1 = vertexes[0].x;
	int x2 = vertexes[1].x;
	int x3 = vertexes[2].x;
	int x4 = vertexes[3].x;
	int y1 = vertexes[0].y;
	int y2 = vertexes[1].y;
	int y3 = vertexes[2].y;
	int y4 = vertexes[3].y;
	double dist1 = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
	double dist2 = sqrt(pow(x3 - x1, 2) + pow(y3 - y1, 2));
	double dist3 = sqrt(pow(x4 - x1, 2) + pow(y4 - y1, 2));
	return (dist1 == dist2 || dist1 == dist3 ||
		dist2 == dist3);
}

void modifyShapesVector(std::vector<Shape> & shapes)
{
	std::sort(shapes.begin(), shapes.end(), compare());
}

std::vector<int>& SumShapes::operator()(std::vector<int> & vector, const Shape & shape) const
{
	if (shape.getVertexCount() < 4){
		++vector[TRIANGLES];
	}
	else if (shape.getVertexCount() == 4){
		if (shape.isSquare()) {
			++vector[SQUARES];
		}
		else{
			++vector[RECTANGLES];
		}
	}
	return vector;
}

bool compare::operator()(const Shape & shape1, const Shape & shape2) const
{
	if (shape1.getVertexCount() != shape2.getVertexCount()){
		return shape1.getVertexCount() < shape2.getVertexCount();
	}
	else{
		if (shape1.getVertexCount() == 4){
			if (shape1.isSquare()){
				if (!shape2.isSquare()){
					return true;
				}
			}
		}
	}
	return false;
}
