#ifndef SHAPES_H
#define SHAPES_H

#include <fstream>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <random>
#include <time.h>
#include <iterator>
#include <boost/bind.hpp>

#define TRIANGLES 0
#define SQUARES 1
#define RECTANGLES 2

struct Point
{
	int x, y;
	void print() const
	{
		std::cout << "x= " << x
			<< " y= " << y << '\n';
	}
} ;
struct Shape
{
	std::vector < Point > vertexes;
	bool isSquare() const;
	int getVertexCount() const
	{
		return vertexes.size();
	}
	Point getPoint() const
	{
		if (vertexes.empty()){
			throw std::invalid_argument("Vertexes vector is empty\n");
		}
		return vertexes[0];
	}
	void print() const
	{
		std::cout << "Vertexes count == " << getVertexCount();
		if (getVertexCount() == 4 && isSquare()){
			std::cout << 's';
		}
		std::cout << "\n";
		//printVector(shape.vertexes); // if neccessary
	}
};

void fillVector(std::vector<Shape> & shapes);
int getVertexesCount(std::vector<Shape> & shapes);
std::vector<int> getShapesCount(const std::vector<Shape> & shapes);
void deletePentagons(std::vector<Shape> & shapes);
void getCoordinates(std::vector<Point> & points,const std::vector<Shape> & shapes);
void modifyShapesVector(std::vector<Shape> & shapes);

template <typename ContainerType>
void printContainer(const ContainerType & elements)
{
	if (elements.empty()){
		std::cout << "Coordinates vector is empty\n";
	}
	else{
		std::for_each(elements.begin(), elements.end(), boost::bind(&ContainerType::value_type::print,_1));
	}
}

struct isPentagon :
	public std::unary_function<Shape, bool>
{
	bool operator()(const Shape & shape) const
	{
		return shape.getVertexCount() == 5;
	}
};

struct getPoints :
	public std::binary_function<std::vector<Point>, Shape, std::vector<Point>>
{
	std::vector<Point>& operator()(std::vector<Point> & points, const Shape & shape) const
	{
		points.push_back(shape.vertexes[0]);
		return points;
	}
};

struct SumShapes :
	public std::binary_function<std::vector<int>, Shape, std::vector<int>>
{
	std::vector<int>& operator()(std::vector<int> & vector, const Shape & shape) const;
};


struct compare:
	public std::binary_function<Shape, Shape, bool>
{
	bool operator()(const Shape & shape1, const Shape & shape2) const;
};

#endif