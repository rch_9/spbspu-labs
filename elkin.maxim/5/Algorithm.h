#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <time.h>
#include <iostream>

const std::string table[10] = { "Zero beginning", "First", "Second string",
"Third in it", "Fourth is here", "Fifth element", "Sixth fade",
"Seventh element", "Eighth try", "Ninth is last" };

typedef struct
{
	int key1;
	int key2;
	std::string str;
} DataStruct;

struct fill_with_random:
	public std::unary_function<DataStruct, void>
{
	void operator()(DataStruct & data) const
	{
		data.key1 = rand() % 11 - 5;
		data.key2 = rand() % 11 - 5;
		int index = rand() % 10;
		data.str = table[index];
	}
};

struct print :
	public std::unary_function<DataStruct, void>
{
	void operator()(const DataStruct & data) const
	{
		std::cout << "Key1 = " << data.key1
			<< " Key2 = " << data.key2 << " Str = "
			<< data.str << "\n";
	}
};

struct compare :
	public std::binary_function<DataStruct, DataStruct, bool>
{
	bool operator()(const DataStruct & data1, const DataStruct & data2) const
	{
		if (data1.key1 != data2.key1){
			return data1.key1 < data2.key1;
		}
		else if (data1.key2 != data2.key2){
			return data1.key2 < data2.key2;
		}
		else{
			return data1.str.length() < data2.str.length();
		}
	}
};

void modifyVector(std::vector<DataStruct> & vector)
{
	std::vector<DataStruct>::iterator begin = vector.begin();
	std::vector<DataStruct>::iterator end = vector.end();
	srand(time(0));
	std::for_each(begin, end, fill_with_random());
	std::cout << "Before sorting\n";
	std::for_each(begin, end, print());
	std::sort(begin, end, compare());
	std::cout << "After sorting\n";
	std::for_each(begin, end, print());
}

#endif