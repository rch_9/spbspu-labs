#include "datastruct.hpp"

#include <string>

DataStruct::DataStruct(int key1, int key2, std::string str)
    : key1_(key1)
    , key2_(key2)
    , str_(str)
{}
