#include "datastruct.hpp"
#include "comparator.hpp"
#include "functors.hpp"

#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>

int main()
{
    srand(time(0));

    const size_t range = 5;
    const size_t amount = 10; // loks nice on 1000
    const size_t str_amount = 10;

    std::string str_table[] = { "x", "xx", "xxx", "xxxx", "xxxxx", "xxxxxx", "xxxxxxx", "xxxxxxxx", "xxxxxxxxx", "xxxxxxxxxx" };

    typedef std::vector< DataStruct > data_vec;

    data_vec v;

    for (size_t i = 0; i < amount; ++i)
    {
        v.push_back (
            DataStruct ( (rand() % (2 * range + 1)) - range,
                         (rand() % (2 * range + 1)) - range,
                         str_table[ rand() % str_amount ]
                       )
        );
    }

    std::cout << "Normal: \n";
    for_each(v.begin(), v.end(), print_data());
    std::cout << std::endl;

    std::sort( v.begin(), v.end(), compare() );

    std::cout << "Sorted: \n";
    for_each(v.begin(), v.end(), print_data());
    std::cout << std::endl;

    return 0;
}
