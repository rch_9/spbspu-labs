#ifndef COMPARATOR_HPP
#define COMPARATOR_HPP

#include "datastruct.hpp"

#include <functional>

// means that `a` is less than `b`
class compare :
    public std::binary_function< DataStruct, DataStruct, bool >
{
public:
    bool operator()( const DataStruct & a, const DataStruct & b ) const
    {

        return (
                 (a.key1_ <  b.key1_)
            || ( (a.key1_ == b.key1_) && (a.key2_ <  b.key2_) )
            || ( (a.key1_ == b.key1_) && (a.key2_ == b.key2_) && (a.str_.length() < b.str_.length()) )
           );
    }
};

#endif

