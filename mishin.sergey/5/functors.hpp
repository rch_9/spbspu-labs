#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include "datastruct.hpp"

#include <iomanip>
#include <iostream>

class print_data
{
public:
	// should `inline`?
	void operator()(const DataStruct & value)
	{
		std::cout << "< " << std::setw(2) << value.key1_ << " "
		                  << std::setw(2) << value.key2_ << " "
		                  <<                  value.str_ << " >" << std::endl;
	}
};

#endif // FUNCTORS_HPP
