#ifndef DATASTRUCT_HPP
#define DATASTRUCT_HPP

#include <string>

class DataStruct
{
public:
    DataStruct(int key1, int key2, std::string str);
    int key1_;
    int key2_;
    std::string str_;
};

#endif
