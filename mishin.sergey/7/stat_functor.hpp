#include <cstddef>
#include <iostream>

class statistics
{
public:
    statistics()
        : average()
        , positive(0)
        , negative(0)
        , sum_odd(0)
        , sum_even(0)
        , first_last_match(false)
        , first_value_found(false)
        , count(0)
        , is_first_set(false)
    {
    }

    inline void operator()(const int & num)
    {
        if (first_value_found)
        {
            if (num > max)
            {
                max = num;
            }

            if (num < min)
            {
                min = num;
            }
        }
        else
        {
            first_value_found = true;
            max = num;
            min = num;
        }

        average = (average * count + num) / (count + 1);
        ++count;

        if (num < 0)
        {
            ++negative;
        }

        if (num > 0)
        {
            ++positive;
        }

        if (num % 2 == 1)
        {
            sum_odd += num;
        }
        else
        {
            sum_even += num;
        }

        if (!is_first_set)
        {
            first = num;
        is_first_set = true;
        first_last_match = true;
        }

        if ( is_first_set && num == first )
        {
            first_last_match = true;
        }
        else
        {
            first_last_match = false;
        }
    }
        // add get mathods
    void print()
    {

        if (!first_value_found)
        {
            std::cout << "Empty collection." << std::endl;
        }
        else
        {
	        std::cout << "max: " << max << std::endl;
	        std::cout << "min: " << min << std::endl;
        }

        std::cout << "avg: " << average << std::endl;
        std::cout << "pos: " << positive << std::endl;
        std::cout << "neg: " << negative << std::endl;
        std::cout << "+od: " << sum_odd << std::endl;
        std::cout << "+ev: " << sum_even << std::endl;

        if (first_last_match)
        {
            std::cout << "First and last are equal" << std::endl;
        }
        else
        {
            std::cout << "First and last are not eq" << std::endl;
        }
    }

private:
    int max;
    int min;
    double average;
    size_t positive; //count
    size_t negative;
    int sum_odd; // 1
    int sum_even; // 2
    bool first_last_match;

    bool first_value_found;
    size_t count;
    int first;
    bool is_first_set;
};

