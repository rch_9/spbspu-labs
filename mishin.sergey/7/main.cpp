#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FirstLabTests

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "stat_functor.hpp"

#include <iostream>
#include <algorithm>
#include <vector>

#include <climits>

BOOST_AUTO_TEST_CASE(testSixItems)
{
    size_t arr_size = 6;
    int arr[] = { -32, 3, 124, -4, 15, 53 };

    std::cout << "Starting values: ";

    for (size_t i = 0; i < arr_size; ++i)
    {
        std::cout << arr[i] << " ";
    }
    /* max = 124
     * min = -32
     * average = ...
     * positive = 4
     * negative = 2
     * sum_odd = 71
     * sum_even = 88
     * fisrt and last are not the same
     */

    std::cout << std::endl;

    std::vector<int> v(arr, arr+6);

    statistics stats;
    stats = std::for_each(v.begin(), v.end(), stats);

    stats.print();

    std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE(testOneItem)
{
    int arr[] = { 44 };

    std::cout << "Starting values: " << arr[0] << std::endl;
    /* max = 44
     * min = 44
     * average = 44
     * positive = 1
     * negative = 0
     * sum_odd = 0
     * sum_even = 44
     * fisrt and last are the same
     */
    std::vector<int> v(arr, arr+1);

    statistics stats;
    stats = std::for_each(v.begin(), v.end(), stats);

    stats.print();

    std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE(testEmpty)
{
    std::vector<int> v;

    std::cout << "No starting values" << std::endl;

    statistics stats;
    stats = std::for_each(v.begin(), v.end(), stats);

    stats.print();

    std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE(testAllNegative)
{
	size_t arr_size = 6;
	int arr[] = { -32, -3, -124, -4, -15, -53 };

	std::cout << "Starting values: ";

	for (size_t i = 0; i < arr_size; ++i)
	{
		std::cout << arr[i] << " ";
	}

	std::cout << std::endl;

	std::vector<int> v(arr, arr+6);

	statistics stats;
	stats = std::for_each(v.begin(), v.end(), stats);

	stats.print();

	std::cout << std::endl;
}
