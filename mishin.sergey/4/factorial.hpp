#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <cstddef>

size_t fac(size_t n);

#endif

