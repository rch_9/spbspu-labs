#include "fac_vec.hpp"
#include "factorial.hpp"

#include <iostream>

FacVector::FacVector()
    : size_(10)
{
}

FacVector::FacVector(size_t size)
    : size_(size)
{
}

FacVector::iterator FacVector::begin()
{
    return iterator(1, size() + 1);
}

FacVector::iterator FacVector::end()
{
    return iterator(size() + 1, size() + 1);
}

size_t FacVector::size()
{
    return size_;
}




FacVector::fac_iterator::fac_iterator()
	: current_(1)
	  , value_(1)
{}


FacVector::fac_iterator::fac_iterator(size_t n, size_t max)
	: maximum_(max)
{
	if (n <= maximum_)
	{
		current_ = n;
	}
	else
	{
		current_ = maximum_;
	}
	value_ = fac(current_);
}

FacVector::fac_iterator::fac_iterator(size_t n)
{
	if (n <= maximum_)
	{
		current_ = n;
	}
	value_ = fac(current_);
}



bool FacVector::fac_iterator::operator==(const FacVector::fac_iterator & rhs) const
{
	return current_ == rhs.current_;
}


bool FacVector::fac_iterator::operator!=(const FacVector::fac_iterator & rhs) const
{
	return current_ != rhs.current_;
}


size_t FacVector::fac_iterator::operator *() const
{
	return value_;
}


FacVector::fac_iterator FacVector::fac_iterator::operator++()
{
	if (current_ <= maximum_)
	{
		current_ += 1;
		value_ *= current_;
	}

	return *this;
}


FacVector::fac_iterator FacVector::fac_iterator::operator++(int)
{
	this_type tmp(*this);
	this->operator++();

	return tmp;
}

FacVector::fac_iterator FacVector::fac_iterator::operator--()
{
	if (current_ >= 1)
	{
		current_ -= 1;
		value_ /= current_;
	}

	return *this;
}

FacVector::fac_iterator FacVector::fac_iterator::operator--(int)
{
	this_type tmp(*this);
	this->operator--();

	return tmp;
}
