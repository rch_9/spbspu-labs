#ifndef FAC_VEC_HPP
#define FAC_VEC_HPP

#include <iostream>
#include <iterator>

class FacVector
{
public:

	
	class fac_iterator:
			public std::iterator< std::forward_iterator_tag, size_t >
	{
	public:
		typedef size_t node_type;
		typedef fac_iterator this_type;
		fac_iterator();
		fac_iterator(size_t n, size_t max);
		//fac_iterator(node_type * fac);
		bool operator==(const this_type&rhs) const;
		bool operator!=( const this_type&rhs) const;
		size_t operator*() const;
		this_type operator++();
		this_type operator++(int);
		this_type operator--();
		this_type operator--(int);
	private:
		node_type current_;
		node_type value_;
		node_type maximum_;

		fac_iterator(size_t n);
	};

    typedef size_t value_type;
    typedef fac_iterator iterator;

    FacVector();
    FacVector(size_t size_);

    iterator begin();
    iterator end();

    size_t size();

private:

    size_t size_;
};


///




#endif
