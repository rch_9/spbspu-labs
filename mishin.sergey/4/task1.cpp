#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FirstLabTests

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "phonebook.hpp"
#include "functors.hpp"

#include <vector>
#include <iostream>

BOOST_AUTO_TEST_CASE(outToStdout)
{
    Phonebook pb;

    pb.push_back("Zeroest", "4");
    pb.push_back("First", "1");
    pb.push_back("Second", "2");
    pb.push_back(Phonebook::Record("Third", "3"));
    pb.push_back("Forth", "4");
    pb.push_back("Fifth", "5");

    for (Phonebook::iterator iter = pb.begin();
         iter != pb.end();
         ++iter)
    {
        std::cout << "< " << iter->getName() << " " << iter->getNumber() << " >" << std::endl;
    }
}

BOOST_AUTO_TEST_CASE(findByName)
{
    Phonebook pb;

    pb.push_back("Zeroest", "4");
    pb.push_back("First", "1");
    pb.push_back("Second", "2");
    pb.push_back(Phonebook::Record("Third", "3"));
    pb.push_back("Forth", "4");
    pb.push_back("Fifth", "5");
    pb.push_back("Second", "6");

    std::string arr[] = {"2", "6"};
    std::vector< std::string > correct(arr, arr+2);
    std::vector< std::string > nts; // result

    // search for
    std::string number = "Second";

    typedef Phonebook::Record record_;
    typedef std::list< record_ > nb_rec;

    notebook_find< nb_rec > noter; // that names!..

    pb.findByName( noter, number ); // search for phone number 4

    std::cout << "Entries \\w name " << number << ": [ ";
    // Put found names to result vector
    for (std::vector< record_ >::iterator iter = noter.notes.begin();
            iter != noter.notes.end();
            ++iter)
    {
        std::cout << iter->getNumber() << ", ";
        nts.push_back(iter->getNumber());
    }
    std::cout << "]" << std::endl;

    BOOST_CHECK_EQUAL_COLLECTIONS(nts.begin(), nts.end(), 
                                  correct.begin(), correct.end());

}
