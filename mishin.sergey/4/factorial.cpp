#include "factorial.hpp"

size_t fac(size_t n)
{
    return n != 1 ? n*fac(n-1) : 1;
}
