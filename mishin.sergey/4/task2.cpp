#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FirstLabTests

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "fac_vec.hpp"

#include <algorithm>
#include <vector>
#include <iostream>

namespace 
{
    FacVector fv((10));
}

BOOST_AUTO_TEST_CASE(standardCtor)
{
    FacVector another_fv;
    
    std::cout << "Standard constructor: ";
    for (FacVector::iterator iter = another_fv.begin(); iter != another_fv.end(); ++iter)
    {
        std::cout << *iter << " ";
    }
    std::cout << "\n" << std::endl;

    BOOST_CHECK_EQUAL_COLLECTIONS(fv.begin(), fv.end(), 
                                  another_fv.begin(), another_fv.end());
}

BOOST_AUTO_TEST_CASE(outToStdout)
{
    std::cout << "Factorials from 1 to 10:" << std::endl;
    for (FacVector::iterator iter = fv.begin(); iter != fv.end(); ++iter)
    {
        std::cout << *iter << std::endl;
    }
}

BOOST_AUTO_TEST_CASE(stdCopyToVector)
{
    size_t arr[] = {1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800};
    std::vector< size_t > correct(arr, arr+10);
    std::vector< size_t > v(10, 0);

    std::copy(fv.begin(), fv.end(), v.begin());

    BOOST_CHECK_EQUAL_COLLECTIONS(v.begin(), v.end(), 
                                  correct.begin(), correct.end());
}
BOOST_AUTO_TEST_CASE(stdFind)
{
    FacVector::iterator it = find (fv.begin(), fv.end(), 720);
    BOOST_CHECK(it != fv.end());
}
