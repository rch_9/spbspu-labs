#include <cstddef>
#include <iostream>
#include <string>

template < typename T >
class notebook_find
{
public:
    void operator() ( typename T::const_iterator begin, typename T::const_iterator end, std::string name)
    {
        while (begin != end)
        {
            if (begin->getName() == name)
            {
                notes.push_back(*begin);
            }
            ++begin;
        }
    }

    std::vector< Phonebook::value_type > notes;
};
