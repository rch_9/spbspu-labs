#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include <string>
#include <list>

class Phonebook;

class Phonebook
{
public:
    class Record
    {
    public:
        Record(std::string name, std::string number);
        std::string getName() const;
        std::string getNumber() const;

        void setName(std::string name);
        void setNumber(std::string number);
    private:
        std::string name_;
        std::string number_;
    };

    typedef Record value_type;
    typedef std::list<value_type> container_type;
    typedef std::list<value_type>::iterator iterator;
    typedef std::list<value_type>::const_iterator const_iterator;

    Phonebook::iterator begin();
    Phonebook::iterator end();
    void insertAfter(Phonebook::iterator iter, const Phonebook::Record & record);
    void insertBefore(Phonebook::iterator iter, const Phonebook::Record & record);
    void push_back(const Phonebook::Record & record);
    void push_back(const std::string name, const std::string number);

    void modifyRecord(Phonebook::iterator iter, Phonebook::Record & record);
    template < typename Op >
    void process( Op & operation );

    template < typename Op >
    void findByName( Op & operation, std::string name ) const;

private:
    std::list<Record> book_;
};


Phonebook::Record::Record(std::string name, std::string number):
    name_(name),
    number_(number)
{
}

void Phonebook::Record::setName(std::string name)
{
    name_ = name;
}

void Phonebook::Record::setNumber(std::string number)
{
    number_ = number;
}

std::string Phonebook::Record::getName() const
{
    return name_;
}


std::string Phonebook::Record::getNumber() const
{
    return number_;
}

Phonebook::iterator Phonebook::begin()
{
    return book_.begin();
}

Phonebook::iterator Phonebook::end()
{
    return book_.end();
}

void Phonebook::insertAfter(Phonebook::iterator iter, const Phonebook::Record & record)
{
    if (iter != book_.end())
    {
        book_.insert(++iter, record);
    }
}

void Phonebook::insertBefore(Phonebook::iterator iter, const Phonebook::Record & record)
{
    book_.insert(iter, record);
}

void Phonebook::push_back(const Phonebook::Record & record)
{
    book_.push_back(record);
}

void Phonebook::push_back(std::string name, std::string number)
{
    book_.push_back(Record(name, number));
}

void Phonebook::modifyRecord(Phonebook::iterator iter, Phonebook::Record & record)
{
    iter->setName(record.getName());
    iter->setNumber(record.getNumber());
}


template < typename Op >
void Phonebook::process( Op & operation )
{
    operation(book_.begin(), book_.end());
}

template < typename Op >
void Phonebook::findByName( Op & operation, std::string name ) const
{
    operation(book_.begin(), book_.end(), name);
}

#endif // PHONEBOOK_HPP

