#include "shapes.hpp"

#include <vector>

Point::Point(int x, int y)
    : x(x)
    , y(y)
{
}

int Shape::getNum() const
{
	return vertexes.size();
}

void Shape::addPoint(const Point & point)
{
    vertexes.push_back(point);
}

// probably to copy? So it would be const...
// or probably `const vector & ...`
std::vector< Point > & Shape::getVertexes() // const
{
    return vertexes;
}
