#include "shapes.hpp"
#include "functors.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <functional>
#include <numeric>

int main()
{
    // triangles, squares, rectangles, pentagons
    // what name to choose: "generate_shape" or "shapes_generator" functor?

    srand(time(0));

    std::vector< Shape > v(10, Shape());

    // 1. fill vector \w shapes

    std::cout << "Vector init" << std::endl;

    std::for_each(v.begin(), v.end(), generate_shape());

    std::for_each(v.begin(), v.end(), print());
    std::cout << "\n" << std::endl;

    // 2. count all vertices

    int vertex_count =
		    std::accumulate(v.begin(), v.end(), 0, add_vertexes_num());

    std::cout << "Total vertices: " << vertex_count << std::endl;

    std::for_each(v.begin(), v.end(), print());
    std::cout << "\n" << std::endl;

    // 3. count triag & rects

    int shapes_count = count_if(v.begin(), v.end(), is_triangle_square_or_rect);
    std::cout << "Total triag & rects: " << shapes_count << std::endl;

    // 4. remove_if: remove if a pentagon!
    // end erase the ending

    std::cout << "Remove pantagons: " << std::endl;
    v.erase(
        std::remove_if(v.begin(), v.end(), pentagons_remover()),
        v.end()
    );

    std::for_each(v.begin(), v.end(), print());
    std::cout << "\n" << std::endl;

    // 5. transform ?
    // we needto create a new vec

    std::vector< Point > r; //(v.size(), Point(0, 0));
    r.reserve(v.size());
    std::transform(v.begin(), v.end(), back_inserter(r), pull_point());

    std::cout << "New vector: " << std::endl;
    std::for_each(r.begin(), r.end(), print_point());
    std::cout << "\n" << std::endl;

    // 6. Sort

    std::sort(v.begin(), v.end(), compare_shapes());

    std::cout << "Sorted: " << std::endl;

    std::for_each(v.begin(), v.end(), print());
    std::cout << "\n" << std::endl;

    return 0;
}
