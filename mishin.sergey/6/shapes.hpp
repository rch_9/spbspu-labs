#ifndef SHAPES_HPP
#define SHAPES_HPP

#include "shapes.hpp"

#include <vector>

class Point
{
public:
    Point(int x, int y);
    int x;
    int y;
};

class Shape
{
public:
//	Shape();
	int getNum() const;

    void addPoint(const Point & point);

    // btw, widely used: index -> indeces, vertex -> verteces, but `-xes` is also allowed;
	std::vector< Point > & getVertexes();// const;

private:
    std::vector< Point > vertexes;
};

#endif // SHAPES_HPP
