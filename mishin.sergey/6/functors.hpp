#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include <cstddef>
#include <cstdlib>
#include <iostream>

#include <algorithm>

int square_dist(const Point & a, const Point & b)
{
	return ( (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

bool is_square(Shape & shape)
{
	if ( shape.getNum() == 4 )
	{
		std::vector< Point > dots = shape.getVertexes();

		int dist[4];
		dist[0] = square_dist(dots[0], dots[1]);
		dist[1] = square_dist(dots[1], dots[2]);
		int diag0 = square_dist(dots[0], dots[2]); // diag 1
		int diag1 = square_dist(dots[1], dots[3]); // diag 2

//		std::cout << "\n* a: " << dist[0] << ", b: " << dist[1]
		//	          << ", d1: " << diag0 << ", d2: " << diag1 << std::endl;

		// (sad face)...
		// return ( dist[0] == dist[1] == dist[2] == dist[3] );

		return ( dist[0] == dist[1]
				&& diag0 == diag1	);
	}

	return false;
}

bool is_triangle(Shape & shape)
{
	if ( shape.getNum() == 3 )
	{
		std::vector< Point > dots = shape.getVertexes();

		int dist[3];
		dist[0] = square_dist(dots[0], dots[1]);
		dist[1] = square_dist(dots[1], dots[2]);
		dist[2] = square_dist(dots[2], dots[0]);

		// (sad face)...
		// return ( dist[0] == dist[1] == dist[2] == dist[3] );

		return ( dist[0] == dist[1]
				&& dist[1] == dist[2]
				&& dist[2] == dist[0]);
	}

	return false;
}

bool is_rectangle(Shape & shape)
{
	if ( shape.getNum() == 4 )
	{
		std::vector< Point > dots = shape.getVertexes();

		int dist[4];
		dist[0] = square_dist(dots[0], dots[1]);
		dist[1] = square_dist(dots[1], dots[2]);
		dist[2] = square_dist(dots[0], dots[2]); // diag 1
		dist[3] = square_dist(dots[1], dots[3]); // diag 2

		// angle btwin first and second line should be pi/2
		// thatmeans that tg_1 = -1 / tg_2
		// TODO add zeroes check
		/*
		double tg_1 = (dots[1].y - dots[0].y) / ( dots[1].x - dots[0].x );
		double tg_2 = (dots[2].y - dots[1].y) / ( dots[2].x - dots[1].x );

		if ( )
		*/
		return ( square_dist(dots[0], dots[1]) == square_dist(dots[2], dots[3])
				&& square_dist(dots[1], dots[2]) == square_dist(dots[3], dots[0])
				&& dist[2] == dist[3] );
				//	&& ( (tg_1 * tg_2) + 1 < 0.00002 ) ); // oh godd*mn...
	}

	return false;
}

// Somewhere here you may feel my pain writing these: fn(...){...}
// insteam of justa `Type`...
bool is_dots_are_square(Shape & shape)
{
	if ( shape.getNum() == 4 )
	{
		std::vector<int> dist;

		for (std::vector< Point >::iterator iter = shape.getVertexes().begin();
		     iter != shape.getVertexes().end();
		     ++iter)
		{
			for (std::vector< Point >::iterator inner = iter+1;
			     inner != shape.getVertexes().end();
			     ++inner)
			{
				int distance = (iter->x - inner->x) * (iter->x - inner->x)
						+ (iter->y - inner->y) * (iter->y - inner->y);

				dist.push_back(distance);
			}
		}
		std::sort(dist.begin(), dist.end());

		return ( (dist[0] == dist[1]) && (dist[1] == dist[2]) && (dist[2] == dist[3])
						&& (dist[4] == dist[5]) );
	}

	return false;
}

class print
{
public:
	void operator()(Shape & shape)
    {
        std::cout << "< " << shape.getNum() << " ";

        switch (shape.getNum())
        {
        case 0: std::cout << "None";
            break;
        case 3: std::cout << "Triang";
            break;
        case 4:
	        if (is_square(shape))
	        {
		        std::cout << "Square";
	        }
	        else if ( is_rectangle(shape))
	        {
		        std::cout << "Rectangle";
	        }
	        break;
        case 5: std::cout << "Penta";
            break;
        }

        std::cout << " > ";
    }
};

class print_point
{
public:
    void operator()(const Point & point)
    {
        std::cout << "(" << point.x << ", " << point.y << ") ";
    }
};

class generate_shape
{
public:
    void operator()(Shape & shape)
    {
        size_t sw = rand() % 4; // FIXME: 4 stands for `one of four types of shapes`

        size_t x0, y0, x1, y1, x2, y2, x3, y3, x4, y4;
        size_t len;

        switch (sw)
        {
        case 0: x0 = rand() % 10000;
	        y0 = rand() % 10000;
	        x1 = rand() % 10000;
	        y1 = rand() % 10000;
	        x2 = rand() % 10000;
	        y2 = rand() % 10000;
                shape.addPoint( Point(x0, y0) );
                shape.addPoint( Point(x1, y1) );
                shape.addPoint( Point(x2, y2) );

                break;
        case 1:
	        x0 = rand() % 10000;
	        y0 = rand() % 10000;
	        len = rand() % 10000;
                shape.addPoint( Point(x0, y0) );
                shape.addPoint( Point(x0, y0+len) );
                shape.addPoint( Point(x0+len, y0+len) ); // clockwise points
                shape.addPoint( Point(x0+len, y0) );

                break;
        case 2: // let's imagine that rectangles have no rotation :)
	        x0 = rand() % 10000;
	        y0 = rand() % 10000;
	        x1 = rand() % 10000;
	        y1 = rand() % 10000;
                shape.addPoint( Point(x0, y0) );
                shape.addPoint( Point(x0, y1) );
                shape.addPoint( Point(x1, y0) );
                shape.addPoint( Point(x1, y1) );

                break;
        case 3: x0 = rand() % 10000;
	        y0 = rand() % 10000;
	        x1 = rand() % 10000;
	        y1 = rand() % 10000;
	        x2 = rand() % 10000;
	        y2 = rand() % 10000;
	        x3 = rand() % 10000;
	        y3 = rand() % 10000;
	        x4 = rand() % 10000;
	        y4 = rand() % 10000; // Point( rand(), rand() ) ???
                shape.addPoint( Point(x0, y0) );
                shape.addPoint( Point(x1, y1) );
                shape.addPoint( Point(x2, y2) );
                shape.addPoint( Point(x3, y3) );
                shape.addPoint( Point(x4, y4) );

                break;
        }
    }
};

class add_vertexes_num :
		public std::binary_function<int,Shape,int>
{
public:
	int operator()(int a, const Shape & shape)
	{
		return (a + shape.getNum());
	}
};

bool is_triangle_square_or_rect(const Shape & shape)
{
	return ( (shape.getNum() == 3) || (shape.getNum() == 4) );
}


class pentagons_remover
{
public:
    bool operator()(Shape & shape)
    {
        return shape.getNum() == 5;
    }
};


class pull_point
{
public:
    Point operator()(Shape & shape)
    {
    // exhaustive check ???
    // if (shape.getNum() > 0)
    // {
        return shape.getVertexes()[0];
    //}
    }
};

class compare_shapes
{
public:
	// no `const` though...
    bool operator()(Shape & a, Shape & b)
    {
	    if (a.getNum() < b.getNum())
	    {
		    return true;
	    }
	    else if (a.getNum() == b.getNum())
	    {
		    return (is_square(a) && !is_square(b));
	    }
	    else
	    {
		    return false;
	    }
    }
};

#endif // FUNCTORS_HPP
