

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>

int main()
{
    std::ifstream fd ("text", std::ifstream::in);

    typedef std::vector< std::string > str_vec;
    str_vec v;

    if (fd)
    {
        /*
         * Well,
         * I probably should use fd.good()
         */
        while ( fd.good() )
        {
            std::string word;
            fd >> word;
            // std::cout << word << " ";
            if ( std::find(v.begin(), v.end(), word) == v.end() )
            {
                v.push_back(word);
            }
        }
    }

    for ( str_vec::iterator iter = v.begin();
          iter != v.end();
          ++iter)
    {
        std::cout << "< " << *iter << " > "; // << std::endl;
    }
    std::cout << "\n" << std::endl;

    return 0;
}
