#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <cstddef>
#include <iostream>

template < typename T >
class Queue
{
public:
    enum Priority {
        High,
        Normal,
        Low
    };

    typedef typename std::list< T > collection_type;
    typedef T value_type;

    void Put(T item, Queue::Priority prior);

    template < typename Op >
    void dump(Op operation) const;

    template < typename Op >
    void process_iter(Op operation); // or maybe bool

    template < typename Op >
    void process_item(Op operation); // or maybe bool

    void Accelerate();

  // Just for test!
    bool empty()
    {
      return ( high_priority_q.empty() && normal_priority_q.empty() && low_priority_q.empty() );
    }
private:
    // list has fast access to any element, deque's only to front & back
    // adding/removing an item invalidates deque's iterator, list's not
    // never iterator invalidation using list
    // std::list<T> q; // `q` for `queue`

    //Well, you know, three lists!

    collection_type high_priority_q;
    collection_type normal_priority_q;
    collection_type low_priority_q;

    template < typename Op >
    void iterate_over(const collection_type & q, Op operation) const;
};

template < typename T >
template < typename Op >
void Queue<T>::iterate_over(const collection_type & q, Op operation) const
{
    for ( typename collection_type::const_iterator iter = q.begin();
          iter != q.end();
          ++iter )
    {
        operation(*iter);
    }
  
}

template < typename T >
template < typename Op > 
void Queue<T>::dump(Op operation) const
{
    iterate_over(high_priority_q, operation);
    iterate_over(normal_priority_q, operation);
    iterate_over(low_priority_q, operation);
}

template < typename T >
template < typename Op > 
void Queue<T>::process_iter(Op operation)
{
    dump(operation);

    high_priority_q.clear();
    normal_priority_q.clear();
    low_priority_q.clear();
}

template < typename T >
template < typename Op > 
void Queue<T>::process_item(Op operation)
{
    value_type current;

    if (!high_priority_q.empty())
    {
        current = high_priority_q.front();
        high_priority_q.pop_front();
    }
    else if (!normal_priority_q.empty())
    {
        current = normal_priority_q.front();
        normal_priority_q.pop_front();
    }
    else if (!low_priority_q.empty())
    {
        current = low_priority_q.front();
        low_priority_q.pop_front();
    }
    else
    {
        return;
    }

    operation(current);
}

template < typename T >
void Queue<T>::Put(T item, Priority prior)
{
    switch (prior)
    {
    case Queue::High   : high_priority_q.push_back(item); 
                         break;
    case Queue::Normal : normal_priority_q.push_back(item); 
                         break;
    case Queue::Low    : low_priority_q.push_back(item); 
                         break;
    }
}

template < typename T >
void Queue<T>::Accelerate()
{
    high_priority_q.splice(high_priority_q.end(), low_priority_q);
}

#endif // QUEUE_HPP

