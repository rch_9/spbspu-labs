#ifndef FILL_LIST_HPP
#define FILL_LIST_HPP

#include <iostream>
#include <list>

template < typename T >
void writeOut(T begin, T end, bool switcher)
{   

    if (begin == end)
    {
        std::cout << *begin << std::endl;
        return;
    }

    if (switcher)
    {
        std::cout << *begin << " "; // Should this `if..else` unroll correctly?
        writeOut(++begin, end, !switcher);
    }
    else
    {
        std::cout << *end << " "; 
        writeOut(begin, --end, !switcher);
    }
}


template < typename T > 
void fillList(typename std::list<T>::iterator begin, 
              typename std::list<T>::iterator end, 
                       std::list<T> & v)
{   
    if (begin == end) 
    {
        return;
    }

    v.push_back(*begin);
    ++begin;
    
    if (begin == end)
    {
        return;
    }
    
    v.push_back(*(--end));

    fillList(begin, end, v);
}

#endif // FILL_LIST_HPP
