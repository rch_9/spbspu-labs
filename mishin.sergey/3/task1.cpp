#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FirstLabTests

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "queue.hpp"
#include "functors.hpp"

#include <iostream>

namespace
{
    typedef Queue< std::string > queue_str;

    queue_str take_pq_initial()
    {
        queue_str pq_initial;

        pq_initial.Put("first l", queue_str::Low);
        pq_initial.Put("second h", queue_str::High);
        pq_initial.Put("third n", queue_str::Normal);
        pq_initial.Put("forth n", queue_str::Normal);
        pq_initial.Put("fifth l", queue_str::Low);

        return pq_initial;
    }

    queue_str take_pq_accel()
    {
        queue_str pq_accelerated;

        pq_accelerated.Put("first l", queue_str::Low);
        pq_accelerated.Put("second h", queue_str::High);
        pq_accelerated.Put("third n", queue_str::Normal);
        pq_accelerated.Put("forth n", queue_str::Normal);
        pq_accelerated.Put("fifth l", queue_str::Low);

        return pq_accelerated;
    }
}


BOOST_AUTO_TEST_CASE(testDump)
{
    queue_str pq = take_pq_initial();

    std::cout << "Initial: " << std::endl;
    pq.dump(get_one_and_print< queue_str::value_type >());

    std::cout << std::endl;

    // dump mustn't change the queue
    // BOOST_CHECK_EQUAL_COLLECTIONS(pq.begin(), pq.end(), 
    //                              pq_initial.begin(), pq_initial.end());
}
    
BOOST_AUTO_TEST_CASE(testAccel)
{
    queue_str pq = take_pq_initial();
    // Unused:
    queue_str pq_accelerated = take_pq_accel();

    std::cout << "Initial: " << std::endl;
    pq.dump(get_one_and_print< queue_str::value_type >());

    pq.Accelerate();

    std::cout << "Accelerated: " << std::endl;
    pq.dump(get_one_and_print< queue_str::value_type >());

    std::cout << std::endl;

    // Well, I don't know how to correctly get pq.begin() ...
    // BOOST_CHECK_EQUAL_COLLECTIONS(pq.begin(), pq.end(), 
    //                              pq_accelerated.begin(), pq_accelerated.end());
}

BOOST_AUTO_TEST_CASE(testProcessIter)
{
    queue_str pq = take_pq_initial();

    std::cout << "Print and remove: " << std::endl;
    pq.process_iter(get_one_and_print< queue_str::value_type >());

    BOOST_CHECK(pq.empty());

    pq.process_iter(get_one_and_print< queue_str::value_type >());
    pq.process_item(get_one_and_print< queue_str::value_type >());

    BOOST_CHECK(pq.empty());
}

BOOST_AUTO_TEST_CASE(testProcessItem)
{
    queue_str pq = take_pq_initial();

    std::cout << "Print and remove: " << std::endl;
    pq.process_item(get_one_and_print< queue_str::value_type >());
    pq.process_item(get_one_and_print< queue_str::value_type >());
    pq.process_item(get_one_and_print< queue_str::value_type >());
    pq.process_item(get_one_and_print< queue_str::value_type >());
    pq.process_item(get_one_and_print< queue_str::value_type >());

    BOOST_CHECK(pq.empty());

    pq.process_item(get_one_and_print< queue_str::value_type >());

    BOOST_CHECK(pq.empty());
}
