#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include "queue.hpp"

template < typename T >
struct get_one_and_print
{
    void operator()( const T & element ) const
    {
        std::cout << "< " << element << " >" << std::endl;
    }
};


#endif // FUNCTORS_HPP
