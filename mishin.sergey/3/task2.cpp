#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FirstLabTests

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "functors.hpp"
#include "fill_list.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <list>

namespace {
    typedef std::list<int> list_int;
}

BOOST_AUTO_TEST_CASE(size0)
{
    list_int l;
    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK(shuffled.empty());
}

BOOST_AUTO_TEST_CASE(size1)
{
    int arr[] = { 2 };
    list_int l(arr, arr+1);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(l.begin(), l.end(), 
                                  shuffled.begin(), shuffled.end());
}

BOOST_AUTO_TEST_CASE(size2)
{
    int arr[] = { 2, 18 };
    int sh_arr[] = { 2, 18 };

    list_int l(arr, arr + 2);
    list_int correct(sh_arr, sh_arr + 2);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(correct.begin(), correct.end(), 
                                  shuffled.begin(), shuffled.end());
}

BOOST_AUTO_TEST_CASE(size3)
{
    int arr[] = { 4, 12, 1 };
    int sh_arr[] = { 4, 1, 12 };

    list_int l(arr, arr + 3);
    list_int correct(sh_arr, sh_arr + 3);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(correct.begin(), correct.end(), 
                                  shuffled.begin(), shuffled.end());
}

BOOST_AUTO_TEST_CASE(size4)
{
    int arr[] = { 1, 6, 12, 19 };
    int sh_arr[] = { 1, 19, 6, 12 };

    list_int l(arr, arr + 4);
    list_int correct(sh_arr, sh_arr + 4);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(correct.begin(), correct.end(), 
                                  shuffled.begin(), shuffled.end());
}

BOOST_AUTO_TEST_CASE(size5)
{
    int arr[] = { 13, 8, 12, 4, 5 };
    int sh_arr[] = { 13, 5, 8, 4, 12 };

    list_int l(arr, arr + 5);
    list_int correct(sh_arr, sh_arr + 5);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(correct.begin(), correct.end(), 
                                  shuffled.begin(), shuffled.end());
}

BOOST_AUTO_TEST_CASE(size7)
{
    int arr[] = { 13, 8, 20, 12, 4, 7, 5 };
    int sh_arr[] = { 13, 5, 8, 7, 20, 4, 12 };

    list_int l(arr, arr + 7);
    list_int correct(sh_arr, sh_arr + 7);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(correct.begin(), correct.end(), 
                                  shuffled.begin(), shuffled.end());
}

BOOST_AUTO_TEST_CASE(size14)
{
    int arr[] = { 8, 9, 13, 8, 2, 6, 20, 12, 4, 6, 1, 7, 5, 17 };
    int sh_arr[] = { 8, 17, 9, 5, 13, 7, 8, 1, 2, 6, 6, 4, 20, 12 };

    list_int l(arr, arr + 14);
    list_int correct(sh_arr, sh_arr + 14);

    list_int shuffled;
    
    fillList< int >(l.begin(), l.end(), shuffled);

    BOOST_CHECK_EQUAL_COLLECTIONS(correct.begin(), correct.end(), 
                                  shuffled.begin(), shuffled.end());
}

