#include "shape.hpp"

Shape::Shape(int x, int y) :
	origin_(Point(x,y))
{
}

Point Shape::getOrigin() const
{
	return origin_;
}

bool Shape::isMoreLeft(const Shape & another) const // okay, `more left` looks ugly...
{
	return (getOrigin().x < another.getOrigin().x);
}

bool Shape::isUpper(const Shape & another) const
{
	// well, Oy direction is `North`
	return (getOrigin().y > another.getOrigin().y);
}
