#include "point.hpp"
#include "shape.hpp"
#include "circle.hpp"
#include "triangle.hpp"
#include "square.hpp"

#include "functors.hpp"

#include <vector>
#include <list>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>

int main()
{
	typedef boost::shared_ptr< Shape > shared_shape;

	typedef std::vector< shared_shape > container_type; // doesn't work with std::list :(

	container_type l;

	boost::shared_ptr< Circle > c1 ( new Circle(2, 3) );
	boost::shared_ptr< Square > s1 ( new Square(5, 1) );
	boost::shared_ptr< Triangle > t1 ( new Triangle(1, 8) );

	l.push_back(c1);
	l.push_back(s1);
	l.push_back(t1);

	std::for_each(l.begin(), l.end(), draw_shape());
	std::cout << "\n "<< std::endl;
	// sorts

	std::cout << "Left to right:" << std::endl;
	std::sort(l.begin(), l.end(), boost::bind(adp_left_to_right(), _1, _2));
	std::for_each(l.begin(), l.end(), draw_shape());
	std::cout  << "\n "<< std::endl;

	std::cout << "Right to left:" << std::endl;
	std::sort(l.begin(), l.end(), boost::bind(adp_rigth_to_left(), _1, _2));
	std::for_each(l.begin(), l.end(), draw_shape());
	std::cout << "\n " << std::endl;

	std::cout << "Up to down:" << std::endl;
	std::sort(l.begin(), l.end(), boost::bind(adp_top_to_bottom(), _1, _2)); // y-axis going up
	std::for_each(l.begin(), l.end(), draw_shape());
	std::cout << "\n " << std::endl;

	std::cout << "Down to up:" << std::endl;
	std::sort(l.begin(), l.end(), boost::bind(adp_bottom_to_top(), _1, _2));
	std::for_each(l.begin(), l.end(), draw_shape());
	std::cout << "\n " << std::endl;

	return 0;
}
