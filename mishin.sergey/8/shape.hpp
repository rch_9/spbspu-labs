#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "point.hpp"

class Shape
{
public:
	Shape(int x, int y);

	Point getOrigin() const;

	bool isMoreLeft(const Shape & another) const; // okay, `more left` looks ugly...
	bool isUpper(const Shape & another) const;

	virtual void draw() const = 0;

private:
	Point origin_; // well, ``origin` means `geometry centre`
};

#endif
