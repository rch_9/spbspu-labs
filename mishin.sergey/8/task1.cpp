#include <iostream>
#include <vector>
#include <algorithm>
#include <functional> // for std::multiplies
#include <cmath>

#include <boost/bind.hpp>

int main()
{
	typedef std::vector< double > double_vec;
	double_vec v;

	v.push_back(1.0);
	v.push_back(2.0);

	// Ok, well, ...
	std::transform(v.begin(), v.end(), v.begin(),
			boost::bind(std::multiplies< double >(), _1, M_PI));

	for (double_vec::iterator iter = v.begin(); iter != v.end(); ++iter)
	{
		std::cout << "< " << *iter << " > ";
	}
	std::cout << std::endl;

	return 0;
}
