#include "square.hpp"

#include <iostream>

Square::Square(int x, int y) :
	Shape(x, y)
{
}

void Square::draw() const
{
	std::cout << "< " << "Square (" << getOrigin().x << ", " << getOrigin().y << ") > ";
}
