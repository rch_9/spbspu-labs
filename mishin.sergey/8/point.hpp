#ifndef POINT_HPP
#define POINT_HPP

class Point
{
public:
	Point(int x, int y);
	int x;
	int y;
};

#endif
