#include "triangle.hpp"

#include <iostream>

Triangle::Triangle(int x, int y) :
	Shape(x, y)
{
}

void Triangle::draw() const
{
	std::cout << "< " << "Triangle (" << getOrigin().x << ", " << getOrigin().y << ") > ";
}
