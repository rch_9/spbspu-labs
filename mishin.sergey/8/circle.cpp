#include "circle.hpp"

#include <iostream>

Circle::Circle(int x, int y) :
	Shape(x, y)
{
}

void Circle::draw() const
{
	std::cout << "< " << "Circle (" << getOrigin().x << ", " << getOrigin().y << ") > ";
}
