#ifndef CIRCLE_HPP
#define CIRCLE_HPP

#include "shape.hpp"

#include <iostream>

class Circle :
		public Shape
{
public:
	Circle(int x, int y);

	void draw() const;

};

#endif
