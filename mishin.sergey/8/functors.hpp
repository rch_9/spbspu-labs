#include "shape.hpp"

#include <boost/shared_ptr.hpp>

namespace
{
	typedef boost::shared_ptr< Shape > shared_shape;
}

class draw_shape
{
public:
	void operator()(const Shape & shape) const // no conversion from boost::shared_ptr to const ref :C
	{                                          // to `Shape *` also
		shape.draw();
	}
	void operator()(const shared_shape & shape) const
	{
		shape->draw();
	}
};

class cmp_2_coords // not used,has similar functionality as below:
{
public:
	bool operator()(int c1, int c2)
	{
		return c1 < c2;
	}
};

class adp_left_to_right
{
public:
	typedef bool result_type; // required by boost::bind
	bool operator()(const shared_shape & sh1, const shared_shape & sh2)
	{
		return sh1->isMoreLeft(*sh2);
	}
};

class adp_rigth_to_left
{
public:
	typedef bool result_type; // required by boost::bind
	bool operator()(const shared_shape & sh1, const shared_shape & sh2)
	{
		return sh2->isMoreLeft(*sh1);
	}
};

class adp_top_to_bottom // up to down // true if sh1.y > sh2.y
{
public:
	typedef bool result_type; // required by boost::bind
	bool operator()(const shared_shape & sh1, const shared_shape & sh2)
	{
		return sh1->isUpper(*sh2);
	}
};

class adp_bottom_to_top
{
public:
	typedef bool result_type; // required by boost::bind
	bool operator()(const shared_shape & sh1, const shared_shape & sh2)
	{
		return sh2->isUpper(*sh1);
	}
};



