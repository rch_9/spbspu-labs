#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cctype> // isspace

const size_t MAX_LINE_LENGTH = 40;
const size_t MAX_WORD_LENGTH = 10;

const std::string DEFAULT_REPLACE = "Vau!!!";

void appendWord(std::vector<std::string> & lines, size_t &  current_line, std::string & word, bool & is_new_line)
{

    if (word.size() > MAX_WORD_LENGTH)
    {
        // the word is "some_long_word,,,,.."
        // should it become "Vau!!!,,,,.." or just "Vau!!!" ???
        word = DEFAULT_REPLACE;
    }
    if (lines[current_line].size() + word.size() + 1 > MAX_LINE_LENGTH)
    {
        lines.push_back("");
        ++current_line;
        is_new_line = true;
    }
 
    if (!is_new_line)
    {
        lines[current_line].append(" ");
    }
    lines[current_line].append(word);

    word.clear();
}

int main()
{
    std::vector<std::string> lines;

    std::ifstream inputfile;
    inputfile.open("text", std::ios::in);
    
    std::string word;
    lines.push_back("");

    std::string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";// A <= x <= z;
    std::string symbolic = ".,!?:;";

    char ch;

    // states: 
    bool last_is_symbolic = false;
    bool is_new_line = true;
    bool is_new_word = false;
    size_t current_line = 0;

    while (inputfile >> std::noskipws >> ch) //! maybe `do while` to exlude adding last word   
    {
        for (size_t i = 0; i < symbolic.size(); ++i)
        {
            if (ch == symbolic[i])
            {
                word.append(1, ch); // add a sym to current word
                last_is_symbolic = true;

                break;
            }
        }
        for (size_t i = 0; i < alphabet.size(); ++i)
        {
            if (ch == alphabet[i])
            {
                if (last_is_symbolic)
                {
                    is_new_word = true;
                }

                if (is_new_word)
                {
                    // word looks like "someword,,,"
                    appendWord(lines, current_line, word, is_new_line);
                    is_new_word = false;
                    is_new_line = false;
                }
                word.append(1, ch);
                last_is_symbolic = false;

                break;
            }
        }
        
        if (isspace(ch)) // this case is separated `by-design`
        {
            last_is_symbolic = true;
        }
    }

    if (word.size()) // move to `do while` ?
    {
        appendWord(lines, current_line, word, is_new_line);
    }

    for (size_t i = 0; i < current_line + 1; ++i)
    {
        // quick line to check all sizes:
        //std::cout << lines[i].size() << " symbols: \"";

        std::cout << "\"" << lines[i] << "\"" <<  std::endl;
        if ( lines[i].size() > MAX_LINE_LENGTH )
        {
            std::cout << "\nTest FAILED! One of the lines is over 40 symbols" << std::endl;
            return 0;
        }
    }

    std::ofstream outputfile;
    outputfile.open("filtered_text", std::ios::out);
    for (size_t i = 0; i < current_line + 1; ++i)
    {
        outputfile << lines[i] << std::endl;
    }

    std::cout << "\nTest passed!" << std::endl;
    return 0;
}
