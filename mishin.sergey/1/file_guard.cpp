#include "file_guard.hpp"

//template < typename T >
CFileGuard::CFileGuard(FILE * fd):
	fd(fd)
{} 

//template < typename T > 
CFileGuard::~CFileGuard()
{ 
    if (fd != 0)
    {
        fclose(fd);
    }
}


//template < typename T > 
FILE * CFileGuard::get() const
{
	return fd;
}
