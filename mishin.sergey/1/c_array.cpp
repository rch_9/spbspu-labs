#include "file_guard.hpp"

#include <utility> // `std::pair` AFAI remember
#include <cstdio> // fopen
#include <cstdlib> // malloc & randmax
#include <ctime> // rand

#include <iostream>


size_t readArrayFromFile(char *&arr) 
{
    CFileGuard file ( fopen("stdin1", "r") );

    size_t multiplier = 2; 
    size_t total_chars = 0;
    if (file.get() != 0)
    {
        while (!feof(file.get())) {

            if (total_chars == 0)
            {
                arr = static_cast<char*>( realloc(arr, 1024) );
                total_chars += fread(arr, sizeof(char), 1, file.get());
            }
            else
            {
                size_t size = total_chars;
                arr = static_cast<char*>( realloc(arr, size * multiplier) );
                
                total_chars += fread(arr+total_chars, sizeof(char), size, file.get());
            }

        }

    }

    // The following line can `native`-ly output an array, wondering
    //std::cout << "arr ptr: " << arr << std::endl;
    return total_chars;
}

void fillRandom(double * array, int size)
{

    for (int i = 0; i < size; ++i)
    {
        int rands = rand();
        //std::cout << rands % RAND_MAX << std::endl;
        double value = rands % RAND_MAX;
//        std::cout << "rm/rm" << (RAND_MAX - 1) % RAND_MAX << std::endl;
        //std::cout << value  << std::endl;
        value = (value - (RAND_MAX / 2)) / (RAND_MAX / 2);
//        std::cout << value << std::endl;
        array[i] = value;
    }
}
