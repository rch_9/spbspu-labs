# Lab #1

## Building

### Requirments 
* Clang++ 4.9 (or another c++03 compiler)
* Boost unit tests
* make

### Build
* To build an `app` use

```
make
``` 

* To build and run use

```
make run
```

### Delete object and binary files
* To delete object and binary files (*.o and `app`)

```
make clean
```

