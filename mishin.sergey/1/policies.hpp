#ifndef POLICIES_HPP
#define POLICIES_HPP

#include <vector>
#include "policies.hpp"

template < typename Collection >
struct get_item_braces
{
    typedef typename Collection::value_type value_t;
    typedef typename Collection::size_type size_t;
    //typedef typename Collection::iterator iter;
    typedef typename Collection::size_type iter;

    static value_t & get( Collection & v, size_t n )
    {
        return v[n];
    }
    static iter begin( Collection & )
    {
        return 0;
    }

    static iter end( Collection & v )
    {
        return v.size();
    }

};


template < typename Collection >
struct get_item_at
{
    typedef typename Collection::value_type value_t;
    typedef typename Collection::size_type size_t;
    typedef typename Collection::size_type iter;

    static value_t & get( Collection & v, size_t n )
    {
        return v.at(n);
    }

    static iter begin( Collection & )
    {
        return 0;
    }

    static iter end( Collection & v )
    {
        return v.size();
    }

};


template < typename Collection >
struct get_item_iter
{
    typedef typename Collection::value_type value_t;
    typedef typename Collection::size_type size_t;
    typedef typename Collection::iterator iter;

    static value_t & get( Collection & , iter iter )
    {
        return *iter;
    }

    static iter begin( Collection & v )
    {
        return v.begin();
    }

    static iter end( Collection & v )
    {
        return v.end();
    }


};

#endif // POLICIES_HPP

