#ifndef ITEM_HPP
#define ITEM_HPP

#include <cstddef> //! for NULL, btw it's possible to use `nullptr` by default in c++11, not 03 :)
#include <iostream>

template < typename T >
class item
{
public:
    T value;
    item<T> * next;

    item( T val );

};

template < typename T >
item<T>::item( T val )
{
    this->value = val;
    next = 0;
}

#endif
