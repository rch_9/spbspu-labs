#ifndef C_ARRAY
#define C_ARRAY

size_t readArrayFromFile(char *&arr); // suppose size of array is 0
void fillRandom(double * array, int size);

#endif
