#ifndef CONS_ITERATOR
#define CONS_ITERATOR

template < typename T >
class cons_iterator:
    public std::iterator< std::forward_iterator_tag, T >
{
public:
    typedef item<T> node_type;
    typedef cons_iterator<T> this_type;
    cons_iterator();
    cons_iterator(node_type * cons);
    bool operator==(const this_type&rhs) const;  
    bool operator !=( const this_type&rhs) const;  
    T & operator*() const;
    T * operator->() const;
    this_type operator+(const int & i);
    this_type & operator++();
    this_type operator++(int);
private:
    node_type* current_ ;
};

template < typename T >
cons_iterator<T>::cons_iterator()
    : current_(0)
{}

template < typename T >
cons_iterator<T>::cons_iterator( cons_iterator<T>::node_type * cons)
    : current_(cons)
{}

template < typename T >
bool cons_iterator<T>::operator==(const cons_iterator<T> & rhs) const
{
    return current_ == rhs.current_;
}

template < typename T >
bool cons_iterator<T>::operator!=(const cons_iterator<T> & rhs) const
{
    return current_ != rhs.current_;
}

template < typename T >
T & cons_iterator<T>::operator *() const
{
    assert(current_ != 0);

    return current_->value;
}

template < typename T >
T * cons_iterator<T>::operator ->() const
{
    assert(current_ != 0);

    return &current_->value;
}

template < typename T >
cons_iterator<T> & cons_iterator<T>::operator++()
{
    assert(current_ != 0);
    current_ = current_->next;

    return *this;
}

template < typename T >
cons_iterator<T> cons_iterator<T>::operator++(int)
{
    this_type tmp(*this);
    this->operator++();

    return tmp;
}

template < typename T >
cons_iterator<T> cons_iterator<T>::operator+(const int & i)
{
    cons_iterator tmp = *this;
    for (int j = 0; j < i; ++j)
    {
        ++tmp;
    }

    return tmp;
}


#endif // CONS_ITERATOR
