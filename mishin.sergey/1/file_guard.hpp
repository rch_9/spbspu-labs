#ifndef FILE_GUARD_HPP
#define FILE_GUARD_HPP

#include <cstdio>

class CFileGuard
{
public:
	typedef CFileGuard this_type;
 
    CFileGuard(FILE * fd);
	~CFileGuard();
 
	FILE * get() const;
private:
	FILE * fd ;
	CFileGuard(const this_type &);
	void operator =(const this_type &);
};

#endif // FILE_GUARD_HPP
