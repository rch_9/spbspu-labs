#ifndef SORT_HPP
#define SORT_HPP

#include "policies.hpp"

#include <iostream>


template < template < typename > class GetPolicy, typename T >
void Sort( T & v ) // sort<Policy>(v);
{
    typedef typename GetPolicy<T>::iter iter;

    for (iter j = GetPolicy<T>::begin(v); j != GetPolicy<T>::end(v); ++j)
    {
        for (iter i = GetPolicy<T>::begin(v); i != GetPolicy<T>::end(v); ++i)
        {
            if ( (GetPolicy<T>::get(v, i)) > (GetPolicy<T>::get(v, j)) ) 
            {
                std::swap(GetPolicy<T>::get(v, i), GetPolicy<T>::get(v, j));
            }
        }
    }
}

#endif //GUARD_CPP
