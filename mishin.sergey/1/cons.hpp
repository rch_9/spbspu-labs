#ifndef CONS_HPP
#define CONS_HPP

#include <iostream>
#include "item.hpp"
#include "cons_iterator.hpp"

#include <stdexcept>
#include <new>

/////////////////////////////////

template < typename T > class ConsList;
template < typename T > std::ostream & operator<< (std::ostream & os, const ConsList<T> & rv);

template < typename T >
class ConsList
{
public:
    typedef  T value_type;
    typedef size_t size_type;
    typedef cons_iterator<T> iterator;
    inline ConsList();

    inline ~ConsList();

    inline cons_iterator<T> begin() const;
    inline cons_iterator<T> end() const;

    inline T get(size_t index) const;
    inline void push_back(T val);
    inline void push_front(T val);
    inline void sort();
    friend std::ostream & operator<< <> (std::ostream & os, const ConsList<T> & rv);
    inline ConsList<T> & operator=(const ConsList<T> &);
private:
    item<T> * head;
    inline ConsList( const ConsList<T> & );
};

/////////////////////////////////

template < typename T >
inline std::ostream & operator <<(std::ostream & os, const ConsList<T> & rv)
{
    item<T> * iter = rv.head;
    while (iter != 0)
    {
        os << iter->value << ", ";
        iter = iter->next;
    }
    return os;
}

template < typename T >
inline ConsList<T>::ConsList()
    : head(0)
{
}

template < typename T >
inline cons_iterator<T> ConsList<T>::begin() const
{
    return head;
}

template < typename T >
inline cons_iterator<T> ConsList<T>::end() const
{
    // BTW, the end of the Cons looks like 0, so:
    return 0;
}

template < typename T >
inline ConsList<T>::~ConsList()
{
    while (head != 0)
    {
        item<T> * prev = head;
        head = head->next;
        delete prev;
    }
}

template < typename T >
inline T ConsList<T>::get(size_t index) const
{
    item<T> * iter = this->head;
    size_t i = 0;
    while (iter != 0)
    {
        if (index == i)
        {
            return iter->value;
        }
        iter = iter->next;
        ++i;
    }

}



template < typename T >
inline void ConsList<T>::push_front( T val )
{
    item<T> * tmp = new item<T>(val);
    tmp->next = head;
    head = tmp;
}

template < typename T >
inline void ConsList<T>::sort()
{
    item<T> * iter = head;

    bool switched = true;
    bool loop = true;
    while(loop)
    {
        switched = false;
        while (true)
        {
            if((iter == head) && (iter->value > iter->next->value))
            {
                item<T> * tmp = iter->next;
                iter->next = iter->next->next;
                head = tmp;
                head->next = iter;
                switched = true;

                iter = head;
            }
            if ((iter->next->next != 0) && (iter->next->value > iter->next->next->value))
            {
                item<T> * tmp = iter->next;
                item<T> * tmp2 = tmp->next->next;
                iter->next = iter->next->next;
                iter->next->next = tmp;
                iter->next->next->next = tmp2;
                switched = true;
            }
            iter = iter->next;
            if ((iter->next == 0) || (iter->next->next == 0))
            {
                break;
            }
        }
        if (!switched)
        {
            break;
        }
        iter = this->head;
    }
}

#endif //CONS_HPP
