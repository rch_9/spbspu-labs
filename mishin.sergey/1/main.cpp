#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE FirstLabTests

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <vector>
#include <iterator>
#include <iomanip>

#include "sort.hpp"
#include "policies.hpp"
#include "cons.hpp"
#include "c_array.hpp"
#include "vec_modifications.hpp"

std::vector<int> take_correct() //! looks nice! Well, not.
{
    std::vector<int> v;
    v.push_back(-1);
    v.push_back(1);
    v.push_back(2);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(5);
    return v;
}

std::vector<int> take_shuffled() //same vector but `shuffled`
{
    std::vector<int> v;
    v.push_back(1);
    v.push_back(5);
    v.push_back(3);
    v.push_back(4);
    v.push_back(5);
    v.push_back(2);
    v.push_back(-1);
    return v;
}

static std::vector<int> correct = take_correct();

BOOST_AUTO_TEST_CASE(sortWithBraces)
{
    std::vector<int> v = take_shuffled();
    Sort< get_item_braces >(v);
 
    BOOST_CHECK(v == correct);
}

BOOST_AUTO_TEST_CASE(sortWithAt)
{
    std::vector<int> v = take_shuffled();
    Sort< get_item_at >(v);

    BOOST_CHECK(v == correct);
}


BOOST_AUTO_TEST_CASE(sortConsList)
{
    ConsList<int> cons;
    
    cons.push_front(1); //! I really don't now how to overload braces 
                  //! or curly-braces for 'one-line-declaration'
                  //! so just `push_front` :)
    cons.push_front(5);
    cons.push_front(3);
    cons.push_front(4);
    cons.push_front(5);
    cons.push_front(2);
    cons.push_front(-1);

    Sort< get_item_iter >(cons);

    std::cout << "[ " << cons << "]" << std::endl;
    std::cout << "Sort cons list maybe passed :)" << std::endl;
    std::cout << std::endl;
}

BOOST_AUTO_TEST_CASE(readFromFile)
{
    char * arr = 0;// = static_cast<char*>(malloc(0)); // Explicit allocation w size 0;

    size_t size = readArrayFromFile(arr);

    std::cout << "Read from file (stdin1) to c_array: size is " << size << std::endl;

    for (size_t i = 0; i < size; ++i)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;

    // Or just:
    // std::cout << "Or just using std::cout <<: \n" << arr << std::endl;
    // No.

    free(arr);
    //delete[] arr;
}


BOOST_AUTO_TEST_CASE(readFromStdin)
{
    std::cout << "Stdin from file 'stdin2'; \n $ ./app < stdin2" << std::endl;
    std::vector<int> v1; //! This vector ends "1 0"

    int last_num;
    int input_num;
    std::cin >> input_num;
    while (input_num != 0)
    {
        last_num = input_num;
        v1.push_back(last_num);
        std::cin >> input_num;
    }
    modifyVector(v1);

    std::vector<int> v2; //! This one ends w "2 0"

    std::cin >> input_num;
    while (input_num != 0)
    {
        last_num = input_num;
        v2.push_back(last_num);
        std::cin >> input_num;
    }
    modifyVector(v2);

    std::cout << "Do Something w vectors:" << std::endl;

    std::cout << "First vector (last value is 1): " << std::endl;
    for (size_t i = 0; i < v1.size(); ++i)
    {
        std::cout << v1[i] << " ";
    }
    std::cout << std::endl;

    std::cout << "Second vector (last value is 2): " << std::endl;
    for (size_t i = 0; i < v2.size(); ++i)
    {
        std::cout << v2[i] << " ";
    }
    std::cout << std::endl << std::endl;
}

BOOST_AUTO_TEST_CASE(fillRandomTest)
{
    //! int size = 50; //! Don't set size more than 62 on x32 machine!
    //! double * arr = static_cast<double*>(malloc(sizeof(double) * size));
    //! fillRandom(arr, size);

    srand(time(0));

    int sizes[5] = { 5, 10, 25, 50, 100 };

    std::cout << "Creating random arrays/vectors: " << std::endl;

    for (size_t i = 0; i < 5; ++i) // create 5 arrays with size == sizes[i]; and put them on screen
    {
        // just a title:
        std::cout << " ====== New array " << sizes[i] << " ======" << std::endl;

        // Is this version correct?
        std::vector<double> random_vec(sizes[i], -2); // -2 is impossible

        fillRandom(&random_vec[0], sizes[i]);
        
        Sort < get_item_at > (random_vec);

        std::cout << std::setprecision(4) << std::fixed << std::showpos;
        for (size_t i = 0; i < random_vec.size(); ++i)
        {
            if (i % 10 == 0) std::cout << std::endl;
            std::cout << random_vec[i] << " ";
        }

        std::cout << std::endl << std::endl << std::setprecision(0) << std::scientific << std::noshowpos;
    }
}

