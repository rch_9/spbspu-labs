#include <vector>

#include "vec_modifications.hpp"

void modifyVector( std::vector< int > & v )
{
    if ( !v.empty() )
    {
        if ( v.back() == 1 )
        {
            for ( std::vector< int >::iterator iter = v.begin();
                  iter != v.end();
                  )
            {
                if ((*iter % 2) == 0)
                {
                    iter = v.erase(iter);
                }
                else
                {
                    ++iter;
                }
            }
        }

        if ( v.back() == 2 )
        {
            std::vector< int >::iterator iter = v.begin();
            while (iter != v.end())
            {
                if ( (*iter % 3) == 0 )
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        iter = v.insert(++iter, 1);
                    }
                }
                else
                {
                    ++iter;
                }
            }
        }
    }
}


