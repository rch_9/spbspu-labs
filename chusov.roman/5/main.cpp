#include <algorithm>
#include <vector>
#include "datastruct.h"
#include "myalgorithms.h"

#define BOOST_TEST_MODULE MyTest
#include <boost/test/included/unit_test.hpp>


BOOST_AUTO_TEST_CASE(overloads_test)
{
    DataStruct a(1, 2, "i'm");
    DataStruct b(2, 2, "i'm");
    DataStruct c(2, 3, "i'");
    DataStruct d(2, 3, "i'm");
    DataStruct e(2, 3, "i'm");    

    BOOST_CHECK(a < b);
    BOOST_CHECK(b < c);
    BOOST_CHECK(c < d);
    BOOST_CHECK(!(d < e));
}


BOOST_AUTO_TEST_CASE(algorithms_test)
{
    srand(time(0));

    Table table, y;

    table.push_back("abcdefghij");
    table.push_back("abcdefghi");
    table.push_back("abcdefgh");
    table.push_back("abcdefg");
    table.push_back("abcdef");
    table.push_back("abcde");
    table.push_back("abcd");
    table.push_back("abc");
    table.push_back("ab");
    table.push_back("a");

    VectorOfDataStruct vect;

    try
    {
        fill_random(10, vect, y);
    }
    catch(const std::length_error excp)
    {
        std::cerr << excp.what();
    }

    fill_random(10, vect, table);
    std::for_each(vect.begin(), vect.end(), print_data);

    std::sort(vect.begin(), vect.end());
    std::cout << std::endl;
    std::for_each(vect.begin(), vect.end(), print_data);

    std::for_each(vect.begin(), vect.end(), equalize_key1);
    std::sort(vect.begin(), vect.end());
    std::cout << std::endl;
    std::for_each(vect.begin(), vect.end(), print_data);

    std::for_each(vect.begin(), vect.end(), equalize_key2);
    std::sort(vect.begin(), vect.end());
    std::cout << std::endl;
    std::for_each(vect.begin(), vect.end(), print_data);

}

