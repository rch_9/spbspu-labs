#include "task1.h"
#include <string>
#include <stdexcept>
#include <vector>
#include <fstream>
#include <iostream>

void readFromFile(const char * name_source, SetStr & set_container)
{
	if (!name_source)
	{
		throw std::invalid_argument("Wrong filename!");
	}

	std::string line;
	std::ifstream source(name_source);

	if (!source.is_open())
	{
		throw std::runtime_error("Couldn't open file!!!");
	}

	while (source.good())
	{
		source >> line;
		set_container.insert(line);
	}
}

std::ostream & operator<<(std::ostream & os, const std::string & some_str)
{
	return os << some_str;
}