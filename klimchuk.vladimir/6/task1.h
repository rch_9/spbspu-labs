#ifndef TASK1_H
#define TASK1_H

#include <set>

typedef std::set<std::string> SetStr;
void readFromFile(const char * name_source, SetStr & set_container);
std::ostream & operator<<(std::ostream & os, const SetStr & some_cont);

#endif