#include "task1.h"
#include "task2.h"
#include <ctime>

int main(void)
{
	srand(time(0));

	SetStr set_container;
	readFromFile("lab6_task1.txt", set_container);
	std::copy(set_container.begin(), set_container.end(), std::ostream_iterator<std::string>(std::cout, " "));


	std::cout << std::endl;
	std::cout << "************************************" << std::endl;
	std::cout << std::endl;

	task2();
	
	return 0;
}