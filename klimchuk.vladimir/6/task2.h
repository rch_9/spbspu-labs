#ifndef SHAPES_H
#define SHAPES_H

#include <vector>
#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <numeric>

struct Point
{
	int x, y;
};

class Shape
{
public:
	Shape(const std::string & some_shape_type);
	const std::string & getShape() const;
	int getNum() const;
	const Point & getFirstPoint() const;  /////////////for 2.5
	void fill(unsigned int count);
	std::string getShape();
private:
	int vertex_num;
	std::vector< Point > vertexes;
	std::string shape_type;
};

std::ostream & operator<<(std::ostream & os, const Point & some_point);
std::ostream & operator<<(std::ostream & os, const Shape & some_shape);

int sum(int sum_vertexes, Shape & some_shape);
const Point & getPoint(Shape & some_shape);
int countShapes(int sum_Shapes, const Shape & some_shape);
void task2();

#endif