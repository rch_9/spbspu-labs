#include "task2.h"
#include <stdexcept>

void Shape::fill(unsigned int count)
{
	Point current;
	for (unsigned int i = 0; i < count; ++i)
	{
		current.x = rand();
		current.y = rand();
		vertexes.push_back(current);
	}
}

Shape::Shape(const std::string & some_shape_type)
{
	if ((some_shape_type == "TRIANGLE") || (some_shape_type == "SQUARE") ||
		(some_shape_type == "RECTANGLE") || (some_shape_type == "PENTAGON"))
	{
		shape_type = some_shape_type;
		if (shape_type == "TRIANGLE"){ fill(3); vertex_num = 3; }
		if (shape_type == "SQUARE"){ fill(4); vertex_num = 4; }
		if (shape_type == "RECTANGLE"){ fill(4); vertex_num = 4; }
		if (shape_type == "PENTAGON"){ fill(5); vertex_num = 5; }
	}
	else
	{
		throw std::invalid_argument("Wrong SHAPE!!!");
	}
}

const std::string & Shape::getShape() const
{
	return shape_type;
}

int Shape::getNum() const
{
	return vertex_num;
}

const Point & Shape::getFirstPoint() const
{
	return vertexes[0];
}

void printPoint(Point & some_point)
{
	std::cout << "X = " << some_point.x << " " << "Y = " << some_point.y << std::endl;
}

int sum(int sum_vertexes, Shape & some_shape)
{
	return sum_vertexes += some_shape.getNum();;
}

int countShapes(int sum_shapes, const Shape & some_shape)
{
	if (some_shape.getNum() != 5)
	{
		sum_shapes++;
	}
	return sum_shapes;
}

bool IsPentagon(Shape & some_shape)
{
	return (some_shape.getNum() == 5);
}

const Point & getPoint(Shape & some_shape)
{
	return some_shape.getFirstPoint();
}

bool compare(Shape & some_shape1, Shape & some_shape2)
{
	if (some_shape1.getNum() < some_shape2.getNum())
	{
		return true;
	}
	if (some_shape1.getNum() > some_shape2.getNum())
	{
		return false;
	}
	if ((some_shape1.getNum() == some_shape2.getNum()) && (some_shape1.getShape() == "SQUARE") &&
		(some_shape2.getShape() == "RECTANGLE"))
	{
		return true;
	}
	return false;
}

std::string Shape::getShape()
{
	return shape_type;
}

std::ostream & operator<<(std::ostream & os, const Shape & my_shape)
{
	switch (my_shape.getNum())
	{
	case 3:
		return os << "Triangle";
	case 4:
		if (my_shape.getShape() == "SQUARE")
		{
			return os << "Square";
		}
		else
		{
			return os << "Rectangle";
		}
	case 5:
		return os << "Pentagon";
	}
	return os;
}

std::ostream & operator<<(std::ostream & os, const Point & my_point)
{
	return os << "x = " << my_point.x << " y = " << my_point.y;
}

void task2()
{
	std::vector< Shape > shape_vec;
	
	Shape shape1("TRIANGLE");
	std::cout << shape1 << ' ';
	Shape shape2("TRIANGLE");
	std::cout << shape2 << ' ';
	Shape shape3("PENTAGON");
	std::cout << shape3 << ' ';
	Shape shape4("TRIANGLE");
	std::cout << shape4 << ' ';
	Shape shape5("TRIANGLE");
	std::cout << shape5 << ' ';
	Shape shape6("PENTAGON");
	std::cout << shape6 << ' ';
	Shape shape7("RECTANGLE");
	std::cout << shape7 << ' ';
	Shape shape8("SQUARE");
	std::cout << shape8 << ' ';

	std::cout << std::endl;

	/////////////////////////////2.1
	shape_vec.push_back(shape1);
	shape_vec.push_back(shape2);
	shape_vec.push_back(shape3);
	shape_vec.push_back(shape4);
	shape_vec.push_back(shape5);
	shape_vec.push_back(shape6);
	shape_vec.push_back(shape7);
	shape_vec.push_back(shape8);
	std::copy(shape_vec.begin(), shape_vec.end(), std::ostream_iterator<Shape>(std::cout, " "));

	std::cout << std::endl;

	/////////////////////////////2.2
	int sum_vertexes = accumulate(shape_vec.begin(), shape_vec.end(), 0, sum);
	std::cout << sum_vertexes << std::endl;
	std::copy(shape_vec.begin(), shape_vec.end(), std::ostream_iterator<Shape>(std::cout, " "));

	std::cout << std::endl;

	/////////////////////////////2.3
	int count_tri_squa_rect = accumulate(shape_vec.begin(), shape_vec.end(), 0, countShapes);
	std::cout << count_tri_squa_rect << std::endl;
	std::copy(shape_vec.begin(), shape_vec.end(), std::ostream_iterator<Shape>(std::cout, " "));

	std::cout << std::endl;
	
	/////////////////////////////2.4
	shape_vec.erase(remove_if(shape_vec.begin(), shape_vec.end(), IsPentagon), shape_vec.end());
	std::copy(shape_vec.begin(), shape_vec.end(), std::ostream_iterator<Shape>(std::cout, " "));

	std::cout << std::endl;
	
	/////////////////////////////2.5
	std::vector< Point > point_vec(shape_vec.size());
	std::transform(shape_vec.begin(), shape_vec.end(), point_vec.begin(), getPoint);
	std::copy(point_vec.begin(), point_vec.end(), std::ostream_iterator<Point>(std::cout, " "));

	std::cout << std::endl;

	/////////////////////////////2.6
	std::sort(shape_vec.begin(), shape_vec.end(), compare);
	std::copy(shape_vec.begin(), shape_vec.end(), std::ostream_iterator<Shape>(std::cout, " "));
}