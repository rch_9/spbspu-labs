#include "taskfunc.h"

int main()
{
	const std::string punc("?!.:;,");
	const std::string word("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONPQRSTUVWXYZ0123456789");

	std::vector<std::string> vec;

	readFromFile("task.txt", vec);
		
	textFormat(vec, word, punc);
	std::cout << std::endl;
	vecModify(vec);

	return 0;
}
