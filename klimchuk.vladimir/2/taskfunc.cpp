#include "taskfunc.h"


void readFromFile(const char * name_source, std::vector<std::string> & vec)
{
	if (!name_source)
	{
		throw std::invalid_argument("Wrong filename!");
	}
	
	std::string line;
	std::ifstream source(name_source);

	if (!source.is_open())
	{
		throw std::runtime_error("Couldn't open a file!");
	}
	
	while (std::getline(source, line))
	{
		vec.push_back(line);
	}
}

void textFormat(std::vector<std::string> & vec, const std::string & word, const std::string & punc)
{
	for (size_t count_vec = 0; count_vec < vec.size(); ++count_vec)
	{
		size_t i = vec[count_vec].find('\t');
		while (i != std::string::npos)
		{
			vec[count_vec].erase(i, 1);
			if (i != 0)
			{
				vec[count_vec].insert(i, 1, ' ');
			}
			i = vec[count_vec].find('\t', i);
		}


		i = vec[count_vec].find(' ');
		while ((i != std::string::npos) && (i < vec[count_vec].size() - 1))
		{
			if  (vec[count_vec][i + 1] == ' ')
			{
				vec[count_vec].erase(i, 1);
				i = vec[count_vec].find(' ', i);
			}
			else
			{
				i = vec[count_vec].find(' ', i + 1);
			}
		}


		i = vec[count_vec].find_first_of(punc);
		while (i != std::string::npos)
		{
			if ((i > 1) &&
				(vec[count_vec][i - 1] == ' ') && (vec[count_vec][i - 2] != '.') && (vec[count_vec][i - 2] != ',') &&
				(vec[count_vec][i - 2] != ':') && (vec[count_vec][i - 2] != ';') && (vec[count_vec][i - 2] != '?') &&
				(vec[count_vec][i - 2] != '!'))
			{
				vec[count_vec].erase(i - 1, 1);
				--i;
			}
			if (i < vec[count_vec].size() - 1)
			{
				i = vec[count_vec].find_first_of(punc, i + 1);
			}
			else
			{
				i = std::string::npos;
			}
		}


		for (size_t j = 0; j < punc.size(); ++j)
		{
			i = vec[count_vec].find(punc[j]);
			while ((i != std::string::npos) && (i < vec[count_vec].size() - 1))
			{
				if (vec[count_vec][i + 1] != ' ')
				{
					vec[count_vec].insert(i + 1, 1, ' ');
				}
				i = vec[count_vec].find(punc[j], i + 1);
			}
		}


		i = vec[count_vec].find_first_of(word);
		int counter = 0;
		while (i != std::string::npos)
		{
			while ((i < vec[count_vec].size()) && (vec[count_vec][i] != '!') && (vec[count_vec][i] != '?') && 
				   (vec[count_vec][i] != '.') && (vec[count_vec][i] != ',') && (vec[count_vec][i] != ';') && 
				   (vec[count_vec][i] != ' ') && (vec[count_vec][i] != ':'))
			{
				++counter;
				++i;
			}
			if (counter > 10)
			{
				vec[count_vec].replace(i - counter, counter, "Vau!!!");
				i = i - counter + 5;
			}
			counter = 0;
			if (i < vec[count_vec].size() - 1)
			{
				i = vec[count_vec].find_first_of(word, i + 1);
			}
			else
			{
				i = std::string::npos;
			}
		}
		std::cout << vec[count_vec] << std::endl;
	}
}


void cutLine(std::vector<std::string>::iterator & it, std::vector<std::string> & vec,
	         std::string::iterator & string_it, int delta_size)
{
        if (it != --vec.end())
        {
                (it + 1)->insert((it + 1)->begin(), string_it, --it->end());
                it->resize(40 - delta_size);
        }
        else
        {
                std::string temp(string_it, it->end());
                it = vec.insert(vec.end(), temp);
                (it - 1)->resize(40 - delta_size);
                --it;
        }
}


void vecModify(std::vector<std::string> & vec)
{
	std::vector<std::string>::iterator it = vec.begin();
	int delta_size = 0;
	while (it != vec.end())
	{
		if (it->size() > 40)
		{
                        std::string::iterator string_it = it->begin() + 40;
                        while (*(string_it) != ' ')
                        {
                                --string_it;
                                delta_size = delta_size + 1;
                        }
                        cutLine(it, vec, string_it , delta_size);
                        delta_size = 0;
		}
		std::cout << *it << std::endl;
		++it;
	}
}
