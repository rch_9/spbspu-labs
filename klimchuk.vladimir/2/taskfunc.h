#ifndef TASKFUNC_H
#define TASKFUNC_H
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>

void readFromFile(const char * name, std::vector<std::string> & vec);
void textFormat(std::vector<std::string> & vec, const std::string & word, const std::string & punc);
void cutLine(std::vector<std::string>::iterator & it, std::vector<std::string> & vec,
	         std::string::iterator & string_it, int delta_size);
void vecModify(std::vector<std::string> & vec);

#endif