#include "datastruct.h"

DataStruct::DataStruct(int some_key1, int some_key2, std::string some_str) : key1(some_key1),
                                                                             key2(some_key2),
                                                                             str(some_str)
{}

void print(const std::vector<DataStruct> & vec)
{
	for (unsigned int i = 0; i < vec.size(); ++i)
	{
		std::cout << vec[i].key1 << ' ' << vec[i].key2 << ' ' << vec[i].str << std::endl;
	}
}

void fill(std::vector<DataStruct> & vec)
{
	for (unsigned int i = 0; i < 10; ++i)
	{
		vec.push_back(DataStruct((rand() % (2 * key_range + 1) - key_range), (rand() % (2 * key_range + 1) - key_range), table[rand() % str_range]));
	}
}

bool compare(const DataStruct & some_data1, const DataStruct & some_data2)
{
	return ((some_data1.key1 < some_data2.key1) ||
		   ((some_data1.key1 == some_data2.key1) && (some_data1.key2 < some_data2.key2)) ||
		   ((some_data1.key1 == some_data2.key1) && (some_data1.key2 == some_data2.key2) &&
		   (some_data1.str.size() < some_data2.str.size())));
}
