#include "datastruct.h"
#include <algorithm>
#include <ctime>

int main(void)
{
	srand(time(0));

	typedef std::vector<DataStruct> data_vec;

	data_vec vec;

	fill(vec);
	print(vec);

	std::cout << "**************************" << std::endl;

	std::sort(vec.begin(), vec.end(), compare);
	print(vec);

	system("pause");
	return 0;
}