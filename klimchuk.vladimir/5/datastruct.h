#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>

class DataStruct
{
public:
	DataStruct(int some_key1, int some_key2, std::string some_str);
	int key1;
	int key2;
	std::string str;
};

const unsigned int table_size = 10;
const std::string table[table_size] = { "a", "ab", "abc", "abcdef", "anvd", "abckd", "e", "efmffmfmd", "bd", "g" };
const size_t str_range = 10;
const size_t key_range = 5;

bool compare(const DataStruct & some_data1, const DataStruct & some_data2);
void print(const std::vector<DataStruct> & vec);
void fill(std::vector<DataStruct> & vec);

#endif
