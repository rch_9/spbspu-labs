#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <string>
#include <list>
#include <iostream>
#include <vector>

class Phonebook{
public:
	struct node;
	typedef std::list< node > container;
	typedef container::iterator it;
	typedef node node_t;
	Phonebook();
	it begin();
	it end();
	it insert_after(it some_it, const node_t & some);
	it insert_before(it some_it, const node_t & some);
	void push_b(const node_t & some);
	void modify_record(it some_it, const node_t & some);
	int size();
private:
	container my_container;
};

struct Phonebook::node
{
	std::string telephone;
	std::string name;
	node();
	node(std::string & some_name, std::string & some_telephone);
};

#endif