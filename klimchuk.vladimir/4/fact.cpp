#include "fact.h"
#include <iostream>

list::list_iterator::list_iterator() : pos(1), result(1)
{}

list::list_iterator::list_iterator(const int & some_pos) : pos(some_pos)
{
	result = 1;
	for (int temp = 1; temp < some_pos; ++temp)
	{
		result *= temp;
	}
}


bool list::list_iterator::operator ==(const it_type & rhs) const
{
	return pos == rhs.pos;
}

bool list::list_iterator::operator != (const it_type & rhs) const
{
	return !(pos == rhs.pos);
}

int list::list_iterator::operator *() const
{
	assert((pos < 11) && (pos >= 1));
	return result;
}

list::it_type & list::list_iterator::operator ++()
{
	assert(pos <= 11);
	++pos;
	result *= pos;
	return *this;
}
	
list::it_type list::list_iterator::operator ++(int)
{
	it_type temp(*this);
	this->operator ++();
	return temp;
}

list::it_type & list::list_iterator::operator --()
{
	assert(pos >= 1);
	--pos;
	result /= (pos + 1);
	return *this;
}

list::it_type list::list_iterator::operator --(int)
{
	it_type temp(*this);
	this->operator --();
	return temp;
}

list::list()
{
	size = 10;
}
	
list::it_type list::begin()
{
	list::it_type it(1);
	return it;
}

list::it_type list::end()
{
	list::it_type it(size+1);
	return it;
}