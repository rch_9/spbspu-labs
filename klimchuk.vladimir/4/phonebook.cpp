#include "phonebook.h"

Phonebook::Phonebook()
{};

Phonebook::node::node()
{};

Phonebook::node::node(std::string & some_name, std::string & some_telephone) : telephone(some_telephone), name(some_name)
{}

Phonebook::it Phonebook::begin()
{
	return my_container.begin();
}

Phonebook::it Phonebook::end()
{
	return my_container.end();
}

Phonebook::it Phonebook::insert_after(it some_it, const node_t & some)
{
	if (my_container.empty())
	{
		std::cout << "Container is empty, so it will be pushed back" << std::endl;
		push_b(some);
	}
	else
	{
		if (some_it == my_container.end())
		{
			push_b(some);
		}
		else
		{
			++some_it;
			my_container.insert(some_it, some);
		}
	}
	some_it = my_container.begin();
	return some_it;
};

Phonebook::it Phonebook::insert_before(it some_it, const node_t & some)
{
	if (my_container.empty())
	{
		std::cout << "Container is empty, so it will be pushed back" << std::endl;
		push_b(some);
	}
	else
	{
		my_container.insert(some_it, some);
	}
	some_it = my_container.begin();
	return some_it;
};

void Phonebook::push_b(const node_t & some)
{
	my_container.push_back(some);
}

int Phonebook::size()
{
	return my_container.size();
}

void Phonebook::modify_record(it some_it, const node_t & some)
{
	if (some_it == my_container.end())
	{
		std::cout << "Couldn't substitute empty element (END) for this, so it will be pushed back" << std::endl;
		push_b(some);
	}
	else
	{
		some_it->name = some.name;
		some_it->telephone = some.telephone;
	}
}