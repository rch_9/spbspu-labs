#ifndef FACT_H
#define FACT_H

#include <iterator>
#include <assert.h>

class list
{
public:
	class list_iterator;
	typedef list_iterator it_type;
	list();
	it_type begin();
	it_type end();
private:
	int size;
};

class list::list_iterator : public std::iterator < std::bidirectional_iterator_tag, int >
{
public:
	list_iterator();
	list_iterator(const int & some_pos);
	bool operator ==(const it_type & rhs) const;
	bool operator != (const it_type & rhs) const;
	int operator *() const;
	it_type & operator ++();
	it_type operator ++(int);
	it_type & operator --();
	it_type operator --(int);
private:
	int pos;
	int result;
};

#endif