#include "phonebook.h"
#include "fact.h"

int main(void)
{
	Phonebook book;
	std::string telephone1("11");
	std::string name1("a");
	std::string telephone2("22");
	std::string name2("b");
	std::string telephone3("33");
	std::string name3("c");
	std::string telephone4("44");
	std::string name4("d");
	std::string telephone5("55");
	std::string name5("e");
	Phonebook::node_t el1(name1, telephone1);
	Phonebook::node_t el2(name2, telephone2);
	Phonebook::node_t el3(name3, telephone3);
	Phonebook::node_t el4(name4, telephone4);
	Phonebook::node_t el5(name5, telephone5);
	
	/////////////Phonebook 1 test (begin, end)
	book.push_b(el1);
	book.push_b(el2);
	Phonebook::it it_el1 = book.begin();
	Phonebook::it it_el2 = --book.end();
	if ((it_el1->name != "a") || (it_el2->name != "b") || (it_el1->telephone != "11") || (it_el2->telephone != "22"))
	{
		std::cerr << "Error in phonebook 1 test (begin, end)" << std::endl;
		return 1;
	}
	/////////////Phonebook 2 test (modify)
	Phonebook::it modified = book.begin();
	book.modify_record(modified, el3);
	if ((modified->name != "c") || modified->telephone != "33")
	{
		std::cerr << "Error in phonebook 1 test (modify)" << std::endl;
		return 1;
	}
	/////////////Phonebook 3 test (insert)
	it_el1 = book.begin();
	book.insert_before(it_el1, el4);
	it_el2 = --book.end();
	book.insert_after(it_el2, el5);
	it_el1 = --book.end();
	if ((book.begin()->name != "d") || (book.begin()->telephone != "44") ||
		(it_el1->name != "e") || (it_el1->telephone != "55"))
	{
		std::cerr << "Error in phonebook 3 test (insert)" << std::endl;
		return 1;
	}

	std::cout << std::endl;
	std::cout << "***********************************************" << std::endl;
	std::cout << "***********************************************" << std::endl;
	std::cout << std::endl;
	
	std::vector< int > vec1(10, 0);
	std::vector< int >::iterator vec1_it1 = vec1.begin();
	while (vec1_it1 != vec1.end())
	{
		std::cout << *vec1_it1 << std::endl;
		++vec1_it1;
	}

	std::cout << "----------------------------" << std::endl;

	list list1;
	list::it_type list1_it1 = list1.begin();
	while (list1_it1 != list1.end())
	{
		std::cout << *list1_it1 << std::endl;
		list1_it1++;
	}

	std::cout << "----------------------------" << std::endl;
	
	std::copy(list1.begin(), list1.end(), vec1.begin());
	vec1_it1 = vec1.begin();
	while (vec1_it1 != vec1.end())
	{
		std::cout << *vec1_it1 << std::endl;
		++vec1_it1;
	}
	return 0;
}