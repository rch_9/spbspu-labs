#include "functor.h"

functor::functor()
{
	max = 0;
	min = 0;
	average = 0;
	positive_count = 0;
	negative_count = 0;
	odd_sum = 0;
	even_sum = 0;
	coincidence = false;
	
	first_elem = 0;
	last_elem = 0;
	first = false;
	count_elem = 0;
};

void functor::operator()(const int & number)
{
	if (!first)
	{
		max = number;
		min = number;
		coincidence = true;
		first_elem = number;
		first = true;
	}
	else
	{
		if (number > max)
		{
			max = number;
		}
		if (number < min)
		{
			min = number;
		}
	}

	++count_elem;
	average = (average*(count_elem - 1) + number) / count_elem;
	if (number < 0)
	{
		++negative_count;
	}
	if (number > 0)
	{
		++positive_count;
	}
	if (number % 2 == 0)
	{
		even_sum = even_sum + number;
	}
	if (number % 2 != 0)
	{
		odd_sum = odd_sum + number;
	}
	last_elem = number;
	if (first_elem == last_elem)
	{
		coincidence = true;
	}
	if (first_elem != last_elem)
	{
		coincidence = false;
	}
}

void functor::print()
{
	if (!first)
	{
		std::cout << "EMPTY" << std::endl;
	}
	else
	{
		std::cout << "MAX ELEM = " << max << std::endl;
		std::cout << "MIN ELEM = " << min << std::endl;
		std::cout << "AVERAGE = " << average << std::endl;
		std::cout << "POSITIVE COUNT = " << positive_count << std::endl;
		std::cout << "NEGATIVE COUNT = " << negative_count << std::endl;
		std::cout << "SUM ODD = " << odd_sum << std::endl;
		std::cout << "SUM EVEN = " << even_sum << std::endl; 
		if (!coincidence)
		{
			std::cout << "FIRST AND LAST ELEM ARE NOT EQUAL" << std::endl;
		}
		else
		{
			std::cout << "FIRST AND LAST ELEM ARE EQUAL" << std::endl;
		}
	}
}