#include "functor.h"
#include <algorithm>
#include <ctime>
#include <cstdlib>

int main(void)
{
	srand(time(0));
	const unsigned int size_arr = 5;
    const size_t range = 500;

	std::vector<int> vec;
	for (unsigned int i = 0; i < size_arr; ++i)
	{
		vec.push_back(rand() % (2 * range - 1) - range);
		std::cout << vec[i] << std::endl;
	}
	
	std::cout << "*******************************************" << std::endl;

	functor func;
	func = std::for_each(vec.begin(), vec.end(), func);
	func.print();

	return 0;
}