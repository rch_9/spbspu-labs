#ifndef FUNCTOR_H
#define FUNCTOR_H

#include <vector>
#include <iostream>

class functor
{
public:
	functor();
	void operator()(const int & number);
	void print();
private:
	int max;
	int min;
	float average;
	size_t positive_count;
	size_t negative_count;
	int odd_sum;
	int even_sum;
	bool coincidence;

	bool first;
	int count_elem;
	int first_elem;
	int last_elem;
};

#endif