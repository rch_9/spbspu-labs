#ifndef FSM_HOLDER_HPP
#define FSM_HOLDER_HPP
#include "FSM_Real_Machine.hpp"//main_machine
#include <stdexcept>
template< class FSM_type >
class FSM_Holder
{
	public:
		typedef FSM_type machine_type;
		typedef FSM_Holder< machine_type > this_type;
		typedef typename machine_type::token_type token_type;
		typedef typename machine_type::container_type container_type;
		
		FSM_Holder(bool detect_punct_like_alpha = false):
			main_machine(detect_punct_like_alpha)
			{
				
			}
				
		void initiate()
		{
			main_machine.initiate();
		}
		
		void terminate()
		{
			main_machine.terminate();
		}
		
		void process()
		{
			if(main_machine.check_ifstream())
			{
				main_machine.process();
			}
		}
		
		void bind_ifstream(const char* file_name)
		{
			if(file_name == 0)
			{
				throw std::invalid_argument("Invalid filename");
			}
			main_machine.bind_ifstream(file_name);
		}
		
		container_type get()
		{
			return main_machine.get();
		}
		
	private:
		FSM_type main_machine;
		
		FSM_Holder(const this_type &);
		this_type & operator= (const this_type &);		
};
#endif//FSM_HOLDER_HPP
