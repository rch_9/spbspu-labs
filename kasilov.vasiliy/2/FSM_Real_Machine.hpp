#ifndef FSM_REAL_MACHINE_HPP
#define FSM_REAL_MACHINE_HPP

#include "Token.h" //struct token

#include <boost/smart_ptr/shared_ptr.hpp>   //smart_ptr_stream type
#include <boost/smart_ptr/make_shared.hpp> //make_shared

#include <boost/mpl/list.hpp> //boost::mpl::list < > reactions

#include <boost/statechart/event.hpp>         //namespace Events
#include <boost/statechart/state_machine.hpp> //struct Machine
#include <boost/statechart/simple_state.hpp>  //namespace States
#include <boost/statechart/transition.hpp>    //typedef ... reactions

#include <fstream> // stream_type
#include <vector>  // container_type
#include <stdexcept>//runtime_error, invalid_argument
namespace boost_fsm = boost::statechart;
namespace Events
{
	struct Initiate : boost_fsm::event< Initiate >{};
	struct Digit : boost_fsm::event< Digit >{};
	struct Alpha : boost_fsm::event< Alpha >{};
	struct Space : boost_fsm::event< Space >{};
	struct Punct : boost_fsm::event< Punct >{};
	struct Endof : boost_fsm::event< Endof >{};
	struct Terminate : boost_fsm::event< Terminate >{};
};

namespace States
{
	struct Initial_state;
	struct Watch_characters;
	struct Word_token_complement;
	struct Punct_token_complement;
	struct Terminate_state;
};

struct Machine : boost_fsm::state_machine< Machine, States::Initial_state >
{
	public:
		typedef token token_type;
		typedef token_type::elementary_type token_elementary_type;
		typedef token_type::format_type token_format_type;
		typedef token_type::token_type_enum token_category_type;
		typedef token_type::container_type container_type;
		typedef std::ifstream stream_type;
		typedef boost::shared_ptr< stream_type > smart_ptr_stream_type;
		
		Machine(bool punct_detection = false);

		container_type get();
		void bind_ifstream(const char* _file_name);
		bool check_ifstream();
		void process();
		
		template < class Event >
		void push_punct_token(const Event&)
		{
			token_word_buffer< Event >(Event());
			token_type new_token = init_token_buffer(token_type::PUNCT);
			token_box.push_back(new_token);
			reset_buffer();
		}
		
		template < class Event >
		void push_word_token_back(const Event&)
		{
			push_word_token< Event >(Event());
			ifstream_backone< Event >(Event());
		}
		
		template < class Event >
		void push_word_token(const Event&)
		{
			token_type new_token = init_token_buffer(token_type::WORD);
			token_box.push_back(new_token);
			reset_buffer();
		}
	
		template < class Event >
		void token_word_buffer(const Event&)
		{
			token_buffer += current_char;
		}
		
		template < class Event >
		void ifstream_backone(const Event&)
		{
			input_stream_ptr->unget();
			if(! *input_stream_ptr)
			{
				input_stream_ptr->close();
				throw std::runtime_error("File stream crash");
			}
		}
		
	private:			
		smart_ptr_stream_type input_stream_ptr;	
		container_type token_box;
		
		token_format_type token_buffer;
		token_elementary_type current_char;
		
		const bool detect_punct_like_alpha;

		Machine(const Machine &);
		Machine & operator= (const Machine &);
		
		void reset_buffer();
		token_type init_token_buffer(token_type::token_type_enum _type);
};

struct States::Initial_state : boost_fsm::simple_state< States::Initial_state, Machine >
{
	typedef boost_fsm::transition< Events::Initiate, States::Watch_characters > reactions;
};

struct States::Watch_characters : boost_fsm::simple_state< States::Watch_characters, Machine >
{	
	typedef boost::mpl::list
	<
		boost_fsm::transition< Events::Digit, States::Word_token_complement, Machine, &Machine::ifstream_backone >,
		boost_fsm::transition< Events::Alpha, States::Word_token_complement, Machine, &Machine::ifstream_backone >,
		boost_fsm::transition< Events::Punct, States::Punct_token_complement, Machine, &Machine::ifstream_backone >,
		boost_fsm::transition< Events::Space, States::Watch_characters >,
		boost_fsm::transition< Events::Endof, States::Terminate_state >
	>
	reactions;
};

struct States::Word_token_complement : boost_fsm::simple_state< States::Word_token_complement, Machine >
{
	typedef boost::mpl::list
	<
		boost_fsm::transition< Events::Digit, States::Word_token_complement, Machine, &Machine::token_word_buffer >,
		boost_fsm::transition< Events::Alpha, States::Word_token_complement, Machine, &Machine::token_word_buffer >,
		boost_fsm::transition< Events::Punct, States::Watch_characters, Machine, &Machine::push_word_token_back >,
		boost_fsm::transition< Events::Space, States::Watch_characters, Machine, &Machine::push_word_token >,
		boost_fsm::transition< Events::Endof, States::Terminate_state, Machine, &Machine::push_word_token_back >
	>
	reactions;
};

struct States::Punct_token_complement : boost_fsm::simple_state< States::Punct_token_complement, Machine >
{
	typedef boost_fsm::transition< Events::Punct, States::Watch_characters, Machine, &Machine::push_punct_token > reactions;
};

struct States::Terminate_state : boost_fsm::simple_state< States::Terminate_state, Machine >
{	
	typedef boost_fsm::transition< Events::Terminate, States::Terminate_state > reactions;
};
#endif//FSM_REAL_MACHINE_HPP
