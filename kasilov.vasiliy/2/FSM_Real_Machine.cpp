#include "FSM_Real_Machine.hpp"
Machine::Machine(bool punct_detection):
	current_char(' '),
	detect_punct_like_alpha(punct_detection)
	{
		reset_buffer();
	}
	
Machine::container_type Machine::get()
{
	return token_box;
}
	
void Machine::bind_ifstream(const char* _file_name)
{
	if(_file_name == 0)
	{
		throw std::invalid_argument("Invalid file name");
	}
	input_stream_ptr = boost::make_shared< stream_type >(_file_name);
	if(!(*input_stream_ptr))
	{
		input_stream_ptr->close();
		throw std::runtime_error("File cannot be opened");
	}
}
		
bool Machine::check_ifstream()
{
	return input_stream_ptr;
}		

void Machine::process()
{
	context< Machine >().process_event(Events::Initiate());
	while (input_stream_ptr->get(current_char))
	{
		if (std::isspace(current_char))
		{
			context< Machine >().process_event(Events::Space());
		}
		else
		if (std::isdigit(current_char))
		{
			context< Machine >().process_event(Events::Digit());
		}
		else
		if (std::isalpha(current_char))
		{
			context< Machine >().process_event(Events::Alpha());
		}
		else
		if (std::ispunct(current_char))
		{
			if(detect_punct_like_alpha)
			{
				context< Machine >().process_event(Events::Alpha());						
			}
			else
			{
				context< Machine >().process_event(Events::Punct());
			}
		}
		else
		if (current_char == std::char_traits< token_elementary_type >::eof())
		{
			context< Machine >().process_event(Events::Endof());
		}
		else
		{
			context< Machine >().process_event(Events::Terminate());
		}
	}
	
	if(! input_stream_ptr->eof())
	{
		input_stream_ptr->close();
		throw std::runtime_error("File stream crash");
	}
	context< Machine >().process_event(Events::Endof());
}
	
void Machine::reset_buffer()
{
	token_buffer.clear();
}

Machine::token_type Machine::init_token_buffer(token_type::token_type_enum _type)
{
	token_type new_token;
	new_token.set_value(token_buffer);
	new_token.token_category = _type;
	return new_token;
}
