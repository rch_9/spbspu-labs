#ifndef DRIVER_HPP
#define DRIVER_HPP
#include "Token.h"//token_type
#include <algorithm>//std::for_each
#include <iostream>//text print
namespace DRIVER_CONST
{
	const int DRIVER_CONST_BORDER = 10;
	const int DRIVER_TEXT_FORMAT_CONST = 40;
	const token::format_type DRIVER_FILL_OUT_OF_BORDER = "Vau!!!";
}

struct Token_driver
{
	public:
		typedef token token_type;
		typedef token::elementary_type token_elementary_type;
		typedef token::token_type_enum token_category_type;
		typedef token::container_type container_token_type;
		typedef token::format_type format_token_type;
		typedef token::text_format_type format_text_type;
		typedef container_token_type::iterator iter_token_type;
		
		static void token_trap(container_token_type & source_token_container)
		{
			if(source_token_container.empty())
			{
				return;
			}
			else
			{
				std::for_each(source_token_container.begin(), source_token_container.end(), hunter());
			}
		}
		
		static void text_convert(container_token_type & source_token_container, format_text_type & destination_text)
		{	
			if(source_token_container.empty())
			{
				return;
			}
			
			destination_text.clear();
			format_token_type accumulate_string;
			token_category_type last_token_category = token_type::WORD;
			for(iter_token_type i = source_token_container.begin(); i != source_token_container.end(); i++)
			{
				if(accumulate_string.empty())
				{
					accumulate_string += i->get_value();
				}
				else
				if(accumulate_string.length() + i->get_size() <= (DRIVER_CONST::DRIVER_TEXT_FORMAT_CONST - 1))
				{
					if(last_token_category == i->get_category())
					{
						accumulate_string += " ";
						accumulate_string += i->get_value();
					}
					else
					if(last_token_category == token_type::WORD && i->get_category() == token_type::PUNCT)
					{
						accumulate_string += i->get_value();	
					}
					else
					{
						accumulate_string += " ";
						accumulate_string += i->get_value();
					}
				}
				else
				{
					destination_text.push_back(accumulate_string);
					accumulate_string.clear();
					accumulate_string += i->get_value();
				}
				last_token_category = i->get_category();
			}
			destination_text.push_back(accumulate_string);
		}
		
		static void unique_token(container_token_type & source_token_container)
		{
			sort_token(source_token_container);
			source_token_container.erase(std::unique(source_token_container.begin(), source_token_container.end(), token_equal()), source_token_container.end());
		}
		
		static void sort_token(container_token_type & source_token_container)
		{
			if(!source_token_container.empty())
			{
				std::sort(source_token_container.begin(), source_token_container.end(), token_compare());		
			}
		}
		
	private	: struct token_equal
		{
			bool operator()(token_type & first_token, token_type & second_token)
			{
				return (first_token.get_value() == second_token.get_value());
			}
		};
	
	private	: struct token_compare
		{
			bool operator()(token_type & first_token, token_type & second_token)
			{
				return first_token.get_value() < second_token.get_value();
			}
		};
		
	private	: struct hunter
		{
			format_token_type filler;
			hunter():
				filler(DRIVER_CONST::DRIVER_FILL_OUT_OF_BORDER)
				{
					
				}
			void operator()(token_type & source_token)
			{
				if(source_token.get_size() > DRIVER_CONST::DRIVER_CONST_BORDER)
				{
					source_token.set_value(filler);			
				}
			}
		};
};
#endif//DRIVER_HPP
