#include "FSM_holder.hpp"
#include "Driver.hpp"
int main()
{
	FSM_Holder< Machine >  TESTmachine;
	TESTmachine.bind_ifstream("input.txt");
	
	TESTmachine.initiate();
	TESTmachine.process();

	FSM_Holder< Machine >::container_type TESTcontainer = TESTmachine.get();

	TESTmachine.terminate();
	
	Token_driver::token_trap(TESTcontainer);
	Token_driver::format_text_type TESTtext;
	Token_driver::text_convert(TESTcontainer, TESTtext);
	
	std::copy(TESTtext.begin(), TESTtext.end(), std::ostream_iterator< Token_driver::format_token_type >(std::cout, "\n"));
	return 0;
}
