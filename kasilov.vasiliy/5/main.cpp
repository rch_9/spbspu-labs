#define __AMOUNT_OF_STRINGS_IN_TABLE__ 10
#define __RIGHT_RANDOM_LENGTH__ 10
#define __LEFT_RANDOM_BORDER__ -5
#include <algorithm>//for_each, sort
#include <iostream>//print console
#include <sstream>
#include <vector>//container_type
#include <string>//string type
#include <ctime>//random generate
#include <cstdlib>//^^^
#include <iomanip>
#include <boost/bind.hpp>
typedef std::string string_type;

struct data_struct
{	
	int key1;
	int key2;
	string_type str;
	
};
typedef std::vector< data_struct > container_type;
typedef const data_struct & const_ref_data_struct;
typedef std::vector< string_type > table_type;

struct random_vector_generator
{
	typedef data_struct result_type;
	data_struct operator()(table_type & table)
	{
		data_struct new_struct;
		new_struct.key1 = rand() % (__RIGHT_RANDOM_LENGTH__ + 1) + __LEFT_RANDOM_BORDER__;
		new_struct.key2 = rand() % (__RIGHT_RANDOM_LENGTH__ + 1) + __LEFT_RANDOM_BORDER__;
		new_struct.str = table[rand() % table.size()];	
		return new_struct;	
	}
};

struct criterion_sorter
{
	bool operator()(const_ref_data_struct uper, const_ref_data_struct downer)
	{
		return (uper.key1 < downer.key1) ||
		(uper.key1 == downer.key1 && uper.key2 < downer.key2) ||
		(uper.key2 == downer.key2 && uper.str.length() < downer.str.length());
	}
};

struct printer
{
	void operator()(const_ref_data_struct value)
	{
		std::cout << std::setw(4) << std::right << value.key1;
		std::cout << std::setw(4) << std::right << value.key2;
		std::cout << std::setw(10) << std::right << value.str << "\n";
	}
};

struct table_generator
{
	typedef string_type result_type;
	string_type operator()(int & value)
	{
		std::stringstream str_stream;
		str_stream << value;
		string_type str;
		str_stream >> str;
		int length_gener = value % 5;
		for(int i = 0; i < length_gener; i++)
		{
			str = "_" + str;
		}
		value++;
		return "s__" + str;
	}
};

struct table_printer
{
	void operator()(string_type & str)
	{
		std::cout << str << std::endl;
	}
};

int main ()
{
	srand(time(0));
	table_type new_table;
	std::generate_n(std::inserter(new_table, new_table.begin()), 10, boost::bind(table_generator(), 0));
	std::cout << "Gener table" << std::endl;
	std::for_each(new_table.begin(), new_table.end(), table_printer());
	std::random_shuffle(new_table.begin(), new_table.end());
	std::cout << "Random shuffle table" << std::endl;
	std::for_each(new_table.begin(), new_table.end(), table_printer());
	
	container_type test_vector;
	std::generate_n(std::inserter(test_vector, test_vector.begin()), 100, boost::bind(random_vector_generator(), new_table));
	std::cout << "---Gener structs---" << std::endl;
	std::for_each(test_vector.begin(), test_vector.end(), printer());
	
	std::cout << "\n";
	std::cout << "+++sorted+++" << std::endl;
	
	std::sort(test_vector.begin(), test_vector.end(), criterion_sorter());
	std::for_each(test_vector.begin(), test_vector.end(), printer());
	return 0;
}
