#ifndef QUEUE_WITH_PRIORITY_HPP
#define QUEUE_WITH_PRIORITY_HPP
#include <queue>
#include <boost/make_shared.hpp>
template < class Item > 
class Queue_with_priority
{
	public:
		typedef enum
		{
			LOW,
			NORMAL,
			HIGH
		} priority;
	
		typedef Item item_type;
		typedef std::deque< item_type > store_type;
		typedef Queue_with_priority< item_type > this_type;
		typedef boost::shared_ptr< item_type > item_type_ptr;
	
		Queue_with_priority();
		void put(const item_type & element, const priority & prior = LOW);
		bool empty();
		item_type_ptr get();
	
		void accelerate();
		
	private:
		store_type low_store;
		store_type normal_store;
		store_type high_store;
		
		Queue_with_priority(const this_type &);
		this_type & operator=(const this_type);
};

template < class Item >
Queue_with_priority< Item >::Queue_with_priority()
{
	
}

template < class Item >
bool Queue_with_priority< Item >::empty()
{
	return (low_store.empty() && normal_store.empty() && high_store.empty());
}

template < class Item >
void Queue_with_priority< Item >::accelerate()
{
	if(!low_store.empty())
	{
		high_store.insert(high_store.end(), low_store.begin(), low_store.end());
		low_store.clear();
	}
}

template < class Item >
typename Queue_with_priority< Item >::item_type_ptr Queue_with_priority< Item >::get()
{
	if(!empty())
	{
		if(!high_store.empty())
		{
			item_type_ptr return_ptr = boost::make_shared< item_type > (high_store.front());
			high_store.pop_front();
			return return_ptr;
		}
		else
		if(!normal_store.empty())
		{
			item_type_ptr return_ptr = boost::make_shared< item_type > (normal_store.front());
			normal_store.pop_front();
			return return_ptr;
		}
		else
		if(!low_store.empty())
		{
			item_type_ptr return_ptr = boost::make_shared< item_type > (low_store.front());
			low_store.pop_front();
			return return_ptr;
		}	
	}
	else
	{
		return boost::make_shared< item_type >();		
	}
}

template < class Item >
void Queue_with_priority< Item >::put(const item_type & item, const priority & prior)
{
	switch (prior)
	{
		case this_type::HIGH :
			high_store.push_back(item);
			return;
					
		case this_type::NORMAL :
			normal_store.push_back(item);
			return;
			
		case this_type::LOW :
			low_store.push_back(item);
			return;
		default:
			return;
	}	
}
#endif//QUEUE_WITH_PRIORITY_HPP

