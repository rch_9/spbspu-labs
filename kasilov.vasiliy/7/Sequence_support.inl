#ifndef SEQUENCE_SUPPORT_INL
#define SEQUENCE_SUPPORT_INL
#include <cstdlib>//rand
struct sequence_printer
{
	void operator()(Sequence_stat::sequence_value_type & value)
	{
		std::cout << value << "\n";
	}	
};

struct sequence_random_generator
{
	void operator()(Sequence_stat::sequence_value_type & value)
	{
		value = rand() % (__LENGTH_GENERATION_BORDER__) + __LEFT_SEQUENCE_BORDER__;
	}
};
#endif//SEQUENCE_SUPPORT_INL
