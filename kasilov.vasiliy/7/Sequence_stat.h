#ifndef SEQUENCE_STAT_H
#define SEQUENCE_STAT_H
class Sequence_stat
{
	public:
		typedef int sequence_value_type;
		typedef unsigned count_type;
		typedef float average_type;
		
		Sequence_stat();
		void print();
		void operator()(sequence_value_type & value);
	
	private:
		sequence_value_type minimum;
		sequence_value_type maximum;

		average_type average;
		
		count_type amount_of_positive;
		count_type amount_of_negative;
		
		sequence_value_type summ_of_even;
		sequence_value_type summ_of_odd;
		
		bool front_back_equal;
	
	//� �������
		sequence_value_type front;
		sequence_value_type back;
		count_type amount_of_sequence;
};
#include "Sequence_stat.inl"
#endif//SEQUENCE_STAT_H
