#include <list>//sequence_type
#include <algorithm>//std::for_each
#include <ctime>//srand
#include "Sequence_stat.h"//Sequence_stat
typedef std::list< Sequence_stat::sequence_value_type > sequence_type;
int main()
{
	srand(time(0));
	
	for(unsigned i = 0; i <= 13; i++)
	{
		sequence_type sequence(i);
		std::for_each(sequence.begin(), sequence.end(), sequence_random_generator());
		std::for_each(sequence.begin(), sequence.end(), sequence_printer());			
		Sequence_stat stat;
		stat = std::for_each(sequence.begin(), sequence.end(), stat);
		stat.print();
		std::cout << "-----------------------------" << std::endl;
	}
	return 0;
}
