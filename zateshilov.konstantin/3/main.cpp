#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <exception>
#include <algorithm>

#include "queue.hpp"
#include "list_functions.hpp"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Tests

#include <boost/test/unit_test.hpp>

	BOOST_AUTO_TEST_CASE(Usual_test_queue) 
	{
		typedef typename Queue_priority<std::string>::node node_t;
		typedef typename Queue_priority<std::string>::element_priority priority_t;

		Queue_priority<std::string> queue;
		std::string name("name");
		//init correct_vec
		std::vector<node_t> correct_vec;
		correct_vec.push_back(node_t(name+"_first",priority_t(2)));
		correct_vec.push_back(node_t(name,priority_t(2)));
		correct_vec.push_back(node_t(name,priority_t(1)));
		correct_vec.push_back(node_t(name,priority_t(1)));
		correct_vec.push_back(node_t(name,priority_t(0)));
		correct_vec.push_back(node_t(name,priority_t(0)));
		correct_vec.push_back(node_t(name+"_last",priority_t(0)));

		queue.Put(node_t(name,priority_t(0)));
		queue.Put(node_t(name,priority_t(0)));
		queue.Put(node_t(name+"_first",priority_t(2)));
		queue.Put(node_t(name,priority_t(1)));
		queue.Put(node_t(name,priority_t(2)));
		queue.Put(node_t(name+"_last",priority_t(0)));
		queue.Put(node_t(name,priority_t(1)));

		std::vector<node_t> test_vec;

		while(!queue.isEmpty())
		{
			test_vec.push_back(*queue.Get());
		}
		BOOST_CHECK(test_vec == correct_vec);
	}
	BOOST_AUTO_TEST_CASE(Accelerate_test_queue)
	{
		typedef typename Queue_priority<std::string>::node node_t;
		typedef typename Queue_priority<std::string>::element_priority priority_t;
		Queue_priority<std::string> queue;
		std::string name("name");

		std::vector<node_t> correct_vec;
		// init correct_vector
		correct_vec.push_back(node_t(name,priority_t(2)));
		correct_vec.push_back(node_t(name+"_second",priority_t(2)));
		correct_vec.push_back(node_t(name,priority_t(2)));
		correct_vec.push_back(node_t(name,priority_t(1)));
		correct_vec.push_back(node_t(name,priority_t(1)));

		std::vector<node_t> test_vec;
		// init test_vec
		queue.Put(node_t(name,priority_t(0)));
		queue.Put(node_t(name,priority_t(1)));
		queue.Put(node_t(name+"_second",priority_t(2)));
		queue.Put(node_t(name,priority_t(1)));
		queue.Put(node_t(name,priority_t(0)));

		queue.Accelerate();


		while(!queue.isEmpty())
		{
			test_vec.push_back(*queue.Get());
		}
		BOOST_CHECK(test_vec == correct_vec);
	}
	BOOST_AUTO_TEST_CASE(Empty_test_queue)
	{			
		Queue_priority<std::string> queue;
		BOOST_CHECK_THROW(queue.Get(),std::runtime_error)
	}
	BOOST_AUTO_TEST_CASE(Usual_test_list_14)
	{
		int source_ar[14] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
		int correct_ar[14] = {1,14,2,13,3,12,4,11,5,10,6,9,7,8};

		std::list<int> source_list(source_ar,source_ar+14);
		std::list<int> test_list;

		std::list<int> correct_list(correct_ar,correct_ar+14);
		print_fill_list(source_list.begin(),source_list.end(),test_list); // --end pointer on last element of list
		BOOST_CHECK(test_list == correct_list);
	}
		BOOST_AUTO_TEST_CASE(Usual_test_list_7)
	{
		int source_ar[7] = {-3,-2,-1,0,1,2,3};
		int correct_ar[7] = {-3,3,-2,2,-1,1,0};

		std::list<int> source_list(source_ar,source_ar+7);
		std::list<int> test_list;
		std::list<int> correct_list(correct_ar,correct_ar+7);
	
		print_fill_list(source_list.begin(),source_list.end(),test_list); // --end pointer on last element of list
		BOOST_CHECK(test_list == correct_list);
	}
		BOOST_AUTO_TEST_CASE(Usual_test_list_1)
	{
		std::list<int> source_list;
		source_list.push_back(1);
		std::list<int> test_list;
		std::list<int> correct_list;
		correct_list.push_back(1);
	
		print_fill_list(source_list.begin(),source_list.end(),test_list); // --end pointer on last element of list
		BOOST_CHECK(test_list == correct_list);
	}
	BOOST_AUTO_TEST_CASE(Usual_test_list_0)
	{
		std::list<int> source_list;
		std::list<int> test_list;
		std::list<int> correct_list;
	
		print_fill_list(source_list.begin(),source_list.end(),test_list); // --end pointer on last element of list
		BOOST_CHECK(test_list == correct_list);
	}