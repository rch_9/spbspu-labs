#include "list_functions.hpp"

#include <list>
#include <iostream>
#include <cstdlib>

void fill_random_list(std::list<int> & list,std::size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		list.push_back(static_cast<int>(rand()%20+1));
	}
}
void print_fill_list(std::list<int>::iterator begin, std::list<int>::iterator end, std::list<int> & new_list)
{
	if (begin == end) //check empty
	{
		return; 
	}
	new_list.push_back(*begin);
	if (--end != begin)
	{
		new_list.push_back(*end);
	}
	else
	{
		return;
	}
	if (++begin != end)
	{
		print_fill_list(begin,end,new_list);
	}
}