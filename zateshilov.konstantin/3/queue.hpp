#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <list>
#include <iostream>
#include <stdexcept>
#include <boost/shared_ptr.hpp>

template <typename T>
class Queue_priority
{
public:
	typedef T value_type;
	enum element_priority 
	{
		LOW,
		NORMAL,
		HIGH
	};
	struct node
	{
		value_type value;
		element_priority priority;
		node(const value_type & value_,const element_priority & priority_):
		value(value_),priority(priority_)
		{}
		bool operator == (const node &rhs) const
		{
			return (this->priority == rhs.priority) && (this->value == rhs.value);
		}
	};
	void Put(node node_)
	{
		list_.push_back(node_);
	}
	boost::shared_ptr<node> Get()
	{
	    typedef typename std::list<node>::iterator it_type;
        if (list_.empty())
        {
        	throw std::runtime_error("Queue is empty"); //should return something?
        }
        else
        {
        	it_type found = list_.begin();
        	for (it_type it = list_.begin(); it != list_.end(); ++it)
        	{
        		if (it->priority == HIGH)
    			{
					found = it;
					break;
    			}
        		if ((it->priority) > (found->priority))
        		{
        			found = it;
        		}
        	}
			boost::shared_ptr<node> ptr (new node(*( found )));
			list_.erase(found);

			return ptr;
		}
	}
	void Accelerate()
	{
		for (typename std::list<node>::iterator it = list_.begin(); it != list_.end(); ++it )
		{
			if (it->priority == LOW)
				{
					it->priority = HIGH;
				}
		}
	}
	bool isEmpty() const
	{
		return list_.empty();
	}
private:
	std::list<node> list_;
};
#endif