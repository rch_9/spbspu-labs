#ifndef LIST_FUNCTIONS_HPP
#define LIST_FUNCTIONS_HPP
#include <list>

void fill_random_list(std::list<int> & list,std::size_t size);
void print_fill_list(std::list<int>::iterator begin, std::list<int>::iterator end,std::list<int> & new_list);


#endif