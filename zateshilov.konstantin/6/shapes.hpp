#ifndef SHAPES_HPP
#define SHAPES_HPP

#include <vector>

struct Point
{
	int x_,y_;
	Point(int x,int y):x_(x),y_(y)
	{
	}
};
struct Shape
{	
	Shape(int type);
	std::vector<Point> vertexes_;
};

struct Shape_types_counter
{
	Shape_types_counter();
	void operator()(const Shape & shape);
	int num_triangles;
	int num_rectangles;
	int num_squares;
};

Shape generate_shape();
Point generate_point();
void print_shape(const Shape & shape);
void print_point(const Point & point);
int sum_vertexes(int accumulator, const Shape & b);
bool isTriangle(const Shape & shape);
bool isPentagon(const Shape & shape);
bool isSquare(const Shape & shape);
bool isRectangle(const Shape & shape);

Point push_point(const Shape & shape);

bool compare_shapes(const Shape & lhs, const Shape & rhs);
#endif