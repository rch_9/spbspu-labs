#include "shapes.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <numeric> // for accumulate
#include <iterator>
#include <set>
#include <iterator>

int main()
{	
	//***TASK1***//
	std::ifstream fin("text.txt");
	if (!fin)
	{
		std::cerr << "Can't open file!";
		return 1;
	}
	typedef std::istream_iterator<std::string> is_iter;
	std::set<std::string> words;
	std::copy(
		is_iter(fin),
		is_iter(),
		std::inserter(words,words.begin())
	);
	for( std::set<std::string>::iterator it = words.begin(); it != words.end(); ++it )
	{
		std::cout << *it << ' ';
	}
	std::cout << std::endl;
	//***TASK2***//
	srand(time(0));
	std::cout << std::endl;

	//generate shapes
	std::vector<Shape> shapes;
	for (int i = 0; i < 10; ++i)
	{
		shapes.push_back(Shape(rand()%4)); //gen all types
	}
	for_each(shapes.begin(),shapes.end(),print_shape);

	//sum all vertexes
	int product = std::accumulate(shapes.begin(),shapes.end(),0,sum_vertexes);
	std::cout << std::endl << " sum of shape's vertexes = " << product << std::endl;
	
	//count triangles, rectangles, and squares
	std::cout << " number of triangles = " << std::count_if(shapes.begin(),shapes.end(),isTriangle)
			<< " number of rectangles = " << std::count_if(shapes.begin(),shapes.end(),isRectangle)
			<< " number of squares = " << std::count_if(shapes.begin(),shapes.end(),isSquare) << std::endl;
	
	//Remove pentagons
	shapes.erase(std::remove_if(shapes.begin(),shapes.end(),isPentagon),shapes.end());
	for_each(shapes.begin(),shapes.end(),print_shape);

	//create std::vector<Point>
	std::vector<Point> points;
	points.reserve(shapes.size());
	std::transform(shapes.begin(),shapes.end(),std::back_inserter(points),push_point);
	std::cout << "vector<Point>" << std::endl;
	for_each(points.begin(),points.end(),print_point);

	//sort
	std::cout << " Sorted : " << std::endl;
	std::sort(shapes.begin(),shapes.end(),compare_shapes);
	for_each(shapes.begin(),shapes.end(),print_shape);
	return 0;
}

