#include "shapes.hpp"
#include <cstdlib>
#include <algorithm>
#include <iostream>


Point generate_point()
{
	return Point(rand()%1000,rand()%1000);
}
Shape::Shape(int type)
{
	switch(type)
	{
	case 0: //Triangle
		{
			vertexes_.push_back(generate_point());
			vertexes_.push_back(generate_point());
			vertexes_.push_back(generate_point());
			break;
		}
	case 1: //Rectangle
		{
			int width = rand()%1000;
			int hight = rand()%1000;
			Point p1(generate_point());
			Point p2(generate_point());
			vertexes_.push_back(p1);
			vertexes_.push_back(p2);
			p1.x_ += width;
			p2.x_ += width;
			p1.y_ += hight;
			p2.y_ += hight;
			vertexes_.push_back(p1);
			vertexes_.push_back(p2);
			break;
		}
	case 2: //Square
		{
			int width = rand()%1000;
			Point p1(generate_point());
			Point p2(generate_point());
			vertexes_.push_back(p1);
			vertexes_.push_back(p2);
			p1.x_ += width;
			p2.x_ += width;
			p1.y_ += width;
			p2.y_ += width;
			vertexes_.push_back(p1);
			vertexes_.push_back(p2);
			break;
		}
	case 3: //Pentagon
		{
			vertexes_.push_back(generate_point());
			vertexes_.push_back(generate_point());
			vertexes_.push_back(generate_point());
			vertexes_.push_back(generate_point());
			vertexes_.push_back(generate_point());			
			break;
		}
	}	
}
bool isTriangle(const Shape & shape)
{
	return shape.vertexes_.size() == 3;
}
bool isPentagon(const Shape & shape)
{
	return shape.vertexes_.size() == 5;
}
bool isRectangle(const Shape & shape)
{
	if (shape.vertexes_.size() == 4)
	{
		return !(isSquare(shape));
	}
	else
	{
		return false;
	}
}
bool isSquare(const Shape & shape)
{
	if (shape.vertexes_.size() == 4)
	{
		//if diagonals equals
		return (abs(shape.vertexes_[0].x_ - shape.vertexes_[2].x_) == abs(shape.vertexes_[1].y_ - shape.vertexes_[3].y_));
	}
	else
	{
		return false;
	}

}
void print_point(const Point & point)
{
	std::cout << " x = " << point.x_ << " y = " << point.y_ << std::endl;
}
void print_shape(const Shape & shape)
{
	if (isTriangle(shape))
	{
		std::cout << "Triangle" << std::endl;
	}
	if (isSquare(shape))
	{
		std::cout << "Square" << std::endl;
	}
	if (isRectangle(shape))
	{
		std::cout << "Rectangle" << std::endl;
	}
	if (isPentagon(shape))
	{
		std::cout << "Pentagon" << std::endl;
	}
	std::for_each(shape.vertexes_.begin(),shape.vertexes_.end(),print_point);
}
int sum_vertexes(int accumulator, const Shape & b)
{
	return accumulator + b.vertexes_.size();
}

Point push_point(const Shape & shape)
{
	return shape.vertexes_[rand()%shape.vertexes_.size()];
}
bool compare_shapes(const Shape & lhs, const Shape & rhs)
{
	if (lhs.vertexes_.size() == rhs.vertexes_.size())
	{
		return !isRectangle(lhs);
	}
	else
	{
		return false;
	}
}