#include <vector>
#include <cstdlib>
#include <iostream>

void rand_vector(std::vector<int> & v, std::size_t size, int upper_bound, int lower_bound)
{
	for ( std::size_t i = 0; i < size; ++i)
	{
		v.push_back(rand()%(upper_bound-lower_bound)+lower_bound);
	}
}