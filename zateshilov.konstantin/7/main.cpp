
#include "functor.hpp"
#include "utility.hpp"
#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
	srand(time(0));
	std::vector<int> v;
	rand_vector(v,100,500,-500);
	Get_info info;
	info = std::for_each(v.begin(),v.end(),info);
	info.print();
	std::cout << "Empty vector : " << std::endl;
	Get_info info_empty;
	v.clear();
	info_empty = std::for_each(v.begin(),v.end(),info_empty);
	info_empty.print();
	return 0;
}