#include "functor.hpp"
#include <iostream>

Get_info::Get_info(): max_num(0), min_num(0), avrg_num(0), pos_amount(0), neg_amount(0),
		even_sum(0), odd_sum(0), is_last_equal_first(false), is_first_added(false)
{
}
void Get_info::operator()(int number)
{
	if (is_first_added)
	{
		if ( number > max_num )
		{
			max_num = number;
		}

		if ( number < min_num )
		{
			min_num = number;
		}
	}
	else
	{
		first = number;
		is_first_added = true;
		max_num = number;
		min_num = number;
	}

	++size;

	if (number < 0)
	{
		++neg_amount;
	}
	if (number > 0)
	{
		++pos_amount;
	}

	if (number % 2 == 0)
	{
		even_sum += number;
	}
	else
	{
		odd_sum += number;
	}

	avrg_num = (odd_sum + even_sum) / size;
	is_last_equal_first = (first == number);
}
void Get_info::print()
{
	if (!is_first_added)
	{
		std::cout << "range is empty!" << std::endl;
	}
	else
	{
		std::cout << "max number = " << max_num << std::endl;
		std::cout << "min number = " << min_num << std::endl;
		std::cout << "average number = " << avrg_num << std::endl;
		std::cout << "amount posive numbers = " << pos_amount << std::endl;
		std::cout << "amount negative numbers = " << neg_amount << std::endl;
		std::cout << "sum of odd numbers = " << odd_sum << std::endl;
		std::cout << "sum of even numbers = " << even_sum << std::endl;
		if (is_last_equal_first)
		{
			std::cout << "last element equal first" << std::endl;
		}
		else 
		{
			std::cout << "last element is not equal first" << std::endl;
		}
	}
}