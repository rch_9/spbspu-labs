#ifndef FUCNTOR_HPP
#define FUCNTOR_HPP

class Get_info
{
public:
	Get_info();
	void operator()(int number);
	void print();
private:
	int max_num;
	int min_num;
	int avrg_num;
	int pos_amount;
	int neg_amount;
	int even_sum;
	int odd_sum;
	int size;
	int first;
	bool is_last_equal_first;
	bool is_first_added;
};
#endif