#include "utility.hpp" 
#include "shapes.hpp"

#include <iostream>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <boost/shared_ptr.hpp>
#include <cstdlib>
#include <boost/bind.hpp>

int main()
{
	srand(time(0));
	// Task1
	std::vector<double> v;
	rand_vector(v,10,-5,5);
	std::cout << "the initial vector: " << std::endl;
	std::copy(v.begin(), v.end(), std::ostream_iterator<double>(std::cout, " "));
	std::transform(v.begin(), v.end(), v.begin(), std::bind2nd(std::multiplies<double>(), acos(-1.0)));
	std::cout << std::endl << "after *PI: " << std::endl;
	std::copy(v.begin(), v.end(), std::ostream_iterator<double>(std::cout, " "));

	// Task2 
	std::vector<boost::shared_ptr<Shape> > shapes;
	rand_shapes(shapes, 10);
	std::cout << std::endl;
	std::for_each(shapes.begin(), shapes.end(), draw_shape);

	std::sort(shapes.begin(), shapes.end(), boost::bind(cmp_left_to_right, _1, _2));
	std::cout << std::endl << "Sorted left to right" << std::endl;
	std::for_each(shapes.begin(), shapes.end(), draw_shape);
	std::cout << std::endl;

	std::sort(shapes.begin(), shapes.end(), boost::bind(cmp_left_to_right, _2, _1));
	std::cout << std::endl << "Sorted right to left" << std::endl;
	std::for_each(shapes.begin(), shapes.end(), draw_shape);
	std::cout << std::endl;


	std::sort(shapes.begin(), shapes.end(), boost::bind(cmp_up_to_down, _1, _2));
	std::cout << std::endl << "Sorted from the top down: " << std::endl;
	std::for_each(shapes.begin(), shapes.end(), draw_shape);
	std::cout << std::endl;


	std::sort(shapes.begin(), shapes.end(), boost::bind(cmp_up_to_down, _2, _1));
	std::cout << std::endl << "Sorted from the bottom to top: " << std::endl;
	std::for_each(shapes.begin(), shapes.end(), draw_shape);
	std::cout << std::endl;
	return 0;
}