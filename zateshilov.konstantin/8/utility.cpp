#include "utility.hpp"
#include <boost/shared_ptr.hpp>
#include <iostream>
#include <cstdlib>

void rand_shapes(std::vector<boost::shared_ptr<Shape> > & v, std::size_t size)
{
	while (size--)
	{
		int i = rand()%3;
		switch(i)
		{
			case 0:
			{
				v.push_back(boost::shared_ptr<Shape>( new Circle( rand()%1024, rand()%1024 )));
				break;
			}
			case 1:
			{
				v.push_back(boost::shared_ptr<Shape>( new Square( rand()%1024, rand()%1024 )));
				break;
			}
			case 2:
			{
				v.push_back(boost::shared_ptr<Shape>( new Triangle( rand()%1024, rand()%1024 )));
				break;
			}
		}
	}
}
void rand_vector(std::vector<double> & v, std::size_t size, int upper_bound, int lower_bound)
{
	for ( std::size_t i = 0; i < size; ++i)
	{
		v.push_back(rand()%(upper_bound-lower_bound)+lower_bound);
	}
}