#include "shapes.hpp"
#include <iostream>

bool Shape::isUpper(const Shape & shape) const
{
	return y_ > shape.y_;
}
bool Shape::isMoreLeft(const Shape & shape) const
{
	return x_ < shape.x_;
}
double Shape::getY() const
{
	return y_;
}
double Shape::getX() const
{
	return x_;
}
Shape::Shape(double x, double y):x_(x),y_(y)
{
}


void Circle::Draw() const 
{
	std::cout << "Circle x = " << getX() << " y = " << getY() << std::endl;
}
Circle::Circle(double x, double y): Shape(x,y)
{
}

void Square::Draw() const
{
	std::cout << "Square x = " << getX() << " y = " << getY() << std::endl;
}
Square::Square(double x, double y): Shape(x,y)
{
}

void Triangle::Draw() const
{
	std::cout << "Triangle x = " << getX() << " y = " << getY() << std::endl;
}
Triangle::Triangle(double x, double y): Shape(x,y)
{
}

void draw_shape(const boost::shared_ptr<Shape> & shape)
{
	shape->Draw();
}

bool cmp_left_to_right(const boost::shared_ptr<Shape> & shape1, const boost::shared_ptr<Shape> & shape2)
{
	return (shape1->isMoreLeft(*shape2));
}
bool cmp_up_to_down(const boost::shared_ptr<Shape> & shape1, const boost::shared_ptr<Shape> & shape2)
{
	return (shape1->isUpper(*shape2));
}