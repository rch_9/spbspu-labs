#ifndef SHAPE_HPP
#define SHAPE_HPP
#include <boost/shared_ptr.hpp>

class Shape
{
public:
	bool isMoreLeft(const Shape & shape) const;
	bool isUpper(const Shape & shape) const;
	double getX() const;
	double getY() const;
	virtual void Draw() const = 0;

	Shape(double x, double y);
private:
	double x_,y_;
};

class Circle: public Shape
{
public:
	Circle(double x, double y);
	void Draw() const;
};

class Triangle: public Shape 
{
public:
	Triangle(double x, double y);
	void Draw() const;
};

class Square: public Shape
{
public:
	Square(double x, double y);
	void Draw() const;
};

void draw_shape(const boost::shared_ptr<Shape> & shape);
bool cmp_left_to_right(const boost::shared_ptr<Shape> & shape1, const boost::shared_ptr<Shape> & shape2);
bool cmp_up_to_down(const boost::shared_ptr<Shape> & shape1, const boost::shared_ptr<Shape> & shape2);
#endif