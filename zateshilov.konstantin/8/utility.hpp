#ifndef UTILITY_HPP
#define UTILITY_HPP
#include "shapes.hpp"

#include <vector>

#include <boost/shared_ptr.hpp>

void rand_shapes(std::vector<boost::shared_ptr<Shape> > & v, std::size_t size);
void rand_vector(std::vector<double> & v, std::size_t size, int upper_bound, int lower_bound);
#endif