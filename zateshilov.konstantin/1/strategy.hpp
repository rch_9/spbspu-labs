#ifndef POLICY_HPP
#define POLICY_HPP

#include <vector>
#include "list.hpp"

template <template <typename > class Policy, typename Container>
void sort(Container &v)	
{
	typedef Policy<Container> Current_Policy;
	typedef typename Current_Policy::position index_type;

	for (index_type i = Current_Policy::begin(v); i != Current_Policy::end(v); ++i)
	{   	
		for (index_type j = i; j != Current_Policy::end(v); ++j)
			if (Current_Policy::get(v,j) < Current_Policy::get(v,i))
			{
				std::swap(Current_Policy::get(v,j),Current_Policy::get(v,i));
			}
	}
}

template <typename Container>
struct atPolicy
{
	typedef typename Container::size_type position;
	typedef typename Container::value_type value;
	static value & get(Container & v, position pos) 
	{
		return v.at(pos);
	}
	static position begin(Container &) 
	{
		return 0;
	}
	static position end(Container & v) 
	{
		return v.size();
	}

};

template <typename Container>
struct bracesPolicy
{
	typedef typename Container::size_type position;
	typedef typename Container::value_type value;
	static value & get(Container & v, position pos)
	{
		return v[pos];
	}
	static position begin(Container & )
	{
		return 0;
	}
	static position end(Container & v) 
	{
		return v.size();
	}

};

template <typename Container>
struct listPolicy
{
	typedef typename Container::size_type position;
	typedef typename Container::value_type value;
	static value & get(Container & , position pos)
	{
		return *pos;
	}
	static position begin(Container & v)
	{
		return v.begin();
	}
	static position end(Container & v) 
	{
		return v.end();
	}

};
#endif