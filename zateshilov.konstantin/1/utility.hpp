#ifndef UTILITY_H
#define UTILITY_H
#include <vector>

void fill_random(double * ar, int size);
void read_file(const char * name, std::vector<char> &v);
void modify_vector(std::vector<int> & v);

#endif