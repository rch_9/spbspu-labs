#include <vector>
#include <cstdlib>
#include <ctype.h>
#include <iostream>
#include <boost/scoped_array.hpp>
#include <fstream>
#include <exception>
#include "utility.hpp"

void read_file(const char * name, std::vector<char> &v)
{
	if (name == 0)
	{
		std::cerr << "name = 0";
	}
	std::ifstream fin(name);

	if (!fin.is_open())
	{
		std::cerr << "File is not open!\n";
	}
	else
	{
		fin.seekg(0, std::ios::end);
		int length = fin.tellg();
		fin.seekg(0);
		boost::scoped_array<char> ar(new char[length-1]);
		int i = 0;
		while (i < length-1)
		{
			fin >> ar[i];
			++i;
		}
		fin.close();
		v.assign(ar.get(), ar.get() + length-1);
	}
}
void fill_random(double * ar, int size)
{
	const int min_rand = -1;
	const int wide = 2;
	for (int i = 0; i < size; ++i)
	{
		ar[i] = min_rand + (double(rand()) / RAND_MAX) * wide;
	}
}
void modify_vector(std::vector<int> & v)
{
	int value;
	do
	{
		std::cin.clear();
		std::cin.sync();
		std::cin >> value;
		if (value)
		{
			v.push_back(value);
		}
	} while (value && !(std::cin.fail()));

	for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
	{
		std::cout << *it << ' ';
	}
	if (v.empty())
	{
		std::cerr << '\n' << "Empty vector" << std::endl;
	}
	else
	{
		if (v.back() == 1)
		{
			for (std::vector<int>::iterator it = v.begin(); it != v.end();)
			{

				if ((*it % 2) == 0)
				{
					it = v.erase(it);
				}
				else
				{
					++it;
				}
			}
		}
		else if (v.back() == 2)
		{
			for (std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
			{
				if ((*it % 3) == 0)
				{
					for(int i = 0; i < 3; ++i)
					{
						it = v.insert(it + 1, 1);	
					}
				}
			}
		}
	}
}