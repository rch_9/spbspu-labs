#ifndef GUARD_HPP
#define GUARD_HPP
template < typename T, typename Policy >
class Guard
{
public:
	typedef Guard <T, Policy> this_t;
	void sort( T & vec);
};

template < typename T, typename Policy >
void Guard < T, Policy >::sort(T & vec)
{
	Policy::sort(vec);
}
#endif

