#include "utility.hpp"
#include "guard.hpp"
#include "strategy.hpp"
#include "list.hpp"

#include <vector>
#include <cstdlib>
#include <stdlib.h>
#include <iostream>
#include <algorithm> // for std::is_sorted

void test_task1()
{
	typedef std::vector<int> v_type;
	typedef flist<int>::flist_iterator flist_iter;
	int correct_ar[10] = { -1,-1,0,0,2,3,4,5,6,7 };
	int random_ar[10] = { 2,3,-1,0,-1,7,6,5,0,4 };
	v_type correct_vec(correct_ar,correct_ar+10);
	v_type random_vec(random_ar,random_ar+10);

	flist<int> random_flist;
	random_flist.push(2);
	random_flist.push(3);
	random_flist.push(-1);
	random_flist.push(0);
	random_flist.push(-1);

	flist<int> correct_flist;
	correct_flist.push(-1);
	correct_flist.push(-1);
	correct_flist.push(0);
	correct_flist.push(2);
	correct_flist.push(3);

	v_type v_braces(random_vec);
	sort<bracesPolicy>(v_braces);
	std::cout << "Task1: " << std::endl << std::boolalpha << (v_braces == correct_vec);

	v_type v_at(random_vec);
	sort<atPolicy>(v_at);
	std::cout << std::endl << std::boolalpha << (v_at == correct_vec);

	sort<listPolicy>(random_flist);
	bool compare = true;
	flist_iter it = random_flist.begin();
	flist_iter it_rhs = correct_flist.begin();
	while (it != random_flist.end() && it_rhs != correct_flist.end())
	{
		if (*it != *it_rhs) compare = false;
		++it;
		++it_rhs;
	}
	std::cout << std::endl << std::boolalpha << compare << std::endl;
}
void test_task2()
{
	std::cout << "Task2: " << std::endl;
	std::vector<char> vec;
	read_file("newfile.txt", vec);
	for (std::vector<char>::iterator it = vec.begin(); it != vec.end(); ++it)
	{
		std::cout << *it;
	}
}
void test_task3()
{
	std::cout << std::endl  << "Task3: " << std::endl;
	std::vector<int> test_vector;
	std::cout << "initial vector: " << std::endl;
	
	modify_vector(test_vector);

	std::cout << std::endl << "modified vector: " << std::endl;
	for (std::vector<int>::iterator it = test_vector.begin(); it != test_vector.end(); ++it)
	{
		std::cout << *it << ' ';
	}
}
void test_task4()
{

	std::vector<double> test_vector;

	test_vector.resize(5);

	fill_random(&test_vector[0], 5);
	test_vector.clear();

	test_vector.resize(10);
	fill_random(&test_vector[0], 10);
	test_vector.clear();

	test_vector.resize(25);
	fill_random(&test_vector[0], 25);
	test_vector.clear();

	test_vector.resize(50);
	fill_random(&test_vector[0], 50);
	test_vector.clear();

	test_vector.resize(100);
	fill_random(&test_vector[0], 100);
}
int main()
{
	srand((unsigned)time(0));
	test_task1();
	test_task2();
	test_task3();
	test_task4();
	std::cout << '\n';
	return 0; 
}