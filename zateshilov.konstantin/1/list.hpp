#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <assert.h>

template <typename T>
class flist
{
private:
	struct node_t
	{
		T value;
		node_t* next;
		node_t(T & val):
			value(val),
			next(0)
		{}
	};
	size_t size;
	node_t *head_;
	node_t *end_;
public:
	class flist_iterator : public std::iterator< std::forward_iterator_tag , T >
	{
	public:
		typedef flist_iterator this_type;

		flist_iterator():current_(0)
		{

		}
		flist_iterator(node_t * flist):
			current_(flist)
		{

		}

		bool operator ==(const flist_iterator & rhs) const
		{
			return current_ == rhs.current_;
		}
		bool operator !=(const flist_iterator & rhs) const
		{
			return current_ != rhs.current_;
		}

		T & operator *() const
		{
			assert(current_ != 0);
			return current_->value;
		}
		T* operator ->() const
		{
			assert(current_ != 0);
			return &current_->value;
		}

		flist_iterator & operator ++()
		{
			assert(current_ != 0);
			current_ = current_->next;
			return *this;
		}
		flist_iterator operator ++(int)
		{
			flist_iterator temp(*this);
			this->operator ++();
			return temp;
		}
	private:
		node_t * current_;

	};
	typedef flist_iterator size_type;
	typedef T value_type;
	flist():head_(0),end_(0)
	{
	}
	~flist();
	void push(T elem);

	flist_iterator begin()
	{
		return  flist_iterator(head_);
	}
	flist_iterator end()
	{
		return flist_iterator(end_->next);
	}
};


template <typename T>
void flist<T>::push(T value)
{
	node_t *new_node = new node_t(value);
	if (head_ != 0)
	{
		end_->next = new_node;
		end_ = new_node;		
	}
	else
	{
		end_ = head_ = new_node;
	}
	++size;
}
template <typename T>
flist<T>::~flist()
{
	while (head_)
	{
		node_t * curr = head_->next;
		delete head_;
		head_ = curr;
	}
}
#endif



