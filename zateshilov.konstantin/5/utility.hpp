#ifndef UTILITY_HPP
#define UTILITY_HPP
#include <vector>
#include <string>
#include <iomanip>
#include <iostream>

struct Data
{
	int key1_;
	int key2_;
	std::string str_;
	Data(int key1, int key2, std::string str);
	friend std::ostream & operator<<(std::ostream & os, const Data & dt);
};


void fill_Random(std::vector<Data> & v, size_t size);

#endif