#include "utility.hpp"

#include <string>
#include <vector>
#include <stdlib.h>

const std::string table_str[] = {"one","two","three","four","five","six","seven","eight","nine","ten"};

Data::Data(int key1, int key2, std::string str)
		:key1_(key1),key2_(key2),str_(str)
{
}
std::ostream & operator<<(std::ostream & os, const Data & dt)
{
	os << std::setw(2) << dt.key1_ << std::setw(2) << dt.key2_ << std::setw(10) << dt.str_ << std::endl;
	return os;
}
void fill_Random(std::vector<Data> & v,size_t size)
{
	const int key_lower_bound = -5;
	const int key_upper_bound = 5;
	const int amount_strings = 10;
	for (size_t i = 0; i < size; ++i)
	{
		v.push_back(
		  Data
		  ( 
		    rand()%(key_upper_bound-key_lower_bound+1)+key_lower_bound,
		    rand()%(key_upper_bound-key_lower_bound+1)+key_lower_bound,
		    table_str[rand()%(amount_strings-1)]
		  ) 
		);
	}
}
