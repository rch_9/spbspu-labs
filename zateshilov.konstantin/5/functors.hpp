#ifndef FUNCTORS_HPP
#define FUNCTORS_HPP

#include <iomanip>
#include <iostream>
#include "utility.hpp"

struct compare_less
{
	bool operator()(const Data & lhs, const Data & rhs) const
	{
		if( lhs.key1_ == rhs.key1_ )
		{
			if (lhs.key2_ == rhs.key2_)
			{
				return lhs.str_.size() < rhs.str_.size();
			}
			else
			{
				return lhs.key2_ < rhs.key2_;
			}
		}
		else
		{
			return lhs.key1_ < rhs.key1_;
		}
	}
};

#endif