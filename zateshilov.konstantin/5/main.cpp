#include "utility.hpp"
#include "functors.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iterator>

int main()
{
	srand(time(0));

	std::vector<Data> v;
	fill_Random(v,10);
	std::copy(v.begin(),v.end(), std::ostream_iterator<Data>(std::cout));
	std::sort(v.begin(),v.end(),compare_less());
	std::cout << "Sorted vector<Data> : " << std::endl;
	std::copy(v.begin(),v.end(), std::ostream_iterator<Data>(std::cout));
	return 0;
}