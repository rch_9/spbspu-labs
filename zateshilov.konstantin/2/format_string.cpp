#include "format_string.hpp"

#include <string>
#include <vector>

std::string punct(",.!?;:");
std::string letters("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
const std::string vau("Vau!!!");
const size_t length_vau = vau.length();

//Erase tabs
void erase_tabs(std::string & file)	
{
	for (size_t i = 0; i < file.size();)
	{
		if (file[i] == '\t')
		{
			file[i] = ' ';
		}
		else
		{
			i++;
		}
	}
}
//Delete excess spaces
void delete_ex_spaces(std::string & file) 	
{
	for (size_t i = 0; i < file.size();)
	{
		if (file[i] == ' ')
		{
			size_t lenght_spaces = file.find_first_not_of(' ', i) - i;
			if (lenght_spaces > 1)
			{
				file.erase(i, lenght_spaces - 1);
			}
			else
			{
				++i;
			}
		}
		else
		{
			++i;
		}
	}
}
void format_punctuation(std::string & file)
{
	for (size_t i = 0; i < file.size();)
	{									  
		if (punct.find(file[i], 0) != std::string::npos)
		{
			if ((i != 0) && (file[i - 1] == ' ')) 
			{
				file.erase(i - 1, 1);
			}
			if ((i != file.size()) && (file[i + 1] != ' '))
			{
				file.insert(i + 1, 1, ' ');
			}
			else
			{
				++i;
			}
		}
		else
		{
			++i;
		}
	}
}
//Replace words with lenght > 10 "Vau!!!"	
void VAU(std::string & file)
{

	for (size_t i = 0; i < file.size();)
	{
		if (letters.find(file[i], 0) != std::string::npos)
		{
			size_t length_word = file.find_first_not_of(letters, i) - i;
			if (length_word > 10)
			{
				file.replace(i, length_word, vau);
				i += length_vau;
			}
			else
			{
				++i;
			}
		}
		else
		{
			++i;
		}
	}
}
void str_to_vec(std::string & file, std::vector<std::string> & result_text)
{
	std::string extra_symbols = letters + punct; 
	const int lenght_string = 40;
	size_t line_begin = 0;
	size_t line_end = lenght_string;
	while (line_end < file.size())
	{

		if (extra_symbols.find(file[line_end], 0) != std::string::npos) // check 40 symbol from line_begin
		{																// if it's letter
			line_end = file.find_last_not_of(extra_symbols, line_end);  // go to the begin of the last word
			result_text.push_back(file.substr(line_begin, line_end - line_begin + 1)); // +1 for symbols on previous strings
			line_begin = line_end + 1; 
			line_end += 40;
		}
		else
		{																// if this is space, push string to result
			result_text.push_back(file.substr(line_begin, line_end - line_begin + 1));
			line_begin = line_end + 1;
			line_end += 40;
		}
	}
	result_text.push_back(file.substr(line_begin, file.size() - line_begin)); // push last string 
}