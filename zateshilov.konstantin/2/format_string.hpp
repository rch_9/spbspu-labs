#ifndef FORMAT_STRING
#define FORMAT_STRING
#include <string>
#include <vector>

void erase_tabs(std::string & file);
void delete_ex_spaces(std::string & file);
void format_punctuation(std::string & file);
void VAU(std::string & file);
void str_to_vec(std::string & file,std::vector<std::string> & result_text);

#endif