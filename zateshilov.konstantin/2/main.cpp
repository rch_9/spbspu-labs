#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "format_string.hpp"

int main(int argc, char *argv[])
{
	try
	{
		std::ifstream fin("text.txt");
		std::string file;
		if (!fin)
		{
			throw std::runtime_error("Can't open file!");
		}
		while (!fin.eof())
		{
			std::string temp;
			getline(fin, temp);
			file += temp + ' ';
		}
		std::cout << "\nBefore\n" << file;
		std::vector<std::string> result_text;
		erase_tabs(file);
		delete_ex_spaces(file);
		format_punctuation(file);
		VAU(file);
		str_to_vec(file, result_text);

		for (std::vector<std::string>::iterator it = result_text.begin(); it != result_text.end(); ++it)
		{
			std::cout << '\n';
			std::cout << *it;
			std::cout << ' ' << (*it).size();
			std::cout << '\n';
		}
	}
	catch (const std::exception & e)
	{
		std::cerr << "e.what() = " << e.what();
		return 1;
	}	
	catch (...)
	{
		std::cerr << "Unknown exeption";
		return 1;
	}
	return 0;

}