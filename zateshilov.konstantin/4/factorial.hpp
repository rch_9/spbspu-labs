#ifndef FACTORIAL_HPP
#define FACTORIAL_HPP

#include <iterator>

class Factorial
{
public:
	class Factorial_iterator: public std::iterator<std::bidirectional_iterator_tag,
			int,std::size_t,int*,int&>
	{
	public:
		Factorial_iterator();
		Factorial_iterator(int value,int count);

		int operator*() const;

		Factorial_iterator & operator--();
		Factorial_iterator operator--(int);

		Factorial_iterator & operator++();
		Factorial_iterator operator++(int);
		
		bool operator==(const Factorial_iterator & rhs) const;
		bool operator!=(const Factorial_iterator & rhs) const;
	private:
		int value_factorial_;
		int count_factorial_;
	};
	Factorial();
	Factorial_iterator begin() const;
	Factorial_iterator end() const;
private:
	const static int size = 10;
};
int calc_factorial(int n);
#endif