#include "notes.hpp"
#include <iterator>
#include <stdexcept>

std::ostream & operator<<(std::ostream & os, const Phonebook::Note & note)
{
		os << "name : " << note.name_ << " phone : " <<
				note.phone_number_;
		os << std::endl;
		return os;
}

void Phonebook::push(const Note & note)
{
	container_.push_back(note);
}
void Phonebook::advance(it_type & it, int count)
{
	if ((count >= std::distance( it,container_.end() )) || (count <= std::distance( it,container_.begin() )))
	{
		throw std::out_of_range(" wrong advance of iterator! ");
	}
	else
	{
		std::advance(it,count);

	}
}
Phonebook::Phonebook(const container_type & container)
			:container_(container)
{
}
Phonebook::Phonebook()
{
}
void Phonebook::insert(const it_type & it, const Note & note)
{
	container_.insert(it, note);
}
void Phonebook::modify(it_type current,const Note & new_note)
{
	*current = new_note;
}
Phonebook::it_type Phonebook::begin()
{
	return container_.begin();
}
Phonebook::it_type Phonebook::end()
{
	return container_.end();
}
void printPhonebook(Phonebook & pb)
{
	for(Phonebook::it_type it = pb.begin(); it != pb.end(); ++it)
	{
		std::cout << *it;
	}
}