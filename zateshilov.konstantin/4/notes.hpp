#ifndef NOTES_HPP
#define NOTES_HPP

#include <iostream>
#include <list>



class Phonebook
{
public:
	struct Note
	{
		std::string name_;
		std::string phone_number_;
		Note(const std::string & name, const std::string & phone_number):name_(name),phone_number_(phone_number)
		{
		}	
	};
	typedef std::list<Note>::iterator it_type;
	typedef std::list<Note> container_type;
	Phonebook(const container_type & container);
	Phonebook();
	void insert(const it_type & it, const Note & note);
	void push(const Note & note);
	void advance(it_type & it, int count);
	void modify(it_type current,const Note & new_note);

	it_type begin();
	it_type end();
private:
	container_type container_;
};
std::ostream & operator<<(std::ostream & os, const Phonebook::Note & note);
void printPhonebook(Phonebook & pb);
#endif