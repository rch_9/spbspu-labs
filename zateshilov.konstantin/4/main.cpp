#include "notes.hpp"
#include "factorial.hpp"
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <iterator>

int main()
{
	try
	{
		// TASK 1
		Phonebook book1; //Show the phonebook
		book1.push(Phonebook::Note("name1","phone1"));
		book1.push(Phonebook::Note("name2","phone2"));
		book1.push(Phonebook::Note("name3","phone3"));
		book1.push(Phonebook::Note("name4","phone4"));
		book1.push(Phonebook::Note("name5","phone5"));

		printPhonebook(book1);

		book1.modify(book1.begin(),Phonebook::Note("new_name1","new_phone1"));
		std::cout << "\nmodify\n";
		printPhonebook(book1);

		std::cout << "\nInsert Note\n";
		printPhonebook(book1);

		std::cout << std::endl;
		book1.insert(book1.begin(),Phonebook::Note("insert_name2","insert_phonet2"));
		printPhonebook(book1);

		Phonebook::it_type it = book1.begin();
		book1.advance(it,5);
		std::cout << "\nbook1.advance(5);\n";
		std::cout << *(it);
		
		// TASK 2
		std::cout << std::endl;
		Factorial f;
		std::copy(f.begin(),f.end(),std::ostream_iterator<int>(std::cout," "));
		std::cout << std::endl;
		std::cout << std::endl;
 		std::vector<int> v(9);
		std::copy(f.begin(),f.end(),v.begin());
		std::copy(v.begin(),v.end(),std::ostream_iterator<int>(std::cout," "));
	}
	catch(const std::exception & e)
	{
		std::cerr << e.what();
	}
	return 0;
}