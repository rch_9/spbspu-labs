#include "factorial.hpp"
#include <iostream>
#include <stdexcept>

int calc_factorial(int n) // to calculate Factorial_iterator.end()
{
	return !n ? 1 : n * calc_factorial(n-1);
}
typedef Factorial::Factorial_iterator fact_it_type;

fact_it_type::Factorial_iterator():value_factorial_(1),count_factorial_(1)
{
}
fact_it_type::Factorial_iterator(int value,int count):value_factorial_(value),count_factorial_(count)
{
}
int fact_it_type::operator*() const
{
	return value_factorial_;
}
fact_it_type & fact_it_type::operator--()
{
	if (count_factorial_ >= 1)
	{
		--count_factorial_;
		value_factorial_ /= count_factorial_;
		return *this;		
	}
	else
	{	
		throw std::out_of_range("Can't decrement Factorial_iterator");
	}
}
fact_it_type fact_it_type::operator--(int)
{
	fact_it_type temp(value_factorial_, count_factorial_);
	this->operator--();
	return temp;		
}

fact_it_type & fact_it_type::operator++()
{
	if (count_factorial_ <= size)
	{
		++count_factorial_;
		value_factorial_*=count_factorial_;
		return *this;
	}
	else
	{
		throw std::out_of_range("Cant increase Factorial_iterator");
	}
}
fact_it_type fact_it_type::operator++(int)
{
	fact_it_type temp(value_factorial_, count_factorial_);
	this->operator++();
	return temp;
}

bool fact_it_type::operator==(const Factorial_iterator & rhs) const
{
	return count_factorial_ == rhs.count_factorial_;
}
bool fact_it_type::operator!=(const Factorial_iterator & rhs) const
{
	return count_factorial_ != rhs.count_factorial_;
}

Factorial::Factorial()
{
}
fact_it_type  Factorial::begin() const
{
	return Factorial_iterator();
}
fact_it_type Factorial::end() const
{
	return Factorial_iterator(calc_factorial(size),size);
}