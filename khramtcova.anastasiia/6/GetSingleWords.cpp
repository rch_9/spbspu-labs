#include<string>
#include<set>
#include<iostream>
#include<fstream>
#include<algorithm>

void GetSingleWords(const char * filename)
{
	std::ifstream f(filename);
	if (!f)
	{
		std::cerr << "Error: file is not accessible.\n";
	}
	else
	{
		std::set< std::string > words;
		std::copy(std::istream_iterator<std::string>(f), std::istream_iterator<std::string>(), std::inserter(words, words.begin()));
		if (words.empty())
		{
			std::cout << "There are no single words\n";
		}
		else
		{
			std::copy(words.begin(), words.end(), std::ostream_iterator< std::string >(std::cout, " "));
		}
	}
}