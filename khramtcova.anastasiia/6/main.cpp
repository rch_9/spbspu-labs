#include <algorithm>
#include <iostream>
#include <numeric>
#include"Shape.h"

void GetSingleWords(const char * filename);

int main()
{
	srand(time(NULL));

	GetSingleWords("Text.txt");

	std::vector<Shape> vectorOfShapes(10);
	std::generate(vectorOfShapes.begin(), vectorOfShapes.end(), GenerateShape);
	std::copy(vectorOfShapes.begin(), vectorOfShapes.end(), std::ostream_iterator< Shape >(std::cout));
	
	std::cout << "Number of vertexes " << std::accumulate(vectorOfShapes.begin(), vectorOfShapes.end(), 0, SumVertexes) << "\n";

	std::cout << "Number of triangles " <<
		std::count_if(vectorOfShapes.begin(), vectorOfShapes.end(), IsTriangle) << "\n";

	std::cout << "Number of squares " <<
		std::count_if(vectorOfShapes.begin(), vectorOfShapes.end(), IsSquare) << "\n";

	std::cout << "Number of rectangles " <<
		std::count_if(vectorOfShapes.begin(), vectorOfShapes.end(),IsRectangle) << "\n";

	std::cout << "Number of pentagons " <<
		std::count_if(vectorOfShapes.begin(), vectorOfShapes.end(), IsPentagon) << "\n";
	
	vectorOfShapes.erase(std::remove_if(vectorOfShapes.begin(), vectorOfShapes.end(), IsPentagon), vectorOfShapes.end());
	std::copy(vectorOfShapes.begin(), vectorOfShapes.end(), std::ostream_iterator< Shape >(std::cout));

	std::vector< Shape::Point > vectorOfPoints(vectorOfShapes.size());
	std::transform(vectorOfShapes.begin(), vectorOfShapes.end(), vectorOfPoints.begin(), GetPoint);
	std::copy(vectorOfPoints.begin(), vectorOfPoints.end(), std::ostream_iterator< Shape::Point >(std::cout));

	std::sort(vectorOfShapes.begin(), vectorOfShapes.end(), Comparator);
	std::copy(vectorOfShapes.begin(), vectorOfShapes.end(), std::ostream_iterator< Shape >(std::cout));

	return 0;

}