#include"Shape.h"

Shape GenerateShape()
{
	std::vector < Shape::Point > vertexes_;
	int type_ = rand() % 4; //0 - triangle 1 - square 2 - rectangle 3 -pentagon
	switch (type_)
	{
	case 0:
	{
		vertexes_.resize(3);//number of points of triangle is 3
		for (int i = 0; i < 3; ++i)
		{
			vertexes_[i].x = -100 + std::rand() % 200;
			vertexes_[i].y = -100 + std::rand() % 200;
		}
		break;
	}
	case 1:
	{
		vertexes_.resize(4);//number of points of square is 4
		for (int i = 0; i < 2; ++i)
		{
			vertexes_[i].x = -100 + std::rand() % 200;
			vertexes_[i].y = -100 + std::rand() % 200;
		}
		double shapeSize = sqrt((vertexes_[0].x + vertexes_[1].x)*(vertexes_[0].x + vertexes_[1].x)
			+ (vertexes_[0].y + vertexes_[1].y)*(vertexes_[0].y + vertexes_[1].y));
		vertexes_[2].x = vertexes_[0].x + shapeSize;
		vertexes_[2].y = vertexes_[0].y;
		vertexes_[3].x = vertexes_[0].x + shapeSize;
		vertexes_[3].y = vertexes_[0].y + shapeSize;
		break;
	}
	case 2:
	{
		vertexes_.resize(4);//number of points of rectangle is 4
		for (int i = 0; i < 4; ++i)
		{
			vertexes_[i].x = -100 + std::rand() % 200;
			vertexes_[i].y = -100 + std::rand() % 200;
		}
		break;
	}
	case 3:
	{
		vertexes_.resize(5);//number of points of pentagon is 5
		for (int i = 0; i < 5; ++i)
		{
			vertexes_[i].x = -100 + std::rand() % 200;
			vertexes_[i].y = -100 + std::rand() % 200;
		}
		break;
	}
	}
	return Shape(type_, vertexes_);
}

int SumVertexes(int sum, const Shape & shape)
{
	return sum = sum + shape.vertexes.size();
}

Shape::Point GetPoint(const Shape & shape)
{
	return shape.vertexes[rand() % shape.vertexes.size()];
}
bool Comparator(const Shape & lhs, const Shape & rhs)
{
	return lhs.type < rhs.type;
}
bool IsTriangle(const Shape & shape)
{
	return shape.type == 0;
}

bool IsSquare(const Shape & shape)
{
	return shape.type == 1;
}
bool IsRectangle(const Shape & shape)
{
	return shape.type == 2;
}
bool IsPentagon(const Shape & shape)
{
	return shape.type == 3;
}