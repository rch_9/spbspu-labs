#ifndef SHAPE_H
#define SHAPE_H

#include<string>
#include<vector>
#include<ctime>
#include<cmath>

struct Shape
{
	struct Point
	{
		double x, y;
		friend std::ostream & operator << (std::ostream & out, const Point & point)
		{
			return out << point.x << " " << point.y << "\n";
		}
	};
	int type; //0 - triangle 1 - square 2 - rectangle 3 -pentagon
	std::vector< Point > vertexes;
	Shape()
	{
	}
	Shape(const int type_, const std::vector< Point > & vertexes_) :
		type(type_),
		vertexes(vertexes_)
	{
	}
	friend std::ostream & operator << (std::ostream & out, const Shape & shape)
	{
		switch (shape.type)
		{
		case 0:
		{
			out << "triangle ";
			break;
		}
		case 1:
		{
			out << "square ";
			break;
		}
		case 2:
		{
			out << "rectangle ";
			break;
		}
		case 3:
		{
			out << "pentagon\n";
			break;
		}
		}
		for (int i = 0; i < shape.vertexes.size(); ++i)
		{
			out << "\n	" << i << ". x = " << shape.vertexes[i].x << " " << " y = " << shape.vertexes[i].y;
		}
		out << "\n";
		return out;
	}
};

int SumVertexes(int sum, const Shape & shape);
Shape GenerateShape();
Shape::Point GetPoint(const Shape & shape);
bool Comparator(const Shape & lhs, const Shape & rhs);
bool IsTriangle(const Shape & shape);
bool IsSquare(const Shape & shape);
bool IsSquare(const Shape & shape);
bool IsRectangle(const Shape & shape);
bool IsPentagon(const Shape & shape);

#endif
