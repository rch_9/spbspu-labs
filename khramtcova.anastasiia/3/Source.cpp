#include "QueueWithPriority.h"
#include "task2.cpp"

int main()
{
	typedef QueueWithPriority<std::string> myQueue_type;
	myQueue_type myQueue;
	myQueue.Put("A", myQueue_type::LOW);
	myQueue.Put("B", myQueue_type::LOW);
	myQueue.Put("C", myQueue_type::NORMAL);
	myQueue.Put("D", myQueue_type::NORMAL);
	myQueue.Put("E", myQueue_type::NORMAL);
	
	std::cout << "Queue in time order\n";
	myQueue.Print();

	cout << "Get elements from queue\n";
	size_t size = myQueue.Size();
	for (size_t i = 0; i < size; ++i)
	{
		std::cout << *myQueue.Get();
	}

	myQueue.Put("1", myQueue_type::LOW);
	myQueue.Put("2", myQueue_type::HIGH);
	myQueue.Put("3", myQueue_type::LOW);
	myQueue.Put("4", myQueue_type::NORMAL);
	myQueue.Put("5", myQueue_type::HIGH);
	
	std::cout << "Queue\n";
	myQueue.Print();
	
	myQueue.Accelerate();
	std::cout << "After acceleration:\n";
	myQueue.Print();

	std::cout << "Get elements from queue\n";
	size = myQueue.Size();
	for (size_t i = 0; i < size; ++i)
	{
		std::cout << *(myQueue.Get()) << std::endl;
	}

	myQueue.Get();
	myQueue.Accelerate();

	std::list<int> intList;
	FillList(intList, 0);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 1);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 2);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 3);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 4);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 5);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 7);
	PrintInFirstLastOrder(intList);

	intList.clear();
	FillList(intList, 14);
	PrintInFirstLastOrder(intList);

	return 0;
}