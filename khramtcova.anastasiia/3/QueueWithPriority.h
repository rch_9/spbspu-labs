#ifndef QUEUEWITHPRIORITY_H
#define QUEUEWITHPRIORITY_H

#include<iterator>
#include<list>
#include<string>
#include<iostream>
#include<stdio.h>
#include<ostream>
#include<boost/shared_ptr.hpp>

template <typename T>
class QueueWithPriority
{
	typedef T value_type;
public:
	enum ElementPriority
	{
		LOW,
		NORMAL,
		HIGH
	};
private:
	struct QueueElement
	{
		value_type  Name;
		ElementPriority Priority;
		QueueElement(const value_type name_, ElementPriority priority_) :
			Name(name_),
			Priority(priority_)
		{
		}
		friend std::ostream& operator << (std::ostream &stream, const QueueElement &element)
		{
			return stream << element.Name << endl;
		}
	}; 
public:
	void Put(const value_type & element, const ElementPriority  priority)
	{
		if (priority == LOW)
		{
			ListLow.push_back(QueueElement(element, priority));
		}
		else if (priority == NORMAL)
		{
			ListNormal.push_back(QueueElement(element, priority));
		}
		else if (priority == HIGH)
		{
			ListHigh.push_back(QueueElement(element, priority));
		}
	}
	void Accelerate()
	{
		if (!ListLow.empty())
		{
			ListHigh.splice(ListHigh.begin(), ListLow);
		}
	}
	void Print()
	{
		for (std::list<QueueElement>::iterator it = ListLow.begin(); it != ListLow.end(); ++it)
		{
			cout << *it;
		}
		for (std::list<QueueElement>::iterator it = ListNormal.begin(); it != ListNormal.end(); ++it)
		{
			cout << *it;
		}
		for (std::list<QueueElement>::iterator it = ListHigh.begin(); it != ListHigh.end(); ++it)
		{
			cout << *it;
		}
	}
	boost::shared_ptr< value_type > Get()
	{
		if (ListLow.empty() && ListNormal.empty() && ListHigh.empty())
		{
			std::cerr << "Empty queue\n";
			boost::shared_ptr< value_type > ptrToReturn;
			return ptrToReturn;
		}
		else if (!ListHigh.empty())
		{
			boost::shared_ptr< value_type > ptrToReturn(new value_type ((ListHigh.front()).Name));
			ListHigh.pop_front();
			return ptrToReturn;
		}
		else if (!ListNormal.empty())
		{
			boost::shared_ptr< value_type > ptrToReturn(new value_type((ListNormal.front()).Name));
			ListNormal.pop_front();
			return ptrToReturn;
		}
		else if (!ListLow.empty())
		{
			boost::shared_ptr< value_type > ptrToReturn(new value_type((ListLow.front()).Name));
			ListLow.pop_front();
			return ptrToReturn;
		}
	}
	size_t Size()
	{
		return ListHigh.size() + ListNormal.size()+ListLow.size();
	}
private:
	std::list<QueueElement> ListLow;
	std::list<QueueElement> ListNormal;
	std::list<QueueElement> ListHigh;
};
#endif