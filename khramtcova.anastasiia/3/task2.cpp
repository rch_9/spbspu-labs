#ifndef TASK2_CPP
#define TASK2_CPP

#include<list>
#include<iostream>
#include<stdio.h>
#include<ctime>

using namespace std;

template<typename T>
void PrintInFirstLastOrder(list<T> &myList)
{
	if (myList.empty())
	{
		cerr << "Empty list." << endl;
	}
	else
	{
		list<T>::iterator begin = myList.begin();
		list<T>::iterator end = myList.end();
		--end;
		if (begin == end) // size of list is 1
		{
			cout << *begin << endl;
		}
		else if (myList.size() % 2 != 0)//size of is odd
		{
			while (begin != end)
			{
				cout << *begin << " " << *end << " ";
				++begin;
				--end;
			}
			cout << *begin << endl;
		}
		else if (myList.size() % 2 == 0)//size of is even
		{
			while (begin != ++end)
			{
				--end;
				cout << *begin << " " << *end << " ";
				++begin;
				--end;
			}
			cout << endl;
		}
	}
}
template<typename T>
static void FillList(list<T> &myList, const size_t size)
{
	srand(static_cast<unsigned int>(time(NULL)));
	for (size_t i = 0; i < size; ++i)
	{
		myList.push_back(1 + rand() % 20);
	}
}
#endif