//Class of forward list
#ifndef SLIST_H
#define SLIST_H
#include <assert.h>
#include <iterator>
#include <iostream>
template <typename T>
class slist
{
private:
	struct node_t
	{
		T value;
		node_t *next;
		node_t(T val) : value(val), next(NULL)
		{
		}
	};
	size_t size_;
	node_t *begin_, *end_;

public:
	typedef T value_type;
	class iterator :
		public std::iterator < std::forward_iterator_tag, T >
	{
	public:
		iterator() :
			current_(0)
		{
		};
		iterator(node_t *slist) :
			current_(slist)
		{
		};

		bool operator == (const iterator &rhs) const
		{
			return current_ == rhs.current_;
		};
		bool operator != (const iterator &rhs) const
		{
			return current_ != rhs.current_;
		};
		bool operator > (const iterator &rhs) const
		{
			return current_ > rhs.current_;
		};
		value_type & operator *() const
		{
			assert(current_ != 0);
			return current_->value;
		};
		value_type * operator ->() const
		{
			assert(current_ != 0);
			return &current_->value;
		};
		iterator& operator ++()
		{
			assert(current_ != 0);
			current_ = current_->next;
			return *this;
		};
		iterator& operator ++(int)
		{
			iterator temp(*this);
			this->operator++();
			return temp;
		};
	private:
		node_t *current_;
	};
	
	inline size_t size() const
	{
		return size_;
	}
	void push_back(value_type val)
	{
		node_t *node = new node_t(val);
		if (!begin_)
		{
			begin_ = end_ = node;
		}
		else
		{
			end_->next = node;
			end_ = node;
		}
		++size_;

	}
	iterator begin()
	{
		return iterator(begin_);
	}

	iterator end()
	{
		return iterator(0);
	}

	slist() : begin_(NULL), end_(NULL), size_(0)
	{
	}

	~slist()
	{
		while (begin_)
		{
			node_t *curr = begin_->next;
			delete begin_;
			begin_ = curr;

		}
	}
	void Print() const
	{
		node_t *current_ = begin_;
		for (size_t i = 0; i < size_; ++i)
		{
			cout << current_->value << " ";
			current_ = (current_->next);
		}
		std::cout << "\n";
	}
};
#endif