//Make FillRandom(double * array, int size), which fill array 
//with random numbers from -1.0 to 1.0
//Then fill vector with it and sort vector
#ifndef TASK4_CPP
#define TASK4_CPP
#include <ctime>
#include <iostream>

static void FillRandom(double *myContainer, unsigned int size)
{
	for (unsigned int i = 0; i < size; ++i)
	{
		myContainer[i] = 1 - 2 * (double)rand() / RAND_MAX;
	}
}
#endif