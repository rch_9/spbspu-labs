//task3 get from standard stream numbers and save in vector
//delete all elements divided by 2 if the last number is 1
//if the last number is 2 then add three 1 after each such number
#include <stdio.h>
#include <iostream>
#include <vector>
#include <cctype>

using namespace std;

static void ReadFromStreamToVector(vector<int> &myVector)
{
	cout << "Print numbers. End of inputing is 0 " << endl; 
	int number;
	cin >> number;
	if (!cin.good())
	{
		cout << "Error: it is not a digit." << endl;
	}
	while (number != 0)
	{
		myVector.push_back(number);
		cin >> number;
		if (!cin.good())
		{
			cout << "Error: it is not a digit." << endl;
		}
	}
}

static void CuriousVectorChange(vector<int> &myVector)
{
	if (myVector.empty())
	{
		cerr << "Error: zero vector." << endl;
	}
	else
	{
		if (myVector.back() == 1)
		{
			vector<int> ::const_iterator it = myVector.begin();
			while (it != myVector.end())
			{
				if ((*it % 2) == 0)
				{
					it = myVector.erase(it);
				}
				else
				{
					++it;
				}
			}
		}
		else if (myVector.back() == 2)
		{
		    vector<int> ::iterator it = myVector.begin();
			while (it != myVector.end())
			{
				if ((*it % 3) == 0)
				{
					int arrayToInsert[] = { 1, 1, 1 };
					it = myVector.insert(it + 1, 3, 1);
					it = it + 3;
				}
				else
				{
					++it;
				}
			}
		}
	}
}