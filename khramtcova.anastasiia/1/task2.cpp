//task 2 read text from file to array 
//and copy it into vector
#ifndef TASK2_CPP
#define TASK2_CPP
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>

using namespace std;

static void ReadFromFileToVector(vector<char> &myVector)
{
	ifstream f("my_file.txt");
	string lines;
	if (!f)
	{
		cerr << "Error: file is not accessible." << endl;
	}
	else
	{
		while (f.good())
		{
			string aLine;
			getline(f, aLine, '\n');
			lines = lines + '\n' + aLine;
		}
		lines.erase(0, 1);
	}
	myVector.assign(lines.begin(), lines.end());
}

#endif