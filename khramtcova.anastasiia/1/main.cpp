#include <ctime>
#include "Slist.h"
#include "SortPolicy.h"
#include "task2.cpp"
#include "task3.cpp"
#include "task4.cpp"

using namespace std;

template<typename TypeOfContainer>
void PrintContainer(const TypeOfContainer myContainer, unsigned int sizeOfContainer)
{
	if ((sizeOfContainer) == 0)
	{
		cerr << "Error: empty container." << "\n";
	}
	else
	{
		for (unsigned int i = 0; i < sizeOfContainer; ++i)
		{
			cout << myContainer[i] << " ";
		}
		cout << endl; 
	}
}

int main()
{
	srand(static_cast<unsigned int>(time(NULL)));
	unsigned int sizeOfVector1;
	cout << "Set size of Vector1 ";
	cin >> sizeOfVector1;
    if (sizeOfVector1 <= 0)
	{
		cerr << "Error size of vector cannot be less than 1" << endl;
	}
	else
	{
	    vector<int> intVector1(sizeOfVector1);
		for (unsigned int i = 0; i < intVector1.size(); ++i)
		{
			intVector1[i] = rand();
		}
		cout << "Vector 1 before" << "\n";
		PrintContainer(intVector1, sizeOfVector1);
		Sort<SortPolicyWithIndex>(intVector1);
		cout << "Vector 1 after" << "\n";
		PrintContainer(intVector1, sizeOfVector1);
	}

	unsigned int sizeOfVector2;
	cout << "Set size of Vector2 ";
	cin >> sizeOfVector2;
	if (sizeOfVector2 <= 0)
	{
		cerr << "Error size of vector cannot be less than 1" << endl;
	}
	else
	{
		vector<int> intVector2(sizeOfVector2);
		for (unsigned int i = 0; i < intVector2.size(); ++i)
		{
			intVector2[i] = rand();
		}
		cout << "Vector 2 before" << "\n";
		PrintContainer(intVector2, sizeOfVector2);
		Sort<SortPolicyWithAt>(intVector2);
		cout << "Vector 2 after" << "\n";
		PrintContainer(intVector2, sizeOfVector2);
	}

	slist<int> intList;
	unsigned int sizeOfList;
	cout << "Set size of list ";
	cin >> sizeOfList;
	if (sizeOfList <= 0)
	{
		cerr << "Error size of list cannot be less than 1" << endl;
	}
	else
	{
		for (unsigned int i = 0; i < sizeOfList; i++)
		{
			int number = rand();
			intList.push_back(number);
		}
		cout << "List before" << "\n";
		intList.Print();
		Sort<SortPolicyWithIterator>(intList);
		cout << "List after" << "\n";
		intList.Print();
	}
	vector<char> charVector;
	ReadFromFileToVector(charVector);
	cout << "Copied array to vector" << endl;
	PrintContainer(charVector, charVector.size());

	vector<int> intVector3;
	ReadFromStreamToVector(intVector3);
	if (intVector3.empty())
	{
		cerr << "Zero vector" << endl;
	}
	else
	{
		cout << "Vector 3 before" << endl;
		PrintContainer(intVector3, intVector3.size());
		CuriousVectorChange(intVector3);
		cout << "Vector 3 after" << endl;
		PrintContainer(intVector3, intVector3.size());
	}
	vector<double> doubleVector;
	
	doubleVector.resize(5);
	FillRandom(&doubleVector[0], doubleVector.size());
	PrintContainer(doubleVector, 5); 
	Sort<SortPolicyWithAt>(doubleVector);
	PrintContainer(doubleVector, 5);

	doubleVector.resize(10);
	FillRandom(&doubleVector[0], doubleVector.size());
	PrintContainer(doubleVector, doubleVector.size());
	Sort<SortPolicyWithAt>(doubleVector);
	PrintContainer(doubleVector, doubleVector.size());

	doubleVector.resize(25);
	FillRandom(&doubleVector[0], doubleVector.size());
	PrintContainer(doubleVector, doubleVector.size());
	Sort<SortPolicyWithAt>(doubleVector);
	PrintContainer(doubleVector, doubleVector.size());

	doubleVector.resize(50);
	FillRandom(&doubleVector[0], doubleVector.size());
	PrintContainer(doubleVector, doubleVector.size());
	Sort<SortPolicyWithAt>(doubleVector);
	PrintContainer(doubleVector, doubleVector.size());

	doubleVector.resize(100);
	FillRandom(&doubleVector[0], doubleVector.size());
	PrintContainer(doubleVector, doubleVector.size());
	Sort<SortPolicyWithAt>(doubleVector);
	PrintContainer(doubleVector, doubleVector.size());

	return 0;
}
