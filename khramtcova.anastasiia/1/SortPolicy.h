#ifndef SORTPOLICY_H
#define SORTPOLICY_H

#include "Slist.h"

template <typename T>
struct SortPolicyWithIndex
{
	typedef typename T::size_type position_type;
	typedef typename T::value_type value_type;

	static position_type begin(T &container)
	{
		return 0;
	}

	static position_type end(T &container)
	{
		return container.size();
	}

	static value_type& GetItem(T &container, position_type position)
	{
		return container[position];
	}

};

template <typename T>
struct SortPolicyWithAt
{
	typedef typename T::size_type position_type;
	typedef typename T::value_type value_type;

	static position_type begin(T &container)
	{
		return 0;
	}

	static position_type end(T &container)
	{
		return container.size();
	}

	static value_type& GetItem(T &container, position_type position)
	{
		return container.at(position);
	}

};

template <typename T>
struct SortPolicyWithIterator
{
	typedef typename T::iterator position_type;
	typedef typename T::value_type value_type;

	static position_type begin(T &container)
	{
		return container.begin();
	}

	static position_type end(T &container)
	{
		return container.end();
	}

	static value_type& GetItem(T &container, position_type position)
	{
		return *position;
	}

};

template <template <typename> class Policy, typename T>
void Sort(T &container)
{
	typedef Policy<T> NowPolicy;
	typedef typename NowPolicy::position_type position_type;

	for (position_type i = NowPolicy::begin(container); i != NowPolicy::end(container); ++i)
	{
		position_type numOfmin = i;
		for (position_type j = i; j != NowPolicy::end(container); ++j)
		{
			if (NowPolicy::GetItem(container, j) < NowPolicy::GetItem(container, numOfmin))
			{
				numOfmin = j;
			}
		}
		if (numOfmin != i)
		{
			swap(NowPolicy::GetItem(container, i), NowPolicy::GetItem(container, numOfmin));
		}
	}
}

#endif