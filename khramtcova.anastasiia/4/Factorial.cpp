unsigned int Factorial(size_t n)
{
	return n != 0 ? n*Factorial(n - 1) : 1;
}