#include"TelephoneBook.h"
TelephoneBook::User::User(const std::string & name_, const std::string & number_) :
name(name_),
number(number_)
{
}

std::ostream& operator << (std::ostream &stream, const TelephoneBook::User & user)
{
	return stream << user.name << " " << user.number;
}
void TelephoneBook::WatchCurrentUser(const bookIterator currentUser)
{
	std::cout << (*currentUser) << std::endl;;
}
TelephoneBook::bookIterator TelephoneBook::SwitchUser(bookIterator currentUser, const int howMuchPositionsToGo)
{
	int i = 0;
	if (howMuchPositionsToGo >= 0)
	{
		while ((currentUser != book.end()) && (i < howMuchPositionsToGo))
		{
			++currentUser;
			++i;
		}
	}
	else if (howMuchPositionsToGo < 0)
	{
		while ((currentUser != book.begin()) && (i > howMuchPositionsToGo))
		{
			--currentUser;
			--i;
		}
	}
	if (i == howMuchPositionsToGo)
	{
		return currentUser;
	}
	else
	{
		std::cerr << "No such element.\n";
		return book.begin();
	}
}
void TelephoneBook::PushBackUser(const std::string & name_,
	const std::string & number_)
{
	book.push_back(TelephoneBook::User(name_, number_));
}
void TelephoneBook::AddUserBefore(bookIterator currentUser, const std::string & name_,
	const std::string & number_)
{
	if (currentUser != book.begin())
	{
		book.insert(--currentUser, TelephoneBook::User(name_, number_));
	}
	else if (currentUser == book.begin())
	{
		book.push_front(TelephoneBook::User(name_, number_));
	}
}
void TelephoneBook::AddUserAfter(bookIterator currentUser, const std::string & name_,
	const std::string & number_)
{
	if (currentUser != book.end())
	{
		book.insert(++currentUser, TelephoneBook::User(name_, number_));
	}
	else if (currentUser == book.end())
	{
		book.push_back(TelephoneBook::User(name_, number_));
	}
}
void TelephoneBook::ModifyUser(const bookIterator currentUser)
{
	std::cout << "Set new name and number\n";
	std::cin >> (*currentUser).name;
	std::cin >> (*currentUser).number;
}
void TelephoneBook::Print()
{
	for (bookIterator it = book.begin(); it != book.end(); ++it)
	{
		std::cout << *it << std::endl;
	}
}

TelephoneBook::bookIterator TelephoneBook::Begin()
{
	return book.begin();
}

TelephoneBook::bookIterator TelephoneBook::End()
{
	return book.end();
}


