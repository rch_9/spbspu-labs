#include "FactorialContainer.h"


FactorialContainer::FactorialContainerIterator::FactorialContainerIterator():
	current(0),
	factorial(0)
{
}

FactorialContainer::FactorialContainerIterator::FactorialContainerIterator(size_t n, size_t max_): 
	max(max_)
{
	if (n <= max)
	{
		current = n;
	}
	else
	{
		current = max;
	}
	factorial = Factorial(current);
}
FactorialContainer::FactorialContainerIterator::FactorialContainerIterator(size_t n)
{
	if (n <= max)
	{
		current = n;
	}
	factorial = Factorial(current);
}
bool FactorialContainer::FactorialContainerIterator::operator == (const FactorialContainerIterator & rhs) const
{
	return current == rhs.current;
}


bool FactorialContainer::FactorialContainerIterator::operator != (const FactorialContainerIterator & rhs) const
{
	return current != rhs.current;
}

int FactorialContainer::FactorialContainerIterator::operator *() const
{
		return factorial;
}

FactorialContainer::FactorialContainerIterator FactorialContainer::FactorialContainerIterator::operator ++()
{
	if (current <= max)
	{
		current = current + 1;
		factorial = factorial * current;
	}
	return *this;
}

FactorialContainer::FactorialContainerIterator FactorialContainer::FactorialContainerIterator::operator ++(int)
{
	iterator temp(*this);
	this->operator++();
	return temp;
}

FactorialContainer::FactorialContainerIterator FactorialContainer::FactorialContainerIterator::operator --()
{
	if (current >= 1)
	{
		factorial = factorial / current;
		current = current - 1;
	}
	return *this;
}

FactorialContainer::FactorialContainerIterator FactorialContainer::FactorialContainerIterator::operator --(int)
{
	iterator temp(*this);
	this->operator--();
	return temp;
}

FactorialContainer::FactorialContainer() :
	Size(11)
{
}

FactorialContainer::FactorialContainer(size_t size_) :
Size(size_)
{
}

FactorialContainer::iterator FactorialContainer::begin()
{
	return iterator(0, size());
}

FactorialContainer::iterator FactorialContainer::end()
{
	return iterator(size(), size());
}
size_t const FactorialContainer::size()
{
	return Size;
}