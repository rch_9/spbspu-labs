#include<algorithm>
#include<vector>
#include "TelephoneBook.h"
#include "FactorialContainer.h"

using namespace std;

int main()
{
	TelephoneBook myBook;
	myBook.PushBackUser("Weasley", "237689");
	myBook.PushBackUser("Black", "148900");
	myBook.PushBackUser("Evans", "783219");
	myBook.PushBackUser("Moody", "438922");
	myBook.PushBackUser("Dumbldore", "118943");
	myBook.Print();

	TelephoneBook::bookIterator myIterator = myBook.Begin();
	myBook.WatchCurrentUser(myIterator);
	myIterator = myBook.SwitchUser(myIterator, 2);
	myBook.WatchCurrentUser(myIterator);
	myIterator = myBook.SwitchUser(myIterator, -1);
	myBook.WatchCurrentUser(myIterator);

	myIterator = myBook.SwitchUser(myIterator, 3);
	myBook.AddUserAfter(myIterator, "Snape", "347866");
	myIterator = myBook.SwitchUser(myIterator, 1);
	myBook.AddUserBefore(myIterator, "Diggory", "236590");
	myIterator = myBook.SwitchUser(myIterator, -1);
	myIterator = myBook.Begin();
	myBook.AddUserBefore(myIterator, "Lupin", "563280");
	myIterator = myBook.End();
	myBook.AddUserAfter(myIterator, "Dobby", "345672");
	
	myIterator = myBook.SwitchUser(myIterator, -4);
	myBook.ModifyUser(myIterator);

	cout << "\n";
	myBook.Print();

	FactorialContainer myFactorial;
	for (FactorialContainer::iterator it = myFactorial.begin(); it != myFactorial.end(); ++it)
	{
		cout << *it << endl;
	}

	vector<int> myVector;
	myVector.resize(myFactorial.size());
	copy(myFactorial.begin(), myFactorial.end(), myVector.begin());
	for (vector<int>::iterator it = myVector.begin(); it != myVector.end(); ++it)
	{
		cout << *it << endl;
	}
	
	return 0;
}