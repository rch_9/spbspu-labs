#ifndef FACTORIALCONTAINER_H
#define FACTORIALCONTAINER_H

#include<iterator>

unsigned int Factorial(size_t n);

class FactorialContainer
{
public:
	class FactorialContainerIterator : public std::iterator<std::bidirectional_iterator_tag, int>
	{
	public:
		typedef  FactorialContainerIterator iterator;
		FactorialContainerIterator();
		FactorialContainerIterator(size_t n, size_t max_);
		bool operator == (const iterator & rhs) const;
		bool operator != (const iterator & rhs) const;
		int operator *() const;
		iterator operator ++();
		iterator operator ++(int);
		iterator operator --();
		iterator operator --(int);
	private:
		int current;
		int factorial;
		int max;
		FactorialContainerIterator(size_t n);
	};
	typedef FactorialContainerIterator iterator;
	FactorialContainer();
	FactorialContainer(size_t size_);
	iterator begin();
	iterator end();
	size_t const size();
private:
	size_t Size;
};

#endif