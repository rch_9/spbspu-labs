#ifndef TELEPHONEBOOK_H
#define TELEPHONEBOOK_H

#include<iostream>
#include<list>
#include<string>
#include<iostream>
#include<ostream>
class TelephoneBook
{
public:
	struct User
	{
		std::string name;
		std::string number;
		User(const std::string & name_, const std::string & number_);
		friend std::ostream & operator << (std::ostream &stream, const User & user);
	};
	typedef std::list<User> Container;
	typedef Container::iterator bookIterator;
	void WatchCurrentUser(const bookIterator currentUser);
	bookIterator SwitchUser(bookIterator currentUser, const int howMuchPositionsToGo);
	void PushBackUser(const std::string & name_,
		const std::string & number_);
	void AddUserBefore(bookIterator currentUser, const std::string & name_,
		const std::string & number_);
	void AddUserAfter(bookIterator currentUser, const std::string & name_,
		const std::string & number_);
	void ModifyUser(const bookIterator currentUser);
	void Print();
	bookIterator Begin();
	bookIterator End();
private:
	Container book;
};

#endif