#include"Figures.h"
#include"Functors.h"
#include<list>
#include<vector>
#include<algorithm>
#include<iostream>
#include<iterator>
#include<cmath>
#include<ctime>
#include<cstdlib>

struct Generate
{
	double operator () ()
	{
		double number = rand() % 10000 / 1000.0;
		return number;
	}
};

int main()
{
	srand(time(NULL));
	std::list<double> dList(10);
	std::generate(dList.begin(), dList.end(), Generate());
	std::copy(dList.begin(), dList.end(), std::ostream_iterator< double >(std::cout, " "));
	std::cout << "\n";
	std::transform(dList.begin(), dList.end(), dList.begin(), std::bind2nd(std::multiplies<double>(), acos(-1.0)));
	std::copy(dList.begin(), dList.end(), std::ostream_iterator< double >(std::cout, " "));
	std::cout << "\n";

	std::vector< boost::shared_ptr< Shape > > shapeVector;
	boost::shared_ptr< Circle >   ptr1(new Circle  (2, 3));
	boost::shared_ptr< Triangle > ptr2(new Triangle(2, 3));
	boost::shared_ptr< Square >   ptr3(new Square  (4, 1));
	boost::shared_ptr< Circle >   ptr4(new Circle  (1, 4));
	boost::shared_ptr< Triangle > ptr5(new Triangle(0, 0));
	boost::shared_ptr< Square >   ptr6(new Square  (0, 2));
	shapeVector.push_back(ptr1);
	shapeVector.push_back(ptr2);
	shapeVector.push_back(ptr3);
	shapeVector.push_back(ptr4);
	shapeVector.push_back(ptr5);
	shapeVector.push_back(ptr6);

	std::cout << "\nFrom left to right\n";
	std::sort(shapeVector.begin(), shapeVector.end(), ComparatorLeft());
	std::for_each(shapeVector.begin(), shapeVector.end(), DrawFigure());
	std::cout << "\nFrom right to left\n";
	std::sort(shapeVector.begin(), shapeVector.end(), ComparatorRight());
	std::for_each(shapeVector.begin(), shapeVector.end(), DrawFigure());
	std::cout << "\nFrom up to down\n";
	std::sort(shapeVector.begin(), shapeVector.end(), ComparatorUpper());
	std::for_each(shapeVector.begin(), shapeVector.end(), DrawFigure());
	std::cout << "\nFrom down to up\n";
	std::sort(shapeVector.begin(), shapeVector.end(), ComparatorDown());
	std::for_each(shapeVector.begin(), shapeVector.end(), DrawFigure());



	return 0;
}