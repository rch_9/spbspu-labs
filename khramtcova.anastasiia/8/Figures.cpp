#include"Figures.h"

Shape::Shape() :
	x(0),
	y(0)
{
}

Shape::Shape(int x_, int y_) :
	x(x_),
	y(y_)
{
}

bool Shape::IsMoreLeft(const Shape rhs) const
{
	return x < rhs.x;
}

bool Shape::IsUpper(const Shape rhs) const
{
	return y > rhs.y;
}

void Shape::Draw() const
{
}

Circle::Circle() : Shape()
{
}

Circle::Circle(int x_, int y_) : Shape(x_, y_)
{
}

void Circle::Draw() const
{
	std::cout << "Circle ( " << x << ", " << y << " )\n";
}

Triangle::Triangle() : Shape()
{
}

Triangle::Triangle(int x_, int y_) : Shape(x_, y_)
{
}

void Triangle::Draw() const
{
	std::cout << "Triangle ( " << x << ", " << y << " )\n";
}

Square::Square() : Shape()
{
}

Square::Square(int x_, int y_) : Shape(x_, y_)
{
}

void Square::Draw() const
{
	std::cout << "Square ( " << x << ", " << y << " )\n";
}

