#ifndef FIGURES_H
#define FIGURES_H

#include<iostream>
#include<boost/shared_ptr.hpp>

class Shape
{
public:
	Shape();
	Shape(int x_, int y_);
	bool IsMoreLeft(const Shape rhs) const;
	bool IsUpper(const Shape rhs) const;
	virtual void Draw() const;
protected:
	int x, y;
};

class Circle : public Shape
{
public:
	Circle();
	Circle(int x_, int y_);
	void Draw() const;
};

class Triangle : public Shape
{
public:
	Triangle();
	Triangle(int x_, int y_);
	void Draw() const;
};

class Square : public Shape
{
public:
	Square();
	Square(int x_, int y_);
	void Draw() const;
};
 
#endif