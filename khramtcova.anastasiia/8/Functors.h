#ifndef FUNCTORS_H
#define FUNCTORS_H

#include "Figures.h"

struct DrawFigure
{
	void operator () (const boost::shared_ptr< Shape > & shape)
	{
		shape->Draw();
	}
};

struct ComparatorLeft
{
	bool operator () (const boost::shared_ptr< Shape > & lhs, const boost::shared_ptr< Shape > & rhs)
	{
		return lhs->IsMoreLeft(*rhs);
	}
};
struct ComparatorRight
{
	bool operator () (const boost::shared_ptr< Shape > & lhs, const boost::shared_ptr< Shape > & rhs)
	{
		return rhs->IsMoreLeft(*lhs);
	}
};
struct ComparatorUpper
{
	bool operator () (const boost::shared_ptr< Shape > & lhs, const boost::shared_ptr< Shape > & rhs)
	{
		return rhs->IsUpper(*lhs);
	}
}; 
struct ComparatorDown
{
	bool operator () (const boost::shared_ptr< Shape > & lhs, const boost::shared_ptr< Shape > & rhs)
	{
		return lhs->IsUpper(*rhs);
	}
};
#endif