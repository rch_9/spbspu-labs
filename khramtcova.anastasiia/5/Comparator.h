#ifndef COMPARATOR_H
#define COMPARATOR_H

#include "DataStruct.h"

struct Comparator 
{
	bool operator () (const DataStruct &lhs, const DataStruct &rhs) const
	{
		return ((lhs.key1 < rhs.key1)
			|| (lhs.key1 == rhs.key1) && (lhs.key2 < rhs.key2)
			|| (lhs.key1 == rhs.key1) && (lhs.key2 == rhs.key2) && (lhs.str.length() < rhs.str.length()));
	}
};

#endif
