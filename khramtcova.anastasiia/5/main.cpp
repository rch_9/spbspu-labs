#include"DataStruct.h"
#include"Comparator.h"
#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;

int main()
{
	srand(static_cast<unsigned int>(time(NULL)));
	vector<DataStruct> myVector(8);
	generate(myVector.begin(), myVector.end(), Generate);
	cout << "Vector before\n";
	copy(myVector.begin(), myVector.end(), ostream_iterator<DataStruct>(cout));

	sort(myVector.begin(), myVector.end(), Comparator());
	cout << "Vector after\n";
	copy(myVector.begin(), myVector.end(), ostream_iterator<DataStruct>(cout));

	return 0;
}