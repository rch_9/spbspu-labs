#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#include<string>
#include<ctime>
static std::string strMatrix[] =
{
	"Leia",
	"Luke",
	"Han",
	"Obi-Van",
	"Yoda",
	"R2-D2",
	"C-3PO",
	"Enakin",
	"Chewbacca",
	"Lando"
};

struct DataStruct
{
	int key1;
	int key2;
	std::string str;
	friend std::ostream & operator << (std::ostream & out, const DataStruct & data)
	{
		return out << data.key1 << " " << data.key2 << " " << data.str << std::endl;
	}
};
static DataStruct Generate()
{
	DataStruct ds;
	ds.str = strMatrix[rand() % 10];
	ds.key1 = rand() % 10 - 5;
	ds.key2 = rand() % 10 - 5;
	return ds;
}
#endif