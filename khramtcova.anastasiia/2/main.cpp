#include <vector>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

const string nonPrintedSymbols = "\n\r\t\v ";
const string punctuation = ".,!?:;";
const string letter = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";

void ReadFromFile(string &lines)
{
	ifstream f("my_file.txt");
	if (!f)
	{
		cerr << "Error: file is not accessible." << endl;
	}
	else
	{
		while (!f.eof())
		{
			string aLine;
			getline(f, aLine, ' ');
			if (aLine.size() >= 20)
			{
				cerr << "Error: too long word." << endl;
			}
			else
			{
				lines = lines + ' ' + aLine;
			}
		}
	}
	lines.erase(0, 1);
}

void DeleteNotSpaces(string &lines)
{
	for (size_t i = 0; i < lines.size(); ++i)
	{
		if (nonPrintedSymbols.find(lines[i]) < nonPrintedSymbols.size())
		{
			lines.replace(i, 1, " ");
		}
	}
}

void DeletePluralSpaces(string &lines)
{
	unsigned int numOfSpaces = 0;
	for (size_t i = 0; i < lines.size(); ++i)
	{
		if (lines[i] == ' ')
		{
			++numOfSpaces;
		}
		else if (numOfSpaces > 1)
		{
			lines.erase(i - numOfSpaces + 2, numOfSpaces - 2);
			i = i - numOfSpaces;
			numOfSpaces = 0;
		}
	}
}

void DeleteSpacesBeforePunctuation(string &lines)
{
	for (size_t i = 0; i < lines.size(); ++i)
	{
		if ((punctuation.find(lines[i]) < punctuation.size()) &&
			(lines[i - 1] == ' '))
		{
			lines.erase(i - 1, 1);
			--i;
		}
	}
}

void InsertSpacesAfterPunctuation(string &lines)
{
	for (size_t i = 0; i < lines.size(); ++i)
	{
		if (punctuation.find(lines[i]) < punctuation.size())
		{
			lines.insert(i + 1, " ");
			++i;
		}
	}
}

void MakeVau(string &lines)
{
	for (size_t i = 0; i < lines.size(); ++i)
	{
		unsigned int numOfLetters = 0;
		while (letter.find(lines[i]) < letter.size())
		{
			++numOfLetters;
			++i;
		}
		if (numOfLetters > 10)
		{
			lines.replace(i - numOfLetters, numOfLetters, "Vau!!!");
			i = i - numOfLetters - 5;
		}
	}
}
void MakeVector(string &lines, vector<string> &vectorOfLines)
{
	string wordInLine;
	string lineInVector;

	for (size_t i = 0; i < lines.size(); ++i)
	{
		while ((lines[i] != ' ') && (i < lines.size()))
		{
			wordInLine += lines[i];
			++i;
		}
		wordInLine = wordInLine + ' ';
		if ((lineInVector.size() + wordInLine.size()) <= 40)
		{
			lineInVector = lineInVector + wordInLine;
			wordInLine.clear();
		}
		else 
		{
			vectorOfLines.push_back(lineInVector);
			lineInVector = wordInLine;
			wordInLine.clear();
		}
		if ((i + 1) == lines.size())
		{
			vectorOfLines.push_back(lineInVector);
		}
		
	}
}

void PrintVector(vector<string> vectorOfLines)
{
	if (!vectorOfLines.empty())
	{
		for (size_t i = 0; i < vectorOfLines.size(); ++i)
		{
			cout << vectorOfLines[i] << endl;
		}
		cout << "\n";
	}
	else
	{
		cerr << "Error: Empty input vector." << endl;

	}
}

int main()
{
	string myLine;
	ReadFromFile(myLine);
	if (!myLine.empty())
	{
		DeleteNotSpaces(myLine);
		InsertSpacesAfterPunctuation(myLine);
		DeletePluralSpaces(myLine);
		DeleteSpacesBeforePunctuation(myLine);
		MakeVau(myLine);
		vector<string> myVector;
		MakeVector(myLine, myVector);
		PrintVector(myVector);
	}
	else
	{
		cerr << "Error: Empty input file." << endl;
	}
	return 0;
}