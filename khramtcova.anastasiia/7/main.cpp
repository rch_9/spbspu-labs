#include<vector>
#include<string>
#include<algorithm>

#include"FunctorStatistic.h"

int main()
{
	srand(time(NULL));
	std::vector<int> myVector(100);
	std::generate(myVector.begin(), myVector.end(), Generate);
	std::copy(myVector.begin(), myVector.end(), std::ostream_iterator< int >(std::cout, " "));
	std::cout << "\n";

	FunctorStatistic functor = std::for_each(myVector.begin(), myVector.end(), FunctorStatistic());
	functor.Print();

	return 0;
}
