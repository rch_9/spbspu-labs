#ifndef FUNCTORSSTATISTIC_H
#define FUNCTORSSTATISTIC_H

#include<ctime>
#include<iostream>

struct FunctorStatistic
{
	FunctorStatistic():
		max(INT_MIN),
		min(INT_MAX),
		averange(0.0),
		amount_positive(0),
		amount_negative(0),
		sum_odd(0),
		sum_even(0),
		first(0),
		count(0),
		last_equal_first(false)
	{}
	void operator() (const int & number)
	{
		if (number > max)
		{
			max = number;
		}
		if (number < min)
		{
			min = number;
		}
		averange = ((count * averange) + number) / (count + 1);
		if (number > 0)
		{
			++amount_positive;
		}
		if (number < 0)
		{
			++amount_negative;
		}
		if (number % 2 != 0)
		{
			sum_odd += number;
		}
		if (number % 2 == 0)
		{
			sum_even += number;
		}
		if (count == 0)
		{
			first = number;
		}
		++count;
		last_equal_first = (number == first);
	}
	void Print()
	{
		std::cout << "Max: " << max << "\n";
		std::cout << "Min: " << min << "\n";
		std::cout << "Averange: " << averange << "\n";
		std::cout << "Amount_positive: " << amount_positive << "\n";
		std::cout << "Amount_negative: " << amount_negative << "\n";
		std::cout << "Sum_odd: " << sum_odd << "\n";
		std::cout << "Sum_even: " << sum_even << "\n";
		std::cout << "Last_equal_first: " << last_equal_first << "\n";
	}
private:
	int max;
	int min;
	double averange;
	int amount_positive;
	int amount_negative;
	int sum_odd;
	int sum_even;
	int first;
	int count;
	bool last_equal_first;
};

static int Generate()
{
	int number = -500 + rand() % 1000;
	return number;
}

#endif