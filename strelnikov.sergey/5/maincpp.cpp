#include <algorithm>
#include <vector>
#include <iterator>
#include <iostream>
#include <ctime>
#include <string>
#include <iomanip> //setw

using namespace std;

typedef struct
{
	int       key1;
	int       key2;
	string	  str;
} DataStruct;

string table[10] = { "My", "name", "is", "Sergey", "I", "was", "born", "in", "Vyborg", "1996_year" };

void randomize(vector<DataStruct> &data)
{
	DataStruct rand_table;
	srand(static_cast<unsigned int>(time(0)));
	for (unsigned int i = 0; i < 10; i++)
	{
		unsigned int pos = rand() % 10;
		rand_table.key1 = rand() % 10 - 5;
		rand_table.key2 = rand() % 10 - 5;
		rand_table.str = table[pos];
		data.push_back(rand_table);
	}
}

bool comp(DataStruct data1, DataStruct data2)
{
	if (data1.key1 == data2.key1)
	{
		if (data1.key2 == data2.key2)
		{
			return (data1.str.size() < data2.str.size());
		}
		else
		{
			return (data1.key2 < data2.key2);
		}
	}
	else
	{
		return (data1.key1 < data2.key1);
	}
}

void print_vector(vector<DataStruct> data)
{
	for (unsigned int i = 0; i < data.size(); i++)
	{
		cout << setw(4) << data.at(i).key1 << setw(5) << data.at(i).key2 << "   " << data.at(i).str << endl;
	}
}
int main()
{
	vector<DataStruct>  data;
	cout << "Unsorted vector:" << endl;
	randomize(data);
	cout << "key1: key2: str: " << endl;
	print_vector(data);
	cout << "\nSorted vector:" << endl;
	sort(data.begin(), data.end(), comp);
	cout << "key1: key2: str: " << endl;
	print_vector(data);
	return 0;
}