#ifndef	QUEUE
#define QUEUE

#include <list>

template < typename T >
class QueueWithPriority
{
public:

	enum Priority
	{
		LOW,
		NORMAL,
		HIGH
	};

	void Put(const T element, Priority priority);

	void Accelerate();

	template < typename Operation >
	void Extract(Operation operation);

	template < typename Operation >
	void GetAll(Operation operation);

private:

	std::list< T > LowPriorityQueue;
	std::list< T > NormalPriorityQueue;
	std::list< T > HighPriorityQueue;
};


#include "queue_methods.h"

#endif







