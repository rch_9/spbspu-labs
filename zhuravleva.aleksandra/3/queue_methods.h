#ifndef	QUEUE_METHODS
#define QUEUE_METHODS

#include <iostream>

template < typename T >
inline void QueueWithPriority< T >::Put(const T element, Priority priority)
{
	switch (priority)
	{
	case QueueWithPriority< T >::LOW :
	{
		LowPriorityQueue.push_back(element);
		break;
	}
	case QueueWithPriority< T >::NORMAL :
	{
		NormalPriorityQueue.push_back(element);
		break;
	}
	case QueueWithPriority< T >::HIGH :
	{
		HighPriorityQueue.push_back(element);
		break;
	}
	}
}

template < typename T >
inline void QueueWithPriority< T >::Accelerate()
{
	if (!LowPriorityQueue.empty())
	{
		HighPriorityQueue.splice(HighPriorityQueue.end(), LowPriorityQueue);
	}
}

template < typename T > template < typename Operation >
inline void QueueWithPriority< T >::Extract(Operation operation)
{
	if (!HighPriorityQueue.empty())
	{
		operation(HighPriorityQueue.front());
		HighPriorityQueue.pop_front();
	}
	else if (!NormalPriorityQueue.empty())
	{
		operation(NormalPriorityQueue.front());
		NormalPriorityQueue.pop_front();
	}
	else if (!LowPriorityQueue.empty())
	{
		operation(LowPriorityQueue.front());
		LowPriorityQueue.pop_front();
	}
}

template < typename T > template < typename Operation >
inline void QueueWithPriority< T >::GetAll(Operation operation)
{
	typedef typename std::list< T >::iterator iterator;

	std::cout << "In the queue:" << std::endl << "  High priority: ";
	if (!HighPriorityQueue.empty())
	{
		for (iterator iter = HighPriorityQueue.begin(); iter != HighPriorityQueue.end(); ++iter)
		{
			operation(*iter);
		}
	}

	std::cout << std::endl << "  Normal priority: ";
	if (!NormalPriorityQueue.empty())
	{
		for (iterator iter = NormalPriorityQueue.begin(); iter != NormalPriorityQueue.end(); ++iter)
		{
			operation(*iter);
		}
	}

	std::cout << std::endl << "  Low priority: ";
	if (!LowPriorityQueue.empty())
	{
		for (iterator iter = LowPriorityQueue.begin(); iter != LowPriorityQueue.end(); ++iter)
		{
			operation(*iter);
		}
	}

	std::cout << std::endl;
}


#endif
