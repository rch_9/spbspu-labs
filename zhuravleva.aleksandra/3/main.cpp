#include "queue.h"
#include "randon_list.h"
#include <string>
#include <iostream>

/******************************************/
template < typename T >
struct GetElement
{
	void operator()(const T & obj) const
	{
		std::cout << "|" << obj.Get() << "|" << " ";
	}
};
/******************************************/
template < typename T >
class QueueElement
{
public:
	QueueElement(T element);
	T Get() const;
private:
	T element_;
};

template < typename T >
QueueElement< T >::QueueElement(T element) :
element_(element)
{}

template < typename T >
T QueueElement< T >::Get() const
{
	return element_;
}

/*****************************************************/
int main(int, char**)
{
	std::cout << "Task #1" << std::endl;

	QueueWithPriority< QueueElement< int > > FirstQueue;

	QueueElement< int > A(21);
	QueueElement< int > B(43);
	QueueElement< int > C(24);
	QueueElement< int > D(56);

	FirstQueue.Put(A, QueueWithPriority< QueueElement< int > >::HIGH);
	FirstQueue.Put(B, QueueWithPriority< QueueElement< int > >::LOW);
	FirstQueue.Put(C, QueueWithPriority< QueueElement< int > >::LOW);
	FirstQueue.Put(D, QueueWithPriority< QueueElement< int > >::NORMAL);

	FirstQueue.GetAll(GetElement< QueueElement< int > >());
	std::cout << std::endl;

	FirstQueue.Accelerate();

	FirstQueue.GetAll(GetElement< QueueElement< int > >());
	std::cout << std::endl;

	std::cout << "Extracted elements:" << std::endl;
	FirstQueue.Extract(GetElement< QueueElement< int > >());
	FirstQueue.Extract(GetElement< QueueElement< int > >());
	FirstQueue.Extract(GetElement< QueueElement< int > >());
	FirstQueue.Extract(GetElement< QueueElement< int > >());
	FirstQueue.Extract(GetElement< QueueElement< int > >());
	std::cout << std::endl;

	/************************************************/
	std::cout << "-------------------------------------" << std::endl;
	QueueWithPriority< QueueElement< std::string > > SecondQueue;
	SecondQueue.Extract(GetElement< QueueElement< std::string > >());

	QueueElement< std::string > first("low");
	QueueElement< std::string > second("normal1");
	QueueElement< std::string > third("high");
	QueueElement< std::string > fourth("normal2");

	SecondQueue.Put(first, QueueWithPriority< QueueElement< std::string > >::LOW);
	SecondQueue.Put(second, QueueWithPriority< QueueElement< std::string > >::NORMAL);
	SecondQueue.Put(third, QueueWithPriority< QueueElement< std::string > >::HIGH);
	SecondQueue.Put(fourth, QueueWithPriority< QueueElement< std::string > >::NORMAL);

	SecondQueue.GetAll(GetElement< QueueElement< std::string > >());
	std::cout << std::endl;

	SecondQueue.Accelerate();

	SecondQueue.GetAll(GetElement< QueueElement< std::string > >());
	std::cout << std::endl;

	std::cout << "Extracted elements:" << std::endl;
	SecondQueue.Extract(GetElement< QueueElement< std::string > >());
	SecondQueue.Extract(GetElement< QueueElement< std::string > >());
	SecondQueue.Extract(GetElement< QueueElement< std::string > >());
	SecondQueue.Extract(GetElement< QueueElement< std::string > >());
	std::cout << std::endl;

	/************************************************/
	std::cout << "-------------------------------------" << std::endl;
	std::cout << std::endl << "Task #2" << std::endl;

	std::list< int > list;

	for (unsigned int i = 0; i <= 5; ++i)
	{
		fillRandom(list, i);
		correctPrint(list);
		printInTheOtherOrder(list);
		std::cout << std::endl;
	}

	fillRandom(list, 7);
	correctPrint(list);
	printInTheOtherOrder(list);
	std::cout << std::endl;

	fillRandom(list, 14);
	correctPrint(list);
	printInTheOtherOrder(list);
	std::cout << std::endl;
}


