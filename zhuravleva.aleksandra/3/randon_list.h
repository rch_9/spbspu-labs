#ifndef RANDOM_LIST_H
#define RANDOM_LIST_H

#include <list>


void fillRandom(std::list< int > & list, const unsigned int size);
void printInTheOtherOrder(std::list< int > & list);
void correctPrint(std::list< int > & list);

#endif
