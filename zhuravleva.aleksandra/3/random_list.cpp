#ifndef RANDOM_LIST
#define RANDOM_LIST

#define MAX_RAND_VALUE 20
#define MAX_SIZE 20

#include <list>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <stdexcept>


void fillRandom(std::list< int > & list, const unsigned int size)
{
	if (size > MAX_SIZE)
	{
		throw(std::invalid_argument("Invalid argument!"));
	}
	if (!list.empty())
	{
		list.clear();
	}
	srand(static_cast<unsigned int>(time(0)));
	for (unsigned int i = 0; i < size; ++i)
	{
		list.push_back(rand() % MAX_RAND_VALUE + 1);
	}
}

static void recursivePrint(std::list< int >::iterator forward_iter, std::list< int >::iterator backward_iter)
{
	if (forward_iter == backward_iter)
	{
		std::cout << *forward_iter << std::endl;
		return;
	}
	else
	{
		std::cout << *forward_iter << " ";
		std::cout << *backward_iter << " ";
		if (++forward_iter == backward_iter)
		{
			return;
		}
		else
		{
			recursivePrint(forward_iter, --backward_iter);
		}
	}
}

void printInTheOtherOrder(std::list< int > & list)
{
	if (list.empty())
	{
		std::cout << "List is empty.";
	}
	else
	{
		std::cout << "Result list: ";
		recursivePrint(list.begin(), --list.end());
	}
	std::cout << std::endl;
}

void correctPrint(std::list< int > & list)
{
	if (!list.empty())
	{
		std::cout << "Correct list: ";
		for (std::list< int >::iterator iter = list.begin(); iter != list.end(); ++iter)
		{
			std::cout << *iter << " ";
		}
	}
	else
	{
		std::cout << "List is empty.";
	}
	std::cout << std::endl;
}


#endif
