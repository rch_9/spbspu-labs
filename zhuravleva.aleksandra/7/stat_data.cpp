#include "stat_data.h"
#include <cmath>
#include <limits>


StatData::StatData() :
	min_(std::numeric_limits<int>::max()),
	max_(std::numeric_limits<int>::min()),
	sumOdd_(0),
	sumEven_(0),
	first_(0),
	allPositive_(0),
	allNegative_(0),
	sizeSequence_(0),
	average_(0),
	first_is_last_(false)
{}

StatData::StatData(int min, int max, int sumOdd, int sumEven, int first,
	unsigned int allPositive, unsigned int allNegative,
	unsigned int sizeSequence, double average, bool first_is_last) :
	min_(min),
	max_(max),
	sumOdd_(sumOdd),
	sumEven_(sumEven),
	first_(first),
	allPositive_(allPositive),
	allNegative_(allNegative),
	sizeSequence_(sizeSequence),
	average_(average),
	first_is_last_(first_is_last)
{}

void StatData::operator()(int current)
{
	++sizeSequence_;
	if (min_ > current)
	{
		min_ = current;
	}
	if (max_ < current)
	{
		max_ = current;
	}
	if (abs(current % 2))
	{
		sumOdd_ += current;
	}
	else
	{
		sumEven_ += current;
	}
	average_ = static_cast<double>(sumOdd_ + sumEven_) / static_cast<double>(sizeSequence_);
	if (current > 0)
	{
		++allPositive_;
	}
	else if (current < 0)
	{
		++allNegative_;
	}
	if (sizeSequence_ == 1)
	{
		first_ = current;
	}
	first_is_last_ = (first_ == current);
}

bool StatData::operator==(const StatData & rhs) const
{
	return ((min_ == rhs.min_) && (max_ == rhs.max_) &&
		(sumOdd_ == rhs.sumOdd_) && (sumEven_ == rhs.sumEven_) &&
		(first_ == rhs.first_) && (allPositive_ == rhs.allPositive_) &&
		(allNegative_ == rhs.allNegative_) && (average_ == rhs.average_) &&
		(first_is_last_ == rhs.first_is_last_));
}

