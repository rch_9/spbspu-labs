#define BOOST_TEST_MODULE Tests

#include "stat_data.h"
#include <boost/test/included/unit_test.hpp>
#include <climits>
#include <vector>
#include <algorithm>


BOOST_AUTO_TEST_CASE(The_usual_sequence)
{
	StatData statistics;
	int arr[] = { 3, 8, 73, -12, 0, 1, 22, 0, 7, 60 };
	std::vector<int> numbers(arr, arr + 10);
	statistics = std::for_each(numbers.begin(), numbers.end(), statistics);
	StatData my_statistics(-12, 73, 84, 78, 3, 7, 1, 10, 16.2, false);
	BOOST_TEST((statistics == my_statistics), "Statistical results do not match");
}

BOOST_AUTO_TEST_CASE(One_positive_odd_number)
{
	StatData statistics;
	int arr[] = { 55 };
	std::vector<int> numbers(arr, arr + 1);
	statistics = std::for_each(numbers.begin(), numbers.end(), statistics);
	StatData my_statistics(55, 55, 55, 0, 55, 1, 0, 1, 55, true);
	BOOST_TEST((statistics == my_statistics), "Statistical results do not match");
}

BOOST_AUTO_TEST_CASE(One_negative_even_number)
{
	StatData statistics;
	int arr[] = { -400 };
	std::vector<int> numbers(arr, arr + 1);
	statistics = std::for_each(numbers.begin(), numbers.end(), statistics);
	StatData my_statistics(-400, -400, 0, -400, -400, 0, 1, 1, -400, true);
	BOOST_TEST((statistics == my_statistics), "Statistical results do not match");
}

BOOST_AUTO_TEST_CASE(No_number)
{
	StatData statistics;
	std::vector<int> numbers;
	statistics = std::for_each(numbers.begin(), numbers.end(), statistics);
	StatData my_statistics(INT_MAX, INT_MIN, 0, 0, 0, 0, 0, 0, 0, false);
	BOOST_TEST((statistics == my_statistics), "Statistical results do not match");
}

BOOST_AUTO_TEST_CASE(All_positive)
{
	StatData statistics;
	int arr[] = { 2, 765, 122, 58, 222, 5, 65 };
	std::vector<int> numbers(arr, arr + 7);
	statistics = std::for_each(numbers.begin(), numbers.end(), statistics);
	StatData my_statistics(2, 765, 835, 404, 2, 7, 0, 7, 177, false);
	BOOST_TEST((statistics == my_statistics), "Statistical results do not match");
}

BOOST_AUTO_TEST_CASE(All_nagative_and_first_is_last)
{
	StatData statistics;
	int arr[] = { -2, -765, -122, -58, -222, -5, -2 };
	std::vector<int> numbers(arr, arr + 7);
	statistics = std::for_each(numbers.begin(), numbers.end(), statistics);
	StatData my_statistics(-765, -2, -770, -406, -2, 0, 7, 7, -168, true);
	BOOST_TEST((statistics == my_statistics), "Statistical results do not match");
}
