#ifndef STATISTICS
#define STATISTICS

class StatData
{
public:
	StatData();
	StatData(int min, int max, int sumOdd, int sumEven, int first,
		unsigned int allPositive, unsigned int allNegative,
		unsigned int sizeSequence, double average, bool first_is_last);
	void operator()(int current);
	bool operator==(const StatData & rhs) const;
private:
	int min_, max_, sumOdd_, sumEven_, first_;
	unsigned int allPositive_, allNegative_, sizeSequence_;
	double average_;
	bool first_is_last_;
};

#endif

