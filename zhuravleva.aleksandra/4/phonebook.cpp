#include "phonebook.h"
#include <string>
#include <stdexcept>
#include <iostream>


Phonebook::Record::Record(std::string name, long long phone) :
	name_(name),
	phone_(phone)
{}

void Phonebook::Record::view() const
{
	std::cout << " Record:" << std::endl;
	std::cout << "  Name: " << name_ << std::endl;
	std::cout << "  Phone Number: " << phone_ << std::endl << std::endl;
}

std::string Phonebook::Record::getName()
{
	return name_;
}

long long Phonebook::Record::getPhone()
{
	return phone_;
}

//////////////////////////////////////////////////////////

Phonebook::iterator Phonebook::begin()
{
	return phonebook_.begin();
}

Phonebook::iterator Phonebook::end()
{
	return phonebook_.end();
}

void Phonebook::add(Phonebook::iterator current, const Phonebook::value_type & newRecord)
{
	std::insert_iterator< Phonebook::container_type > insert_iter(phonebook_, current);
	insert_iter = newRecord;
}

void Phonebook::modify(Phonebook::iterator current, const Phonebook::value_type & newRecord)
{
	*current = newRecord;
}

void Phonebook::view(const Phonebook::iterator & current) const
{
	if (current == phonebook_.end())
	{
		throw std::runtime_error("Illegal operation!\n");
	}
	current->view();
}

void Phonebook::move(iterator & current, int n)
{
	if (phonebook_.empty())
	{
		std::cerr << "Illegal operation!\n" << std::endl;
	}
	else
	{
		if (n > 0)
		{
			for (int i = 0; i < n; ++i)
			{
				if ((++current == phonebook_.end()) && (i < (n - 1)))
				{
					throw std::runtime_error("Illegal operation!\n");
				}
			}
		}
		else if (n < 0)
		{
			for (int i = 0; i > n; --i)
			{
				if (current == phonebook_.begin())
				{
					throw std::runtime_error("Illegal operation!\n");
				}
				--current;
			}
		}
	}
}


