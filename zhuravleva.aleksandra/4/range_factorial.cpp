#include "range_factorial.h"
#include <stdexcept>


static unsigned int factorial(unsigned int position)
{
	return position != 1 ? position*factorial(position - 1) : 1;
}

/////////////////////////////////////////////////////////////////////////

Range::Range() :
	size_(10)
{}

Range::iterator Range::begin() const
{
	return Range::iterator(1);
}

Range::iterator Range::end() const
{
	return Range::iterator(size_ + 1);
}


////////////////////////////////////////////////////////////////////////


Range::iterator::iterator() :
	max_(10),
	position_(1),
	value_(1)
{}

Range::iterator::iterator(unsigned int position) :
	max_(10)
{
	if ((position < 1) || (position >(max_ + 1)))
	{
		throw std::out_of_range("Out of range!\n");
	}
	else
	{
		position_ = position;
		value_ = factorial(position_);
	}
}

bool Range::iterator::operator ==(const iterator & rhs) const
{
	return (rhs.position_ == position_);
}

bool Range::iterator::operator !=(const iterator & rhs) const
{
	return (rhs.position_ != position_);
}

unsigned int Range::iterator::operator *() const
{
	if (position_ >= (max_ + 1))
	{
		throw std::runtime_error("Illegal operation!\n");
	}

	return value_;
}

Range::iterator & Range::iterator::operator ++()
{
	if (position_ >= (max_ + 1))
	{
		std::cerr << "Illegal operation!\n" << std::endl;
	}
	else
	{
		++position_;
		value_ *= position_;
	}

	return *this;
}

Range::iterator Range::iterator::operator ++(int)
{
	iterator temp(*this);
	this->operator++();

	return temp;
}

Range::iterator & Range::iterator::operator --()
{
	if (position_ <= 1)
	{
		std::cerr << "Illegal operation!\n" << std::endl;
	}
	else
	{
		value_ /= position_;
		--position_;
	}

	return *this;
}

Range::iterator Range::iterator::operator --(int)
{
	iterator temp(*this);
	this->operator--();

	return temp;
}

