#ifndef PHONEBOOK
#define PHONEBOOK

#include <list>
#include <iterator>

class Phonebook
{
public:

	class Record;

	typedef Record value_type;
	typedef std::list<value_type> container_type;
	typedef std::list<value_type>::iterator iterator;

	iterator begin();
	iterator end();

	void add(iterator current, const value_type & newRecord);
	void modify(iterator current, const value_type & newRecord);
	void view(const iterator & current) const;
	void move(iterator & current, int n);

private:

	container_type phonebook_;
};

////////////////////////////////////////////////////

class Phonebook::Record
{
public:

	Record(std::string name, long long phone);
	void view() const;
	std::string getName();
	long long getPhone();

private:

	std::string name_;
	long long phone_;
};


#endif
