#include "phonebook.h"
#include "range_factorial.h"

int main(int, char**)
{
	Phonebook::Record first("Sasha", 123456789);
	Phonebook::Record second("Sofia", 987654321);
	Phonebook::Record third("Dima", 678954321);
	Phonebook::Record fourth("Masha", 985654398);
	Phonebook::Record fifth("Maksim", 257349743);
	Phonebook::Record sixth("Marina", 9874632829);
	Phonebook myBook;
	Phonebook::iterator iter = myBook.begin();
	myBook.add(iter, first);
	myBook.add(iter, second);
	myBook.add(iter, third);
	myBook.add(iter, fourth);
	myBook.add(iter, fifth);
	iter = myBook.begin();
	++iter;
	++iter;
	myBook.modify(iter, sixth);
	try
	{
		for (iter = myBook.begin(); iter != myBook.end(); ++iter)
		{
			myBook.view(iter);
		}

		try
		{
		myBook.move(iter, -3); 
		myBook.view(iter);
		myBook.modify(iter, first);
		myBook.view(iter);
		myBook.move(iter, 1);
		}
		catch (const std::exception & e)
		{
			throw e;
		}
		myBook.view(iter);

		////////////////////////////////////////////////////////////////////

		Range container;
		for (Range::iterator iter = container.begin(); iter != container.end(); ++iter)
		{
			std::cout << *iter << " ";
		}
		std::cout << std::endl;
		std::list< unsigned int > list(10);
		std::copy(container.begin(), container.end(), list.begin());
		for (Range::iterator iter = container.end(); iter != container.begin();)
		{
			std::cout << *(--iter) << " ";
		}
		std::cout << std::endl;
		for (std::list< unsigned int >::iterator iter = list.begin(); iter != list.end(); ++iter)
		{
			std::cout << *iter << " ";
		}
	}
	catch (const std::exception & e)
	{
		std::cerr << std::endl << e.what();
	}
	std::cout << std::endl;

	return 0;
}
