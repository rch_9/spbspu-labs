#ifndef FACTORIAL
#define FACTORIAL

#include <iostream>
#include <iterator>

class Range
{
public:

	class iterator;

	Range();

	iterator begin() const;
	iterator end() const;

private:

	const unsigned int size_;

};


class Range::iterator :
	public std::iterator< std::bidirectional_iterator_tag, unsigned int >
{
public:

	iterator();
	iterator(unsigned int position);

	bool operator ==(const iterator & rhs) const;
	bool operator !=(const iterator & rhs) const;

	unsigned int operator *() const;

	iterator & operator ++();
	iterator operator ++(int);

	iterator & operator --();
	iterator operator --(int);

private:

	unsigned int max_;
	unsigned int position_;
	unsigned int value_;
};


#endif
