#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <cstdlib>  //rand, srand


const std::string column[10] = { "January", "February", "March", "April", "May",
								"June", "July", "August", "September", "October" };

typedef struct
{
	int key1_;
	int key2_;
	std::string str_;
} Data;

std::vector<Data> fillVector(size_t size)
{
	srand(static_cast<unsigned int>(time(0)));
	std::vector<Data> vector;
	for (size_t n = 0; n < size; ++n)
	{
		static Data data;
		data.key1_ = rand() % 11 - 5;
		data.key2_ = rand() % 11 - 5;
		data.str_ = column[rand() % 10];
		vector.push_back(data);
	}
	return vector;
}

struct printData
{
	void operator()(const Data & data)
	{
		std::cout << data.key1_ << " " << data.key2_ << " " << data.str_ << std::endl;
	}
};

struct comparison
{
	bool operator()(const Data & first, const Data & second)
	{
		if (first.key1_ == second.key1_)
		{
			if (first.key2_ == second.key2_)
			{
				return (first.str_.length() < second.str_.length());
			}
			return (first.key2_ < second.key2_);
		}
		return (first.key1_ < second.key1_);
	}
};

int main()
{ 
	std::vector<Data> dataContainer = fillVector(20);

	std::for_each(dataContainer.begin(), dataContainer.end(), printData());

	std::cout << std::endl;

	std::sort(dataContainer.begin(), dataContainer.end(), comparison());

	std::for_each(dataContainer.begin(), dataContainer.end(), printData());

	return 0;
}


