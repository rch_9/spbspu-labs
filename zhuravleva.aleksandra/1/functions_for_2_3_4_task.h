#ifndef TASK_2_3_4
#define TASK_2_3_4

#include <vector>

//*******************************************************************//
std::vector<char> read_file(const char * file_name, const unsigned int sizeFile);

std::vector<int> format_console_int_vector(std::istream & stream);

void fillRandom(double * randArray, int size);
//*******************************************************************//

#endif

