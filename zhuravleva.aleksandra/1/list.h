#ifndef SLIST_AND_ITERATOR_DECLARATION
#define SLIST_AND_ITERATOR_DECLARATION

#include <vector>


template <typename T>
class list
{
public:
	
	struct node_t;
	class iterator;

	typedef  T value_type;
	typedef iterator pointer;

	list();
	list(const std::vector<T> & vector);
	~list();

	iterator start() const;
	iterator end() const;

	void clear();

private:
	node_t * head;
};

template<typename T>
struct list<T>::node_t
{
	node_t(const T value_);
	~node_t();
	T value;
	node_t * next;
};

template <typename T>
class list<T>::iterator :
	public std::iterator<std::forward_iterator_tag, T>
{
public:
	typedef node_t node_type;
	typedef iterator this_type;

	iterator();
	iterator(node_type * slist);

	bool operator ==(const this_type & rhs) const;
	bool operator !=(const this_type & rhs) const;

	T & operator *() const;
	T * operator ->() const;

	this_type & operator ++();
	this_type operator ++(int);

private:
	node_type * current_;
};


#include "list_methods.h"

#endif

