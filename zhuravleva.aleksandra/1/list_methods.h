#ifndef NODE_ITERATOR_LIST
#define NODE_ITERATOR_LIST


#include <cassert>

//*******************************************************************//
//*******************      LIST METHODS       ***********************//
//*******************************************************************//
template < typename T >
inline list<T>::list() :
head(0)
{}

template < typename T >
inline list<T>::list(const std::vector<T> & vector)
{
	head = 0;
	if (!vector.empty())
	{
		try
		{
			node_t * headNode = new node_t(vector[0]); //construct the first node (value = vector[0], *next = NULL)
			head = headNode;
			node_t * runnerNode = headNode;
			for (unsigned int i = 1; i < (vector.size()); ++i)
			{
				runnerNode->next = new node_t(vector[i]);
				runnerNode = runnerNode->next;
			}
		}
		catch (const std::exception & e)
		{
			clear();
			throw e;
		}
	}
}

template < typename T >
inline list<T>::~list()
{
	clear();
}

template < typename T >
inline typename list<T>::iterator list<T>::start() const
{
	return head;
}

template < typename T >
inline typename list<T>::iterator list<T>::end() const
{
	return 0;
}

template < typename T >
inline void list<T>::clear()
{
	node_t * tmp;
	while (head != 0)
	{
		tmp = head;
		head = head->next;
		delete tmp;
	}
}


//*******************************************************************//
//*************************  NODE METHODS  **************************//
//*******************************************************************//

template <typename T>
inline list<T>::node_t::node_t(const T value_) :
	value(value_),
	next(0)
{}

template <typename T>
inline list<T>::node_t::~node_t()
{}


//*******************************************************************//
//**************      SLIST ITERATOR METHODS       ******************//
//*******************************************************************//
template <typename T>
inline list<T>::iterator::iterator() :
	current_(0)
{}

template <typename T>
inline list<T>::iterator::iterator(node_t * slist) :
	current_(slist)
{}

template <typename T>
inline bool list<T>::iterator::operator==(const iterator & rhs) const
{
	return current_ == rhs.current_;
}

template <typename T>
inline bool list<T>::iterator::operator !=(const iterator & rhs) const
{
	return current_ != rhs.current_;
}

template <typename T>
inline T & list<T>::iterator::operator *() const
{
	assert(current_ != 0);

	return current_->value;
}

template <typename T>
inline T * list<T>::iterator::operator ->() const
{
	assert(current_ != 0);

	return &current_->value;
}

template <typename T>
inline typename list<T>::iterator& list<T>::iterator::operator++()
{
	assert(current_ != 0);

	current_ = current_->next;

	return *this;
}

template <typename T>
inline typename list<T>::iterator list<T>::iterator::operator++(int)
{
	this_type temp(*this);

	this->operator ++();

	return temp;
}



#endif

