#define _CRT_SECURE_NO_WARNINGS

#include "file.h"
#include <fstream>
#include <iostream>
#include <vector>
#include <time.h>
#include <boost/shared_array.hpp>


std::vector<char> read_file(const char * fileName, const unsigned int sizeFile)
{
	std::vector<char> vectorForFile;
	if ((fileName) && (sizeFile))
	{
		File file;
		if (file.open(fileName, "r"))
		{
			boost::shared_array <char> array_(new char[sizeFile]);
			fread(array_.get(), sizeof(char), sizeFile, file.get());
			vectorForFile.assign(array_.get(), array_.get() + sizeFile);	//copy vector in one line
		}
	}
	return vectorForFile;
}


std::vector<int> format_console_int_vector(std::istream & stream)
{
	std::vector<int> consoleVector;
	int consoleNumber;
	while ((stream >> consoleNumber) && (consoleNumber))
	{
		consoleVector.push_back(consoleNumber);
	}
	if (consoleNumber)
	{
		std::cout << "Wrong input!" << std::endl;
		return consoleVector;
	}
	if (!consoleVector.empty())
	{
		switch (consoleVector.back())
		{
			case 1:
			{
				std::vector<int>::iterator iterator_ = consoleVector.begin();
				while (iterator_ != consoleVector.end())
				{
					if ((*iterator_ % 2) == 0)
					{
						try
						{
							iterator_ = consoleVector.erase(iterator_);
						}
						catch (const std::exception & e)
						{
							throw e;
						}
					}
					else
					{
						++iterator_;
					}
				}
				break;
			}
			case 2:
			{
				std::vector<int>::iterator iterator_ = consoleVector.begin();
				while (iterator_ != consoleVector.end())
				{
					if ((*iterator_ % 3) == 0)
					{
						iterator_ = consoleVector.insert(++iterator_, 3, 1);
						iterator_ += 3;
					}
					else
					{
						++iterator_;
					}
				}
				break;
			}
		}
	}
	else
	{
		std::cout << "There are no values for vector. Skipping test3." << std::endl;
	}
	std::cin.sync();
	return consoleVector;
}


void fillRandom(double * array, int size)
{
	srand((unsigned int)time(0)); 
	double var_1;
	double var_2;
	for (int i = 0; i < size; i++)
	{
		var_1 = (rand() % 21 - 10);
		var_2 = var_1 / 10;
		array[i] = var_2;
	}
}

