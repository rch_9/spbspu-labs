#define BOOST_TEST_MODULE My_Tests


#include "strategy.h"
#include "test_functions.h"
#include "functions_for_2_3_4_task.h"
#include <boost/test/included/unit_test.hpp>
#include <algorithm>
#include <Windows.h>


BOOST_AUTO_TEST_CASE(Task1_index_sort_int_vector)
{
	int arr[] = { 3, 8, 73, -12, 0, 1, 22, 0, 7, 60 };
	std::vector<int> vector(arr, arr + 10);
	int sortedArr[] = { -12, 0, 0, 1, 3, 7, 8, 22, 60, 73 };
	const std::vector<int> sortedVector(sortedArr, sortedArr + 10);
	try
	{
		sort <vector_index_sort_policy>(vector);
	}
	catch (const std::exception & e)
	{
		throw e;
	}
	BOOST_TEST(vector == sortedVector, "Vector are not equal");
}

BOOST_AUTO_TEST_CASE(Task1_at_sort_int_vector)
{
	int arr[] = { 9, 6, 2, 7, -5, -3, 1, 77, -18, 8 };
	std::vector<int> vector(arr, arr + 10);
	int sortedArr[] = { -18, -5, -3, 1, 2, 6, 7, 8, 9, 77 };
	const std::vector<int> sortedVector(sortedArr, sortedArr + 10);
	try
	{
		sort <vector_at_sort_policy>(vector);
	}
	catch (const std::exception & e)
	{
		throw e;
	}
	BOOST_TEST(vector == sortedVector, "Vector are not equal");
}

BOOST_AUTO_TEST_CASE(Task1_list_sort_int_vector)
{
	int arr[] = { 4, 2, -7, 0, 1, 5, 20, 91, 6, 12 };
	std::vector<int> vector(arr, arr + 10);
	int sortedArr[] = { -7, 0, 1, 2, 4, 5, 6, 12, 20, 91 };
	std::vector<int> sortedVector(sortedArr, sortedArr + 10);
	try
	{
		list<int> sortedList(sortedVector);
		list<int> list(vector);
		sort <list_sort_policy>(list);
		BOOST_TEST(compare_lists(list, sortedList), "Lists are not equal");
	}
	catch (const std::exception & e)
	{
		throw e;
	}

}

BOOST_AUTO_TEST_CASE(Task2_read_file_)
{
	std::auto_ptr <char> fileName(new char[20]);
	if (GetTempFileName(".", "MY", 0, fileName.get()))
	{
		const char array_[] = "Just do it!";	//write any data to write to the file
		std::vector<char> DATA(array_, array_ + sizeof(array_)/sizeof(char));
		BOOST_TEST(fill_file(fileName.get(), DATA), "file didn't open");
		std::vector < char > data(read_file(fileName.get(), sizeof(array_) / sizeof(char)));
		remove(fileName.get());
		BOOST_TEST(!data.empty(), "'data' is empty");
		BOOST_TEST(data == DATA, "'data' is incorrect");
	}
	else
	{
		BOOST_TEST(false, "file don't created");
	}
}

BOOST_AUTO_TEST_CASE(Task3_read_and_fotmat_console_vector)
{
	std::istringstream s1("21 8 5 3 2 1 0");
	std::istream stream_1(s1.rdbuf());
	int arr_1[] = {21, 5, 3, 1};
	std::vector<int> formattedVector_1(arr_1, arr_1 + sizeof(arr_1)/sizeof(int));
	try
	{
		std::vector<int> testVector_1 = format_console_int_vector(stream_1);
		BOOST_TEST(testVector_1 == formattedVector_1, "Wrong formatting of the vector or invalid input values");
	}
	catch (const std::exception & e)
	{
		throw e;
	}
	std::istringstream s2("21 8 5 3 2 2 0");
	std::istream stream_2(s2.rdbuf());
	int arr_2[] = { 21, 1, 1, 1, 8, 5, 3, 1, 1, 1, 2, 2 };
	std::vector<int> formattedVector_2(arr_2, arr_2 + sizeof(arr_2) / sizeof(int));
	try
	{
		std::vector<int> testVector_2 = format_console_int_vector(stream_2);
		BOOST_TEST(testVector_2 == formattedVector_2, "Wrong formatting of the vector or invalid input values");
	}
	catch (const std::exception & e)
	{
		throw e;
	}
}

BOOST_AUTO_TEST_CASE(Task4_sort_random_double_vectors)
{
	for (unsigned int i = 0; i < 5; ++i)
	{
		// fill the vector 5, 10, 25, 50, 100 values
		unsigned int vectorSize = (int)((int)(100 / pow(2, i)) / 5) * 5;
		std::vector<double> randVect;
		std::vector<double> correctVect;
		double * array_ = new double[vectorSize];
		fillRandom(array_, vectorSize);
		randVect.assign(array_, array_ + vectorSize);
		correctVect.assign(array_, array_ + vectorSize);
		delete[] array_;
		sort(correctVect.begin(), correctVect.begin() + vectorSize);
		try
		{
			sort <vector_index_sort_policy>(randVect);
		}
		catch (const std::exception & e)
		{
			throw e;
		}
		BOOST_TEST(randVect == correctVect, "Vector doesn't match with example");
	}
	
	std::cout << "Press any key";
}

