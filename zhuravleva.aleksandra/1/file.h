#ifndef CLASS_FILE
#define CLASS_FILE

#include <cstdio>

class File
{
public:
	File();
	~File();
	bool open(const char * fileName, const char * mode);
	bool close();
	FILE * get();
private:
	File(const File &);
	File & operator=(const File &);

	FILE * file;
};




#endif

