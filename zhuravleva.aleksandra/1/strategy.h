#ifndef STRATEGY_DECLARATION
#define STRATEGY_DECLARATION

#include "list.h"


//*******************************************************************//
//************************   SORT FUNCTION  *************************//
//*******************************************************************//
template <template <typename > class Policy, typename C>
void sort(C & data)
{
	typedef typename Policy<C> policy;
	typedef typename policy::pointer pointer;

	for (pointer i = policy::begin(data); i != policy::end(data); ++i)
	{
		for (pointer j = i; j != policy::end(data); ++j)
		{
			try
			{
				if (policy::get(data, j) < policy::get(data, i))
				{
					std::swap(policy::get(data, j), policy::get(data, i));
				}
			}
			catch (const std::exception & e)
			{
				throw e;
			}
		}
	}
}


//*******************************************************************//
//*************   VECTOR SORT POLICY USING OPERATOR[]   *************//
//*******************************************************************//
template <typename C>
struct vector_index_sort_policy
{
	typedef typename C::size_type pointer;
	typedef typename C::value_type value;

	static pointer begin(C & ) 
	{
		return 0;
	}

	static pointer end(C & container) 
	{
		return container.size();
	}

	static value & get(C & container, pointer pos)
	{
		if (pos >= end(container)) 
		{
			throw std::invalid_argument("Bad arguments");
		}
		return container[pos];
	}
};

//*******************************************************************//
//*************   VECTOR SORT POLICY USING AT() METOD   *************//
//*******************************************************************//
template <typename C>
struct vector_at_sort_policy
{
	typedef typename C::size_type pointer;
	typedef typename C::value_type value;

	static pointer begin(C & ) 
	{
		return 0;
	}

	static pointer end(C & container) 
	{
		return container.size();
	}

	static value & get(C & container, pointer pos)
	{
		return container.at(pos);
	}
};

//*******************************************************************//
//********************   LIST SORT POLICY   *************************//
//*******************************************************************//
template <typename C>
struct list_sort_policy
{
	typedef typename C::pointer pointer;
	typedef typename C::value_type value;

	static pointer begin(C & container) 
	{
		return container.start();
	}

	static pointer end(C & container) 
	{
		return container.end();
	}

	static value & get(C & container, pointer pos)
	{
		if (begin(container) == end(container)) 
		{
			throw std::invalid_argument("Bad arguments");
		}
		return *pos;
	}
};


#endif

