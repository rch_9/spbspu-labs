#ifndef TEST_FUNCTIONS
#define TEST_FUNCTIONS

#define _CRT_SECURE_NO_WARNINGS

#include "list.h"
#include "file.h"
#include <iostream>


//*******************************************************************//
//***************   COMPARE TWO SLISTS FOR EQUALITY   ***************//
//*******************************************************************//
template <typename T>
bool compare_lists(const list<T> & lhs,const list<T> & rhs)
{
	list<T>::iterator copy_lhs = lhs.start();
	list<T>::iterator copy_rhs = rhs.start();
	for (; (copy_lhs != lhs.end()) || (copy_rhs != rhs.end()); ++copy_lhs, ++copy_rhs)
	{
		if ((*copy_lhs) != (*copy_rhs))
		{
			return false;
		}
	}
	if ((copy_lhs == rhs.end()) && (copy_rhs == rhs.end()))
	{
		return true;
	}
	else
	{
		return false;
	}
	
}


//*******************************************************************//
//**************   FILL FILE BY CHAR VECTOR 'DATA'  *****************//
//*******************************************************************//
bool fill_file(const char * file_name, const std::vector<char> & DATA)
{
	File file;
	if (file.open(file_name, "w"))
	{
		for (unsigned int i = 0; i < DATA.size(); ++i)
		{
			fprintf(file.get(), "%c", DATA[i]);
		}
	}
	else
	{
		return false;
	}
	return true;
}


#endif

