#define _CRT_SECURE_NO_WARNINGS

#include "file.h"

File::File() : 
	file(0)
{}

File::~File()
{
	if (file)
	{
		fclose(file);
	}
}

bool File::open(const char *fileName, const char *mode)
{
	if (file)
	{
		fclose(file);
	}
	if ((fileName) && (mode))
	{
		file = fopen(fileName, mode);
	}
	return file;	// 0 - not opened
}

bool File::close()
{
	return (fclose(file));	// 0 - closed
}

FILE * File::get()
{
	return file;
}


