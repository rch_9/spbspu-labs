#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <set>
#include <iterator>
#include <cstring>
#include <cerrno>


void create_file(const char * name)
{
	if (name)
	{
		std::ofstream out(name);
		if (out)
		{
			out << "I love c++ I love c++ too. I love love love" << std::endl;
			if (out.fail())
			{
				std::cerr << "An error occurred while writing to the file" << std::endl;
				return;
			}
		}
		else
		{
			std::cerr << "An error occurred while opening the file" << std::endl;
			return;
		}
	}
	else
	{
		std::cerr << "No file name" << std::endl;
	}
}

std::set< std::string > make_tokens(const char * name)
{
	std::set< std::string > tokens;
	if (name)
	{
		std::ifstream in(name);
		if (in)
		{
			std::copy(std::istream_iterator< std::string >(in),
					  std::istream_iterator< std::string >(),
					  std::insert_iterator< std::set< std::string > >(tokens, tokens.begin()));
			std::cout << strerror(errno) << std::endl;	// said that no error  :( but in.fail - true..
			if (in.bad())
			{
				std::cerr << "An error occurred while reading the file" << std::endl;
				return tokens;
			}
		}
		else
		{
			std::cerr << "An error occurred while opening the file" << std::endl;
			return tokens;
		}
	}
	else
	{
		std::cerr << "No file name" << std::endl;
	}
	return tokens;
}

void print_tokens(std::set< std::string > tokens)
{
	std::copy(tokens.begin(), tokens.end(), std::ostream_iterator< std::string >(std::cout, " "));
	std::cout << std::endl;
}

void delete_file(const char * name)
{
	if (std::remove(name))
	{
		std::cerr << "An error occurred while removing the file" << std::endl;
	}

}
