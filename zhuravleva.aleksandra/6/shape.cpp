#include "shape.h"

Point::Point(int x, int y):
	x_(x),
	y_(y)
{}

int Point::getX() const
{
	return x_;
}

int Point::getY() const
{
	return y_;
}

void Shape::addVertex(const Point & vertex)
{
	vertices_.push_back(vertex);
}

unsigned int Shape::getVertexNum() const
{
	return vertices_.size();
}

Point & Shape::operator[](unsigned int i)
{
	return vertices_[i];
}