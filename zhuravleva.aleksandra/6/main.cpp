#include "functions.h"
#include <iostream>
#include <algorithm>
#include <numeric>
#include <ctime>
#include <cstdlib>  

int main(int, char**)
{	
	/////////////////////   1   //////////////////////
	const char *  name = "file.txt";
	create_file(name);
	print_tokens(make_tokens(name));
	delete_file(name);
	std::cout << std::endl;
	/////////////////////   2.1   ////////////////////
	srand(static_cast< unsigned int >(time(0)));
	std::vector< Shape > shapes(7);
	std::generate(shapes.begin(), shapes.end(), generateShape);
	std::cout << "The list of shapes" << std::endl;
	std::for_each(shapes.begin(), shapes.end(), printShape);
	std::cout << std::endl;
	/////////////////////   2.2   ////////////////////
	unsigned int sumVerticies = std::accumulate(shapes.begin(), shapes.end(), 0, countSum);
	std::cout << "The amount of vertices is " << sumVerticies << std::endl << std::endl;
	/////////////////////   2.3   ////////////////////
	unsigned int triangles = std::count_if(shapes.begin(), shapes.end(), isTriangle);
	std::cout << "The amount of triangles is " << triangles << std::endl << std::endl;
	unsigned int squares = std::count_if(shapes.begin(), shapes.end(), isSquare);
	std::cout << "The amount of square is " << squares << std::endl << std::endl;
	unsigned int rectangles = std::count_if(shapes.begin(), shapes.end(), isRectangle);
	std::cout << "The amount of rectangles is " << rectangles << std::endl << std::endl;
	/////////////////////   2.4   ////////////////////
	std::vector< Shape >::iterator pPentagons = std::remove_if(shapes.begin(),shapes.end(), isPentagon);
	pPentagons = shapes.erase(pPentagons, shapes.end());
	std::cout << "The new list of shapes without pentagons" << std::endl;
	std::for_each(shapes.begin(), shapes.end(), printShape);
	std::cout << std::endl;
	/////////////////////   2.5   ////////////////////
	std::vector< Point > vertices(shapes.size(), Point(0, 0));
	std::transform(shapes.begin(), shapes.end(), vertices.begin(), getRandVertex);
	std::cout << "The list of vertices" << std::endl;
	std::for_each(vertices.begin(), vertices.end(), printVertex);
	std::cout << std::endl;
	/////////////////////   2.6   ////////////////////
	std::sort(shapes.begin(), shapes.end(), compareShapes);
	std::cout << "The new list of sorted shapes" << std::endl;
	std::for_each(shapes.begin(), shapes.end(), printShape);
	std::cout << std::endl;
	getchar();
	return 0;
}


