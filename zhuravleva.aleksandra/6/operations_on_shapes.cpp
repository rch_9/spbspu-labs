#include "shape.h"
#include <cstdlib> 
#include <iostream>
#include <algorithm>
#include <cmath>


const int MAX_SIDE = 1000; // only for square and rectangle 
const int MAX_X = 1000;
const int MAX_Y = 1000;
const int MIN_Y = -1000;
const int MIN_X = -1000;
// but a square and a rectangle can go beyond this grid size

void printVertex(const Point & vertex)
{
	std::cout << "  x: " << vertex.getX() << "  y: " << vertex.getY() << std::endl;
}

bool isSquare(Shape & shape)
{
	if (shape.getVertexNum() == 4)
	{
		unsigned int leftHigh = shape[1].getY() - shape[0].getY();
		unsigned int bottomWidth = shape[3].getX() - shape[0].getX();
		if (leftHigh == bottomWidth)
		{
			unsigned int rightHigh = shape[1].getY() - shape[0].getY();
			unsigned int upperWidth = shape[3].getX() - shape[0].getX();
			return (sqrt(rightHigh*rightHigh + upperWidth*upperWidth)) ==
				   (sqrt(leftHigh*leftHigh + bottomWidth*bottomWidth));
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

void printShape(Shape & shape)
{
	switch (shape.getVertexNum())
	{
	case (3) :
	{
		std::cout << "Triangle:" << std::endl;
		break;
	}
	case (4) :
	{
		if (isSquare(shape))
		{
			std::cout << "Square:" << std::endl;
		}
		else
		{
			std::cout << "Rectangle:" << std::endl;
		}
		break;

	}
	case (5) :
	{
		std::cout << "Pentagon:" << std::endl;
		break;
	}
	}
	for (unsigned int i = 0; i < shape.getVertexNum(); ++i)
	{
		printVertex(shape[i]);
	}
}

unsigned int countSum(const unsigned int sum, const Shape & shape)
{
	return sum + shape.getVertexNum();
}

bool isTriangle(const Shape & shape)
{
	return shape.getVertexNum() == 3;
}

bool isRectangle(Shape & shape)	
{
	if(shape.getVertexNum() == 4) 
	{
		unsigned int leftHigh = shape[1].getY() - shape[0].getY();
		unsigned int bottomWidth = shape[3].getX() - shape[0].getX();
		unsigned int rightHigh = shape[1].getY() - shape[0].getY();
		unsigned int upperWidth = shape[3].getX() - shape[0].getX();
		if ((leftHigh == rightHigh) && (bottomWidth == upperWidth))
		{
			return (sqrt(rightHigh*rightHigh + upperWidth*upperWidth)) ==
				(sqrt(leftHigh*leftHigh + bottomWidth*bottomWidth));
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

bool isPentagon(const Shape & shape)
{
	return shape.getVertexNum() == 5;
}

const Point getRandVertex(Shape & shape)
{
	return shape[rand() % shape.getVertexNum()];
}

bool compareShapes(Shape & first, Shape & second)	
{
	if ((first.getVertexNum() == 4) && (second.getVertexNum() == 4))
	{
		return ((isSquare(first)) && (!isSquare(second)));
	}
	return first.getVertexNum() < second.getVertexNum();
}

Shape generateShape() 
{
	Shape shape;
	int shapeDefiner = rand() % 4;
	switch (shapeDefiner)
	{
	case 0: //triangle
	{
		for (unsigned int i = 0; i < 3; ++i)
		{
			shape.addVertex(Point(rand() % (MAX_X - MIN_X) + MIN_X,
				rand() % (MAX_Y - MIN_Y) + MIN_Y));
		}
		break;
	}
	case 1: // square
	{
		int x = rand() % (MAX_X - MIN_X) + MIN_X;
		int y = rand() % (MAX_Y - MIN_Y) + MIN_Y;
		unsigned int side = rand() % MAX_SIDE + 1;
		shape.addVertex(Point(x, y));
		shape.addVertex(Point(x, y + side));
		shape.addVertex(Point(x + side, y + side));
		shape.addVertex(Point(x + side, y));
		break;
	}
	case 2: // rectangle
	{
		int x = rand() % (MAX_X - MIN_X) + MIN_X;
		int y = rand() % (MAX_Y - MIN_Y) + MIN_Y;
		unsigned int width = rand() % MAX_SIDE + 1;
		unsigned int height = rand() % MAX_SIDE + 1;
		shape.addVertex(Point(x, y));
		shape.addVertex(Point(x, y + height));
		shape.addVertex(Point(x + width, y + height));
		shape.addVertex(Point(x + width, y));
		break;
	}
	case 3: // pentagon
	{
		for (unsigned int i = 0; i < 5; ++i)
		{
			shape.addVertex(Point(rand() % (MAX_X - MIN_X) + MIN_X,
				rand() % (MAX_Y - MIN_Y) + MIN_Y));
		}
		break;
	}
	}
	return shape;
}

