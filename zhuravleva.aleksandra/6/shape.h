#ifndef SHAPE
#define SHAPE

#include <vector>

class Point
{
public:
	Point(int x, int y);
	int getX() const;
	int getY() const;
private:
	int x_, y_;
};

class Shape
{
public:
	unsigned int getVertexNum() const;
	void addVertex(const Point & vertex);
	Point & operator[](unsigned int i);
private:
	std::vector< Point > vertices_;
};

#endif

