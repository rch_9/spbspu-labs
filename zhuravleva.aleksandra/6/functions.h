#ifndef FUNCTIONS
#define FUNCTIONS

#include "shape.h"
#include <string>
#include <set>

void create_file(const char * name);
std::set< std::string > make_tokens(const char * name);
void print_tokens(std::set< std::string > tokens);
void delete_file(const char * name);

void printVertex(const Point & vertex);
void printShape(Shape & shape);
unsigned int countSum(const unsigned int sum, const Shape & shape);
bool isTriangle(const Shape & shape);
bool isSquare(Shape & shape);
bool isRectangle(Shape & shape);
bool isPentagon(const Shape & shape);
const Point getRandVertex(Shape & shape);
bool compareShapes(Shape & first, Shape & second);
Shape generateShape();

#endif
