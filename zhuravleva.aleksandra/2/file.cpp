#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "file.h"

File::File() :
file(0)
{}

File::~File()
{
	if (file)
	{
		fclose(file);
	}
}

int File::open(const char *fileName, char *mode)
{
	if (file)
	{
		fclose(file);
	}
	file = fopen(fileName, mode);
	return (file == 0) ? 0 : 1;	// 0 - was not opened
}

int File::close()
{
	if (file)
	{
		return (fclose(file) == 0) ? 0 : 1;	// 0 - was closed
	}
	return 0;
}

FILE * File::get()
{
	return file;
}


