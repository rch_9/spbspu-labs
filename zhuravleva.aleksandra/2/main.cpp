#include <vector>
#include <string>
#include <iostream>


void format_text(const char * fileName, std::vector<std::string> & text);
void print_text(std::vector <std::string> & text);

int main()
{
	const char * fileName_r = "input.txt";

	std::vector <std::string> text;
	format_text(fileName_r, text);
	print_text(text);
	getchar();
}

