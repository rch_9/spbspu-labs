#ifndef CLASS_FILE
#define CLASS_FILE


class File
{
public:
	File();
	~File();
	int open(const char * fileName, char * mode);
	int close();
	FILE * get();
private:
	FILE * file;
};

#endif

