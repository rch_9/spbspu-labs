#define _CRT_SECURE_NO_WARNINGS

#include <vector>
#include <string>
#include <iostream>
#include "file.h"

/***********************************************************************/
void format_text(const char * fileName, std::vector<std::string> & text);
void print_text(std::vector <std::string> & text);
/***********************************************************************/


/*It is given that the file contains only symbols from 'L', 'P', 'S' (look below). 
  Other symbols will be ignored in this program.
  But if the file need to check the extra symbols, I can do this check 
  (It is not difficult. But I thought it was unnecessary.) */

void format_text(const char * fileName, std::vector<std::string> & text)
{
	const std::string L = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";	// Literal symbols
	const std::string P = ".,!?:;";															// Punctuation symbols
	const std::string S = " \t\v\n";														// Whitespace symbols
	const std::string anotherWord = "Vau!!!";
	unsigned int maxWord = 10;
	unsigned int maxStr = 40;
	File file;

	if (file.open(fileName, "r"))
	{
		std::string str;		
		std::string word;		// contains only literal symbols
		char c = 0;				// readable symbol
		unsigned int j = 0;		// counter for symbols from 'str'
		unsigned int k = 0;		// counter for symbols from 'word'

		while (!feof(file.get()))
		{ 
			while ((j <= maxStr) && (!feof(file.get())))
			{
				fscanf(file.get(), "%c", &c);
				//--------if symbol 'c' is literal symbol--------//
				if (L.find(c) != std::string::npos)
				{
					//--if last symbol in 'str' is punctuation symbol--//
					if ((j > 0) && (P.find(str[j - 1]) != std::string::npos))
					{
						str.push_back(' ');
						++j;
					}
					while ((L.find(c) != std::string::npos) && (!feof(file.get())))
					{
						word.push_back(c);
						++k;
						fscanf(file.get(), "%c", &c);
					}
					//--------if 'word' is more than 'maxWord' symbols------//
					if (word.size() > maxWord)
					{
						word.clear();
						word = anotherWord;
						k = anotherWord.size();
					}
					//if the length of 'str + word' is more than 'maxStr' symbols//
					if ((j + k) > maxStr)
					{
						text.push_back(str);
						str.clear();
						j = 0;
					}
					str += word;
					word.clear();
					j += k;
					k = 0;
				}
				if ((j < maxStr) && (!feof(file.get())))
				{				
					//--if symbol 'c' is punctuation symbol--//
					if (P.find(c) != std::string::npos)			
					{
						//--if last symbol in 'str' is punctuation symbol--//
						if ((j > 0) && (P.find(str[j - 1]) != std::string::npos))
						{										
							str.push_back(' ');
							++j;
							if (j == maxStr)
							{
								text.push_back(str);
								str.clear();
								j = 0;
							}
							str.push_back(c);
							++j;
						}		
						//----------if last symbol in 'str' is ' '---------//
						else if ((j > 0) && (S.find(str[j - 1]) != std::string::npos))
						{				
							//if last symbol in 'str' is ' ' and penultimate symbol is literal symbol//
							if ((j > 1) && (L.find(str[j - 2]) != std::string::npos))
							{
								str.pop_back();
								str.push_back(c);
							}
							//if last symbol in 'str' is ' ' and penultimate symbol is punctuation symbol//
							else
							{
								str.push_back(c);
								++j;
							}
						}
						//---if last symbol in 'str' is literal symbol---//
						else
						{
							str.push_back(c);
							++j;
						}
					}
					//------if symbol 'c' is whitespace symbol------//
					else if ((S.find(c) != std::string::npos) && (((j > 0) && (str[j - 1] != ' ')) || (j == 0)))
					{
						str.push_back(' ');
						++j;
					}
				}
				//if the length of 'str + word' is more then 'maxStr' symbols//
				else
				{
					j = maxStr + 1;
				}
			}
			text.push_back(str);
			str.clear();	
			j = 0;
		}
	}
	else 
	{
		std::cerr << "File wasn't opened for reading" << std::endl;
	}

}

///////////////////////////////////////////////////////////////////////////

void print_text(std::vector <std::string> & text)
{
	if (!text.empty())
	{
		for (unsigned int i = 0; i < text.size(); ++i)
		{
			std::cout << text[i] << " - " << text[i].size() << std::endl;
		}
	}
	else
	{
		std::cerr << "There aren't text information" <<
			" because file for reading is empty or wasn't opened"
			<< std::endl;
	}
}

