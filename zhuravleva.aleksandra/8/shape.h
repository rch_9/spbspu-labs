#ifndef SHAPE
#define SHAPE

#include "point.h"


class Shape
{
public:
	Shape(int x, int y);
	bool isMoreLeft(const Shape & shape) const;
	bool isUpper(const Shape & shape) const;
	virtual void draw() const = 0;
	Point getCenter() const;
private:
	Point center_;
};

#endif
