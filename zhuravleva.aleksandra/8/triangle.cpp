#include "triangle.h"
#include <iostream>


Triangle::Triangle(int x, int y) :
	Shape(x, y)
{}

void Triangle::draw() const
{
	std::cout << "The center of the triangle: " << getCenter().getX() 
			  << ", " << getCenter().getY() << std::endl;
}





