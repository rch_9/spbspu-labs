#ifndef CIRCLE
#define CIRCLE

#include "shape.h"


class Circle : public Shape
{
public:
	Circle(int x, int y);
	void draw() const;
};



#endif
