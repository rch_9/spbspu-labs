#include "circle.h"
#include <iostream>


Circle::Circle(int x, int y):
	Shape(x, y)
{}


void Circle::draw() const
{
	std::cout << "The center of the circle  : " << getCenter().getX() 
			  << ", " << getCenter().getY() << std::endl;
}





