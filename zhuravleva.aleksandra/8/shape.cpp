#include "shape.h"



Shape::Shape(int x, int y):
	center_(Point(x,y))
{}

bool Shape::isMoreLeft(const Shape & shape) const
{
	return center_.getX() < shape.getCenter().getX();
}

bool Shape::isUpper(const Shape & shape) const
{
	return center_.getY() > shape.getCenter().getY();
}

Point Shape::getCenter() const
{
	return center_;
}


