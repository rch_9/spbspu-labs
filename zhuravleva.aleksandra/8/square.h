#ifndef SQUARE
#define SQUARE

#include "shape.h"



class Square : public Shape
{
public:
	Square(int x, int y);
	void draw() const;
};


#endif
