#ifndef FUNCTORS
#define FUNCTORS

#include <boost/shared_ptr.hpp>
#include "shape.h"


struct drawShape
{
	void operator()(const boost::shared_ptr< Shape > & shape) const;
};

struct compareFromLeftToRight
{
	typedef bool result_type;
	bool operator()(const boost::shared_ptr< Shape > & lsh, 
					const boost::shared_ptr< Shape > & rsh);
};

struct compareFromTopToBottom
{
	typedef bool result_type;
	bool operator()(const boost::shared_ptr< Shape > & lsh,
					const boost::shared_ptr< Shape > & rsh);
};




#endif
