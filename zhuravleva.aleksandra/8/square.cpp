#include "square.h"
#include <iostream>


Square::Square(int x, int y):
	Shape(x, y)
{}

void Square::draw() const
{
	std::cout << "The center of the square  : "   << getCenter().getX()
			  << ", " << getCenter().getY() << std::endl;
}


