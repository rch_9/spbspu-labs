#include "functors.h"
#include "circle.h"
#include "square.h"
#include "triangle.h"
#include <ctime>
#include <cstdlib> 
#include <vector>
#include <algorithm>
#include <functional>
#include <cmath>
#include <iostream>
#include <iterator>
#include <boost/bind.hpp>


int main(int, char**)
{
	srand(static_cast< unsigned int >(time(0)));
	double arr[] = {4.54, 53.6, 9.0, 12.11, 23};
	std::vector< double > vector(arr, arr + 5);
	std::copy(vector.begin(), vector.end(), std::ostream_iterator< double >(std::cout, " "));
	std::cout << std::endl;
	std::transform(vector.begin(), vector.end(), vector.begin(), std::bind1st(std::multiplies< double >(), std::acos(-1)));
	std::copy(vector.begin(), vector.end(), std::ostream_iterator< double >(std::cout, " "));
	std::cout << std::endl << std::endl;

	//////////////////////////////////////

	boost::shared_ptr< Square > pSquare(new Square(10, -5));
	boost::shared_ptr< Triangle > pTriangle_1(new Triangle(-4, 12));
	boost::shared_ptr< Triangle > pTriangle_2(new Triangle(4, 2));
	boost::shared_ptr< Circle > pCircle_1(new Circle(0, 0));
	boost::shared_ptr< Circle > pCircle_2(new Circle(7, 5));
	boost::shared_ptr< Circle > pCircle_3(new Circle(-6, -6));

	std::vector < boost::shared_ptr< Shape > > shapes;
	shapes.push_back(pSquare);
	shapes.push_back(pTriangle_1);
	shapes.push_back(pTriangle_2);
	shapes.push_back(pCircle_1);
	shapes.push_back(pCircle_2);
	shapes.push_back(pCircle_3);

	std::cout << "Arrangement of shapes from left to right:" << std::endl;
	std::sort(shapes.begin(), shapes.end(), boost::bind(compareFromLeftToRight(), _1, _2));
	std::for_each(shapes.begin(), shapes.end(), drawShape());
	std::cout << std::endl << std::endl;

	std::cout << "Arrangement of shapes from right to left:" << std::endl;
	std::sort(shapes.begin(), shapes.end(), boost::bind(compareFromLeftToRight(), _2, _1));
	std::for_each(shapes.begin(), shapes.end(), drawShape());
	std::cout << std::endl << std::endl;

	std::cout << "Arrangement of shapes from top to bottom:" << std::endl;
	std::sort(shapes.begin(), shapes.end(), boost::bind(compareFromTopToBottom(), _1, _2));
	std::for_each(shapes.begin(), shapes.end(), drawShape());
	std::cout << std::endl << std::endl;

	std::cout << "Arrangement of shapes from bottom to top:" << std::endl;
	std::sort(shapes.begin(), shapes.end(), boost::bind(compareFromTopToBottom(), _2, _1));
	std::for_each(shapes.begin(), shapes.end(), drawShape());
	std::cout << std::endl << std::endl;

	return 0;
}









