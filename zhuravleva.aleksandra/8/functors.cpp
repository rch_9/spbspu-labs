#include "functors.h"


void drawShape::operator()(const boost::shared_ptr< Shape > & shape) const
{
	shape->draw();
}

bool compareFromLeftToRight::operator()(const boost::shared_ptr< Shape > & lsh,
										const boost::shared_ptr< Shape > & rsh)
{
	return lsh->isMoreLeft(*rsh);
}

bool compareFromTopToBottom::operator()(const boost::shared_ptr< Shape > & lsh,
										const boost::shared_ptr< Shape > & rsh)
{
	return lsh->isUpper(*rsh);
}

