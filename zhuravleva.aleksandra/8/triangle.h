#ifndef TRIANGLE
#define TRIANGLE

#include "shape.h"



class Triangle : public Shape
{
public:
	Triangle(int x, int y);
	void draw() const;
};


#endif
