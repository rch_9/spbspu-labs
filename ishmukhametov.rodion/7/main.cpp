#include <algorithm>
#include <time.h> // srand

#include "sequence_stats.h"
#include "vector_filling.h"

int main()
{
	srand(static_cast< unsigned >(time(0)));

	intVector myVector(10);
	sequenceStats stats;

	fillVector(myVector, -500, 500);
	printVector(myVector);

	stats = std::for_each(myVector.begin(), myVector.end(), stats);

	stats.print();

	return 0;
}
