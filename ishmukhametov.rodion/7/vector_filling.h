#ifndef VECTOR_FILLING_H
#define VECTOR_FILLING_H

#include <vector>
#include <iostream>

typedef std::vector< int > intVector;

void fillVector(intVector &vect, int rangeMin, int rangeMax) // fill vector with random numbers from range
{
	if (rangeMin > rangeMax)
	{
		int x = rangeMin;
		rangeMin = rangeMax;
		rangeMax = x;
	}

	for (intVector::iterator iter = vect.begin(); iter != vect.end(); ++iter)
	{
		(*iter) = rand() % (rangeMax - rangeMin + 1) + rangeMin;
	}
}

void printVector(const intVector &vect)
{
	std::cout << "Vector:" << std::endl;

	for (intVector::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
	{
		std::cout << (*iter) << " ";
	}

	std::cout << "\n\n";
}

#endif