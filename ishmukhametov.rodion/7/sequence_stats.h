#ifndef SEQUENCE_STATS_H
#define SEQUENCE_STATS_H

#include <iostream>
#include <iomanip> // std::setprecision

class sequenceStats
{
private:
	int max_, min_;
	double avg_;
	int positives_, negatives_;
	int sumOdd_, sumEven_;
	bool firstEqualsLast_;

	int sum_; // sum of all elements
	int count_; // amount of all elements
	int first_;
	bool gotFirst_;

public:
	sequenceStats() :
	  max_(), min_(),
	  avg_(),
	  positives_(0), negatives_(0),
	  sumOdd_(0), sumEven_(0),
	  firstEqualsLast_(true),

	  sum_(0),
	  count_(0),
	  gotFirst_(false)
	  {}

	void operator() (int x)
	{
		if (!gotFirst_)
		{
			first_ = x;
			gotFirst_ = true;

			max_ = first_;
			min_ = first_;
		}
		else
		{
			firstEqualsLast_ = (first_ == x);
		}

		if (x > max_)
		{
			max_ = x;
		}

		if (x < min_)
		{
			min_ = x;
		}

		sum_ += x;
		count_++;
		avg_ = sum_ / static_cast< double >(count_);

		if (x > 0)
		{
			positives_++;
		}

		if (x < 0)
		{
			negatives_++;
		}

		if (x % 2 != 0)
		{
			sumOdd_ += x;
		}
		else
		{
			sumEven_ += x;
		}
	}

	int max() const
	{
		return max_;
	}

	int min() const
	{
		return min_;
	}

	double avg() const
	{
		return avg_;
	}

	int positives() const
	{
		return positives_;
	}

	int negatives() const
	{
		return negatives_;
	}

	int sumOdd() const
	{
		return sumOdd_;
	}

	int sumEven() const
	{
		return sumEven_;
	}

	bool firstEqualsLast() const
	{
		return firstEqualsLast_;
	}

	void print()
	{
		std::cout << "Max: " << max() << std::endl;
		std::cout << "Min: " << min() << std::endl;
		std::cout << "Average: " << std::setprecision(4) << avg() << std::endl;
		std::cout << "Positives: " << positives() << std::endl;
		std::cout << "Negatives: " << negatives() << std::endl;
		std::cout << "Sum of odd: " << sumOdd() << std::endl;
		std::cout << "Sum of even: " << sumEven() << std::endl;
		std::cout << "First equals last: " << std::boolalpha << firstEqualsLast() << std::endl;
	}
};

#endif
