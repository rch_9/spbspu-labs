#include "read_file.h"
#include "text_parsing.h"

int main()
{
	std::string text;
	readFileToString("input.txt", text);

	replaceLongWords(text, 10, "Vau!!!");
	std::cout << text << "\n\n";

	StringVector lines = separateText(text, 40);
	printVector(lines);

	return 0;
}
