#ifndef TEXT_PARSING_H
#define TEXT_PARSING_H

#include <string>
#include <vector>
#include <iterator> // std::distance
#include <iostream>

typedef std::vector< std::string > StringVector;

void replaceLongWords(std::string &text, const int &length, const std::string &s)
{
	std::string::iterator iter1, iter2; // iter1 should point to beginning of the long word, iter2 - to end

	for (iter1 = text.begin(); iter1 != text.end(); ++iter1)
	{
		if (isspace(*iter1) || iter1 == text.begin()) // words start after space or in beginning of the string
		{
			if (iter1 != text.begin())
			{
				iter1++;
			}

			iter2 = iter1;

			while (isalnum(*iter2)) // move iter2 to end of the word
			{
				iter2++;
			}

			if (std::distance(iter1, iter2) >= length)
			{
				text.replace(iter1, iter2, s);
			}
		}
	}
}

StringVector separateText(std::string text, const int &length)
{
	StringVector lines; // resulting vector of strings
	std::string::iterator iter1 = text.begin(); // iter1 should point to beginning of each line
	std::string::iterator iter2; // iter2 - to end

	while (std::distance(iter1, text.end()) > length) // add all lines except the last one
	{
		iter2 = iter1;

		while (std::distance(iter1, iter2) < length) // move iter2 to end of the line
		{
			iter2++;
		}

		if (!isspace(*(iter2))) // move iter2 back if it breaks the word
		{
			while (!isspace(*(iter2)))
			{
				iter2--;
			}
		}

		lines.push_back(std::string(iter1, iter2));

		iter1 = iter2 + 1; // move iter1 to beginning of the next line
	}

	if (iter1 != text.end()) // add the last line
	{
			lines.push_back(std::string(iter1, text.end()));
	}

	return lines;
}

template < typename T >
void printVector(const std::vector< T > &vect)
{
	for (std::vector< T >::const_iterator iter = vect.begin(); iter != vect.end(); ++iter)
	{
		std::cout << *iter << std::endl;
	}
}

#endif