#ifndef READ_FILE_H
#define READ_FILE_H

#include <string>
#include <vector>
#include <fstream>

bool isPunctMark(char c)
{
	return c == '.' || c == ',' || c == '!' || c == '?' || c == ':' || c == ';';
}

void readFileToString(const char* fileName, std::string &text)
{
	std::ifstream file(fileName);
	char c;

	if (file.is_open() && file.good())
	{
		while (file >> std::noskipws >> c && file.is_open() && file.good())
		{
			if (isPunctMark(c) && isspace(text.back())) // no spaces between word and punctuation mark
			{
				text.insert(--text.end(), 1, c);
			}
			else if (isalnum(c) && isPunctMark(text.back())) // put space between punctuation mark and word
			{
				text.push_back(' ');
				text.push_back(c);
			}
			else if (!isspace(c) || !isspace(text.back()) && !text.empty()) // don't read space if the last character is space
			                                                                  // or text is empty
			{
				text.push_back(c);
			}
		}

		unsigned textSize = text.size();
		while (isspace(text.back()) && textSize > 0) // remove spaces at the end of the string
		{
			text.resize(textSize - 1);
		}
	}
}

#endif
